package com.iqitco.elepur_api;

import android.content.Context;

/**
 * date 5/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface BaseView {

    void showLoading();

    void hideLoading();

    void showUserMessage(String message);

    void showErrorMessage(String message);

    Context getContext();

}
