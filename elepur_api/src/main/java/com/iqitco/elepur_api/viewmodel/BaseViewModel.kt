package com.iqitco.elepur_api.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData

/**
 * date 8/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
open class BaseViewModel(app: Application) : AndroidViewModel(app){

    val errorMessage = MutableLiveData<String>()

}