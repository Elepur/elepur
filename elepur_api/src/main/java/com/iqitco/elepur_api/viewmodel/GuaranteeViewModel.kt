package com.iqitco.elepur_api.viewmodel

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.iqitco.elepur_api.data.QueryDataManager
import com.iqitco.elepur_api.modules.Guarantee
import com.iqitco.elepur_api.modules.GuaranteeType
import com.iqitco.elepur_api.modules.respones.BaseResponse

/**
 * date 8/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class GuaranteeViewModel(app: Application) : BaseViewModel(app) {

    private var dataManager = QueryDataManager(app.applicationContext)

    val guaranteeTypeList = MutableLiveData<BaseResponse<List<GuaranteeType>>>()
    val guaranteeList = MutableLiveData<BaseResponse<List<Guarantee>>>()

    fun getGuaranteeList(typeId: Int) {
        dataManager.getGuaranteeList(typeId)
                .subscribe({
                    guaranteeList.value = it!!
                }, {
                    errorMessage.value = it.message
                })
    }

    fun getGuaranteeTypeList() {
        dataManager.getGuaranteeTypeList()
                .subscribe({
                    guaranteeTypeList.value = it!!
                }, {
                    errorMessage.value = it.message
                })
    }

}