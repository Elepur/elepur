package com.iqitco.elepur_api.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.database.AppDatabase
import com.iqitco.elepur_api.modules.MainPageSection
import com.iqitco.elepur_api.modules.SellerProfile
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea
import com.iqitco.elepur_api.modules.request.RequestUpdateSellerProfile
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.*

/**
 * date 8/17/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class EditSellerProfileViewModel(app: Application) : AndroidViewModel(app) {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)
    private val database = AppDatabase.getInstance(app.applicationContext)

    var sellerProfileLd = MutableLiveData<BaseResponse<SellerProfile>>()
        private set

    var statesList = MutableLiveData<List<State>>()
        private set

    var areasList = MutableLiveData<List<StateArea>>()
        private set

    var categoriesList = MutableLiveData<List<MainPageSection>>()
        private set

    var messages = MutableLiveData<String>()
        private set

    var onUpdateSellerProfile = MutableLiveData<Boolean>()
        private set

    fun getSellerProfile(id: Int) {
        compositeDisposable.add(
                api.getSellerProfile(id)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            sellerProfileLd.value = it
                        }, {
                            messages.value = it.message
                        })
        )
    }

    fun getStates() {
        compositeDisposable.add(
                database.stateDao.all
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            statesList.value = it
                        }, {
                            messages.value = it.message
                        })
        )
    }

    fun getAreas(stateId: Int) {
        compositeDisposable.add(
                database.stateAreaDao.getAll(stateId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            areasList.value = it
                        }, {
                            messages.value = it.message
                        })
        )
    }

    fun getCategories() {
        compositeDisposable.add(
                database.mainPageSectionDao.all
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            categoriesList.value = it
                        }, {
                            messages.value = it.message
                        })
        )
    }

    fun updateSellerProfile(body: RequestUpdateSellerProfile) {
        compositeDisposable.add(
                api.updateSellerProfile(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            if (it.isStatus) {
                                onUpdateSellerProfile.value = true
                            } else {
                                messages.value = it.message
                            }
                        }, {
                            onUpdateSellerProfile.value = false
                            messages.value = it.message
                        })
        )
    }

    interface Api {

        @GET("seller/get@profile@edit")
        fun getSellerProfile(@Query("id") id: Int): Single<BaseResponse<SellerProfile>>


        @Headers("Content-Type: application/json")
        @PUT("seller/edit@profile")
        fun updateSellerProfile(@Body body: RequestUpdateSellerProfile): Single<BaseResponse<Boolean>>

    }


}