package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 7/27/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class SellerProfile {


    @field:SerializedName("phone")
    var phone: String? = null

    @field:SerializedName("state")
    var state: String? = null

    @field:SerializedName("pic_cover")
    var picCover: String? = null

    @field:SerializedName("pic_logo")
    var picLogo: String? = null

    @field:SerializedName("shopName")
    var shopName: String? = null

    @field:SerializedName("shopName_ar")
    var shopNameAr: String? = null

    @field:SerializedName("owner_name")
    var ownerName: String? = null

    @field:SerializedName("location")
    var location: String? = null

    @field:SerializedName("location_long")
    var locationLong: String? = null

    @field:SerializedName("location_laut")
    var locationLaut: String? = null

    @field:SerializedName("iD_seller")
    var sellerId: Int? = null

    @field:SerializedName("rate_Happy")
    var rateHappy: Int? = null

    @field:SerializedName("rate_sad")
    var rateSad: Int? = null

    @field:SerializedName("favourite")
    var favourite: Int? = null

    @field:SerializedName("rate_Happy_c")
    var rateHappyC: Int? = null

    @field:SerializedName("seller_category")
    var sellerCategory: List<Int>? = null

    @field:SerializedName("adv_seller")
    var advSeller: ArrayList<SellerProduct>? = null

    @field:SerializedName("rate_sad_c")
    var rateSadC: Int? = null

    @field:SerializedName("favourite_c")
    var favouriteC: Int? = null

    @field:SerializedName("time")
    var time: Int? = null

    @field:SerializedName("blue")
    var blue: Int? = null

    @field:SerializedName("time_open")
    var timeOpen: String? = null

    @field:SerializedName("time_close")
    var timeClose: String? = null

    @field:SerializedName("kind")
    var kind: String? = null

    @field:SerializedName("kind_ar")
    var kindAr: String? = null

    @field:SerializedName("area")
    var area: String? = null

    @field:SerializedName("state_id")
    var stateId: Int? = null

    @field:SerializedName("area_id")
    var areaId: Int? = null

    @field:SerializedName("elego")
    var elego: Int? = null

    @field:SerializedName("authorize")
    var authorize: Int? = null

}