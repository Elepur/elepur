package com.iqitco.elepur_api.modules.respones;

import com.google.gson.annotations.SerializedName;
import com.iqitco.elepur_api.modules.BrandPic;
import com.iqitco.elepur_api.modules.ElegoSecondAdv;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.PopAdv;
import com.iqitco.elepur_api.modules.Slides;
import com.iqitco.elepur_api.modules.State;

import java.util.List;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ResponseMainCache {

    @SerializedName("list_main_page_section")
    private List<MainPageSection> listMainPageSection;

    @SerializedName("list_slides")
    private List<Slides> listSlides;

    @SerializedName("state")
    private List<State> state;

    @SerializedName("pop_adv")
    private List<PopAdv> popAdv;

    @SerializedName("elego_second_adv")
    private List<ElegoSecondAdv> elegoSecondAdv;

    @SerializedName("brand_pic")
    private List<BrandPic> brandPic;

    @SerializedName("date")
    private String date;

    public List<MainPageSection> getListMainPageSection() {
        return listMainPageSection;
    }

    public void setListMainPageSection(List<MainPageSection> listMainPageSection) {
        this.listMainPageSection = listMainPageSection;
    }

    public List<Slides> getListSlides() {
        return listSlides;
    }

    public void setListSlides(List<Slides> listSlides) {
        this.listSlides = listSlides;
    }

    public List<State> getState() {
        return state;
    }

    public void setState(List<State> state) {
        this.state = state;
    }

    public List<PopAdv> getPopAdv() {
        return popAdv;
    }

    public void setPopAdv(List<PopAdv> popAdv) {
        this.popAdv = popAdv;
    }

    public List<BrandPic> getBrandPic() {
        return brandPic;
    }

    public void setBrandPic(List<BrandPic> brandPic) {
        this.brandPic = brandPic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<ElegoSecondAdv> getElegoSecondAdv() {
        return elegoSecondAdv;
    }

    public void setElegoSecondAdv(List<ElegoSecondAdv> elegoSecondAdv) {
        this.elegoSecondAdv = elegoSecondAdv;
    }
}
