package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 7/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class SellerProduct {

    @field:SerializedName("row_num")
    var rowNum: Int? = null

    @field:SerializedName("total_count")
    var totalCount: Int? = null

    @field:SerializedName("iD_adv")
    var idAdv: Int? = null

    @field:SerializedName("area_id")
    var areaId: Int? = null

    @field:SerializedName("state_id")
    var stateId: Int? = null

    @field:SerializedName("category")
    var category: Int? = null

    @field:SerializedName("capacity")
    var capacity: Int? = null

    @field:SerializedName("amount")
    var amount: Int? = null

    @field:SerializedName("kind_id")
    var kindId: Int? = null

    @field:SerializedName("blue_mark")
    var blueMark: Int? = null

    @field:SerializedName("price")
    var price: Int? = null

    @field:SerializedName("special")
    var special: Int? = null

    @field:SerializedName("brands")
    var brands: String? = null

    @field:SerializedName("adv_name")
    var advName: String? = null

    @field:SerializedName("kind")
    var kind: String? = null

    @field:SerializedName("shop_name")
    var shopName: String? = null

    @field:SerializedName("shop_logo")
    var shopLogo: String? = null

    @field:SerializedName("n_o")
    var no: String? = null

    @field:SerializedName("date")
    var date: String? = null

    @field:SerializedName("pic1")
    var pic1: String? = null

}