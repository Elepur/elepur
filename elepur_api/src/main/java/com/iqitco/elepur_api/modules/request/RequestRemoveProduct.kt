package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date: 2018-08-08
 *
 * @author Mohammad Al-Najjar
 */
class RequestRemoveProduct {

    @field:SerializedName("ID_ADV")
    var advId: Int? = null

    @field:SerializedName("Saller_id")
    var sellerId: Int? = null

}