package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "State")
public class State {

    @PrimaryKey
    @SerializedName("id")
    private Integer id;

    @SerializedName("name_state_ar")
    private String nameStateAr;

    @SerializedName("name_state_en")
    private String nameStateEn;

    @Ignore
    @SerializedName("area")
    private List<StateArea> areaList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameStateAr() {
        return nameStateAr;
    }

    public void setNameStateAr(String nameStateAr) {
        this.nameStateAr = nameStateAr;
    }

    public String getNameStateEn() {
        return nameStateEn;
    }

    public void setNameStateEn(String nameStateEn) {
        this.nameStateEn = nameStateEn;
    }

    public List<StateArea> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<StateArea> areaList) {
        this.areaList = areaList;
    }
}
