package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "Brand", primaryKeys = {"mainPageSectionId", "id"})
public class Brand {

    private int mainPageSectionId;

    @SerializedName("id")
    private int id;

    @SerializedName("brands_name_en")
    private String brandsNameEn;

    @SerializedName("brands_name_ar")
    private String brandsNameAr;

    public Integer getMainPageSectionId() {
        return mainPageSectionId;
    }

    public void setMainPageSectionId(Integer mainPageSectionId) {
        this.mainPageSectionId = mainPageSectionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrandsNameEn() {
        return brandsNameEn;
    }

    public void setBrandsNameEn(String brandsNameEn) {
        this.brandsNameEn = brandsNameEn;
    }

    public String getBrandsNameAr() {
        return brandsNameAr;
    }

    public void setBrandsNameAr(String brandsNameAr) {
        this.brandsNameAr = brandsNameAr;
    }
}
