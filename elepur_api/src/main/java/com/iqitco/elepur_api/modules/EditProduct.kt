package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 9/15/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
data class EditProduct(
        @SerializedName("cat_id") val catId: Int,
        @SerializedName("brand_id") val brandId: Int,
        @SerializedName("sub_brand_id") val subBrandId: Int,
        @SerializedName("kind_id") val kindId: Int,
        @SerializedName("adV_brand") val advBrand: String,
        @SerializedName("adV_Name") val advName: String,
        @SerializedName("adV_category") val advCategory: String,
        @SerializedName("adV_Kind") val advKind: String,
        @SerializedName("price") val price: Int,
        @SerializedName("amount") val amount: Int,
        @SerializedName("n_O") val newOldFlag: String,
        @SerializedName("pic1") val pic1: String,
        @SerializedName("pyment") val payment: String,
        @SerializedName("pyment_id") val paymentId: Int,
        @SerializedName("capacity") val capacity: Int,
        @SerializedName("decs") val decs: String,
        @SerializedName("saller_id") val sellerId: Int,
        @SerializedName("granty") val guarantee: String,
        @SerializedName("granty_id") val guaranteeId: Int,
        @SerializedName("free_gift") val freeGiftList: List<EditProductFreeGift>,
        @SerializedName("pics") val picList: List<EditProductPic>,
        @SerializedName("color") val colorsList: List<EditProductColor>
)

data class EditProductFreeGift(
        @SerializedName("id") val id: Int,
        @SerializedName("kind_name") val kindName: String,
        @SerializedName("kind_ID") val kindId: Int
)

data class EditProductPic(
        @SerializedName("iD_pic") val picId: Int,
        @SerializedName("pics") val url: String
)

data class EditProductColor(
        @SerializedName("iD_color") val colorId: Int,
        @SerializedName("name") val name: String,
        @SerializedName("tag") val tag: String
)

class RequestUpdateProduct{

    @SerializedName("ID_ADV")
    var advId: Int? = null

    @SerializedName("Saller_id")
    var sellerId: Int? = null

    @SerializedName("Price")
    var price: Int? = null

    @SerializedName("DECS")
    var description: String? = null

    @SerializedName("N_O")
    var newOldFlag: String? = null

    @SerializedName("granty_id")
    var guaranteeId: Int? = null

    @SerializedName("capacity")
    var capacity: Int? = null

    @SerializedName("amount")
    var amount: Int? = null

    @SerializedName("payment_id")
    var paymentId: Int? = null

    @SerializedName("colors")
    var colors: List<Int>? = null

    @SerializedName("free_gift")
    var freeGifts: List<Int>? = null

}