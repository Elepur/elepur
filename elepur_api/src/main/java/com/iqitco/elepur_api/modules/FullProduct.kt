package com.iqitco.elepur_api.modules

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * date 7/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class FullProduct {

    @field:SerializedName("adV_brand")
    var advBrand: String? = null

    @field:SerializedName("adV_Name")
    var advName: String? = null

    @field:SerializedName("adV_category")
    var advCategory: String? = null

    @field:SerializedName("adV_Kind")
    var advKind: String? = null

    @field:SerializedName("n_O")
    var no: String? = null

    @field:SerializedName("pic1")
    var pic1: String? = null

    @field:SerializedName("pyment")
    var pyment: String? = null

    @field:SerializedName("decs")
    var decs: String? = null

    @field:SerializedName("seller_name")
    var sellerName: String? = null

    @field:SerializedName("phone")
    var phone: String? = null

    @field:SerializedName("granty")
    var guarantee: String? = null

    @field:SerializedName("date")
    var date: String? = null

    @field:SerializedName("date_expire")
    var dateExpire: String? = null

    @field:SerializedName("price")
    var price: Int? = null

    @field:SerializedName("amount")
    var amount: Int? = null

    @field:SerializedName("capacity")
    var capacity: Int? = null

    @field:SerializedName("saller_id")
    var sallerId: Int? = null

    @field:SerializedName("days_left")
    var daysLeft: Int? = null

    @field:SerializedName("call_count")
    var callCount: Int? = null

    @field:SerializedName("elego")
    var elego: Int? = null

    @field:SerializedName("special")
    var special: Int? = null

    @field:SerializedName("views")
    var views: Int? = null

    @field:SerializedName("happy")
    var happy: Int? = null

    @field:SerializedName("sad")
    var sad: Int? = null

    @field:SerializedName("fav")
    var fav: Int? = null

    @field:SerializedName("customer_happy")
    var customerHappy: Int? = null

    @field:SerializedName("customer_sad")
    var customerSad: Int? = null

    @field:SerializedName("customer_fav")
    var customerFav: Int? = null

    @field:SerializedName("store_time")
    var storeTime: Int? = null

    @field:SerializedName("seller_logo")
    var sellerLogo: String? = null

    @field:SerializedName("seller_blue")
    var sellerBlue: Int = 0

    @field:SerializedName("free_gift")
    var freeGift: List<FreeGift>? = null

    @field:SerializedName("pics")
    var pics: ArrayList<Pics>? = null

    @field:SerializedName("color")
    var color: List<Color>? = null

    inner class FreeGift {
        @field:SerializedName("kind_name")
        var kindName: String? = null

        @field:SerializedName("id")
        var id: Int? = null

        @field:SerializedName("kind_ID")
        var kindId: Int? = null
    }

    class Pics() : Parcelable {
        @field:SerializedName("iD_pic")
        var idPic: Int? = null

        @field:SerializedName("pics")
        var pics: String? = null

        constructor(parcel: Parcel) : this() {
            idPic = parcel.readValue(Int::class.java.classLoader) as? Int
            pics = parcel.readString()
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeValue(idPic)
            parcel.writeString(pics)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Pics> {
            override fun createFromParcel(parcel: Parcel): Pics {
                return Pics(parcel)
            }

            override fun newArray(size: Int): Array<Pics?> {
                return arrayOfNulls(size)
            }
        }
    }

    class Color {
        @field:SerializedName("iD_color")
        var idColor: Int? = null

        @field:SerializedName("name")
        var name: String? = null

        @field:SerializedName("tag")
        var tag: String? = null
    }

}