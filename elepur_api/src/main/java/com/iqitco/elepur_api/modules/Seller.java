package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class Seller {

    @SerializedName("iD_seller")
    private Integer idSeller;

    @SerializedName("row_num")
    private Integer rowNum;

    @SerializedName("total_count")
    private Integer totalCount;

    @SerializedName("pic_logo")
    private String picLogo;

    @SerializedName("kind")
    private String kind;

    @SerializedName("time")
    private Integer time;

    @SerializedName("time_open")
    private String timeOpen;

    @SerializedName("time_close")
    private String timeClose;

    @SerializedName("shop_name")
    private String shopName;

    @SerializedName("blue")
    private Integer blue;

    @SerializedName("special")
    private Integer special;

    @SerializedName("discount")
    private Integer discount;

    @SerializedName("sad")
    private Integer sad;

    @SerializedName("happy")
    private Integer happy;

    @SerializedName("fav")
    private Integer fav;

    @SerializedName("area")
    private String area;

    @SerializedName("state")
    private String state;

    @SerializedName("area_id")
    private Integer areaId;

    @SerializedName("state_id")
    private Integer stateId;

    @SerializedName("date")
    private String date;

    public Integer getIdSeller() {
        return idSeller;
    }

    public void setIdSeller(Integer idSeller) {
        this.idSeller = idSeller;
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getPicLogo() {
        return picLogo;
    }

    public void setPicLogo(String picLogo) {
        this.picLogo = picLogo;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getTimeOpen() {
        return timeOpen;
    }

    public void setTimeOpen(String timeOpen) {
        this.timeOpen = timeOpen;
    }

    public String getTimeClose() {
        return timeClose;
    }

    public void setTimeClose(String timeClose) {
        this.timeClose = timeClose;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getBlue() {
        return blue;
    }

    public void setBlue(Integer blue) {
        this.blue = blue;
    }

    public Integer getSpecial() {
        return special;
    }

    public void setSpecial(Integer special) {
        this.special = special;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getSad() {
        return sad;
    }

    public void setSad(Integer sad) {
        this.sad = sad;
    }

    public Integer getHappy() {
        return happy;
    }

    public void setHappy(Integer happy) {
        this.happy = happy;
    }

    public Integer getFav() {
        return fav;
    }

    public void setFav(Integer fav) {
        this.fav = fav;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
