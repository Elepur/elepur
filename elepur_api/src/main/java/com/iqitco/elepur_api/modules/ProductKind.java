package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ProductKind {

    @SerializedName("id")
    private Integer id;

    @SerializedName("cat_id")
    private Integer catId;

    @SerializedName("kind_name_en")
    private String kindNameEn;

    @SerializedName("kind_name_ar")
    private String kindNameAr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getKindNameEn() {
        return kindNameEn;
    }

    public void setKindNameEn(String kindNameEn) {
        this.kindNameEn = kindNameEn;
    }

    public String getKindNameAr() {
        return kindNameAr;
    }

    public void setKindNameAr(String kindNameAr) {
        this.kindNameAr = kindNameAr;
    }
}
