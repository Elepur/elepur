package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 7/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class RateResult {

    @field:SerializedName("check")
    var check: Int? = null

    @field:SerializedName("happy")
    var happy: Int? = null

    @field:SerializedName("sad")
    var sad: Int? = null

    @field:SerializedName("fav")
    var fav: Int? = null

    @field:SerializedName("check_happy")
    var checkHappy: Int? = null

    @field:SerializedName("check_sad")
    var checkSad: Int? = null

    @field:SerializedName("check_fav")
    var checkFav: Int? = null


}