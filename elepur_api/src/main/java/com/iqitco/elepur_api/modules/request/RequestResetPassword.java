package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/5/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestResetPassword {

    @SerializedName("username")
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
