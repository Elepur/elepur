package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class Color {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name_en")
    private String nameEn;

    @SerializedName("name_ar")
    private String nameAr;

    @SerializedName("tag")
    private String tag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
