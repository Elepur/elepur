package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

class RequestAddAdv {

    @SerializedName("ADV_Name")
    var advName: String? = null
    @SerializedName("category_id")
    var categoryId: Int? = null
    @SerializedName("Saller_id")
    var sallerId: Int? = null
    @SerializedName("brand_id")
    var brandId: Int? = null
    @SerializedName("sub_brand_id")
    var subBrandId: Int? = null
    @SerializedName("adv_kind_id")
    var advKindId: Int? = null
    @SerializedName("Price")
    var price: Int? = null
    @SerializedName("DECS")
    var dECS: String? = null
    @SerializedName("N_O")
    var nO: String? = null
    @SerializedName("granty_id")
    var grantyId: Int? = null
    @SerializedName("capacity")
    var capacity: Int? = null
    @SerializedName("amount")
    var amount: Int? = null
    @SerializedName("payment_id")
    var paymentId: Int? = null
    @SerializedName("colors")
    var colors: List<Int?>? = null
    @SerializedName("free_gift")
    var freeGift: List<Int?>? = null
    @SerializedName("sub_brand")
    var subBrand: String? = null
    @SerializedName("Pic1")
    var mainPic: String? = null
    @SerializedName("Pic1_format")
    var mainPicExt: String? = null
    @SerializedName("otherPic")
    var otherPic: ArrayList<OtherPic?>? = null

}