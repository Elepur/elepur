package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

class OtherPic(@SerializedName("pic")
               var pic: String,
               @SerializedName("ext")
               var ext: String)