package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 8/4/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class StoreResult {

    @field:SerializedName("calls")
    val calls: Int? = null

    @field:SerializedName("elego")
    val elego: Int? = null

    @field:SerializedName("elego_payment")
    val elegoPayment: Int? = null

    @field:SerializedName("adv")
    val adv: Int? = null

    @field:SerializedName("seen")
    val seen: Int? = null

    @field:SerializedName("date")
    val date: String? = null

    @field:SerializedName("date_update")
    val dateUpdate: String? = null

    @field:SerializedName("happy")
    val happy: Int? = null

    @field:SerializedName("sad")
    val sad: Int? = null

    @field:SerializedName("fav")
    val fav: Int? = null

}