package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

data class Guarantee(
        @SerializedName("id") var id: Int?,
        @SerializedName("name") var name: String?,
        @SerializedName("link") var link: String?
)