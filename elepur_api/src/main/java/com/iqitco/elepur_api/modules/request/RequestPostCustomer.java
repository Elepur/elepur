package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date 5/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestPostCustomer {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("pic")
    private String pic;

    @SerializedName("pic_format")
    private String picFormat;

    @SerializedName("name")
    private String name;

    @SerializedName("Gender")
    private String gender;

    @SerializedName("Age")
    private Integer age;

    @SerializedName("Notifcation_id")
    private String notifcationId;

    @SerializedName("birth")
    private String birth;

    @SerializedName("mac_address")
    private String macAddress;

    @SerializedName("Device_type")
    private String deviceType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPicFormat() {
        return picFormat;
    }

    public void setPicFormat(String picFormat) {
        this.picFormat = picFormat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNotifcationId() {
        return notifcationId;
    }

    public void setNotifcationId(String notifcationId) {
        this.notifcationId = notifcationId;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
