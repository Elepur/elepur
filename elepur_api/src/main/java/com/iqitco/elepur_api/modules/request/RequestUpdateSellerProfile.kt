package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

class RequestUpdateSellerProfile {

    @SerializedName("phone")
    var phone: String? = null
    @SerializedName("shop_name")
    var shopName: String? = null
    @SerializedName("shop_name_ar")
    var shopNameAr: String? = null
    @SerializedName("Time_open")
    var timeOpen: String? = null
    @SerializedName("Time_close")
    var timeClose: String? = null
    @SerializedName("state_id")
    var stateId: Int? = null
    @SerializedName("area_id")
    var areaId: Int? = null
    @SerializedName("location")
    var location: String? = null
    @SerializedName("elego")
    var elego: Int? = null
    @SerializedName("ID_seller")
    var iDSeller: Int? = null
    @SerializedName("owner_name")
    var ownerName: String? = null
    @SerializedName("kind")
    var kind: String? = null
    @SerializedName("pic_logo")
    var picLogo: String? = null
    @SerializedName("cover_photo")
    var coverPhoto: String? = null
    @SerializedName("pic_logo_format")
    var picLogoFormat: String? = null
    @SerializedName("cover_photo_format")
    var coverPhotoFormat: String? = null
    @SerializedName("kind_ar")
    var kindAr: String? = null
    @SerializedName("seller_category")
    var sellerCategory: List<Int?>? = null

}