package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date 8/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class RequestDeleteUserChoice {

    @field:SerializedName("customer_id")
    var userId: Int? = null

}