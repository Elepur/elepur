package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date: 2018-08-02
 *
 * @author Mohammad Al-Najjar
 */
class RequestSellerNotification {

    @field:SerializedName("ID_seller")
    var sellerId: Int? = null

    @field:SerializedName("ID")
    var notificationId: Int? = null

}