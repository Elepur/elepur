package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

data class GuaranteeType(
        @SerializedName("id") var id: Int?,
        @SerializedName("name_en") var nameEn: String?,
        @SerializedName("name_ar") var nameAr: String?
)