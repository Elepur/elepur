package com.iqitco.elepur_api.modules.respones;

import com.google.gson.annotations.SerializedName;

/**
 * date 5/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BaseResponse<DataType> {

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    @SerializedName("data")
    private DataType data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataType getData() {
        return data;
    }

    public void setData(DataType data) {
        this.data = data;
    }
}
