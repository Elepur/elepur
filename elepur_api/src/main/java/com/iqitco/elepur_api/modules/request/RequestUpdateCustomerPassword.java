package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date 7/3/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestUpdateCustomerPassword {

    @SerializedName("id_customer")
    private Integer id;

    @SerializedName("password")
    private String password;

    @SerializedName("password_old")
    private String passwordOld;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordOld() {
        return passwordOld;
    }

    public void setPasswordOld(String passwordOld) {
        this.passwordOld = passwordOld;
    }
}
