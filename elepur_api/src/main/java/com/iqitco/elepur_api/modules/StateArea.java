package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "StateArea", primaryKeys = {"stateId", "id"})
public class StateArea {

    private int stateId;

    @SerializedName("id")
    private int id;

    @SerializedName("name_area_ar")
    private String nameAreaAr;

    @SerializedName("name_area_en")
    private String nameAreaEn;

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameAreaAr() {
        return nameAreaAr;
    }

    public void setNameAreaAr(String nameAreaAr) {
        this.nameAreaAr = nameAreaAr;
    }

    public String getNameAreaEn() {
        return nameAreaEn;
    }

    public void setNameAreaEn(String nameAreaEn) {
        this.nameAreaEn = nameAreaEn;
    }
}
