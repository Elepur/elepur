package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestLogoutCustomer {

    @SerializedName("ID_C")
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
