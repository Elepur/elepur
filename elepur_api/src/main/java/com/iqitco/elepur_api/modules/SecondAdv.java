package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "SecondAdv")
public class SecondAdv {

    @PrimaryKey
    private int id;

    @SerializedName("buttoun_second_adv_en")
    private String buttounSecondAdvEn;

    @SerializedName("buttoun_second_adv_ar")
    private String buttounSecondAdvAr;

    @SerializedName("buttoun_second_adv_link")
    private String buttounSecondAdvLink;

    @SerializedName("type_link")
    private Integer typeLink;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getButtounSecondAdvEn() {
        return buttounSecondAdvEn;
    }

    public void setButtounSecondAdvEn(String buttounSecondAdvEn) {
        this.buttounSecondAdvEn = buttounSecondAdvEn;
    }

    public String getButtounSecondAdvAr() {
        return buttounSecondAdvAr;
    }

    public void setButtounSecondAdvAr(String buttounSecondAdvAr) {
        this.buttounSecondAdvAr = buttounSecondAdvAr;
    }

    public String getButtounSecondAdvLink() {
        return buttounSecondAdvLink;
    }

    public void setButtounSecondAdvLink(String buttounSecondAdvLink) {
        this.buttounSecondAdvLink = buttounSecondAdvLink;
    }

    public Integer getTypeLink() {
        return typeLink;
    }

    public void setTypeLink(Integer typeLink) {
        this.typeLink = typeLink;
    }
}
