package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date 7/3/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestUpdateCustomerProfile {

    @SerializedName("id_customer")
    private Integer idCustomer;

    @SerializedName("pic")
    private String pic;

    @SerializedName("pic_format")
    private String picFormat = "png";

    @SerializedName("name")
    private String name;

    @SerializedName("Gender")
    private String gender;

    @SerializedName("Age")
    private Integer age;

    @SerializedName("Notifcation_id")
    private String notificationId;

    @SerializedName("birth")
    private String birth;

    @SerializedName("mac_address")
    private String macAddress;

    @SerializedName("Device_type")
    private String deviceType = "1";

    public Integer getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Integer idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
