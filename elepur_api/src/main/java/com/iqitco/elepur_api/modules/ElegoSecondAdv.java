package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

public class ElegoSecondAdv {

    @SerializedName("second_adv")
    private List<SecondAdv> secondAdv;

    @SerializedName("elego_main")
    private List<ElegoMain> elegoMain;

    @SerializedName("landing_page_background")
    private List<LandingPageBackground> landingPageBackground;

    public List<SecondAdv> getSecondAdv() {
        return secondAdv;
    }

    public void setSecondAdv(List<SecondAdv> secondAdv) {
        this.secondAdv = secondAdv;
    }

    public List<ElegoMain> getElegoMain() {
        return elegoMain;
    }

    public void setElegoMain(List<ElegoMain> elegoMain) {
        this.elegoMain = elegoMain;
    }

    public List<LandingPageBackground> getLandingPageBackground() {
        return landingPageBackground;
    }

    public void setLandingPageBackground(List<LandingPageBackground> landingPageBackground) {
        this.landingPageBackground = landingPageBackground;
    }
}
