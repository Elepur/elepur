package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "MainPageSection")
public class MainPageSection {

    @PrimaryKey
    @SerializedName("id")
    private Integer id;

    @SerializedName("category_name")
    private String categoryName;

    @SerializedName("category_name_ar")
    private String categoryNameAr;

    @SerializedName("pic_en")
    private String picEn;

    @SerializedName("pic_ar")
    private String picAr;

    @SerializedName("pic_in_en")
    private String picInEn;

    @SerializedName("pic_in_ar")
    private String picInAr;

    @Ignore
    @SerializedName("brands")
    private List<Brand> brands;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryNameAr() {
        return categoryNameAr;
    }

    public void setCategoryNameAr(String categoryNameAr) {
        this.categoryNameAr = categoryNameAr;
    }

    public String getPicEn() {
        return picEn;
    }

    public void setPicEn(String picEn) {
        this.picEn = picEn;
    }

    public String getPicAr() {
        return picAr;
    }

    public void setPicAr(String picAr) {
        this.picAr = picAr;
    }

    public String getPicInEn() {
        return picInEn;
    }

    public void setPicInEn(String picInEn) {
        this.picInEn = picInEn;
    }

    public String getPicInAr() {
        return picInAr;
    }

    public void setPicInAr(String picInAr) {
        this.picInAr = picInAr;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }
}
