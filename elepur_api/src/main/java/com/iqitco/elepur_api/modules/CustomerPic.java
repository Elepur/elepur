package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

/**
 * date: 2018-07-01
 *
 * @author Mohammad Al-Najjar
 */
public class CustomerPic {

    @SerializedName("pic")
    private String pic;

    @SerializedName("name")
    private String name;

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
