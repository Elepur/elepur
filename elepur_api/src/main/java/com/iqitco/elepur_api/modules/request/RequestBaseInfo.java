package com.iqitco.elepur_api.modules.request;

import android.content.Context;
import android.provider.Settings;

import java.util.Locale;

/**
 * date 5/30/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestBaseInfo {

    private Context context;

    public RequestBaseInfo(Context context) {
        this.context = context;
    }

    public String getMacAddress() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getDeviceType() {
        /*return android.os.Build.MANUFACTURER + "/" + android.os.Build.MODEL;*/
        return "1";
    }

    public String getLanguage() {
        return Locale.getDefault().toString();
    }

}
