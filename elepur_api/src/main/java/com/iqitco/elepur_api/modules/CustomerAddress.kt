package com.iqitco.elepur_api.modules

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * date 7/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class CustomerAddress() : Parcelable {

    @field:SerializedName("iD_location")
    var locationId: Int? = null

    @field:SerializedName("date")
    var date: String? = null

    @field:SerializedName("state")
    var state: String? = null

    @field:SerializedName("area")
    var area: String? = null

    @field:SerializedName("location_name")
    var locationName: String? = null

    @field:SerializedName("location")
    var location: String? = null

    @field:SerializedName("phone")
    var phone: String? = null

    @field:SerializedName("iD_state")
    var stateId: Int? = null

    @field:SerializedName("iD_area")
    var areaId: Int? = null

    @field:SerializedName("laut")
    var latitude: String? = null

    @field:SerializedName("longe")
    var longitude: String? = null

    constructor(parcel: Parcel) : this() {
        locationId = parcel.readValue(Int::class.java.classLoader) as? Int
        date = parcel.readString()
        state = parcel.readString()
        area = parcel.readString()
        locationName = parcel.readString()
        location = parcel.readString()
        phone = parcel.readString()
        stateId = parcel.readValue(Int::class.java.classLoader) as? Int
        areaId = parcel.readValue(Int::class.java.classLoader) as? Int
        latitude = parcel.readString()
        longitude = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(locationId)
        parcel.writeString(date)
        parcel.writeString(state)
        parcel.writeString(area)
        parcel.writeString(locationName)
        parcel.writeString(location)
        parcel.writeString(phone)
        parcel.writeValue(stateId)
        parcel.writeValue(areaId)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CustomerAddress> {
        override fun createFromParcel(parcel: Parcel): CustomerAddress {
            return CustomerAddress(parcel)
        }

        override fun newArray(size: Int): Array<CustomerAddress?> {
            return arrayOfNulls(size)
        }
    }


}