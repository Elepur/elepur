package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 8/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class StoreProductStatus {

    @field:SerializedName("row_num")
    var rowNum: Int? = null

    @field:SerializedName("total_count")
    var totalCount: Int? = null

    @field:SerializedName("iD_adv")
    var advId: Int? = null

    @field:SerializedName("expire_date")
    var expireDate: Int? = null

    @field:SerializedName("fav")
    var fav: Int? = null

    @field:SerializedName("happy")
    var happy: Int? = null

    @field:SerializedName("category")
    var category: Int? = null

    @field:SerializedName("capacity")
    var capacity: Int? = null

    @field:SerializedName("amount")
    var amount: Int? = null

    @field:SerializedName("brands")
    var brands: String? = null

    @field:SerializedName("adv_name")
    var advName: String? = null

    @field:SerializedName("kind")
    var kind: String? = null

    @field:SerializedName("kind_id")
    var kindId: Int? = null

    @field:SerializedName("sad")
    var sad: Int? = null

    @field:SerializedName("views")
    var views: Int? = null

    @field:SerializedName("n_o")
    var newOldFlag: String? = null

    @field:SerializedName("blue_mark")
    var blueMark: Int? = null

    @field:SerializedName("date")
    var date: String? = null

    @field:SerializedName("pic1")
    var pic1: String? = null

    @field:SerializedName("authorize")
    var authorize: Int? = null

    @field:SerializedName("hide")
    var hide: Int? = null

    @field:SerializedName("update")
    var update: Int? = null

    @field:SerializedName("update_status")
    var updateStatus: String? = null


}