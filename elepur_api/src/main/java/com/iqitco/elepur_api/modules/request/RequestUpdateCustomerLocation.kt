package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date 7/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class RequestUpdateCustomerLocation {

    @field:SerializedName("customer_location_id")
    var addressId: Int? = null

    @field:SerializedName("ID_customer")
    var customerId: Int? = null

    @field:SerializedName("location_name")
    var name: String? = null

    @field:SerializedName("id_area")
    var areaId: Int? = null

    @field:SerializedName("id_state")
    var stateId: Int? = null

    @field:SerializedName("address_details")
    var addressDetails: String? = null

    @field:SerializedName("phone")
    var phone: String? = null

    @field:SerializedName("Long")
    var longitude: String? = null

    @field:SerializedName("laut")
    var latitude: String? = null

}