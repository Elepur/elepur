package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestInsertMessage {

    @SerializedName("ID_login")
    private Integer loginId;

    @SerializedName("message")
    private String message;

    @SerializedName("header")
    private String header;

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
