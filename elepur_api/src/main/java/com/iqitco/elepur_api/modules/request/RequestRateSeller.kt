package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date 7/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class RequestRateSeller {

    @field:SerializedName("seller_ID")
    var sellerId: Int? = null

    @field:SerializedName("Customer_ID")
    var customerId: Int? = null

}