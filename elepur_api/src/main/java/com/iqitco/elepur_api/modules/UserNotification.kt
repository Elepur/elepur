package com.iqitco.elepur_api.modules

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * date: 2018-08-02
 *
 * @author Mohammad Al-Najjar
 */
class UserNotification() : Parcelable {

    @field:SerializedName("iD_n")
    var idN: Int? = null

    @field:SerializedName("id")
    var id: Int? = null

    @field:SerializedName("header")
    var header: String? = null

    @field:SerializedName("name")
    var name: String? = null

    @field:SerializedName("message")
    var message: String? = null

    @field:SerializedName("notification")
    var notification: String? = null

    @field:SerializedName("type")
    var type: Int? = null

    @field:SerializedName("type_page_action")
    var typePageAction: Int? = null

    @field:SerializedName("type_c_s_v")
    var typeCsv: Int? = null

    @field:SerializedName("seen")
    var seen: Int? = null

    @field:SerializedName("date")
    var date: String? = null

    @field:SerializedName("pic")
    var pic: String? = null

    @field:SerializedName("row_num")
    var rowNum: Int? = null

    @field:SerializedName("total_count")
    var totalCount: Int? = null

    constructor(parcel: Parcel) : this() {
        idN = parcel.readValue(Int::class.java.classLoader) as? Int
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        header = parcel.readString()
        name = parcel.readString()
        message = parcel.readString()
        notification = parcel.readString()
        type = parcel.readValue(Int::class.java.classLoader) as? Int
        typePageAction = parcel.readValue(Int::class.java.classLoader) as? Int
        typeCsv = parcel.readValue(Int::class.java.classLoader) as? Int
        seen = parcel.readValue(Int::class.java.classLoader) as? Int
        date = parcel.readString()
        pic = parcel.readString()
        rowNum = parcel.readValue(Int::class.java.classLoader) as? Int
        totalCount = parcel.readValue(Int::class.java.classLoader) as? Int
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(idN)
        parcel.writeValue(id)
        parcel.writeString(header)
        parcel.writeString(name)
        parcel.writeString(message)
        parcel.writeString(notification)
        parcel.writeValue(type)
        parcel.writeValue(typePageAction)
        parcel.writeValue(typeCsv)
        parcel.writeValue(seen)
        parcel.writeString(date)
        parcel.writeString(pic)
        parcel.writeValue(rowNum)
        parcel.writeValue(totalCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserNotification> {
        override fun createFromParcel(parcel: Parcel): UserNotification {
            return UserNotification(parcel)
        }

        override fun newArray(size: Int): Array<UserNotification?> {
            return arrayOfNulls(size)
        }
    }


}