package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/7/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestCustomerFacebook {

    @SerializedName("email")
    private String email;

    @SerializedName("pic")
    private String pic;

    @SerializedName("name")
    private String name;

    @SerializedName("Gender")
    private String gender;

    @SerializedName("Notifcation_id")
    private String notifcationId;

    @SerializedName("mac_address")
    private String macAddress;

    @SerializedName("Device_type")
    private String deviceType;

    @SerializedName("facebook_id")
    private String facebookId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNotifcationId() {
        return notifcationId;
    }

    public void setNotifcationId(String notifcationId) {
        this.notifcationId = notifcationId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }
}
