package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "LandingPageBackground")
public class LandingPageBackground {

    @PrimaryKey
    private int id;

    @SerializedName("pic_en")
    private String picEn;

    @SerializedName("pic_ar")
    private String picAr;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicEn() {
        return picEn;
    }

    public void setPicEn(String picEn) {
        this.picEn = picEn;
    }

    public String getPicAr() {
        return picAr;
    }

    public void setPicAr(String picAr) {
        this.picAr = picAr;
    }
}
