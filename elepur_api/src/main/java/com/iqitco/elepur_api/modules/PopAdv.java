package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "PopAdv")
public class PopAdv {

    @PrimaryKey
    @SerializedName("id")
    private Integer id;

    @SerializedName("role")
    private String role;

    @SerializedName("role_id")
    private Integer roleId;

    @SerializedName("pic_en")
    private String picEn;

    @SerializedName("pic_ar")
    private String picAr;

    @SerializedName("link")
    private String link;

    @SerializedName("link_type")
    private Integer linkType;

    @SerializedName("show")
    private Integer show;

    @SerializedName("date_start")
    private String dateStart;

    @SerializedName("date_end")
    private String dateEnd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getPicEn() {
        return picEn;
    }

    public void setPicEn(String picEn) {
        this.picEn = picEn;
    }

    public String getPicAr() {
        return picAr;
    }

    public void setPicAr(String picAr) {
        this.picAr = picAr;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getLinkType() {
        return linkType;
    }

    public void setLinkType(Integer linkType) {
        this.linkType = linkType;
    }

    public Integer getShow() {
        return show;
    }

    public void setShow(Integer show) {
        this.show = show;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }
}
