package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date 7/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class RequestReportAdv {

    @field:SerializedName("ADV_ID")
    var advId: Int? = null

    @field:SerializedName("Customer_ID")
    var customerId: Int? = null

    @field:SerializedName("message")
    var message: String? = null

    @field:SerializedName("header")
    var header: String? = null

}