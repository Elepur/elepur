package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/7/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class UserData {

    @SerializedName("iD_Login")
    private Integer idLogin;

    @SerializedName("roll")
    private String roll;

    @SerializedName("iD_id")
    private Integer idId;

    @SerializedName("email")
    private String email;

    @SerializedName("facebook_id")
    private String facebookId;

    @SerializedName("password")
    private String password;

    @SerializedName("block")
    private Integer block;

    @SerializedName("name_c")
    private String name;

    public Integer getIdLogin() {
        return idLogin;
    }

    public void setIdLogin(Integer idLogin) {
        this.idLogin = idLogin;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public Integer getIdId() {
        return idId;
    }

    public void setIdId(Integer idId) {
        this.idId = idId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ResponsePostCustomer{" +
                "idLogin=" + idLogin +
                ", roll='" + roll + '\'' +
                ", idId=" + idId +
                ", email='" + email + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", password='" + password + '\'' +
                ", block=" + block +
                ", name='" + name + '\'' +
                '}';
    }

}
