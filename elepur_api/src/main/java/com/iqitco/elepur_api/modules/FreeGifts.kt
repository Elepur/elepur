package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

data class FreeGifts(
        @SerializedName("kind_name_en") val kindNameEn: String?,
        @SerializedName("id") val id: Int?,
        @SerializedName("kind_name_ar") val kindNameAr: String?
)