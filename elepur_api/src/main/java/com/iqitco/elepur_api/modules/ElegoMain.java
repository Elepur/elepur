package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
@Entity(tableName = "ElegoMain")
public class ElegoMain {

    @PrimaryKey
    private int id;

    @SerializedName("buttoun_main_elego_en")
    private String buttounMainElegoEn;

    @SerializedName("buttoun_main_elego_ar")
    private String buttounMainElegoAr;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getButtounMainElegoEn() {
        return buttounMainElegoEn;
    }

    public void setButtounMainElegoEn(String buttounMainElegoEn) {
        this.buttounMainElegoEn = buttounMainElegoEn;
    }

    public String getButtounMainElegoAr() {
        return buttounMainElegoAr;
    }

    public void setButtounMainElegoAr(String buttounMainElegoAr) {
        this.buttounMainElegoAr = buttounMainElegoAr;
    }
}
