package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date 7/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class RequestDeleteCustomerAddress {

    @field:SerializedName("ID_customer")
    var customerId: Int? = null

    @field:SerializedName("customer_location_id")
    var addressId: Int? = null

}