package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestAllAds {

    @SerializedName("adv_name")
    private List<Integer> advName;

    @SerializedName("adv_kind")
    private List<Integer> advKind;

    @SerializedName("color")
    private List<Integer> color;

    @SerializedName("Brand")
    private List<Integer> brand;

    @SerializedName("lan")
    private String lan;

    @SerializedName("n_o")
    private String no;

    @SerializedName("DisplayLength")
    private Integer displayLength;

    @SerializedName("DisplayStart")
    private Integer displayStart;

    @SerializedName("hieghest_price")
    private Integer hieghestPrice;

    @SerializedName("lowest_price")
    private Integer lowestPrice;

    @SerializedName("adv_department")
    private Integer advDepartment;

    @SerializedName("pyment")
    private Integer pyment;

    @SerializedName("elego")
    private Integer elego;

    @SerializedName("SortCol")
    private Integer sortCol;

    @SerializedName("SortDir")
    private String sortDir;

    @SerializedName("area_id")
    private Integer areaId;

    @SerializedName("state_id")
    private Integer stateId;

    @SerializedName("search")
    private String search;

    @SerializedName("customer_id")
    private Integer customerId = 0;

    public List<Integer> getAdvName() {
        return advName;
    }

    public void setAdvName(List<Integer> advName) {
        this.advName = advName;
    }

    public List<Integer> getAdvKind() {
        return advKind;
    }

    public void setAdvKind(List<Integer> advKind) {
        this.advKind = advKind;
    }

    public List<Integer> getColor() {
        return color;
    }

    public void setColor(List<Integer> color) {
        this.color = color;
    }

    public List<Integer> getBrand() {
        return brand;
    }

    public void setBrand(List<Integer> brand) {
        this.brand = brand;
    }

    public String getLan() {
        return lan;
    }

    public void setLan(String lan) {
        this.lan = lan;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Integer getDisplayLength() {
        return displayLength;
    }

    public void setDisplayLength(Integer displayLength) {
        this.displayLength = displayLength;
    }

    public Integer getDisplayStart() {
        return displayStart;
    }

    public void setDisplayStart(Integer displayStart) {
        this.displayStart = displayStart;
    }

    public Integer getHieghestPrice() {
        return hieghestPrice;
    }

    public void setHieghestPrice(Integer hieghestPrice) {
        this.hieghestPrice = hieghestPrice;
    }

    public Integer getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(Integer lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    public Integer getAdvDepartment() {
        return advDepartment;
    }

    public void setAdvDepartment(Integer advDepartment) {
        this.advDepartment = advDepartment;
    }

    public Integer getPyment() {
        return pyment;
    }

    public void setPyment(Integer pyment) {
        this.pyment = pyment;
    }

    public Integer getElego() {
        return elego;
    }

    public void setElego(Integer elego) {
        this.elego = elego;
    }

    public Integer getSortCol() {
        return sortCol;
    }

    public void setSortCol(Integer sortCol) {
        this.sortCol = sortCol;
    }

    public String getSortDir() {
        return sortDir;
    }

    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
}
