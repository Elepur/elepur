package com.iqitco.elepur_api.modules;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Entity(tableName = "Slides")
public class Slides {

    @PrimaryKey
    @SerializedName("id")
    private Integer id;

    @SerializedName("pic_en")
    private String picEn;

    @SerializedName("date")
    private String date;

    @SerializedName("date_expire")
    private String dateExpire;

    @SerializedName("pic_ar")
    private String picAr;

    @SerializedName("link")
    private String link;

    @SerializedName("link_type")
    private Integer linkType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPicEn() {
        return picEn;
    }

    public void setPicEn(String picEn) {
        this.picEn = picEn;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateExpire() {
        return dateExpire;
    }

    public void setDateExpire(String dateExpire) {
        this.dateExpire = dateExpire;
    }

    public String getPicAr() {
        return picAr;
    }

    public void setPicAr(String picAr) {
        this.picAr = picAr;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getLinkType() {
        return linkType;
    }

    public void setLinkType(Integer linkType) {
        this.linkType = linkType;
    }
}
