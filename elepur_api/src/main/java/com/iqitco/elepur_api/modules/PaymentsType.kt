package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

data class PaymentsType(
        @SerializedName("id") val id: Int?,
        @SerializedName("name_en") val nameEn: String?,
        @SerializedName("name_ar") val nameAr: String?
)