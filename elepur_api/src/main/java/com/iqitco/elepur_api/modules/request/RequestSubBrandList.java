package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestSubBrandList {

    @SerializedName("ID_cat")
    private Integer catId;

    @SerializedName("id_brand")
    private List<Integer> brandsIds;

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public List<Integer> getBrandsIds() {
        return brandsIds;
    }

    public void setBrandsIds(List<Integer> brandsIds) {
        this.brandsIds = brandsIds;
    }
}
