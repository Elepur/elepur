package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date: 2018-08-02
 *
 * @author Mohammad Al-Najjar
 */
class RequestCustomerNotification {

    @field:SerializedName("ID_customer")
    var customerId: Int? = null

    @field:SerializedName("ID")
    var notificationId: Int? = null
}