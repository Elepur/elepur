package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

/**
 * date: 2018-06-12
 *
 * @author Mohammad Al-Najjar
 */
public class RequestInsertVisitor {

    @SerializedName("mac_address")
    private String macAddress;

    @SerializedName("notfy_id")
    private String notificationId;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }
}
