package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

/**
 * date: 2018-07-02
 *
 * @author Mohammad Al-Najjar
 */
public class CustomerNotification {

    @SerializedName("id")
    private Integer id;

    @SerializedName("iD_customer")
    private Integer idCustomer;

    @SerializedName("header")
    private String header;

    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("message")
    private String message;

    @SerializedName("notification_customer")
    private String notificationCustomer;

    @SerializedName("type")
    private Integer type;

    @SerializedName("type_page_action")
    private Integer typePageAction;

    @SerializedName("type_c_s_v")
    private Integer typeCsv;

    @SerializedName("seen")
    private Integer seen;

    @SerializedName("date")
    private String date;

    @SerializedName("row_num")
    private Integer rowNum;

    @SerializedName("total_count")
    private Integer totalCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Integer idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotificationCustomer() {
        return notificationCustomer;
    }

    public void setNotificationCustomer(String notificationCustomer) {
        this.notificationCustomer = notificationCustomer;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTypePageAction() {
        return typePageAction;
    }

    public void setTypePageAction(Integer typePageAction) {
        this.typePageAction = typePageAction;
    }

    public Integer getTypeCsv() {
        return typeCsv;
    }

    public void setTypeCsv(Integer typeCsv) {
        this.typeCsv = typeCsv;
    }

    public Integer getSeen() {
        return seen;
    }

    public void setSeen(Integer seen) {
        this.seen = seen;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
