package com.iqitco.elepur_api.modules;

import com.google.gson.annotations.SerializedName;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class Ads {

    @SerializedName("row_num")
    private Integer rowNum;

    @SerializedName("total_count")
    private Integer totalCount;

    @SerializedName("iD_adv")
    private Integer idAdv;

    @SerializedName("area_id")
    private Integer areaId;

    @SerializedName("state_id")
    private Integer stateId;

    @SerializedName("category")
    private Integer category;

    @SerializedName("capacity")
    private Integer capacity;

    @SerializedName("amount")
    private Integer amount;

    @SerializedName("brands")
    private String brands;

    @SerializedName("adv_name")
    private String advName;

    @SerializedName("kind")
    private String kind;

    @SerializedName("kind_id")
    private Integer kindId;

    @SerializedName("shop_name")
    private String shopName;

    @SerializedName("shop_logo")
    private String shopLogo;

    @SerializedName("n_o")
    private String no;

    @SerializedName("blue_mark")
    private Integer blueMark;

    @SerializedName("price")
    private Integer price;

    @SerializedName("date")
    private String date;

    @SerializedName("pic1")
    private String pic1;

    @SerializedName("shop_id")
    private Integer shopId;

    @SerializedName("special")
    private Integer special;

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getIdAdv() {
        return idAdv;
    }

    public void setIdAdv(Integer idAdv) {
        this.idAdv = idAdv;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getBrands() {
        return brands;
    }

    public void setBrands(String brands) {
        this.brands = brands;
    }

    public String getAdvName() {
        return advName;
    }

    public void setAdvName(String advName) {
        this.advName = advName;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Integer getKindId() {
        return kindId;
    }

    public void setKindId(Integer kindId) {
        this.kindId = kindId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Integer getBlueMark() {
        return blueMark;
    }

    public void setBlueMark(Integer blueMark) {
        this.blueMark = blueMark;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPic1() {
        return pic1;
    }

    public void setPic1(String pic1) {
        this.pic1 = pic1;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSpecial() {
        return special;
    }

    public void setSpecial(Integer special) {
        this.special = special;
    }
}
