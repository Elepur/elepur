package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 7/29/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class GreenLight {

    @field:SerializedName("notfy")
    var notify: Int? = null

    @field:SerializedName("reciept")
    var receipt: Int? = null

}