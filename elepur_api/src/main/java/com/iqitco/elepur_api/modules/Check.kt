package com.iqitco.elepur_api.modules

import com.google.gson.annotations.SerializedName

/**
 * date 9/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
data class Check(@SerializedName("check") val check: Int)