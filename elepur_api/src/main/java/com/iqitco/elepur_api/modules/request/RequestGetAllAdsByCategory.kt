package com.iqitco.elepur_api.modules.request

import com.google.gson.annotations.SerializedName

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class RequestGetAllAdsByCategory {

    @field:SerializedName("DisplayLength")
    var displayLength: Int? = null

    @field:SerializedName("DisplayStart")
    var displayStart: Int? = null

    @field:SerializedName("adv_name")
    var advName: List<Int>? = null

    @field:SerializedName("adv_kind")
    var advKind: List<Int>? = null

    @field:SerializedName("color")
    var color: List<Int>? = null

    @field:SerializedName("Brand")
    var brand: List<Int>? = null

    @field:SerializedName("hieghest_price")
    var hieghestPrice: Int? = null

    @field:SerializedName("lowest_price")
    var lowestPrice: Int? = null

    @field:SerializedName("lan")
    var lan: String? = null

    @field:SerializedName("adv_department")
    var advDepartment: Int? = null

    @field:SerializedName("n_o")

    var newOldFlag: String? = null

    @field:SerializedName("pyment")
    var pyment: Int? = null

    @field:SerializedName("elego")
    var elego: Int? = null

    @field:SerializedName("SortCol")
    var sortCol: Int? = null

    @field:SerializedName("SortDir")
    var sortDir: String? = null

    @field:SerializedName("area_id")
    var areaId: Int? = null

    @field:SerializedName("state_id")
    var stateId: Int? = null







}