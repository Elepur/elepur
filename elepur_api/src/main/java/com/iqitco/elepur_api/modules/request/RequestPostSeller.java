package com.iqitco.elepur_api.modules.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * date 6/2/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RequestPostSeller {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("phone")
    private String phone;

    @SerializedName("owner_name")
    private String ownerName;

    @SerializedName("shopeName")
    private String shopeName;

    @SerializedName("shopeName_ar")
    private String shopeNameAr;

    @SerializedName("time_open")
    private String timeOpen;

    @SerializedName("time_close")
    private String timeClose;

    @SerializedName("device_type")
    private Integer deviceType;

    @SerializedName("State")
    private Integer state;

    @SerializedName("area")
    private Integer area;

    @SerializedName("location")
    private String location;

    @SerializedName("elego")
    private Integer elego;

    @SerializedName("Notification_id")
    private String notificationId;

    @SerializedName("location_long")
    private String locationLong;

    @SerializedName("location_laut")
    private String locationLaut;

    @SerializedName("kind")
    private String kind;

    @SerializedName("kind_ar")
    private String kindAr;

    @SerializedName("Mac_add")
    private String macAdd;

    @SerializedName("PicLogo")
    private String picLogo;

    @SerializedName("shop_logo_format")
    private String shopLogoFormat;

    @SerializedName("Cover_Photo")
    private String coverPhoto;

    @SerializedName("cover_photo_format")
    private String coverPhotoFormat;

    @SerializedName("lics_pic")
    private String licsPic;

    @SerializedName("lics_format")
    private String licsFormat;

    @SerializedName("seller_category")
    private List<Integer> sellerCategory;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getShopeName() {
        return shopeName;
    }

    public void setShopeName(String shopeName) {
        this.shopeName = shopeName;
    }

    public String getShopeNameAr() {
        return shopeNameAr;
    }

    public void setShopeNameAr(String shopeNameAr) {
        this.shopeNameAr = shopeNameAr;
    }

    public String getTimeOpen() {
        return timeOpen;
    }

    public void setTimeOpen(String timeOpen) {
        this.timeOpen = timeOpen;
    }

    public String getTimeClose() {
        return timeClose;
    }

    public void setTimeClose(String timeClose) {
        this.timeClose = timeClose;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getElego() {
        return elego;
    }

    public void setElego(Integer elego) {
        this.elego = elego;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getLocationLong() {
        return locationLong;
    }

    public void setLocationLong(String locationLong) {
        this.locationLong = locationLong;
    }

    public String getLocationLaut() {
        return locationLaut;
    }

    public void setLocationLaut(String locationLaut) {
        this.locationLaut = locationLaut;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getKindAr() {
        return kindAr;
    }

    public void setKindAr(String kindAr) {
        this.kindAr = kindAr;
    }

    public String getMacAdd() {
        return macAdd;
    }

    public void setMacAdd(String macAdd) {
        this.macAdd = macAdd;
    }

    public String getPicLogo() {
        return picLogo;
    }

    public void setPicLogo(String picLogo) {
        this.picLogo = picLogo;
    }

    public String getShopLogoFormat() {
        return shopLogoFormat;
    }

    public void setShopLogoFormat(String shopLogoFormat) {
        this.shopLogoFormat = shopLogoFormat;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getCoverPhotoFormat() {
        return coverPhotoFormat;
    }

    public void setCoverPhotoFormat(String coverPhotoFormat) {
        this.coverPhotoFormat = coverPhotoFormat;
    }

    public String getLicsPic() {
        return licsPic;
    }

    public void setLicsPic(String licsPic) {
        this.licsPic = licsPic;
    }

    public String getLicsFormat() {
        return licsFormat;
    }

    public void setLicsFormat(String licsFormat) {
        this.licsFormat = licsFormat;
    }

    public List<Integer> getSellerCategory() {
        return sellerCategory;
    }

    public void setSellerCategory(List<Integer> sellerCategory) {
        this.sellerCategory = sellerCategory;
    }
}
