package com.iqitco.elepur_api.data;

import com.google.gson.GsonBuilder;
import com.iqitco.elepur_api.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * date 5/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RetrofitConnectionFactory {

    private static final Retrofit RETROFIT;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .build();

        RETROFIT = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_LINK)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.create(
                                new GsonBuilder().serializeNulls().create()
                        )
                )
                .build();
    }

    public static Retrofit getRetrofit() {
        return RETROFIT;
    }
}
