package com.iqitco.elepur_api.data

import com.iqitco.elepur_api.modules.*
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

/**
 * date 8/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
internal interface ElepurRestfulApis {

    @GET("seller/get@granty")
    fun getGuaranteeList(@Query("id") id: Int): Single<BaseResponse<List<Guarantee>>>

    @GET("seller/get@granty@type")
    fun getGuaranteeTypeList(): Single<BaseResponse<List<GuaranteeType>>>

    @GET("seller/get@payment")
    fun getPaymentsTypes(): Single<BaseResponse<List<PaymentsType>>>

    @GET("seller/get@free@gift")
    fun getFreeGifts(): Single<BaseResponse<List<FreeGifts>>>

    @GET("seller/get@color")
    fun getColors(): Single<BaseResponse<List<Color>>>

    @GET("seller/get@kind")
    fun getKind(@Query("cat_id") categoryId: Int): Single<BaseResponse<List<ProductKind>>>

    @GET("seller/get@sub@brand")
    fun getSubBrands(@Query("cat_id") categoryId: Int, @Query("brand_id") brandId: Int): Single<BaseResponse<List<SubBrand>>>

    @GET("customer/get/customer/pic")
    fun getCustomerPic(@Query("id") id: Int, @Header("Accept-Language") language: String): Single<BaseResponse<CustomerPic>>

    @GET("customer/get@share@link")
    fun getShareLink(): Single<BaseResponse<String>>

    @Headers("Content-Type: application/json")
    @GET("seller/get@profile")
    fun getSellerProfile(
            @Query("id") sellerId: Int,
            @Query("id_c") customerId: Int,
            @Query("lan") language: String
    ): Single<BaseResponse<SellerProfile>>

    @GET("seller/get@profile@seller")
    fun getProfileForSeller(
            @Query("id") id: Int,
            @Query("lan") language: String
    ): Single<BaseResponse<SellerProfile>>

    @Headers("Content-Type: application/json")
    @GET("seller/get@adv@seller@pr")
    fun getSellerAdv(
            @Query("id_s") sellerId: Int,
            @Query("cat_id") categoryId: Int,
            @Query("page") page: Int,
            @Query("size") size: Int,
            @Query("lan") language: String
    ): Single<BaseResponse<List<SellerProduct>>>

    @GET("seller/get@add@edit")
    fun getEditProductData(@Query("id_adv") advId: Int, @Query("lan") language: String): Single<BaseResponse<EditProduct>>

}