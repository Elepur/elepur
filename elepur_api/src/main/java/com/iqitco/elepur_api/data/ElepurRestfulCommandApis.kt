package com.iqitco.elepur_api.data

import com.iqitco.elepur_api.modules.RequestUpdateProduct
import com.iqitco.elepur_api.modules.request.RequestAddAdv
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

/**
 * date 9/21/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface ElepurRestfulCommandApis {

    @Multipart
    @POST("http://www.elepur.info/api/upload/up/adv/pic/update")
    fun updateProductPic(
            @Query("id_adv") advId: Int,
            @Query("id_seller") sellerId: Int,
            @Query("id_pic") picId: Int?,
            @Query("adv_name") advName: String,
            @Part pic: MultipartBody.Part
    ): Single<Boolean>

    @Multipart
    @POST("http://www.elepur.info/api/upload/up/adv/pic1/update")
    fun updateProductMainPic(
            @Query("id_adv") advId: Int,
            @Query("id_seller") sellerId: Int,
            @Query("adv_name") advName: String,
            @Part pic: MultipartBody.Part
    ): Single<Boolean>

    @POST("seller/add@post@android")
    fun addAdv(@Body body: RequestAddAdv): Single<BaseResponse<Boolean>>

    @PUT("http://www.elepur.info/api/seller/edit@post@seller")
    fun updateProduct(@Body body: RequestUpdateProduct): Single<BaseResponse<Boolean>>

}