package com.iqitco.elepur_api.data

import android.content.Context
import com.iqitco.elepur_api.modules.RequestUpdateProduct
import com.iqitco.elepur_api.modules.request.RequestAddAdv
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import retrofit2.http.Body

/**
 * date 8/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class CommandDataManager(private val context: Context) {

    private val api = RetrofitConnectionFactory.getRetrofit().create(ElepurRestfulCommandApis::class.java)

    fun addAdv(body: RequestAddAdv): Single<BaseResponse<Boolean>> {
        return api.addAdv(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun updateProductPic(advId: Int, sellerId: Int, picId: Int?, advName: String, pic: MultipartBody.Part): Single<Boolean> {
        return api.updateProductPic(advId, sellerId, picId, advName, pic)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun updateProductMainPic(advId: Int, sellerId: Int, advName: String, pic: MultipartBody.Part): Single<Boolean> {
        return api.updateProductMainPic(advId, sellerId, advName, pic)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun updateProduct(@Body body: RequestUpdateProduct): Single<BaseResponse<Boolean>> {
        return api.updateProduct(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

}