package com.iqitco.elepur_api.data

import android.content.Context
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.database.AppDatabase
import com.iqitco.elepur_api.modules.*
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * date 8/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class QueryDataManager(context: Context) {

    private val database = AppDatabase.getInstance(context)
    private val api = RetrofitConnectionFactory.getRetrofit().create(ElepurRestfulApis::class.java)

    fun getGuaranteeTypeList(): Single<BaseResponse<List<GuaranteeType>>> {
        return api.getGuaranteeTypeList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getGuaranteeList(id: Int): Single<BaseResponse<List<Guarantee>>> {
        return api.getGuaranteeList(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getPaymentsTypes(): Single<BaseResponse<List<PaymentsType>>> {
        return api.getPaymentsTypes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getFreeGifts(): Single<BaseResponse<List<FreeGifts>>> {
        return api.getFreeGifts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getColors(): Single<BaseResponse<List<Color>>> {
        return api.getColors()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getKind(categoryId: Int): Single<BaseResponse<List<ProductKind>>> {
        return api.getKind(categoryId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getSubBrands(categoryId: Int, brandId: Int): Single<BaseResponse<List<SubBrand>>> {
        return api.getSubBrands(categoryId, brandId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getCategories(): Flowable<List<MainPageSection>> {
        return database.mainPageSectionDao.all
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getBrands(): Flowable<List<Brand>> {
        return database.brandDao.allBrand
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getBrands(categoryId: Int): Flowable<List<Brand>> {
        return database.brandDao.getAllBrand(categoryId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getCustomerPic(id: Int, language: String): Single<BaseResponse<CustomerPic>> {
        return api.getCustomerPic(id, language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getShareLink(): Single<BaseResponse<String>> {
        return api.getShareLink()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getSellerProfile(sellerId: Int, customerId: Int, language: String): Single<BaseResponse<SellerProfile>> {
        return api.getSellerProfile(sellerId, customerId, language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getProfileForSeller(id: Int, language: String): Single<BaseResponse<SellerProfile>> {
        return api.getProfileForSeller(id, language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getSellerCategories(idsList: List<Int>): Flowable<List<MainPageSection>> {
        return database.mainPageSectionDao.getByIds(idsList)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getSellerAdv(sellerId: Int, categoryId: Int, page: Int, language: String): Single<BaseResponse<List<SellerProduct>>> {
        return api.getSellerAdv(sellerId, categoryId, page, Constant.PAGE_SIZE, language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun getEditProductData(advId: Int, language: String): Single<BaseResponse<EditProduct>> {
        return api.getEditProductData(advId, language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

}