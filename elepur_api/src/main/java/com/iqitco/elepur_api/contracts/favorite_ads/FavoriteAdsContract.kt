package com.iqitco.elepur_api.contracts.favorite_ads

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.Ads

/**
 * date 7/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface FavoriteAdsContract {

    interface Presenter : BasePresenter {

        fun getCustomerFavAds(page: Int)

    }

    interface View : BaseView {

        fun getId(): Int

        fun publishAds(list: List<Ads>, page: Int)

    }

}