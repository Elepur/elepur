package com.iqitco.elepur_api.contracts.seller_store_result

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.StoreResult

/**
 * date 8/4/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface SellerStoreResultContract {

    interface Presenter : BasePresenter

    interface View : BaseView {

        fun getId(): Int

        fun publishResult(result: StoreResult)

    }

}