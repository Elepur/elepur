package com.iqitco.elepur_api.contracts.home

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.Brand
import com.iqitco.elepur_api.modules.MainPageSection

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface CategoryBrandsContract {

    interface Presenter : BasePresenter {

    }

    interface View : BaseView {

        fun getCategoryId(): Int

        fun publishCategory(category: MainPageSection)

        fun publishBrands(list: List<Brand>)

    }

}