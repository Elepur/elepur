package com.iqitco.elepur_api.contracts.reports_adv

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView

/**
 * date 7/22/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface ReportsAdvContract {


    interface Presenter : BasePresenter {

        fun reportAdv()

    }

    interface View : BaseView {

        fun getHeader(): String

        fun getMessage(): String

        fun getAdvId(): Int

        fun getCustomerId(): Int

        fun onReportSuccess()

    }

}