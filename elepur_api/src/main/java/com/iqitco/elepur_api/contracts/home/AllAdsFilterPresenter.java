package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.database.AppDatabase;
import com.iqitco.elepur_api.modules.Color;
import com.iqitco.elepur_api.modules.ProductKind;
import com.iqitco.elepur_api.modules.SubBrand;
import com.iqitco.elepur_api.modules.request.RequestSubBrandList;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import org.reactivestreams.Subscription;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AllAdsFilterPresenter implements AllAdsFilterContract.Presenter {

    private final AllAdsFilterContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);
    private final AppDatabase database;


    public AllAdsFilterPresenter(AllAdsFilterContract.View view) {
        this.view = view;
        this.database = AppDatabase.getInstance(view.getContext());
    }

    @Override
    public void getSubBrand() {
        RequestSubBrandList body = new RequestSubBrandList();
        body.setBrandsIds(view.getBrands());
        body.setCatId(view.getCategories());

        compositeDisposable.add(
                api.getSubBrands(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::showLoading)
                        .subscribe(this::onGetSubBrandSuccess, this::onError)
        );
    }

    @Override
    public void getProductKind() {
        compositeDisposable.add(
                api.getKinds(view.getCategories())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::showLoading)
                        .subscribe(this::onGetProductKindSuccess, this::onError)
        );
    }

    @Override
    public void onCreate() {
        compositeDisposable.addAll(
                database.getBrandDao().getAllBrand()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishBrands, this::onError),
                database.getMainPageSectionDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishCategories, this::onError),
                api.getColors()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(this::onGetColorSuccess, this::onError)
        );
    }

    private void onGetProductKindSuccess(BaseResponse<List<ProductKind>> body) {
        if (body.isStatus()) {
            view.publishProductKind(body.getData());
        } else {
            view.showUserMessage(body.getMessage());
        }
        view.hideLoading();
    }

    private void onGetSubBrandSuccess(BaseResponse<List<SubBrand>> body) {
        if (body.isStatus()) {
            view.publishSubBrand(body.getData());
        } else {
            view.showUserMessage(body.getMessage());
        }
        view.hideLoading();
    }

    private void onGetColorSuccess(BaseResponse<List<Color>> body) {
        if (body.isStatus()) {
            view.publishColors(body.getData());
        } else {
            view.showErrorMessage(body.getMessage());
        }
    }

    private void showLoading(Subscription subscription) throws Exception {
        view.showLoading();
    }

    private void showLoading(Disposable disposable) throws Exception {
        view.showLoading();
    }

    private void onError(Throwable throwable) throws Exception {
        view.showErrorMessage(throwable.getMessage());
        view.hideLoading();
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("seller/get@color")
        Single<BaseResponse<List<Color>>> getColors();

        @Headers("Content-Type: application/json")
        @POST("seller/get@sub@brand@list")
        Single<BaseResponse<List<SubBrand>>> getSubBrands(@Body RequestSubBrandList body);

        @Headers("Content-Type: application/json")
        @GET("seller/get@kind")
        Single<BaseResponse<List<ProductKind>>> getKinds(@Query("cat_id") Integer catIds);

    }
}
