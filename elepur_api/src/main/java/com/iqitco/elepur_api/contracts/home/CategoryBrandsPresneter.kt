package com.iqitco.elepur_api.contracts.home

import com.iqitco.elepur_api.database.AppDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class CategoryBrandsPresneter(private val view: CategoryBrandsContract.View) : CategoryBrandsContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val database = AppDatabase.getInstance(view.context)

    override fun onCreate() {
        compositeDisposable.addAll(
                database.mainPageSectionDao.getById(view.getCategoryId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishCategory, this::onError),
                database.brandDao.getAllBrand(view.getCategoryId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishBrands, this::onError)
        )
    }

    private fun onError(throwable: Throwable) {
        view.showErrorMessage(throwable.message)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }
}