package com.iqitco.elepur_api.contracts.create_account_customer;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.UserData;

/**
 * date 5/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface CreateAccountCustomerContract {

    interface Presenter extends BasePresenter {

        void registration();

    }

    interface View extends BaseView {

        void showNotValidPassword(boolean flag);

        void showNotValidEmail(boolean flag);

        void emptyBirthday(boolean flag);

        void emptyGender(boolean flag);

        void emptyPassword(boolean flag);

        void errorName(boolean flag);

        void errorEmail(boolean flag);

        String getName();

        String getEmail();

        String getPassword();

        String getPic();

        String getPicFormat();

        String getGender();

        String getBirth();

        void publishCustomerData(UserData body);

        String getNotificationId();

    }

}
