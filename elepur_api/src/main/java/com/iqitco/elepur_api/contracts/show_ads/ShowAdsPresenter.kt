package com.iqitco.elepur_api.contracts.show_ads

import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.FullProduct
import com.iqitco.elepur_api.modules.RateResult
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.request.RequestRateAdv
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.*

/**
 * date 7/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ShowAdsPresenter(private val view: ShowAdsContract.View) : ShowAdsContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)

    override fun insertCallCustomer() {
        compositeDisposable.add(
                api.insertCallCustomer(view.getCustomerId(), view.getSellerId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.onInsertCustomerSuccess(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun rateAdvHappy() {
        val body = RequestRateAdv()
        body.customerId = view.getCustomerId()
        body.advId = view.getAdvId()

        compositeDisposable.add(
                api.rateAdvHappy(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.publishRate(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun rateAdvSad() {
        val body = RequestRateAdv()
        body.customerId = view.getCustomerId()
        body.advId = view.getAdvId()

        compositeDisposable.add(
                api.rateAdvSad(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.publishRate(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun rateAdvFav() {
        val body = RequestRateAdv()
        body.customerId = view.getCustomerId()
        body.advId = view.getAdvId()

        compositeDisposable.add(
                api.rateAdvFav(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.publishRate(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun onCreate() {
        val info = RequestBaseInfo(view.context)
        compositeDisposable.addAll(
                api.getAdv(
                        view.getAdvId(),
                        view.getCustomerId(),
                        info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.publishProduct(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError),
                api.getSellerAdv(
                        view.getSellerId(),
                        view.getAdvId(),
                        info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.publishSellerProduct(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private fun onError(throwable: Throwable) {
        view.showErrorMessage(throwable.message)
        view.hideLoading()
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("customer/get@seller@ADV")
        fun getSellerAdv(
                @Query("seller_id") sellerId: Int,
                @Query("adv_id") advId: Int,
                @Query("lan") language: String
        ): Single<BaseResponse<List<Ads>>>

        @Headers("Content-Type: application/json")
        @GET("customer/get@add@id")
        fun getAdv(
                @Query("id_adv") advId: Int,
                @Query("id_customer") customerId: Int,
                @Query("lan") language: String
        ): Single<BaseResponse<FullProduct>>

        @Headers("Content-Type: application/json")
        @POST("customer/Insert@calls")
        fun insertCallCustomer(@Query("c_id") customerId: Int, @Query("s_id") sellerId: Int): Single<BaseResponse<Boolean>>

        @Headers("Content-Type: application/json")
        @POST("customer/rate@adv@happy")
        fun rateAdvHappy(@Body body: RequestRateAdv): Single<BaseResponse<RateResult>>

        @Headers("Content-Type: application/json")
        @POST("customer/rate@adv@sad")
        fun rateAdvSad(@Body body: RequestRateAdv): Single<BaseResponse<RateResult>>

        @Headers("Content-Type: application/json")
        @POST("customer/add@fav@ADV")
        fun rateAdvFav(@Body body: RequestRateAdv): Single<BaseResponse<RateResult>>
    }
}