package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.data.QueryDataManager;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * date: 2018-07-01
 *
 * @author Mohammad Al-Najjar
 */
public class VisitorProfilePresenter implements VisitorProfileContract.Presenter {

    private final VisitorProfileContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final QueryDataManager queryDataManager;

    public VisitorProfilePresenter(VisitorProfileContract.View view) {
        this.view = view;
        this.queryDataManager = new QueryDataManager(view.getContext());
    }

    @Override
    public void onCreate() {

        compositeDisposable.add(
                queryDataManager.getShareLink()
                        .doOnSubscribe(v -> view.showLoading())
                        .subscribe(body -> {
                            if (body.isStatus()) {
                                view.publishShareLink(body.getData());
                            } else {
                                view.showUserMessage(body.getMessage());
                            }
                            view.hideLoading();
                        }, throwable -> {
                            view.hideLoading();
                            view.showErrorMessage(throwable.getMessage());
                        })
        );

        if (view.getLoginId() < 1) {
            return;
        }

        RequestBaseInfo info = new RequestBaseInfo(view.getContext());
        if (view.getUserRole().equalsIgnoreCase(Constant.USER_ROLE_SELLER)) {
            compositeDisposable.add(
                    queryDataManager.getSellerProfile(view.getLoginId(), 0, info.getLanguage())
                            .doOnSubscribe(this::doOnSubscribe)
                            .subscribe(body -> {
                                if (body.isStatus()) {
                                    view.publishSellerProfile(body.getData());
                                } else {
                                    view.showUserMessage(body.getMessage());
                                }
                                view.hideLoading();
                            }, this::onError)
            );
        } else {
            compositeDisposable.addAll(
                    queryDataManager.getCustomerPic(view.getLoginId(), info.getLanguage())
                            .doOnSubscribe(v -> view.showLoading())
                            .subscribe(body -> {
                                if (body.isStatus()) {
                                    view.publishCustomerPix(body.getData());
                                } else {
                                    view.showUserMessage(body.getMessage());
                                }
                                view.hideLoading();
                            }, this::onError)
            );
        }
    }

    private void doOnSubscribe(Disposable disposable) {
        view.showLoading();
    }

    private void onError(Throwable throwable) {
        view.hideLoading();
        view.showErrorMessage(throwable.getMessage());
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }
}
