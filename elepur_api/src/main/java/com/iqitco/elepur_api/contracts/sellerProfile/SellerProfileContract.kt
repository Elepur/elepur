package com.iqitco.elepur_api.contracts.sellerProfile

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.MainPageSection
import com.iqitco.elepur_api.modules.SellerProduct
import com.iqitco.elepur_api.modules.SellerProfile

/**
 * date 9/8/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface SellerProfileContract {

    interface Presenter : BasePresenter {

        fun getSellerCategories(idsList: List<Int>)

        fun getAdv(pageNumber: Int)
    }

    interface View : BaseView {

        fun getSellerId(): Int

        fun getCategoryId(): Int

        fun publishSellerProfile(profile: SellerProfile)

        fun publishSellerCategory(list: List<MainPageSection>)

        fun publishSellerAdv(list: List<SellerProduct>, pageNumber: Int)

    }

}