package com.iqitco.elepur_api.contracts.login;

import android.text.TextUtils;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.UserData;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestCustomerFacebook;
import com.iqitco.elepur_api.modules.request.RequestLogin;
import com.iqitco.elepur_api.modules.respones.BaseResponse;
import com.iqitco.elepur_api.util.ValidationUtil;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 6/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void login() {

        RequestLogin body = new RequestLogin();

        body.setUsername(view.getEmail());
        boolean usernameFlag = TextUtils.isEmpty(body.getUsername());
        view.errorEmail(usernameFlag);

        body.setPassword(view.getPassword());
        boolean passwordFlag = TextUtils.isEmpty(body.getPassword());
        view.errorPassword(passwordFlag);

        if (usernameFlag || passwordFlag) {
            return;
        }

        if (!ValidationUtil.isValidEmail(body.getUsername())) {
            view.errorEmail(true);
            return;
        } else {
            view.errorEmail(false);
        }

        if (body.getPassword() == null || body.getPassword().length() < 6) {
            view.errorPassword(true);
            return;
        } else {
            view.errorPassword(false);
        }

        body.setPassword(ValidationUtil.getMd5String(body.getPassword()));

        body.setNotificationId(view.getNotificationId());
        if (body.getNotificationId() == null) {
            body.setNotificationId("");
        }
        RequestBaseInfo info = new RequestBaseInfo(view.getContext());
        body.setDeviceType(Integer.parseInt(info.getDeviceType()));

        view.showLoading();
        compositeDisposable.add(
                api.login(body, info.getLanguage())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(d -> view.showLoading())
                        .subscribe(resp -> {
                            view.hideLoading();
                            if (resp.isStatus()) {
                                view.publishData(resp.getData());
                            } else {
                                view.showUserMessage(resp.getMessage());
                            }
                        }, throwable -> {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideLoading();
                        })
        );
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
    }

    @Override
    public void loginFacebook() {
        RequestCustomerFacebook body = new RequestCustomerFacebook();

        body.setEmail(view.getEmail());
        body.setPic(view.getPic());
        body.setName(view.getName());
        body.setGender(view.getGender());
        body.setFacebookId(view.getFacebookId());
        body.setNotifcationId(view.getNotificationId());

        RequestBaseInfo baseInfo = new RequestBaseInfo(view.getContext());

        body.setDeviceType(baseInfo.getDeviceType());
        body.setMacAddress(baseInfo.getMacAddress());

        compositeDisposable.add(
                api.loginFacebook(body, baseInfo.getLanguage())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(d -> view.showLoading())
                        .subscribe(resp -> {
                            view.hideLoading();
                            if (resp.isStatus()) {
                                view.publishData(resp.getData());
                            } else {
                                view.showUserMessage(resp.getMessage());
                            }
                        }, throwable -> {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideLoading();
                        })
        );
    }

    @Override
    public void onCreate() {

    }

    private interface Api {
        @Headers("Content-Type: application/json")
        @POST("ele@login")
        Single<BaseResponse<UserData>> login(@Body RequestLogin body, @Header("Accept-Language") String language);

        @Headers("Content-Type: application/json")
        @POST("post@customer@facebook")
        Single<BaseResponse<UserData>> loginFacebook(@Body RequestCustomerFacebook body, @Header("Accept-Language") String language);
    }
}
