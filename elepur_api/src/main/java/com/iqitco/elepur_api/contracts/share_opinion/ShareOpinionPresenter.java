package com.iqitco.elepur_api.contracts.share_opinion;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.request.RequestInsertMessage;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ShareOpinionPresenter implements ShareOpinionContract.Presenter {

    private final ShareOpinionContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public ShareOpinionPresenter(ShareOpinionContract.View view) {
        this.view = view;
    }

    @Override
    public void insertMessage() {
        RequestInsertMessage body = new RequestInsertMessage();
        body.setHeader(view.getHeader());
        body.setMessage(view.getMessage());
        body.setLoginId(view.getLoginId());

        compositeDisposable.add(
                api.postInsertMessage(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(v -> view.showLoading())
                        .subscribe(resp -> {
                            if (resp.isStatus()) {
                                view.onSuccess(resp.getData());
                            } else {
                                view.showUserMessage(resp.getMessage());
                            }
                            view.hideLoading();
                        }, throwable -> {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideLoading();
                        })
        );
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @POST("customer/Insert@message")
        Single<BaseResponse<Boolean>> postInsertMessage(@Body RequestInsertMessage body);

    }
}
