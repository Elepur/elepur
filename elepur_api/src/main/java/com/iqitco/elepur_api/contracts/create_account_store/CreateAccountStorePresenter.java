package com.iqitco.elepur_api.contracts.create_account_store;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.database.AppDatabase;
import com.iqitco.elepur_api.modules.UserData;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestPostSeller;
import com.iqitco.elepur_api.modules.respones.BaseResponse;
import com.iqitco.elepur_api.util.ValidationUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CreateAccountStorePresenter implements CreateAccountStoreContract.Presenter {

    private final CreateAccountStoreContract.View view;
    private final AppDatabase database;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

    public CreateAccountStorePresenter(CreateAccountStoreContract.View view) {
        this.view = view;
        this.database = AppDatabase.getInstance(view.getContext());
    }

    @Override
    public void registration() {
        RequestPostSeller body = new RequestPostSeller();
        body.setShopeName(view.getShopNameEn());
        boolean nameArFlag = TextUtils.isEmpty(body.getShopeName());
        view.errorShopNameEn(nameArFlag);

        body.setShopeNameAr(view.getShopNameAr());
        boolean nameEnFlag = TextUtils.isEmpty(body.getShopeNameAr());
        view.errorShopNameAr(nameEnFlag);

        body.setOwnerName(view.getOwnerName());
        boolean ownerNameFlag = TextUtils.isEmpty(body.getOwnerName());
        view.errorOwnerName(ownerNameFlag);

        body.setEmail(view.getEmail());
        boolean emailFlag = TextUtils.isEmpty(body.getEmail());
        view.errorEmail(emailFlag);

        body.setPassword(view.getPassword());
        boolean passwordFlag = TextUtils.isEmpty(body.getPassword());
        view.errorPassword(passwordFlag);

        body.setPhone(view.getTelephone());
        boolean phoneFlag = TextUtils.isEmpty(body.getPhone());
        view.errorTelephone(phoneFlag);

        body.setLocation(view.getLocation());
        boolean locationFlag = TextUtils.isEmpty(body.getLocation());
        view.errorLocation(locationFlag);

        body.setKind(view.getDescriptionEn());
        boolean descriptionEnFlag = TextUtils.isEmpty(body.getKind());
        view.errorDescriptionEn(descriptionEnFlag);

        body.setKindAr(view.getDescriptionAr());
        boolean descriptionArFlag = TextUtils.isEmpty(body.getKindAr());
        view.errorDescriptionAr(descriptionArFlag);

        body.setState(view.getStateId());
        boolean stateFlag = body.getState() == null;
        view.errorStateId(stateFlag);

        body.setArea(view.getAreaId());
        boolean areaFlag = body.getArea() == null;
        view.errorAreaId(areaFlag);

        body.setShopLogoFormat("png");
        body.setPicLogo(view.getLogoBase64());
        boolean logoFlag = TextUtils.isEmpty(body.getPicLogo());
        view.errorLogo(logoFlag);

        body.setCoverPhotoFormat("png");
        body.setCoverPhoto(view.getCoverBase64());
        boolean coverFlag = TextUtils.isEmpty(body.getCoverPhoto());
        view.errorCover(coverFlag);

        body.setLicsFormat("png");
        body.setLicsPic(view.getLicenseBase64());
        boolean licenseFlag = TextUtils.isEmpty(body.getLicsPic());
        view.errorLicense(licenseFlag);

        body.setSellerCategory(view.getCategories());
        boolean categoriesFlag = body.getSellerCategory() == null || body.getSellerCategory().size() == 0;
        view.errorCategories(categoriesFlag);

        Calendar openTime = view.getOpenTime();
        boolean openTimeFlag = openTime == null;
        view.errorOpenTime(openTimeFlag);
        if (!openTimeFlag) {
            body.setTimeOpen(simpleDateFormat.format(openTime.getTime()));
        }

        Calendar closeTime = view.getCloseTime();
        boolean closeTimeFlag = closeTime == null;
        view.errorCloseTime(closeTimeFlag);
        if (!closeTimeFlag) {
            body.setTimeClose(simpleDateFormat.format(closeTime.getTime()));
        }

        if (TextUtils.isEmpty(body.getEmail()) || body.getEmail().length() < 6 || !ValidationUtil.isValidEmail(body.getEmail())) {
            view.showNotValidEmail(true);
            return;
        } else {
            view.showNotValidEmail(false);
        }

        if (TextUtils.isEmpty(body.getPassword()) || body.getPassword().length() < 6) {
            view.showNotValidPassword(true);
            return;
        } else {
            view.showNotValidPassword(false);
        }

        if (nameArFlag ||
                nameEnFlag ||
                ownerNameFlag ||
                emailFlag ||
                passwordFlag ||
                phoneFlag ||
                locationFlag ||
                descriptionEnFlag ||
                descriptionArFlag ||
                stateFlag ||
                areaFlag ||
                logoFlag ||
                coverFlag ||
                licenseFlag ||
                openTimeFlag ||
                closeTimeFlag ||
                categoriesFlag) {
            /*if one mandatory return null or empty, stop*/
            return;
        }

        body.setLocationLaut(view.getLatitude());
        body.setLocationLong(view.getLongitude());
        body.setElego(view.getDeliveryCheck() ? 1 : 0);

        RequestBaseInfo info = new RequestBaseInfo(view.getContext());
        body.setMacAdd(info.getMacAddress());
        body.setNotificationId(view.getNotificationId());
        body.setDeviceType(Integer.parseInt(info.getDeviceType()));

        /*change password string to md5 base64*/
        body.setPassword(ValidationUtil.getMd5String(body.getPassword()));

        Log.d("gson", new GsonBuilder().serializeNulls().create().toJson(body));

        compositeDisposable.add(
                api.postSeller(body, info.getLanguage())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(d -> view.showLoading())
                        .subscribe(resp -> {
                            if (resp.isStatus()) {
                                view.publishSellerData(resp.getData());
                            } else {
                                view.showUserMessage(resp.getMessage());
                            }
                            view.hideLoading();
                        }, throwable -> {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideLoading();
                        })
        );

    }

    @Override
    public void getStateArea() {
        compositeDisposable.add(
                database.getStateAreaDao().getAll(view.getStateId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishArea, throwable -> view.showErrorMessage(throwable.getMessage()))
        );
    }

    @Override
    public void onCreate() {
        compositeDisposable.addAll(
                database.getStateDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishState, throwable -> view.showErrorMessage(throwable.getMessage())),
                database.getMainPageSectionDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishProfessions, throwable -> view.showErrorMessage(throwable.getMessage()))
        );
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {
        @Headers("Content-Type: application/json")
        @POST("post/seller")
        Single<BaseResponse<UserData>> postSeller(@Body RequestPostSeller body, @Header("Accept-Language") String language);
    }
}
