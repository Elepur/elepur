package com.iqitco.elepur_api.contracts.login_vistor;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;

/**
 * date: 2018-06-12
 *
 * @author Mohammad Al-Najjar
 */
public interface LoginVisitorContract {

    interface Presenter extends BasePresenter {
        void loginVisitor();
    }

    interface View extends BaseView {

        String getNotificationId();

        void onSuccess(boolean flag);

    }

}
