package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.database.AppDatabase;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestLogoutCustomer;
import com.iqitco.elepur_api.modules.request.RequestLogoutSeller;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 6/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class HomePresenter implements HomeContract.Presenter {

    private final HomeContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final AppDatabase database;
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public HomePresenter(HomeContract.View view) {
        this.view = view;
        this.database = AppDatabase.getInstance(view.getContext());
    }

    @Override
    public void logout() {

        RequestBaseInfo info = new RequestBaseInfo(view.getContext());

        Disposable disposable;
        if (view.getRole().equalsIgnoreCase("seller")) {
            RequestLogoutSeller body = new RequestLogoutSeller();
            body.setId(view.getId());
            disposable = api.logoutSeller(body, info.getLanguage())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe(this::showLoading)
                    .subscribe(this::Success, this::onError);
        } else {
            RequestLogoutCustomer body = new RequestLogoutCustomer();
            body.setId(view.getId());
            disposable = api.logoutCustomer(body, info.getLanguage())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe(this::showLoading)
                    .subscribe(this::Success, this::onError);
        }

        compositeDisposable.add(disposable);
    }

    private void showLoading(Disposable disposable) throws Exception {
        view.showLoading();
    }

    private void Success(BaseResponse<Boolean> body) throws Exception {
        view.hideLoading();
        if (body.isStatus()) {
            view.onSuccess();
        } else {
            view.showUserMessage(body.getMessage());
        }
    }

    private void onError(Throwable throwable) throws Exception {
        view.showErrorMessage(throwable.getMessage());
        view.hideLoading();
    }

    @Override
    public void onCreate() {
        int roleId;
        if (view.getRole().equalsIgnoreCase("seller")) {
            roleId = 3;
        } else if (view.getRole().equalsIgnoreCase("customer")) {
            roleId = 2;
        } else {
            roleId = 1;
        }

        compositeDisposable.add(
                database.getPopAdvDao().getAllById(roleId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishMainAdv, this::onError)
        );
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @POST("seller/insert@logout")
        Single<BaseResponse<Boolean>> logoutSeller(@Body RequestLogoutSeller body, @Header("Accept-Language") String language);

        @Headers("Content-Type: application/json")
        @POST("customer/insert@logout")
        Single<BaseResponse<Boolean>> logoutCustomer(@Body RequestLogoutCustomer body, @Header("Accept-Language") String language);

    }
}
