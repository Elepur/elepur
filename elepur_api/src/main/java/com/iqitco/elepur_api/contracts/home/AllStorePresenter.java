package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.database.AppDatabase;
import com.iqitco.elepur_api.modules.Seller;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * date 6/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AllStorePresenter implements AllStoreContract.Presenter {

    private final AllStoreContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);
    private final AppDatabase database;

    public AllStorePresenter(AllStoreContract.View view) {
        this.view = view;
        this.database = AppDatabase.getInstance(view.getContext());
    }

    @Override
    public void getAllSellers(int pageNumber) {
        RequestBaseInfo info = new RequestBaseInfo(view.getContext());
        compositeDisposable.add(
                api.getAllSellers(
                        info.getLanguage(),
                        Constant.PAGE_SIZE,
                        pageNumber,
                        view.getStateId(),
                        view.getAreaId(),
                        info.getLanguage(),
                        view.getSearchStr())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::onStart)
                        .subscribe(response -> {
                            view.hideLoading();
                            if (response.isStatus()) {
                                view.publishAllSellers(response.getData(), pageNumber);
                            } else {
                                view.showUserMessage(response.getMessage());
                            }
                        },this::onError)
        );
    }

    @Override
    public void getAreas() {
        compositeDisposable.addAll(
                database.getStateAreaDao().getAll(view.getStateId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishArea, this::onError)
        );
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    @Override
    public void onCreate() {
        RequestBaseInfo info = new RequestBaseInfo(view.getContext());

        compositeDisposable.addAll(
                database.getStateDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishState, this::onError),
                api.getAllSellers(
                        info.getLanguage(),
                        Constant.PAGE_SIZE,
                        1,
                        view.getStateId(),
                        view.getAreaId(),
                        info.getLanguage(),
                        "")
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::onStart)
                        .subscribe(response -> {
                            view.hideLoading();
                            if (response.isStatus()) {
                                view.publishAllSellers(response.getData(), 1);
                            } else {
                                view.showUserMessage(response.getMessage());
                            }
                        },this::onError)
        );
    }

    private void onError(Throwable throwable) {
        view.showErrorMessage(throwable.getMessage());
        view.hideLoading();
    }

    private void onStart(Disposable disposable) throws Exception {
        view.showLoading();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("seller/get@all@seller")
        Single<BaseResponse<List<Seller>>> getAllSellers(
                @Header("Accept-Language") String language,
                @Query("DisplayLength") Integer displayLength,
                @Query("DisplayStart") Integer displayStart,
                @Query("state_id") Integer stateId,
                @Query("area_id") Integer areaId,
                @Query("lan") String lan,
                @Query("search") String search
        );

    }

}
