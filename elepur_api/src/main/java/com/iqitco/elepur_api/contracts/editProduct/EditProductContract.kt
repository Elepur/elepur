package com.iqitco.elepur_api.contracts.editProduct

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.*

/**
 * date 9/15/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface EditProductContract {

    interface Presenter : BasePresenter {
        fun getBrands()
        fun getKind()
        fun getGuaranteeList()
        fun getSubBrands()
        fun updateMainPic()
        fun updatePic(picId: Int?, picPosition: Int)
        fun updateProduct()
    }

    interface View : BaseView {

        fun getAdvId(): Int

        fun publishProduct(product: EditProduct)
        fun publishColors(list: List<Color>?)
        fun publishFreeGifts(list: List<FreeGifts>?)
        fun publishGuaranteeType(list: List<GuaranteeType>?)
        fun publishPaymentTypes(list: List<PaymentsType>?)
        fun publishCategories(list: List<MainPageSection>)
        fun getCategoryId(): Int
        fun getGuaranteeTypeId(): Int
        fun publishGuarantee(list: List<Guarantee>)
        fun publishKind(list: List<ProductKind>)
        fun getBrandId(): Int
        fun publishSubBrands(list: List<SubBrand>)
        fun publishBrands(list: List<Brand>)
        fun getSellerId(): Int
        fun getAdvName(): String
        fun getMainPic(): ByteArray
        fun getPic(picId: Int): ByteArray
        fun onUpdateMainPicSuccess()
        fun onUpdatePicSuccess(picId: Int)
        fun onProductUpdate()
        fun getRequestBodyProduct(): RequestUpdateProduct
    }

}