package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.Ads;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;

import java.util.List;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface AllAdsContract {

    interface Presenter extends BasePresenter {

        void getAllAds(int pageNumber);

        void getAreas();

    }

    interface View extends BaseView {

        void publishAdv(List<Ads> list, int pageNumber);

        void publishState(List<State> list);

        void publishArea(List<StateArea> list);

        Integer getStateId();

        Integer getAreaId();

        Integer getSortColumn();

        String getSortDirection();

        List<Integer> getBrandsIds();

        List<Integer> getSubBrands();

        List<Integer> getColors();

        List<Integer> getKind();

        String getNewOldFlag();

        Boolean getElegoFlag();

        Integer getLowestPrice();

        Integer getHighestPrice();

    }

}
