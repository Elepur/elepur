package com.iqitco.elepur_api.contracts.notification

import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.modules.request.RequestCustomerNotification
import com.iqitco.elepur_api.modules.request.RequestSellerNotification
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * date: 2018-08-02
 *
 * @author Mohammad Al-Najjar
 */
class ShowNotificationPresenter(private val view: ShowNotificationContract.View) : ShowNotificationContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)

    override fun deleteNotification() {
        if (view.getRole().equals("seller", ignoreCase = true)) {
            val body = RequestSellerNotification()
            body.sellerId = view.getUserId()
            body.notificationId = view.getNotificationId()
            compositeDisposable.add(
                    api.deleteSellerNotification(body)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .doOnSubscribe { view.showLoading() }
                            .subscribe(this::onDeleteSuccess, this::onError)
            )
        } else if (view.getRole().equals("customer", ignoreCase = true)) {
            val body = RequestCustomerNotification()
            body.customerId = view.getUserId()
            body.notificationId = view.getNotificationId()
            compositeDisposable.add(
                    api.deleteCustomerNotification(body)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .doOnSubscribe { view.showLoading() }
                            .subscribe(this::onDeleteSuccess, this::onError)
            )
        }
    }

    override fun updateSeenNotification() {
        if (view.getRole().equals("seller", ignoreCase = true)) {
            val body = RequestSellerNotification()
            body.sellerId = view.getUserId()
            body.notificationId = view.getNotificationId()
            compositeDisposable.add(
                    api.updateSellerNotification(body)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(this::onUpdateSuccess, this::onError)
            )
        } else if (view.getRole().equals("customer", ignoreCase = true)) {
            val body = RequestCustomerNotification()
            body.customerId = view.getUserId()
            body.notificationId = view.getNotificationId()
            compositeDisposable.add(
                    api.updateCustomerNotification(body)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(this::onUpdateSuccess, this::onError)
            )
        }
    }

    private fun onUpdateSuccess(body: BaseResponse<Boolean>) {
        if (body.isStatus) {
            view.onUpdateSuccess(body.data)
        } else {
            view.showUserMessage(body.message)
        }
        view.hideLoading()
    }

    private fun onDeleteSuccess(body: BaseResponse<Boolean>) {
        if (body.isStatus) {
            view.onDeleteSuccess(body.data)
        } else {
            view.showUserMessage(body.message)
        }
        view.hideLoading()
    }

    private fun onError(throwable: Throwable) {
        view.showErrorMessage(throwable.message)
        view.hideLoading()
    }

    override fun onCreate() {
        updateSeenNotification()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @POST("seller/update@show@notfy@app")
        fun deleteSellerNotification(@Body body: RequestSellerNotification): Single<BaseResponse<Boolean>>

        @POST("seller/update@seen@notfy@app")
        fun updateSellerNotification(@Body body: RequestSellerNotification): Single<BaseResponse<Boolean>>

        @POST("customer/update@show@notfy@app")
        fun deleteCustomerNotification(@Body body: RequestCustomerNotification): Single<BaseResponse<Boolean>>

        @POST("customer/update@seen@notfy@app")
        fun updateCustomerNotification(@Body body: RequestCustomerNotification): Single<BaseResponse<Boolean>>

    }
}