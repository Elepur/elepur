package com.iqitco.elepur_api.contracts.favorite_stores

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.Seller

/**
 * date 7/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface FavoriteStoresContract {

    interface Presenter : BasePresenter {

        fun getCustomerFavSellers(page : Int)

    }

    interface View : BaseView {

        fun getId() : Int

        fun publishSellers(list : List<Seller>, pageNumber : Int)

    }

}