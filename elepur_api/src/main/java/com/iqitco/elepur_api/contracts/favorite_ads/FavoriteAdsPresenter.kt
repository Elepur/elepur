package com.iqitco.elepur_api.contracts.favorite_ads

import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

/**
 * date 7/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class FavoriteAdsPresenter(private val view: FavoriteAdsContract.View) : FavoriteAdsContract.Presenter {

    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)
    private val compositeDisposable = CompositeDisposable()

    override fun getCustomerFavAds(page: Int) {
        val info = RequestBaseInfo(view.context)

        compositeDisposable.add(
                api.getCustomerFavAds(
                        view.getId(),
                        Constant.PAGE_SIZE,
                        page,
                        info.language
                ).observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            when (it.isStatus) {
                                true -> view.publishAds(it.data, page)
                                false -> view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, {
                            view.showErrorMessage(it.message)
                            view.hideLoading()
                        })
        )
    }

    override fun onCreate() {
        getCustomerFavAds(1)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("customer/get@fav@ADV")
        fun getCustomerFavAds(
                @Query("customer_id") id: Int,
                @Query("size") size: Int,
                @Query("page") page: Int,
                @Query("lan") lan: String
        ): Single<BaseResponse<List<Ads>>>

    }
}