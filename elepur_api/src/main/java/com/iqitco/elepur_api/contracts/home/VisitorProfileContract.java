package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.CustomerPic;
import com.iqitco.elepur_api.modules.SellerProfile;

/**
 * date: 2018-07-01
 *
 * @author Mohammad Al-Najjar
 */
public interface VisitorProfileContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView {

        Integer getLoginId();

        String getUserRole();

        void publishCustomerPix(CustomerPic pic);

        void publishShareLink(String str);

        void publishSellerProfile(SellerProfile profile);
    }

}
