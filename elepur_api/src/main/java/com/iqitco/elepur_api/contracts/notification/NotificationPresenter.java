package com.iqitco.elepur_api.contracts.notification;

import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.UserNotification;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * date: 2018-07-02
 *
 * @author Mohammad Al-Najjar
 */
public class NotificationPresenter implements NotificationContract.Presenter {

    private final NotificationContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public NotificationPresenter(NotificationContract.View view) {
        this.view = view;
    }

    @Override
    public void getNotification(int pageNumber) {
        if (view.getRole().equalsIgnoreCase("seller")) {
            compositeDisposable.add(
                    api.getSellerNotification(view.getId(), Constant.PAGE_SIZE, pageNumber)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .doOnSubscribe(v -> view.showLoading())
                            .subscribe(body -> {
                                if (body.isStatus()) {
                                    view.publishNotification(body.getData(), pageNumber);
                                } else {
                                    view.showUserMessage(body.getMessage());
                                }
                                view.hideLoading();
                            }, this::onError)
            );
        } else if (view.getRole().equalsIgnoreCase("customer")) {
            compositeDisposable.add(
                    api.getCustomerNotification(view.getId(), Constant.PAGE_SIZE, pageNumber)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .doOnSubscribe(v -> view.showLoading())
                            .subscribe(body -> {
                                if (body.isStatus()) {
                                    view.publishNotification(body.getData(), pageNumber);
                                } else {
                                    view.showUserMessage(body.getMessage());
                                }
                                view.hideLoading();
                            }, this::onError)
            );
        } else {
            compositeDisposable.add(
                    api.getVisitorNotification(Constant.PAGE_SIZE, pageNumber)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .doOnSubscribe(v -> view.showLoading())
                            .subscribe(body -> {
                                if (body.isStatus()) {
                                    view.publishNotification(body.getData(), pageNumber);
                                } else {
                                    view.showUserMessage(body.getMessage());
                                }
                                view.hideLoading();
                            }, this::onError)
            );
        }
    }

    private void onError(Throwable throwable) {
        view.hideLoading();
        view.showErrorMessage(throwable.getMessage());
    }

    @Override
    public void onCreate() {
        getNotification(1);
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("customer/get@customer@notfy@app")
        Single<BaseResponse<List<UserNotification>>> getCustomerNotification(
                @Query("id") Integer id,
                @Query("size") Integer size,
                @Query("page") Integer page
        );

        @Headers("Content-Type: application/json")
        @GET("seller/get@seller@notfy@app")
        Single<BaseResponse<List<UserNotification>>> getSellerNotification(
                @Query("id") Integer id,
                @Query("size") Integer size,
                @Query("page") Integer page
        );

        @Headers("Content-Type: application/json")
        @GET("customer/get@vistor@notfy@app")
        Single<BaseResponse<List<UserNotification>>> getVisitorNotification(
                @Query("size") Integer size,
                @Query("page") Integer page
        );

    }
}
