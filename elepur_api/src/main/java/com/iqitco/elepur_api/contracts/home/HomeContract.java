package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.PopAdv;

/**
 * date 6/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface HomeContract {

    interface Presenter extends BasePresenter {

        void logout();

    }

    interface View extends BaseView {

        Integer getId();

        String getRole();

        void onSuccess();

        void publishMainAdv(PopAdv popAdv);

    }

}
