package com.iqitco.elepur_api.contracts.editProduct

import com.iqitco.elepur_api.data.CommandDataManager
import com.iqitco.elepur_api.data.QueryDataManager
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import io.reactivex.disposables.CompositeDisposable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody


/**
 * date 9/15/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class EditProductPresenter(private val view: EditProductContract.View) : EditProductContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val queryDataManager = QueryDataManager(view.context)
    private val commandDataManager = CommandDataManager(view.context)
    private val info = RequestBaseInfo(view.context)

    private fun onError(throwable: Throwable) {
        view.hideLoading()
        view.showErrorMessage(throwable.message)
    }

    override fun getGuaranteeList() {
        compositeDisposable.add(
                queryDataManager.getGuaranteeList(view.getGuaranteeTypeId())
                        .subscribe({
                            if (it.isStatus) {
                                view.publishGuarantee(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getKind() {
        compositeDisposable.add(
                queryDataManager.getKind(view.getCategoryId())
                        .subscribe({
                            if (it.isStatus) {
                                view.publishKind(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getSubBrands() {
        compositeDisposable.add(
                queryDataManager.getSubBrands(view.getCategoryId(), view.getBrandId())
                        .subscribe({
                            if (it.isStatus) {
                                view.publishSubBrands(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getBrands() {
        compositeDisposable.add(queryDataManager.getBrands(view.getCategoryId()).subscribe(view::publishBrands, this::onError))
    }

    override fun onCreate() {
        compositeDisposable.addAll(
                queryDataManager.getColors().subscribe({
                    if (it.isStatus) {
                        view.publishColors(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getFreeGifts().subscribe({
                    if (it.isStatus) {
                        view.publishFreeGifts(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getGuaranteeTypeList().subscribe({
                    if (it.isStatus) {
                        view.publishGuaranteeType(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getPaymentsTypes().subscribe({
                    if (it.isStatus) {
                        view.publishPaymentTypes(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getCategories().subscribe(view::publishCategories, this::onError),
                queryDataManager.getEditProductData(view.getAdvId(), info.language)
                        .doOnSubscribe { view.showLoading() }
                        .doFinally { view.hideLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishProduct(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun updateMainPic() {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), view.getMainPic())
        val body = MultipartBody.Part.createFormData("pic", "pic.jpg", requestFile)
        compositeDisposable.add(
                commandDataManager.updateProductMainPic(view.getAdvId(), view.getSellerId(), view.getAdvName(), body)
                        .doOnSubscribe { view.showLoading() }
                        .doFinally { view.hideLoading() }
                        .subscribe({
                            view.onUpdateMainPicSuccess()
                        }, {
                            view.onUpdateMainPicSuccess()
                        })
        )
    }

    override fun updatePic(picId: Int?, picPosition: Int) {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), view.getPic(picPosition))
        val body = MultipartBody.Part.createFormData("pic", "pic.jpg", requestFile)
        compositeDisposable.add(
                commandDataManager.updateProductPic(view.getAdvId(), view.getSellerId(), picId, view.getAdvName(), body)
                        .doOnSubscribe { view.showLoading() }
                        .doFinally { view.hideLoading() }
                        .subscribe({
                            view.onUpdatePicSuccess(picPosition)
                        }, {
                            view.onUpdatePicSuccess(picPosition)
                        })
        )
    }

    override fun updateProduct() {
        compositeDisposable.add(
                commandDataManager.updateProduct(view.getRequestBodyProduct())
                        .doOnSubscribe { view.showLoading() }
                        .doFinally { view.hideLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.onProductUpdate()
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }
}