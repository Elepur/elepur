package com.iqitco.elepur_api.contracts.update_customer_profile;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;

/**
 * date 7/3/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface UpdatePasswordContract {

    interface Presenter extends BasePresenter {

        void updateCustomerPassword();

    }

    interface View extends BaseView {

        String getOldPassword();

        String getNewPassword();

        Integer getId();

        void errorOldPassword(boolean flag);

        void errorNewPassword(boolean flag);

        void onUpdateSuccess();

    }

}
