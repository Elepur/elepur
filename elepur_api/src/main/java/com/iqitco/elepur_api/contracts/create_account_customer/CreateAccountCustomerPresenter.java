package com.iqitco.elepur_api.contracts.create_account_customer;

import android.text.TextUtils;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.UserData;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestPostCustomer;
import com.iqitco.elepur_api.modules.respones.BaseResponse;
import com.iqitco.elepur_api.util.ValidationUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 5/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CreateAccountCustomerPresenter implements CreateAccountCustomerContract.Presenter {

    private final CreateAccountCustomerContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public CreateAccountCustomerPresenter(CreateAccountCustomerContract.View view) {
        this.view = view;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void registration() {
        RequestPostCustomer body = new RequestPostCustomer();

        body.setName(view.getName());
        boolean nameFlag = TextUtils.isEmpty(body.getName());
        view.errorName(nameFlag);

        body.setEmail(view.getEmail());
        boolean emailFlag = TextUtils.isEmpty(body.getEmail());
        view.errorEmail(emailFlag);

        body.setPassword(view.getPassword());
        boolean passwordFlag = TextUtils.isEmpty(body.getPassword());
        view.emptyPassword(passwordFlag);

        body.setGender(view.getGender());
        boolean genderFlag = TextUtils.isEmpty(body.getGender());
        view.emptyGender(genderFlag);

        body.setBirth(view.getBirth());
        boolean birthFlag = TextUtils.isEmpty(body.getBirth());
        view.emptyBirthday(birthFlag);

        if (TextUtils.isEmpty(body.getEmail()) || body.getEmail().length() < 6 || !ValidationUtil.isValidEmail(body.getEmail())) {
            view.showNotValidEmail(true);
            return;
        } else {
            view.showNotValidEmail(false);
        }

        if (TextUtils.isEmpty(body.getPassword()) || body.getPassword().length() < 6) {
            view.showNotValidPassword(true);
            return;
        } else {
            view.showNotValidPassword(false);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            calendar.setTime(dateFormat.parse(body.getBirth()));


            body.setAge(currentYear - calendar.get(Calendar.YEAR));
        } catch (Exception e) {
            return;
        }

        if (nameFlag || emailFlag || passwordFlag || genderFlag || birthFlag) {
            return;
        }

        body.setPic(view.getPic());
        body.setPicFormat(view.getPicFormat());

        RequestBaseInfo requestBaseInfo = new RequestBaseInfo(view.getContext());
        body.setNotifcationId(view.getNotificationId());
        body.setMacAddress(requestBaseInfo.getMacAddress());
        body.setDeviceType(requestBaseInfo.getDeviceType());

        /*change password string to md5 base64*/
        body.setPassword(ValidationUtil.getMd5String(body.getPassword()));

        compositeDisposable.add(
                api.postCustomer(body, requestBaseInfo.getLanguage())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(d -> view.showLoading())
                        .subscribe(resp -> {
                            if (resp.isStatus()) {
                                view.publishCustomerData(resp.getData());
                            } else {
                                view.showUserMessage(resp.getMessage());
                            }
                            view.hideLoading();
                        }, throwable -> {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideLoading();
                        })
        );
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {
        @Headers("Content-Type: application/json")
        @POST("post@customer")
        Single<BaseResponse<UserData>> postCustomer(@Body RequestPostCustomer body, @Header("Accept-Language") String language);
    }
}
