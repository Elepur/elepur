package com.iqitco.elepur_api.contracts.update_customer_profile;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestUpdateCustomerPassword;
import com.iqitco.elepur_api.modules.respones.BaseResponse;
import com.iqitco.elepur_api.util.ValidationUtil;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 7/3/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class UpdatePasswordPresenter implements UpdatePasswordContract.Presenter {

    private final UpdatePasswordContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public UpdatePasswordPresenter(UpdatePasswordContract.View view) {
        this.view = view;
    }

    @Override
    public void updateCustomerPassword() {
        RequestBaseInfo info = new RequestBaseInfo(view.getContext());
        RequestUpdateCustomerPassword body = new RequestUpdateCustomerPassword();
        body.setId(view.getId());

        if (view.getOldPassword().length() < 6) {
            view.errorOldPassword(true);
            return;
        } else {
            view.errorOldPassword(false);
        }

        if (view.getNewPassword().length() < 6) {
            view.errorNewPassword(true);
            return;
        } else {
            view.errorNewPassword(false);
        }

        body.setPassword(ValidationUtil.getMd5String(view.getNewPassword()));
        body.setPasswordOld(ValidationUtil.getMd5String(view.getOldPassword()));

        compositeDisposable.add(
                api.updateCustomerPassword(body, info.getLanguage())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(v -> view.showLoading())
                        .subscribe(respBody -> {
                            view.hideLoading();
                            if (respBody.isStatus()) {
                                view.onUpdateSuccess();
                            } else {
                                view.showUserMessage(respBody.getMessage());
                            }
                        }, throwable -> {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideLoading();
                        })
        );
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @POST("customer/update@profile@password")
        Single<BaseResponse<Boolean>> updateCustomerPassword(@Body RequestUpdateCustomerPassword body, @Header("Accept-Language") String lang);
    }
}
