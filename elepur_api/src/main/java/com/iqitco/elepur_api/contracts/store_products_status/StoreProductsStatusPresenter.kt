package com.iqitco.elepur_api.contracts.store_products_status

import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.modules.Check
import com.iqitco.elepur_api.modules.StoreProductStatus
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.request.RequestRemoveProduct
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.*

/**
 * date 8/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class StoreProductsStatusPresenter(private val view: StoreProductsStatusContract.View) : StoreProductsStatusContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)
    private val info = RequestBaseInfo(view.context)

    override fun getSellerProducts(pageNumber: Int) {
        compositeDisposable.add(
                api.getSellerProductsStatus(
                        view.getId(),
                        Constant.PAGE_SIZE,
                        pageNumber,
                        view.getSearchStr(),
                        info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishProducts(it.data, pageNumber)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, {
                            view.showErrorMessage(it.message)
                            view.hideLoading()
                        })
        )
    }

    override fun removeFromStore(advId: Int) {
        val body = RequestRemoveProduct()
        body.advId = advId
        body.sellerId = view.getId()
        compositeDisposable.add(
                api.removeFromStore(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.onRemoveProductSuccess(it.data.check == 1)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, {
                            view.showErrorMessage(it.message)
                            view.hideLoading()
                        })
        )
    }

    override fun removeFromElepur(advId: Int) {
        val body = RequestRemoveProduct()
        body.advId = advId
        body.sellerId = view.getId()
        compositeDisposable.add(
                api.removeFromElepur(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.onRemoveProductSuccess(it.data.check == 1)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, {
                            view.showErrorMessage(it.message)
                            view.hideLoading()
                        })
        )
    }

    override fun onCreate() {
        getSellerProducts(1)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("seller/get@seller@adv@status")
        fun getSellerProductsStatus(
                @Query("id") id: Int,
                @Query("size") size: Int,
                @Query("page") pageNumber: Int,
                @Query("search") search: String?,
                @Query("lan") language: String
        ): Single<BaseResponse<List<StoreProductStatus>>>

        @POST("seller/Delete@adv@store")
        fun removeFromStore(@Body body: RequestRemoveProduct): Single<BaseResponse<Check>>

        @POST("seller/Delete@adv@ele")
        fun removeFromElepur(@Body body: RequestRemoveProduct): Single<BaseResponse<Check>>

    }
}