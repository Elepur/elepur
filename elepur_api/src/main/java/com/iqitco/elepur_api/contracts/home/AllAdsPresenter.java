package com.iqitco.elepur_api.contracts.home;

import android.support.annotation.NonNull;

import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.database.AppDatabase;
import com.iqitco.elepur_api.modules.Ads;
import com.iqitco.elepur_api.modules.request.RequestAllAds;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AllAdsPresenter implements AllAdsContract.Presenter {

    private final AllAdsContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);
    private final AppDatabase database;

    public AllAdsPresenter(AllAdsContract.View view) {
        this.view = view;
        this.database = AppDatabase.getInstance(view.getContext());
    }

    @Override
    public void getAllAds(int pageNumber) {
        RequestAllAds body = createRequestBody(pageNumber);

        compositeDisposable.addAll(
                api.getAllAds(body, body.getLan())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::onStart)
                        .subscribe(response -> {
                            if (response.isStatus()) {
                                view.publishAdv(response.getData(), pageNumber);
                            } else {
                                view.showUserMessage(response.getMessage());
                            }
                            view.hideLoading();
                        }, this::onError)
        );
    }

    @Override
    public void getAreas() {
        compositeDisposable.addAll(
                database.getStateAreaDao().getAll(view.getStateId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishArea, this::onError)
        );
    }

    @Override
    public void onCreate() {
        RequestAllAds body = createRequestBody(1);

        compositeDisposable.addAll(
                database.getStateDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishState, this::onError),
                api.getAllAds(body, body.getLan())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::onStart)
                        .subscribe(response -> {
                            if (response.isStatus()) {
                                view.publishAdv(response.getData(), 1);
                            } else {
                                view.showUserMessage(response.getMessage());
                            }
                            view.hideLoading();
                        }, this::onError)
        );
    }

    @NonNull
    private RequestAllAds createRequestBody(int pageNumber) {
        RequestBaseInfo info = new RequestBaseInfo(view.getContext());
        RequestAllAds body = new RequestAllAds();
        body.setAdvName(view.getSubBrands());
        body.setAdvKind(view.getKind());
        body.setColor(view.getColors());
        body.setBrand(view.getBrandsIds());
        body.setLan(info.getLanguage());
        body.setNo(view.getNewOldFlag());
        body.setDisplayLength(Constant.PAGE_SIZE);
        body.setDisplayStart(pageNumber);
        body.setHieghestPrice(view.getHighestPrice());
        body.setLowestPrice(view.getLowestPrice());
        body.setAdvDepartment(0);
        body.setPyment(0);
        body.setElego(view.getElegoFlag() ? 1 : 0);
        body.setSortCol(view.getSortColumn());
        body.setSortDir(view.getSortDirection());
        body.setAreaId(view.getAreaId());
        body.setStateId(view.getStateId());
        return body;
    }

    private void onError(Throwable throwable) {
        view.showErrorMessage(throwable.getMessage());
        view.hideLoading();
    }

    private void onStart(Disposable disposable) throws Exception {
        view.showLoading();
    }

    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @POST("customer/get@filter@")
        Single<BaseResponse<List<Ads>>> getAllAds(@Body RequestAllAds body, @Header("Accept-Language") String language);

    }
}
