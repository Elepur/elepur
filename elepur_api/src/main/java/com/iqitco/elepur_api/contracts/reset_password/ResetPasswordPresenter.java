package com.iqitco.elepur_api.contracts.reset_password;

import android.text.TextUtils;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestResetPassword;
import com.iqitco.elepur_api.modules.respones.BaseResponse;
import com.iqitco.elepur_api.util.ValidationUtil;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date 6/5/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ResetPasswordPresenter implements ResetPasswordContract.Presenter {

    private final ResetPasswordContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public ResetPasswordPresenter(ResetPasswordContract.View view) {
        this.view = view;
    }

    @Override
    public void resetPassword() {
        RequestResetPassword body = new RequestResetPassword();
        body.setUsername(view.getEmail());
        boolean usernameFlag = TextUtils.isEmpty(body.getUsername());
        view.errorEmail(usernameFlag);

        if (!ValidationUtil.isValidEmail(body.getUsername())) {
            view.errorEmail(true);
            return;
        } else {
            view.errorEmail(false);
        }

        if (usernameFlag) {
            return;
        }

        RequestBaseInfo info = new RequestBaseInfo(view.getContext());

        compositeDisposable.add(
                api.resetPassword(body, info.getLanguage())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(d -> view.showLoading())
                        .subscribe(resp -> {
                            if (resp.isStatus()) {
                                view.onSuccess();
                            } else {
                                view.showUserMessage(resp.getMessage());
                            }
                            view.hideLoading();
                        }, throwable -> {
                            view.showErrorMessage(throwable.getMessage());
                            view.hideLoading();
                        })
        );

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @POST("login/forget/password")
        Single<BaseResponse<Boolean>> resetPassword(@Body RequestResetPassword body, @Header("Accept-Language") String language);

    }

}
