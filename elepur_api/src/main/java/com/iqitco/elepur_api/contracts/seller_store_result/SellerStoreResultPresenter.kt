package com.iqitco.elepur_api.contracts.seller_store_result

import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.modules.StoreResult
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * date 8/4/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class SellerStoreResultPresenter(private val view: SellerStoreResultContract.View) : SellerStoreResultContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)

    override fun onCreate() {
        compositeDisposable.add(
                api.getStoreResult(view.getId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishResult(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, {
                            view.showErrorMessage(it.message)
                            view.hideLoading()
                        })
        )
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @GET("seller/get@seller@status")
        fun getStoreResult(@Query("id") id: Int): Single<BaseResponse<StoreResult>>

    }
}