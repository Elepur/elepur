package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.database.AppDatabase;
import com.iqitco.elepur_api.modules.GreenLight;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * date 6/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class MainPagePresenter implements MainPageContract.Presenter {

    private final MainPageContract.View view;
    private final AppDatabase database;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public MainPagePresenter(MainPageContract.View view) {
        this.view = view;
        this.database = AppDatabase.getInstance(view.getContext());
    }

    @Override
    public void getGreenLight() {
        int rollId;
        if (view.getRole().equalsIgnoreCase("seller")) {
            rollId = 2;
        } else if (view.getRole().equalsIgnoreCase("customer")) {
            rollId = 1;
        } else {
            rollId = 0;
        }

        if (rollId > 0) {
            compositeDisposable.add(
                    api.getGreenLight(view.getUserId(), rollId)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(resp -> {
                                if (resp.isStatus()) {
                                    view.publishGreenLight(resp.getData());
                                } else {
                                    view.showUserMessage(resp.getMessage());
                                }
                            }, throwable -> view.showErrorMessage(throwable.getMessage()))
            );
        }
    }

    @Override
    public void onCreate() {
        compositeDisposable.addAll(
                database.getSlidesDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishSlides, this::onError),
                database.getMainPageSectionDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishMainSections, this::onError),
                database.getBrandPicDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishBrandPix, this::onError),
                database.getSecondAdvDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishSecondAdv, this::onError),
                database.getElegoMainDao().getAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishElegoMain, this::onError)
        );

        getGreenLight();
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private void onError(Throwable throwable) {
        view.showErrorMessage(throwable.getMessage());
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("customer/get@green@light")
        Single<BaseResponse<GreenLight>> getGreenLight(@Query("id") Integer id, @Query("roll") Integer roll);

    }
}
