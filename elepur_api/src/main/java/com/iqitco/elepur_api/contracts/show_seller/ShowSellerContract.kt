package com.iqitco.elepur_api.contracts.show_seller

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.MainPageSection
import com.iqitco.elepur_api.modules.RateResult
import com.iqitco.elepur_api.modules.SellerProduct
import com.iqitco.elepur_api.modules.SellerProfile

/**
 * date 7/27/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface ShowSellerContract {

    interface Presenter : BasePresenter {

        fun getSellerCategory(list: List<Int>)

        fun rateHappy()

        fun rateSad()

        fun favorite()

        fun insertCall()

        fun getAdv(pageNumber: Int)

    }

    interface View : BaseView {

        fun publishSellerProfile(profile: SellerProfile)

        fun publishSellerCategory(list: List<MainPageSection>)

        fun getSellerId(): Int

        fun getCustomerId(): Int

        fun publishRate(result: RateResult)

        fun onInsertCall(flag: Boolean)

        fun getCategoryId(): Int

        fun publishSellerAdv(list: List<SellerProduct>, pageNumber: Int)

    }

}