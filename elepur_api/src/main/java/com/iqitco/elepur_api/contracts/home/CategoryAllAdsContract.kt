package com.iqitco.elepur_api.contracts.home

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface CategoryAllAdsContract {

    interface Presenter : BasePresenter {
        fun getAllAds(pageNumber: Int)
        fun getAreas()
    }

    interface View : BaseView {

        fun publishAds(list: List<Ads>, pageNumber: Int)
        fun publishStates(list: List<State>)
        fun publishArea(list: List<StateArea>)
        fun getStateId(): Int
        fun getAreaId(): Int
        fun getSortColumn(): Int
        fun getSortDirection(): String
        fun getBrandsIds(): List<Int>
        fun getSubBrands(): List<Int>
        fun getColors(): List<Int>
        fun getKind(): List<Int>
        fun getNewOldFlag(): String?
        fun getElegoFlag(): Boolean
        fun getLowestPrice(): Int
        fun getHighestPrice(): Int
        fun getAdvDepartment(): Int

    }

}