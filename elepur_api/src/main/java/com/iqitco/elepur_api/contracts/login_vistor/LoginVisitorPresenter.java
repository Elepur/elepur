package com.iqitco.elepur_api.contracts.login_vistor;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestInsertVisitor;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * date: 2018-06-12
 *
 * @author Mohammad Al-Najjar
 */
public class LoginVisitorPresenter implements LoginVisitorContract.Presenter {

    private final LoginVisitorContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);

    public LoginVisitorPresenter(LoginVisitorContract.View view) {
        this.view = view;
    }

    @Override
    public void loginVisitor() {
        RequestBaseInfo info = new RequestBaseInfo(view.getContext());

        RequestInsertVisitor body = new RequestInsertVisitor();
        body.setMacAddress(info.getMacAddress());
        body.setNotificationId(view.getNotificationId());

        compositeDisposable.add(
                api.insertVisitor(info.getLanguage(), body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(d -> view.showLoading())
                        .subscribe(resp -> {
                            view.hideLoading();
                            if (resp.isStatus()) {
                                view.onSuccess(resp.getData());
                            } else {
                                view.showUserMessage(resp.getMessage());
                            }
                        }, throwable -> {
                            view.hideLoading();
                            view.showErrorMessage(throwable.getMessage());
                        })
        );
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {
        @Headers("Content-Type: application/json")
        @POST("customer/Insert@vistor")
        Single<BaseResponse<Boolean>> insertVisitor(@Header("Accept-Language") String language, @Body RequestInsertVisitor body);
    }
}
