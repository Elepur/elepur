package com.iqitco.elepur_api.contracts.update_customer_profile;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.CustomerProfile;

import java.util.Calendar;

/**
 * date: 2018-07-03
 *
 * @author Mohammad Al-Najjar
 */
public interface UpdateCustomerProfileContract {

    interface Presenter extends BasePresenter {

        void updateCustomerProfile();

    }

    interface View extends BaseView {

        Integer getId();

        String getBase64Pic();

        String getName();

        String getGender();

        Calendar getBirthDay();

        String getNotificationId();

        void publishCustomerProfile(CustomerProfile profile);

        void onUpdateSuccess();

    }

}
