package com.iqitco.elepur_api.contracts.home

import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.database.AppDatabase
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.request.RequestGetAllAdsByCategory
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class CategoryAllAdsPresenter(private val view: CategoryAllAdsContract.View) : CategoryAllAdsContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val database = AppDatabase.getInstance(view.context)
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)

    override fun getAllAds(pageNumber: Int) {
        val body = createRequestBody(pageNumber)

        compositeDisposable.addAll(
                api.getAllByCategory(body, body.lan!!)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::onStart)
                        .subscribe({
                            if (it.isStatus) {
                                view.publishAds(it.data, pageNumber)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun getAreas() {
        compositeDisposable.addAll(
                database.stateAreaDao.getAll(view.getStateId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishArea, this::onError)
        )
    }

    override fun onCreate() {
        val body = createRequestBody(1)

        compositeDisposable.addAll(
                database.stateDao.all!!
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishStates, this::onError),
                api.getAllByCategory(body, body.lan!!)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(this::onStart)
                        .subscribe({
                            if (it.isStatus) {
                                view.publishAds(it.data, 1)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private fun onError(throwable: Throwable) {
        view.showErrorMessage(throwable.message)
        view.hideLoading()
    }

    @Throws(Exception::class)
    private fun onStart(disposable: Disposable) {
        view.showLoading()
    }

    private fun createRequestBody(pageNumber: Int): RequestGetAllAdsByCategory {
        val info = RequestBaseInfo(view.context)
        val body = RequestGetAllAdsByCategory()
        body.advName = view.getSubBrands()
        body.advKind = view.getKind()
        body.color = view.getColors()
        body.brand = view.getBrandsIds()
        body.lan = info.language
        body.newOldFlag = view.getNewOldFlag()
        body.displayLength = Constant.PAGE_SIZE
        body.displayStart = pageNumber
        body.hieghestPrice = view.getHighestPrice()
        body.lowestPrice = view.getLowestPrice()
        body.advDepartment = view.getAdvDepartment()
        body.pyment = 0
        body.elego = if (view.getElegoFlag()) 1 else 0
        body.sortCol = view.getSortColumn()
        body.sortDir = view.getSortDirection()
        body.areaId = view.getAreaId()
        body.stateId = view.getStateId()
        return body
    }

    private interface Api {
        @Headers("Content-Type: application/json")
        @POST("customer/get@sps@adds")
        fun getAllByCategory(@Body body: RequestGetAllAdsByCategory, @Header("Accept-Language") language: String): Single<BaseResponse<List<Ads>>>

    }

}