package com.iqitco.elepur_api.contracts.elego_address

import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.database.AppDatabase
import com.iqitco.elepur_api.modules.CustomerAddress
import com.iqitco.elepur_api.modules.request.RequestAddCustomerLocation
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.request.RequestDeleteCustomerAddress
import com.iqitco.elepur_api.modules.request.RequestUpdateCustomerLocation
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.*

/**
 * date 7/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ElegoAddressPresenter(private val view: ElegoAddressContract.View) : ElegoAddressContract.Presenter {

    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)
    private val compositeDisposable = CompositeDisposable()
    private val database: AppDatabase = AppDatabase.getInstance(view.context)

    override fun getStatusAreas() {
        compositeDisposable.addAll(
                database.stateAreaDao.getAll(view.getStateId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishArea, this::onError)
        )
    }

    override fun updateCustomerLocation() {
        val body = RequestUpdateCustomerLocation()
        body.addressId = view.getAddressId()
        body.customerId = view.getId()
        body.stateId = view.getStateId()
        body.areaId = view.getAreaId()
        body.name = view.getName()
        body.addressDetails = view.getAddressDescription()
        body.phone = view.getMobile()
        body.latitude = view.getLatitude()
        body.longitude = view.getLongitude()

        compositeDisposable.add(
                api.updateCustomerAddress(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.onUpdateSuccess()
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun deleteAddress() {
        val body = RequestDeleteCustomerAddress()
        body.customerId = view.getId()
        body.addressId = view.getAddressId()
        compositeDisposable.add(
                api.deleteCustomerAddress(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.onDeleteSuccess()
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun getCustomerLocations() {
        val info = RequestBaseInfo(view.context)
        compositeDisposable.add(
                api.getCustomerAddresses(view.getId(), info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishAddresses(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun addCustomerAddress() {
        val body = RequestAddCustomerLocation()
        body.customerId = view.getId()
        body.stateId = view.getStateId()
        body.areaId = view.getAreaId()
        body.name = view.getName()
        body.addressDetails = view.getAddressDescription()
        body.phone = view.getMobile()
        body.latitude = view.getLatitude()
        body.longitude = view.getLongitude()

        compositeDisposable.add(
                api.addCustomerAddress(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.onAddSuccess()
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun onCreate() {
        val info = RequestBaseInfo(view.context)
        compositeDisposable.addAll(
                api.getCustomerAddresses(view.getId(), info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishAddresses(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError),
                database.stateDao.all
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishStatus, this::onError)
        )
    }

    private fun onError(consumer: Throwable) {
        view.showErrorMessage(consumer.message)
        view.hideLoading()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("customer/get@customer@location@elego")
        fun getCustomerAddresses(
                @Query("id_customer") id: Int,
                @Query("lan") language: String
        ): Single<BaseResponse<List<CustomerAddress>>>

        @Headers("Content-Type: application/json")
        @POST("customer/add@customer@location")
        fun addCustomerAddress(@Body body: RequestAddCustomerLocation): Single<BaseResponse<Boolean>>

        @Headers("Content-Type: application/json")
        @POST("customer/delete@customer@location")
        fun deleteCustomerAddress(@Body body: RequestDeleteCustomerAddress): Single<BaseResponse<Boolean>>

        @Headers("Content-Type: application/json")
        @POST("customer/update@customer@location")
        fun updateCustomerAddress(@Body body: RequestUpdateCustomerLocation): Single<BaseResponse<Boolean>>
    }
}