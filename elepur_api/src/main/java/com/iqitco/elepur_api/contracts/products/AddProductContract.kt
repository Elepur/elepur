package com.iqitco.elepur_api.contracts.products

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.*
import com.iqitco.elepur_api.modules.request.RequestAddAdv

/**
 * date 8/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface AddProductContract {

    interface Presenter : BasePresenter{
        fun getGuaranteeList()
        fun getKind()
        fun getBrands()
        fun getSubBrands()
        fun addAdv()
    }

    interface View : BaseView {
        fun publishColors(list: List<Color>)
        fun publishFreeGifts(list: List<FreeGifts>)
        fun publishGuaranteeType(list: List<GuaranteeType>)
        fun publishPaymentTypes(list: List<PaymentsType>)
        fun publishGuarantee(list: List<Guarantee>)
        fun publishKind(list: List<ProductKind>)
        fun publishSubBrands(list: List<SubBrand>)
        fun publishCategories(list: List<MainPageSection>)
        fun publishBrands(list: List<Brand>)

        fun getGuaranteeTypeId(): Int
        fun getCategoryId(): Int
        fun getBrandId(): Int
        fun getRequestAddAdv(): RequestAddAdv

        fun onAddSuccess(flag: Boolean)
    }

}