package com.iqitco.elepur_api.contracts.notification

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView

/**
 * date: 2018-08-02
 *
 * @author Mohammad Al-Najjar
 */
interface ShowNotificationContract {

    interface Presenter : BasePresenter {

        fun deleteNotification()

        fun updateSeenNotification()
    }

    interface View : BaseView {

        fun getNotificationId(): Int

        fun getUserId(): Int

        fun getRole(): String?

        fun onDeleteSuccess(flag: Boolean)

        fun onUpdateSuccess(flag: Boolean)
    }

}