package com.iqitco.elepur_api.contracts.main_search

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea

/**
 * date 8/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface MainSearchContract {

    interface Presenter : BasePresenter {

        fun getUserSearchChoice()

        fun getSearchChoice()

        fun deleteUserSearchChoice()

        fun getUserFilter(pageNumber: Int)

        fun getAreas()
    }

    interface View : BaseView {

        fun getChoice(): String

        fun getId(): Int

        fun getCustomerId(): Int

        fun getStateId(): Int

        fun getAreaId(): Int

        fun publishChoice(words: List<String>)

        fun publishUserChoice(list: List<String>)

        fun onDeleteChoiceSuccess(flag: Boolean)

        fun publishStates(list: List<State>)

        fun publishArea(list: List<StateArea>)

        fun getSortColumn(): Int

        fun getSortDirection(): String

        fun getBrandsIds(): List<Int>

        fun getSubBrands(): List<Int>

        fun getColors(): List<Int>

        fun getKind(): List<Int>

        fun getNewOldFlag(): String?

        fun getElegoFlag(): Boolean

        fun getLowestPrice(): Int

        fun getHighestPrice(): Int

        fun showFilterLoading()

        fun hideFilterLoading()

        fun publishAds(list: List<Ads>, pageNumber: Int)

    }

}