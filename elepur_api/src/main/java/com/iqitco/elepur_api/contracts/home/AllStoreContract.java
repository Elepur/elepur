package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.Seller;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;

import java.util.List;

/**
 * date 6/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface AllStoreContract {

    interface Presenter extends BasePresenter {

        void getAllSellers(int pageNumber);

        void getAreas();

    }

    interface View extends BaseView {

        void publishAllSellers(List<Seller> list, int pageNumber);

        void publishState(List<State> list);

        void publishArea(List<StateArea> list);

        Integer getStateId();

        Integer getAreaId();

        String getSearchStr();

    }

}
