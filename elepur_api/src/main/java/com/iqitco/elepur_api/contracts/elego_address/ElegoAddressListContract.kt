package com.iqitco.elepur_api.contracts.elego_address

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.CustomerAddress

/**
 * date 7/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface ElegoAddressListContract {

    interface Presenter : BasePresenter {

    }

    interface View : BaseView {
        fun publishAddresses(list: List<CustomerAddress>)

        fun getId(): Int
    }

}