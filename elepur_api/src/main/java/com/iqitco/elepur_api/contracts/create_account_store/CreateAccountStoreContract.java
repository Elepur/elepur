package com.iqitco.elepur_api.contracts.create_account_store;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;
import com.iqitco.elepur_api.modules.UserData;

import java.util.Calendar;
import java.util.List;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface CreateAccountStoreContract {

    interface Presenter extends BasePresenter {

        void getStateArea();

        void registration();
    }

    interface View extends BaseView {

        void publishState(List<State> list);

        void publishArea(List<StateArea> list);

        void publishProfessions(List<MainPageSection> list);

        Integer getStateId();

        void errorStateId(boolean flag);

        Integer getAreaId();

        void errorAreaId(boolean flag);

        String getShopNameAr();

        void errorShopNameAr(boolean flag);

        String getShopNameEn();

        void errorShopNameEn(boolean flag);

        String getOwnerName();

        void errorOwnerName(boolean flag);

        String getEmail();

        void errorEmail(boolean flag);

        String getPassword();

        void errorPassword(boolean flag);

        String getTelephone();

        void errorTelephone(boolean flag);

        String getLocation();

        void errorLocation(boolean flag);

        String getDescriptionAr();

        void errorDescriptionAr(boolean flag);

        String getDescriptionEn();

        void errorDescriptionEn(boolean flag);

        String getLogoBase64();

        void errorLogo(boolean flag);

        String getCoverBase64();

        void errorCover(boolean flag);

        String getLicenseBase64();

        void errorLicense(boolean flag);

        void showNotValidEmail(boolean flag);

        void showNotValidPassword(boolean flag);

        List<Integer> getCategories();

        void errorCategories(boolean flag);

        String getLatitude();

        String getLongitude();

        Calendar getOpenTime();

        void errorOpenTime(boolean flag);

        Calendar getCloseTime();

        void errorCloseTime(boolean flag);

        boolean getDeliveryCheck();

        void publishSellerData(UserData body);

        String getNotificationId();
    }

}
