package com.iqitco.elepur_api.contracts.store_products_status

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.StoreProductStatus

/**
 * date 8/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface StoreProductsStatusContract {


    interface Presenter : BasePresenter {
        fun getSellerProducts(pageNumber: Int)

        fun removeFromStore(advId: Int)

        fun removeFromElepur(advId: Int)
    }

    interface View : BaseView {

        fun getId(): Int

        fun getSearchStr(): String

        fun publishProducts(list: List<StoreProductStatus>, pageNumber: Int)

        fun onRemoveProductSuccess(check: Boolean)

    }

}