package com.iqitco.elepur_api.contracts.share_opinion;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface ShareOpinionContract {

    interface Presenter extends BasePresenter{

        void insertMessage();

    }

    interface View extends BaseView {

        String getMessage();

        String getHeader();

        Integer getLoginId();

        void onSuccess(boolean flag);

    }

}
