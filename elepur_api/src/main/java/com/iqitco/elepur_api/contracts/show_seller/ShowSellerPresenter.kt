package com.iqitco.elepur_api.contracts.show_seller

import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.database.AppDatabase
import com.iqitco.elepur_api.modules.RateResult
import com.iqitco.elepur_api.modules.SellerProduct
import com.iqitco.elepur_api.modules.SellerProfile
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.request.RequestRateSeller
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.*

/**
 * date 7/27/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ShowSellerPresenter(private val view: ShowSellerContract.View) : ShowSellerContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)
    private val database = AppDatabase.getInstance(view.context)

    override fun getAdv(pageNumber: Int) {
        val info = RequestBaseInfo(view.context)
        compositeDisposable.add(
                api.getSellerAdv(
                        view.getSellerId(),
                        view.getCategoryId(),
                        pageNumber,
                        Constant.PAGE_SIZE,
                        info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishSellerAdv(it.data, pageNumber)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun rateHappy() {
        val body = RequestRateSeller()
        body.sellerId = view.getSellerId()
        body.customerId = view.getCustomerId()
        compositeDisposable.add(
                api.rateHappy(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishRate(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun rateSad() {
        val body = RequestRateSeller()
        body.sellerId = view.getSellerId()
        body.customerId = view.getCustomerId()
        compositeDisposable.add(
                api.rateSad(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishRate(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun favorite() {
        val body = RequestRateSeller()
        body.sellerId = view.getSellerId()
        body.customerId = view.getCustomerId()
        compositeDisposable.add(
                api.favorite(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishRate(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun insertCall() {
        compositeDisposable.add(
                api.insertCallCustomer(view.getCustomerId(), view.getSellerId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.onInsertCall(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getSellerCategory(list: List<Int>) {
        compositeDisposable.add(
                database.mainPageSectionDao.getByIds(list)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishSellerCategory) {
                            view.showErrorMessage(it.message)
                        }
        )
    }

    override fun onCreate() {
        val info = RequestBaseInfo(view.context)

        compositeDisposable.addAll(
                api.getSellerProfile(
                        view.getSellerId(),
                        view.getCustomerId(),
                        info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishSellerProfile(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    private fun onError(it: Throwable) {
        view.showErrorMessage(it.message)
        view.hideLoading()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("seller/get@adv@seller@pr")
        fun getSellerAdv(
                @Query("id_s") sellerId: Int,
                @Query("cat_id") categoryId: Int,
                @Query("page") page: Int,
                @Query("size") size: Int,
                @Query("lan") language: String
        ): Single<BaseResponse<List<SellerProduct>>>

        @Headers("Content-Type: application/json")
        @POST("customer/Insert@calls")
        fun insertCallCustomer(@Query("c_id") customerId: Int, @Query("s_id") sellerId: Int): Single<BaseResponse<Boolean>>

        @Headers("Content-Type: application/json")
        @POST("customer/add@fav@seller")
        fun favorite(@Body body: RequestRateSeller): Single<BaseResponse<RateResult>>

        @Headers("Content-Type: application/json")
        @PUT("customer/rate@seller@sad")
        fun rateSad(@Body body: RequestRateSeller): Single<BaseResponse<RateResult>>

        @Headers("Content-Type: application/json")
        @PUT("customer/rate@seller@happy")
        fun rateHappy(@Body body: RequestRateSeller): Single<BaseResponse<RateResult>>

        @Headers("Content-Type: application/json")
        @GET("seller/get@profile")
        fun getSellerProfile(
                @Query("id") sellerId: Int,
                @Query("id_c") customerId: Int,
                @Query("lan") language: String
        ): Single<BaseResponse<SellerProfile>>

    }
}