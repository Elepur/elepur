package com.iqitco.elepur_api.contracts.login;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.UserData;

/**
 * date 6/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface LoginContract {

    interface Presenter extends BasePresenter {

        void login();

        void loginFacebook();
    }

    interface View extends BaseView {

        String getEmail();

        void errorEmail(boolean flag);

        String getPassword();

        void errorPassword(boolean flag);

        String getNotificationId();

        String getName();

        String getGender();

        String getFacebookId();

        String getPic();

        void publishData(UserData body);

    }

}
