package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.BrandPic;
import com.iqitco.elepur_api.modules.ElegoMain;
import com.iqitco.elepur_api.modules.GreenLight;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.SecondAdv;
import com.iqitco.elepur_api.modules.Slides;

import java.util.List;

/**
 * date 6/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface MainPageContract {

    interface Presenter extends BasePresenter {

        void getGreenLight();

    }

    interface View extends BaseView {

        void publishSlides(List<Slides> list);

        void publishMainSections(List<MainPageSection> list);

        void publishBrandPix(List<BrandPic> list);

        void publishSecondAdv(List<SecondAdv> list);

        void publishElegoMain(List<ElegoMain> list);

        void publishGreenLight(GreenLight greenLight);

        String getRole();

        Integer getUserId();

    }

}
