package com.iqitco.elepur_api.contracts.home;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.Brand;
import com.iqitco.elepur_api.modules.Color;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.ProductKind;
import com.iqitco.elepur_api.modules.SubBrand;

import java.util.List;

/**
 * date 6/26/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface AllAdsFilterContract {

    interface Presenter extends BasePresenter {

        void getSubBrand();

        void getProductKind();

    }

    interface View extends BaseView {

        List<Integer> getBrands();

        Integer getCategories();

        void publishBrands(List<Brand> list);

        void publishColors(List<Color> list);

        void publishCategories(List<MainPageSection> list);

        void publishSubBrand(List<SubBrand> list);

        void publishProductKind(List<ProductKind> list);

    }

}
