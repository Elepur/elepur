package com.iqitco.elepur_api.contracts.products

import com.iqitco.elepur_api.data.CommandDataManager
import com.iqitco.elepur_api.data.QueryDataManager
import io.reactivex.disposables.CompositeDisposable

/**
 * date 8/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class AddProductPresenter(private val view: AddProductContract.View) : AddProductContract.Presenter {

    private val queryDataManager = QueryDataManager(view.context)
    private val commandDataManager = CommandDataManager(view.context)
    private val compositeDisposable = CompositeDisposable()

    override fun addAdv() {
        compositeDisposable.add(
                commandDataManager.addAdv(view.getRequestAddAdv())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus){
                                view.onAddSuccess(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun getGuaranteeList() {
        compositeDisposable.add(
                queryDataManager.getGuaranteeList(view.getGuaranteeTypeId())
                        .subscribe({
                            if (it.isStatus) {
                                view.publishGuarantee(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getKind() {
        compositeDisposable.add(
                queryDataManager.getKind(view.getCategoryId())
                        .subscribe({
                            if (it.isStatus) {
                                view.publishKind(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getSubBrands() {
        compositeDisposable.add(
                queryDataManager.getSubBrands(view.getCategoryId(), view.getBrandId())
                        .subscribe({
                            if (it.isStatus) {
                                view.publishSubBrands(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getBrands() {
        compositeDisposable.add(queryDataManager.getBrands(view.getCategoryId()).subscribe(view::publishBrands, this::onError))
    }

    override fun onCreate() {
        compositeDisposable.addAll(
                queryDataManager.getColors().subscribe({
                    if (it.isStatus) {
                        view.publishColors(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getFreeGifts().subscribe({
                    if (it.isStatus) {
                        view.publishFreeGifts(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getGuaranteeTypeList().subscribe({
                    if (it.isStatus) {
                        view.publishGuaranteeType(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getPaymentsTypes().subscribe({
                    if (it.isStatus) {
                        view.publishPaymentTypes(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                }, this::onError),
                queryDataManager.getCategories().subscribe(view::publishCategories, this::onError)
        )
    }

    private fun onError(throwable: Throwable) {
        view.showErrorMessage(throwable.message)
        view.hideLoading()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }
}