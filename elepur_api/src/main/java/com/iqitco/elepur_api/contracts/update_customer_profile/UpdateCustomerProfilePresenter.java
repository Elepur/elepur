package com.iqitco.elepur_api.contracts.update_customer_profile;

import com.iqitco.elepur_api.data.RetrofitConnectionFactory;
import com.iqitco.elepur_api.modules.CustomerProfile;
import com.iqitco.elepur_api.modules.request.RequestBaseInfo;
import com.iqitco.elepur_api.modules.request.RequestUpdateCustomerProfile;
import com.iqitco.elepur_api.modules.respones.BaseResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * date: 2018-07-03
 *
 * @author Mohammad Al-Najjar
 */
public class UpdateCustomerProfilePresenter implements UpdateCustomerProfileContract.Presenter {

    private final UpdateCustomerProfileContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Api api = RetrofitConnectionFactory.getRetrofit().create(Api.class);
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

    public UpdateCustomerProfilePresenter(UpdateCustomerProfileContract.View view) {
        this.view = view;
    }

    @Override
    public void updateCustomerProfile() {
        RequestBaseInfo info = new RequestBaseInfo(view.getContext());
        RequestUpdateCustomerProfile body = new RequestUpdateCustomerProfile();
        body.setIdCustomer(view.getId());
        body.setPic(view.getBase64Pic());
        body.setName(view.getName());
        body.setMacAddress(info.getMacAddress());
        body.setNotificationId(view.getNotificationId());
        Calendar birthday = view.getBirthDay();
        body.setAge(Calendar.getInstance().get(Calendar.YEAR) - birthday.get(Calendar.YEAR));
        body.setBirth(simpleDateFormat.format(birthday.getTime()));
        body.setGender(view.getGender());

        compositeDisposable.add(
                api.updateCustomerProfile(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(v -> view.showLoading())
                        .subscribe(respBody -> {
                            view.hideLoading();
                            if (respBody.isStatus()) {
                                view.onUpdateSuccess();
                            } else {
                                view.showUserMessage(respBody.getMessage());
                            }
                        }, throwable -> {
                            view.hideLoading();
                            view.showErrorMessage(throwable.getMessage());
                        })
        );

    }

    @Override
    public void onCreate() {
        compositeDisposable.add(
                api.getCustomerProfile(view.getId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(v -> view.showLoading())
                        .subscribe(body -> {
                            if (body.isStatus()) {
                                view.publishCustomerProfile(body.getData());
                            } else {
                                view.showUserMessage(body.getMessage());
                            }
                            view.hideLoading();
                        }, throwable -> {
                            view.hideLoading();
                            view.showErrorMessage(throwable.getMessage());
                        })
        );
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("customer/get@customer@prfile")
        Single<BaseResponse<CustomerProfile>> getCustomerProfile(@Query("id") Integer id);

        @Headers("Content-Type: application/json")
        @PUT("customer/update@profile")
        Single<BaseResponse<Boolean>> updateCustomerProfile(@Body RequestUpdateCustomerProfile body);

    }

}
