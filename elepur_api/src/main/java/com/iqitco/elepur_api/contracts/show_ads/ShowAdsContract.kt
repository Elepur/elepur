package com.iqitco.elepur_api.contracts.show_ads

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.FullProduct
import com.iqitco.elepur_api.modules.RateResult

/**
 * date 7/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface ShowAdsContract {

    interface Presenter : BasePresenter {

        fun insertCallCustomer()

        fun rateAdvHappy()

        fun rateAdvSad()

        fun rateAdvFav()

    }

    interface View : BaseView {

        fun publishProduct(product: FullProduct)

        fun publishSellerProduct(list: List<Ads>)

        fun publishRate(result: RateResult)

        fun onReportSuccess()

        fun getAdvId(): Int

        fun getCustomerId(): Int

        fun getSellerId(): Int

        fun onInsertCustomerSuccess(result: Boolean)

        fun getReportMessage(): String

        fun getReportHeader(): String

    }

}