package com.iqitco.elepur_api.contracts.notification;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;
import com.iqitco.elepur_api.modules.UserNotification;

import java.util.List;

/**
 * date: 2018-07-02
 *
 * @author Mohammad Al-Najjar
 */
public interface NotificationContract {

    interface Presenter extends BasePresenter {

        void getNotification(int pageNumber);

    }

    interface View extends BaseView {

        Integer getId();

        void publishNotification(List<UserNotification> list, int pageNumber);

        String getRole();

    }

}
