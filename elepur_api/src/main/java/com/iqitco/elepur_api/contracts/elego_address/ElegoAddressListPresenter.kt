package com.iqitco.elepur_api.contracts.elego_address

import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.modules.CustomerAddress
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

/**
 * date 7/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ElegoAddressListPresenter(private val view: ElegoAddressListContract.View) : ElegoAddressListContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)

    override fun onCreate() {
        val info = RequestBaseInfo(view.context)
        compositeDisposable.add(
                api.getCustomerAddresses(view.getId(), info.language)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishAddresses(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    private fun onError(consumer: Throwable) {
        view.showErrorMessage(consumer.message)
        view.hideLoading()
    }


    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @Headers("Content-Type: application/json")
        @GET("customer/get@customer@location@elego")
        fun getCustomerAddresses(
                @Query("id_customer") id: Int,
                @Query("lan") language: String
        ): Single<BaseResponse<List<CustomerAddress>>>
    }
}