package com.iqitco.elepur_api.contracts.reset_password;

import com.iqitco.elepur_api.BasePresenter;
import com.iqitco.elepur_api.BaseView;

/**
 * date 6/5/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface ResetPasswordContract {

    interface Presenter extends BasePresenter {

        void resetPassword();

    }

    interface View extends BaseView {

        String getEmail();

        void errorEmail(boolean flag);

        void onSuccess();

    }

}
