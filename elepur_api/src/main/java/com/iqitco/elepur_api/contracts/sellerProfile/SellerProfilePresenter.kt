package com.iqitco.elepur_api.contracts.sellerProfile

import com.iqitco.elepur_api.data.QueryDataManager
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import io.reactivex.disposables.CompositeDisposable

/**
 * date 9/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class SellerProfilePresenter(private val view: SellerProfileContract.View) : SellerProfileContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val queryDataManager = QueryDataManager(view.context)
    private val info = RequestBaseInfo(view.context)

    override fun onCreate() {
        compositeDisposable.add(
                queryDataManager.getProfileForSeller(view.getSellerId(), info.language)
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishSellerProfile(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun getAdv(pageNumber: Int) {
        compositeDisposable.add(
                queryDataManager.getSellerAdv(view.getSellerId(), view.getCategoryId(), pageNumber, info.language)
                        .subscribe({
                            if (it.isStatus) {
                                view.publishSellerAdv(it.data, pageNumber)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun getSellerCategories(idsList: List<Int>) {
        compositeDisposable.add(
                queryDataManager.getSellerCategories(idsList)
                        .subscribe(view::publishSellerCategory, this::onError)
        )
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private fun onError(throwable: Throwable) {
        view.showErrorMessage(throwable.message)
        view.hideLoading()
    }
}