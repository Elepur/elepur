package com.iqitco.elepur_api.contracts.main_search

import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.database.AppDatabase
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.request.RequestAllAds
import com.iqitco.elepur_api.modules.request.RequestBaseInfo
import com.iqitco.elepur_api.modules.request.RequestDeleteUserChoice
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * date 8/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class MainSearchPresenter(private val view: MainSearchContract.View) : MainSearchContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)
    private val database = AppDatabase.getInstance(view.context)

    override fun getSearchChoice() {
        if (view.getChoice().isEmpty()) {
            view.publishChoice(listOf())
            return
        }
        compositeDisposable.add(
                api.getSearchChoice(view.getChoice())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            if (it.isStatus) {
                                view.publishChoice(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, this::onError)
        )
    }

    override fun getAreas() {
        compositeDisposable.addAll(
                database.stateAreaDao.getAll(view.getStateId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishArea, this::onError)
        )
    }

    override fun deleteUserSearchChoice() {
        val body = RequestDeleteUserChoice()
        body.userId = view.getId()
        compositeDisposable.add(
                api.deleteUserSearchChoice(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.onDeleteChoiceSuccess(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError)
        )
    }

    override fun getUserSearchChoice() {
        api.getUserSearchChoice(view.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { view.showLoading() }
                .subscribe({
                    if (it.isStatus) {
                        view.publishUserChoice(it.data)
                    } else {
                        view.showUserMessage(it.message)
                    }
                    view.hideLoading()
                }, this::onError)
    }

    override fun getUserFilter(pageNumber: Int) {
        val info = RequestBaseInfo(view.context)
        val body = RequestAllAds()

        body.advKind = view.getKind()
        body.color = view.getColors()
        body.brand = view.getBrandsIds()
        body.lan = info.language
        body.no = view.getNewOldFlag()
        body.displayLength = Constant.PAGE_SIZE
        body.displayStart = pageNumber
        body.hieghestPrice = view.getHighestPrice()
        body.lowestPrice = view.getLowestPrice()
        body.advDepartment = 0
        body.pyment = 0
        body.elego = if (view.getElegoFlag()) 1 else 0
        body.sortCol = view.getSortColumn()
        body.sortDir = view.getSortDirection()
        body.areaId = view.getAreaId()
        body.stateId = view.getStateId()
        body.search = view.getChoice()
        body.customerId = view.getCustomerId()

        compositeDisposable.add(
                api.getUserFilter(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showFilterLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishAds(it.data, pageNumber)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideFilterLoading()
                        }, {
                            view.hideFilterLoading()
                            view.showErrorMessage(it.message)
                        })
        )
    }

    private fun onError(throwable: Throwable) {
        view.showErrorMessage(throwable.message)
        view.hideLoading()
    }

    override fun onCreate() {
        compositeDisposable.addAll(
                api.getUserSearchChoice(view.getId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            if (it.isStatus) {
                                view.publishUserChoice(it.data)
                            } else {
                                view.showUserMessage(it.message)
                            }
                            view.hideLoading()
                        }, this::onError),
                database.stateDao.all!!
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(view::publishStates, this::onError)
        )
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {

        @GET("customer/get@search@choice")
        fun getSearchChoice(@Query("choice") choice: String): Single<BaseResponse<List<String>>>

        @GET("customer/get@search@name@customer")
        fun getUserSearchChoice(@Query("id") userId: Int): Single<BaseResponse<List<String>>>

        @POST("customer/Delete@search@customer")
        fun deleteUserSearchChoice(@Body body: RequestDeleteUserChoice): Single<BaseResponse<Boolean>>

        @POST("customer/get@filter@search@adv")
        fun getUserFilter(@Body body: RequestAllAds): Single<BaseResponse<List<Ads>>>

    }
}