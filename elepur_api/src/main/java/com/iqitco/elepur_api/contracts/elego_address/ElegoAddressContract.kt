package com.iqitco.elepur_api.contracts.elego_address

import com.iqitco.elepur_api.BasePresenter
import com.iqitco.elepur_api.BaseView
import com.iqitco.elepur_api.modules.CustomerAddress
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea

/**
 * date 7/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
interface ElegoAddressContract {

    interface Presenter : BasePresenter {

        fun getStatusAreas()

        fun addCustomerAddress()

        fun getCustomerLocations()

        fun deleteAddress()

        fun updateCustomerLocation()
    }

    interface View : BaseView {

        fun publishStatus(list: List<State>)

        fun publishArea(list: List<StateArea>)

        fun publishAddresses(list: List<CustomerAddress>)

        fun getId(): Int

        fun getStateId(): Int

        fun getAreaId(): Int

        fun getName(): String

        fun getMobile(): String

        fun getAddressDescription(): String

        fun onAddSuccess()

        fun onDeleteSuccess()

        fun onUpdateSuccess()

        fun getLatitude(): String

        fun getLongitude(): String

        fun getAddressId(): Int?

    }

}