package com.iqitco.elepur_api.contracts.reports_adv

import com.iqitco.elepur_api.data.RetrofitConnectionFactory
import com.iqitco.elepur_api.modules.request.RequestReportAdv
import com.iqitco.elepur_api.modules.respones.BaseResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * date 7/22/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ReportsAdvPresenter(private val view: ReportsAdvContract.View) : ReportsAdvContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val api = RetrofitConnectionFactory.getRetrofit().create(Api::class.java)


    override fun reportAdv() {
        val body = RequestReportAdv()
        body.customerId = view.getCustomerId()
        body.advId = view.getAdvId()
        body.message = view.getMessage()
        body.header = view.getHeader()

        compositeDisposable.add(
                api.reportAdv(body)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { view.showLoading() }
                        .subscribe({
                            view.hideLoading()
                            if (it.isStatus) {
                                view.onReportSuccess()
                            } else {
                                view.showUserMessage(it.message)
                            }
                        }, {
                            view.showErrorMessage(it.message)
                            view.hideLoading()
                        })
        )
    }

    override fun onCreate() {

    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    private interface Api {
        @Headers("Content-Type: application/json")
        @POST("customer/add@report@adv")
        fun reportAdv(@Body body: RequestReportAdv): Single<BaseResponse<Int?>>
    }
}