package com.iqitco.elepur_api.services;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iqitco.elepur_api.BuildConfig;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.modules.respones.BaseResponse;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * date 6/7/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CheckBlockService extends JobIntentService {

    private static final String TAG = CheckBlockService.class.getSimpleName();
    private static final int JOB_ID = 1542;

    /**
     * Convenience method for enqueuing work in to this service.
     */
    static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, CheckBlockService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.d(TAG, "start");
        try {
            SecurePrefManager securePrefManager = SecurePrefManager.with(this);
            String email = securePrefManager.get(Constant.EMAIL).defaultValue("").go();
            if (email.isEmpty()) {
                Log.d(TAG, "is block user: no user");
                return;
            }

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\"username\": \"" + email + "\"}");
            Request request = new Request.Builder()
                    .url(BuildConfig.API_LINK + "login/check/block")
                    .post(body)
                    .addHeader("Accept-Language", Locale.getDefault().toString())
                    .addHeader("Content-Type", "application/json")
                    .build();

            Response response = client.newCall(request).execute();
            Gson gson = new GsonBuilder().create();

            BaseResponse<Integer> resp = gson.fromJson(response.body().string(), new TypeToken<BaseResponse<Integer>>() {
            }.getType());

            if (resp.isStatus()) {
                securePrefManager.set(Constant.BLOCK).value(resp.getData()).go();
                Log.d(TAG, "is block user: " + String.valueOf(resp.getData()));
            }

        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }
        Log.d(TAG, "finish");
    }
}
