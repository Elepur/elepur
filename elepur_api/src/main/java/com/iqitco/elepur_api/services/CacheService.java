package com.iqitco.elepur_api.services;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iqitco.elepur_api.BuildConfig;
import com.iqitco.elepur_api.database.AppDatabase;
import com.iqitco.elepur_api.modules.Brand;
import com.iqitco.elepur_api.modules.BrandPic;
import com.iqitco.elepur_api.modules.ElegoMain;
import com.iqitco.elepur_api.modules.ElegoSecondAdv;
import com.iqitco.elepur_api.modules.LandingPageBackground;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.PopAdv;
import com.iqitco.elepur_api.modules.SecondAdv;
import com.iqitco.elepur_api.modules.Slides;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;
import com.iqitco.elepur_api.modules.respones.BaseResponse;
import com.iqitco.elepur_api.modules.respones.ResponseMainCache;

import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CacheService extends JobIntentService {

    private static final String TAG = CacheService.class.getSimpleName();


    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.d(TAG, "start");
        try {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(BuildConfig.API_LINK + "customer/get@main@cashe?date=")
                    .get()
                    .addHeader("Accept-Language", Locale.getDefault().toString())
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Postman-Token", "0417f0de-c8b6-4ff4-a330-fb361a8c6f72")
                    .build();

            Response response = client.newCall(request).execute();
            Gson gson = new GsonBuilder().create();
            BaseResponse<ResponseMainCache> resp = gson.fromJson(response.body().string(), new TypeToken<BaseResponse<ResponseMainCache>>() {
            }.getType());

            if (resp.isStatus()) {
                cacheData(resp.getData());
                Log.d(TAG, "done caching");
            }

        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }
        Log.d(TAG, "finish");
    }

    private void cacheData(ResponseMainCache data) {
        AppDatabase database = AppDatabase.getInstance(this);
        database.getBrandDao().deleteAll();
        database.getMainPageSectionDao().deleteAll();
        database.getBrandPicDao().deleteAll();
        database.getPopAdvDao().deleteAll();
        database.getSlidesDao().deleteAll();
        database.getStateAreaDao().deleteAll();
        database.getStateDao().deleteAll();
        database.getElegoMainDao().deleteAll();
        database.getLandingPageBackgroundDao().deleteAll();
        database.getSecondAdvDao().deleteAll();

        ElegoSecondAdv adv = data.getElegoSecondAdv().get(0);

        List<ElegoMain> elegoMainList = adv.getElegoMain();
        for (int i = 0; i < elegoMainList.size(); i++) {
            ElegoMain elegoMain = elegoMainList.get(i);
            elegoMain.setId(i + 1);
            database.getElegoMainDao().insert(elegoMain);
        }

        List<LandingPageBackground> backgroundList = adv.getLandingPageBackground();
        for (int i = 0; i < backgroundList.size(); i++) {
            LandingPageBackground background = backgroundList.get(i);
            background.setId(i + 1);
            database.getLandingPageBackgroundDao().insert(background);
        }

        List<SecondAdv> secondAdvList = adv.getSecondAdv();
        for (int i = 0; i < secondAdvList.size(); i++) {
            SecondAdv secondAdv = secondAdvList.get(i);
            secondAdv.setId(i + 1);
            database.getSecondAdvDao().insert(secondAdv);
        }

        for (MainPageSection section : data.getListMainPageSection()) {
            database.getMainPageSectionDao().insert(section);
            for (Brand brand : section.getBrands()) {
                brand.setMainPageSectionId(section.getId());
                database.getBrandDao().insert(brand);
            }
        }

        for (BrandPic brandPic : data.getBrandPic()) {
            database.getBrandPicDao().insert(brandPic);
        }

        for (Slides slides : data.getListSlides()) {
            database.getSlidesDao().insert(slides);
        }

        for (PopAdv popAdv : data.getPopAdv()) {
            database.getPopAdvDao().insert(popAdv);
        }

        for (State state : data.getState()) {
            database.getStateDao().insert(state);
            for (StateArea area : state.getAreaList()) {
                area.setStateId(state.getId());
                database.getStateAreaDao().insert(area);
            }
        }


    }

}
