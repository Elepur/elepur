package com.iqitco.elepur_api;

/**
 * date 5/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface Constant {

    String ID_LOGIN = "idLogin";
    String ROLL = "roll";
    String ID_ID = "idId";
    String EMAIL = "email";
    String FACEBOOK_ID = "facebookId";
    String PASSWORD = "password";
    String BLOCK = "block";
    String NAME = "name";
    String CACHE = "cache";
    String TITLE = "title";
    String CATEGORY_ID = "categoryId";
    String LANGUAGE = "lang";
    String INTERNAL_REQUEST_CODE = "internalRequestCode";
    String BRANDS_LIST = "brandsList";
    String ADDRESS = "address";
    String ADV_ID = "advId";
    String SELLER_ID = "sellerId";
    String POSITION = "position";
    String PHOTOS = "photos";
    String IS_VISITOR = "isVisitor";

    Integer PAGE_SIZE = 15;

    String IS_FACEBOOK_USER = "isFacebookUser";
    String FACEBOOK_USER_AUTH = "facebookUserAuth";
    String NOTIFICATION_ID = "notificationId";

    String USER_ROLE_SELLER = "seller";
    String USER_ROLE_CUSTOMER = "customer";

}
