package com.iqitco.elepur_api;

/**
 * date 5/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public interface BasePresenter  {

    void onCreate();

    void onDestroy();

}
