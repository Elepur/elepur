package com.iqitco.elepur_api.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iqitco.elepur_api.modules.ElegoMain;

import java.util.List;

import io.reactivex.Flowable;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Dao
public interface ElegoMainDao {

    @Query("SELECT * FROM ElegoMain")
    Flowable<List<ElegoMain>> getAll();

    @Query("DELETE FROM ElegoMain")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ElegoMain elegoMain);

}
