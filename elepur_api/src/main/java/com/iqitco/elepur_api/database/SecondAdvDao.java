package com.iqitco.elepur_api.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iqitco.elepur_api.modules.SecondAdv;

import java.util.List;

import io.reactivex.Flowable;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Dao
public interface SecondAdvDao {

    @Query("SELECT * FROM SecondAdv")
    Flowable<List<SecondAdv>> getAll();

    @Query("DELETE FROM SecondAdv")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SecondAdv secondAdv);

}
