package com.iqitco.elepur_api.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iqitco.elepur_api.modules.State;

import java.util.List;

import io.reactivex.Flowable;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Dao
public interface StateDao {

    @Query("SELECT * FROM State")
    Flowable<List<State>> getAll();

    @Query("DELETE FROM State")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(State state);

}
