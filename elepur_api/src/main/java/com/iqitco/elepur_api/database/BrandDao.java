package com.iqitco.elepur_api.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iqitco.elepur_api.modules.Brand;

import java.util.List;

import io.reactivex.Flowable;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Dao
public interface BrandDao {

    @Query("SELECT * FROM Brand WHERE mainPageSectionId = :mainPageSectionId")
    Flowable<List<Brand>> getAllBrand(int mainPageSectionId);

    @Query("SELECT * FROM Brand")
    Flowable<List<Brand>> getAllBrand();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Brand brand);

    @Query("DELETE FROM Brand")
    void deleteAll();

}
