package com.iqitco.elepur_api.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.iqitco.elepur_api.modules.Brand;
import com.iqitco.elepur_api.modules.BrandPic;
import com.iqitco.elepur_api.modules.ElegoMain;
import com.iqitco.elepur_api.modules.LandingPageBackground;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.PopAdv;
import com.iqitco.elepur_api.modules.SecondAdv;
import com.iqitco.elepur_api.modules.Slides;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Database(
        entities = {
                Brand.class,
                BrandPic.class,
                MainPageSection.class,
                PopAdv.class,
                Slides.class,
                State.class,
                StateArea.class,
                SecondAdv.class,
                LandingPageBackground.class,
                ElegoMain.class
        },
        version = 2,
        exportSchema = false
)
public abstract class AppDatabase extends RoomDatabase {

    private static volatile AppDatabase INSTANCE;

    public abstract BrandDao getBrandDao();

    public abstract BrandPicDao getBrandPicDao();

    public abstract MainPageSectionDao getMainPageSectionDao();

    public abstract PopAdvDao getPopAdvDao();

    public abstract SlidesDao getSlidesDao();

    public abstract StateAreaDao getStateAreaDao();

    public abstract StateDao getStateDao();

    public abstract SecondAdvDao getSecondAdvDao();

    public abstract LandingPageBackgroundDao getLandingPageBackgroundDao();

    public abstract ElegoMainDao getElegoMainDao();


    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "elepur.db")
                            .addMigrations(new Migration(1, 2) {
                                @Override
                                public void migrate(@NonNull SupportSQLiteDatabase database) {
                                    database.execSQL("CREATE TABLE `SecondAdv`(\n" +
                                            "    `id` INTEGER NOT NULL,\n" +
                                            "    `buttounSecondAdvEn` TEXT,\n" +
                                            "    `buttounSecondAdvAr` TEXT,\n" +
                                            "    `buttounSecondAdvLink` TEXT,\n" +
                                            "    `typeLink` INTEGER,\n" +
                                            "    PRIMARY KEY(`id`)\n" +
                                            ")");

                                    database.execSQL("CREATE TABLE `LandingPageBackground`(\n" +
                                            "    `id` INTEGER NOT NULL,\n" +
                                            "    `picEn` TEXT,\n" +
                                            "    `picAr` TEXT,\n" +
                                            "    PRIMARY KEY(`id`)\n" +
                                            ")");

                                    database.execSQL("CREATE TABLE `ElegoMain`(\n" +
                                            "    `id` INTEGER NOT NULL,\n" +
                                            "    `buttounMainElegoEn` TEXT,\n" +
                                            "    `buttounMainElegoAr` TEXT,\n" +
                                            "    PRIMARY KEY(`id`)\n" +
                                            ")");
                                }
                            })
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
