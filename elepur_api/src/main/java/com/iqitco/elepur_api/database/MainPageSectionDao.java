package com.iqitco.elepur_api.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iqitco.elepur_api.modules.MainPageSection;

import java.util.List;

import io.reactivex.Flowable;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Dao
public interface MainPageSectionDao {

    @Query("SELECT * FROM MainPageSection")
    Flowable<List<MainPageSection>> getAll();

    @Query("SELECT * FROM MainPageSection WHERE id IN (:ids)")
    Flowable<List<MainPageSection>> getByIds(List<Integer> ids);

    @Query("SELECT * FROM MainPageSection WHERE id = :categoryId")
    Flowable<MainPageSection> getById(int categoryId);

    @Query("DELETE FROM MainPageSection")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MainPageSection mainPageSection);

}
