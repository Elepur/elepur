package com.iqitco.elepur_api.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iqitco.elepur_api.modules.PopAdv;

import java.util.List;

import io.reactivex.Flowable;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@Dao
public interface PopAdvDao {

    @Query("SELECT * FROM PopAdv")
    Flowable<List<PopAdv>> getAll();

    @Query("SELECT * FROM PopAdv WHERE id = :id")
    Flowable<PopAdv> getAllById(int id);

    @Query("DELETE FROM PopAdv")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PopAdv popAdv);

}
