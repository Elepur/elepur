package com.iqitco.elepur.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.widget.EditText

/**
 * date 8/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

fun <T : ViewModel> AppCompatActivity.obtainViewModel(viewModelClass: Class<T>) = ViewModelProviders.of(this).get(viewModelClass)

fun EditText.getNumber(): Int? {
    return try {
        text.toString().trim().toInt()
    } catch (ex: Exception) {
        null
    }
}