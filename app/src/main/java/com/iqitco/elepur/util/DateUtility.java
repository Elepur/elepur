package com.iqitco.elepur.util;

import com.akexorcist.localizationactivity.LanguageSetting;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * date 5/30/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class DateUtility {

    public static final String API_IN_DATE_FORMAT = "dd-MM-yyyy";

    public static String formatDate(Calendar calendar, String pattern) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static String formatDate(long timeInMillis, String pattern) {
        return formatDate(timeInMillis, pattern, Locale.getDefault());
    }

    public static String formatDate(long timeInMillis, String pattern, Locale locale) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, locale);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeInMillis);
            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    private static final SimpleDateFormat time24parser = new SimpleDateFormat("HH:mm", Locale.ENGLISH);


    public static String convertTime(String time) {
        try {
            SimpleDateFormat timeFormat = new SimpleDateFormat("hh a", LanguageSetting.getLocale());
            Date date = time24parser.parse(time);
            return timeFormat.format(date);
        } catch (Exception e) {
            /*NOTHING*/
            return "";
        }
    }

    public static String getTime(Calendar calendar) {
        return time24parser.format(calendar.getTime());
    }

    public static Calendar getCalendarFromTime(String time24) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(time24parser.parse(time24));
            return calendar;
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    private static final SimpleDateFormat dateParse = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH);

    public static String formatNotificationDate(String dateStr) {
        try {
            String pattern;
            if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
                pattern = "yyyy/MM/dd";
            } else {
                pattern = "dd/MM/yyyy";
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, LanguageSetting.getLocale());
            return dateFormat.format(dateParse.parse(dateStr));
        } catch (Exception e) {
            return dateStr;
        }
    }

}
