package com.iqitco.elepur.util

import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity
import com.iqitco.elepur.items.ProductItem
import com.iqitco.elepur_api.Constant
import com.mikepenz.fastadapter.IAdapter

/**
 * date 9/15/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

fun AppCompatActivity.onProductItemItemClick(view: View?, adapter: IAdapter<ProductItem>?, item: ProductItem, position: Int): Boolean {
    startActivity(
            Intent(this, ShowAdsActivity::class.java)
                    .putExtra(Constant.ADV_ID, item.o.idAdv)
                    .putExtra(Constant.SELLER_ID, item.o.shopId)
    )
    return true
}

fun Fragment.onProductItemItemClick(view: View?, adapter: IAdapter<ProductItem>?, item: ProductItem, position: Int): Boolean {
    startActivity(
            Intent(context, ShowAdsActivity::class.java)
                    .putExtra(Constant.ADV_ID, item.o.idAdv)
                    .putExtra(Constant.SELLER_ID, item.o.shopId)
    )
    return true
}