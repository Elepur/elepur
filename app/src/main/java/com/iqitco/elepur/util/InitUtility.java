package com.iqitco.elepur.util;

import android.app.FragmentManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author Mohammad Al-Najjar
 * @version 1.0
 * @since 8/27/2015
 */
public class InitUtility {

    public static int[] getColors(Context context, int styleId, int[] attrs) {
        /*int[] attrs = {android.R.attr.textColorPrimary, R.attr.iconActiveColor, R.attr.iconInactiveColor, R.attr.iconTintColor, R.attr.backColor};*/
        if (attrs == null || context == null || styleId == 0) {
            return null;
        }
        int[] results = new int[attrs.length];
        Arrays.sort(attrs);
        TypedArray ta = context.obtainStyledAttributes(styleId, attrs);
        for (int i = 0; i < attrs.length; i++) {
            results[i] = ta.getColor(Arrays.binarySearch(attrs, attrs[i]), Color.BLACK);
        }
        /*textColorPrimary = ta.getColor(Arrays.binarySearch(attrs, android.R.attr.textColorPrimary), Color.BLACK);
        int iconActiveColor = ta.getColor(Arrays.binarySearch(attrs, R.attr.iconActiveColor), Color.BLACK);
        iconInactiveColor = ta.getColor(Arrays.binarySearch(attrs, R.attr.iconInactiveColor), Color.BLACK);
        windowBackgroundColor = ta.getColor(Arrays.binarySearch(attrs, R.attr.backColor), Color.BLACK);
        iconTint = ta.getColorStateList(Arrays.binarySearch(attrs, R.attr.iconTintColor));*/
        ta.recycle();
        return results;
    }

    public static int getThemeId(Context context) throws Exception {
        Class<?> wrapper = Context.class;
        Method method = wrapper.getMethod("getThemeResId");
        method.setAccessible(true);
        return (Integer) method.invoke(context);
    }

    public static void initDateInput(final EditText editText, final Calendar calendar, final FragmentManager fragmentManager, final OnSelect onSelect) {
        editText.setInputType(InputType.TYPE_NULL);
        editText.setOnKeyListener(null);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    editText.requestFocus();
                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {
                                    calendar.set(Calendar.YEAR, i);
                                    calendar.set(Calendar.MONTH, i1);
                                    calendar.set(Calendar.DAY_OF_MONTH, i2);

                                    editText.setText(DateUtility.formatDate(calendar, DateUtility.API_IN_DATE_FORMAT));
                                    if (onSelect != null) {
                                        onSelect.onSelect();
                                    }
                                }
                            },
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)
                    );
                    datePickerDialog.show(fragmentManager, "date");
                }
                return false;
            }
        });
    }

    public static void initTimeInput(final EditText editText, final Calendar calendar, final FragmentManager fragmentManager, final OnSelect onSelect) {
        initTimeInput(editText, calendar, fragmentManager, true, onSelect);
    }

    public static void initTimeInput(final EditText editText, final Calendar calendar, final FragmentManager fragmentManager, final boolean enableMinutes, final OnSelect onSelect) {
        editText.setInputType(InputType.TYPE_NULL);
        editText.setOnKeyListener(null);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    editText.requestFocus();
                    TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePickerDialog timePickerDialog, int hourOfDay, int minute, int second) {
                                    editText.setText(String.format(Locale.getDefault(), "%02d:%02d", hourOfDay, enableMinutes ? minute : 0));
                                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    calendar.set(Calendar.MINUTE, enableMinutes ? minute : 0);
                                    if (onSelect != null) {
                                        onSelect.onSelect();
                                    }
                                }
                            },
                            calendar.get(Calendar.HOUR_OF_DAY),
                            calendar.get(Calendar.MINUTE),
                            true);
                    timePickerDialog.enableMinutes(enableMinutes);
                    timePickerDialog.show(fragmentManager, "date");
                }
                return false;
            }
        });
    }

    public interface OnSelect {
        void onSelect();
    }

}
