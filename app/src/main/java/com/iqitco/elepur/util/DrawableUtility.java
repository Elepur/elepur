package com.iqitco.elepur.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.DrawableRes;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.util.StateSet;

import com.iqitco.elepur.R;

import java.util.Arrays;

/**
 * date: 12/23/2015
 *
 * @author Mohammad Al-Najjar
 * @version 1.0
 * @since lib 1.5.20
 */

public class DrawableUtility {

    public static Drawable getTintDrawable(Context context, @DrawableRes int drawableResId, int color) {
        @SuppressLint("RestrictedApi")
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableResId);
        drawable = DrawableCompat.wrap(drawable).getConstantState().newDrawable();
        DrawableCompat.setTint(drawable, color);
        return drawable;
    }

    public static Drawable getTintListDrawable(Context context, @DrawableRes int drawableResId, ColorStateList colorStateList) {
        @SuppressLint("RestrictedApi")
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableResId);
        drawable = DrawableCompat.wrap(drawable).getConstantState().newDrawable();
        DrawableCompat.setTintList(drawable, colorStateList);
        return drawable;
    }

    public static Drawable getAccentDrawable(Context context, @DrawableRes int drawableResId) {
        return getAccentDrawable(context, context.getResources().getDrawable(drawableResId));
    }

    public static Drawable getAccentDrawable(Context context, Drawable drawable) {
        try {
            int[] attrs = {R.attr.colorAccent};
            Arrays.sort(attrs);
            TypedArray ta = context.obtainStyledAttributes(InitUtility.getThemeId(context), attrs);
            int colorAccent = ta.getColor(Arrays.binarySearch(attrs, R.attr.colorAccent), Color.BLACK);
            ta.recycle();
            drawable = DrawableCompat.wrap(drawable).getConstantState().newDrawable();
            DrawableCompat.setTint(drawable, colorAccent);
            return drawable;
        } catch (Exception e) {
            return drawable;
        }
    }

    public static StateListDrawable getRectanglePressDrawable(Context context, int defaultColor, int corners) {
        TypedArray array = context.getTheme().obtainStyledAttributes(new int[]{
                android.R.attr.colorBackground
        });
        int backgroundColor = array.getColor(0, 0xFF00FF);
        array.recycle();

        int pressedColor = Color.argb(102, Color.red(defaultColor), Color.green(defaultColor), Color.blue(defaultColor));
        double darkness = 1 - (0.299 * Color.red(backgroundColor) + 0.587 * Color.green(backgroundColor) + 0.114 * Color.blue(backgroundColor)) / 255;
        int disableColor;
        if (darkness > 0.5) {
            disableColor = Color.parseColor("#1FFFFFFF");
        } else {
            disableColor = Color.parseColor("#1F000000");
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{-android.R.attr.state_enabled}, getRectangleDrawable(context, disableColor, corners));
        stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, getRectangleDrawable(context, pressedColor, corners));
        stateListDrawable.addState(StateSet.NOTHING, getRectangleDrawable(context, defaultColor, corners));
        return stateListDrawable;
    }

    public static GradientDrawable getRectangleDrawable(Context context, int color, float corners) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(corners);
        shape.setColor(color);
        return shape;
    }

    public static GradientDrawable getRectangleDrawable(int color, float corners) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(corners);
        shape.setColor(color);
        return shape;
    }

    /*public static GradientDrawable getRectangleDrawable(Context context, int color) {
        return getRectangleDrawable(context, color, context.getResources().getDimension(R.dimen.corners));
    }*/

}
