package com.iqitco.elepur.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.iqitco.elepur.R;
import com.iqitco.elepur.items.BottomDialogItem2;
import com.iqitco.elepur.items.SpinnerDividerItemDecoration;
import com.iqitco.elepur.items.on_click.MultiCheckBoxOnClickListener;
import com.iqitco.elepur.items.on_click.SingleCheckBoxOnClickListener;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.select.SelectExtension;

import java.util.ArrayList;
import java.util.List;


/**
 * date 5/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@SuppressWarnings("unchecked")
public abstract class BaseBottomSheetDialog2 extends BottomSheetDialog {

    private FastItemAdapter<BottomDialogItem2> adapter = new FastItemAdapter<>();
    private OnMainButtonClick onMainButtonClick;
    private OnNonSelect onNonSelect;
    private SelectExtension<BottomDialogItem2> selectExtension;

    public BaseBottomSheetDialog2(@NonNull Context context) {
        super(context);
        init(context, false);
    }

    public BaseBottomSheetDialog2(@NonNull Context context, boolean multiSelect) {
        super(context);
        init(context, multiSelect);
    }

    public BaseBottomSheetDialog2(@NonNull Context context, int theme) {
        super(context, theme);
        init(context, false);
    }

    public BaseBottomSheetDialog2(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context, false);
    }

    private void init(Context context, boolean multiSelect) {
        setContentView(R.layout.bottom_dialog_base);
        setCanceledOnTouchOutside(true);
        selectExtension = new SelectExtension<>();
        selectExtension.withSelectable(true);
        selectExtension.init(adapter);

        if (multiSelect) {
            selectExtension.withMultiSelect(true);
            adapter.withOnClickListener(new MultiCheckBoxOnClickListener(selectExtension));
        } else {
            adapter.withOnClickListener(new SingleCheckBoxOnClickListener(selectExtension));
        }

        initItems(context, adapter);
        RecyclerView recyclerView = findViewById(R.id.list);
        assert recyclerView != null;
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new SpinnerDividerItemDecoration(getContext()));
        findViewById(R.id.mainBtn).setOnClickListener(this::onMainBtnClick);
    }

    public List<Integer> getSelectedList() {
        return new ArrayList<>(selectExtension.getSelections());
    }

    private void onMainBtnClick(View view) {
        if (onMainButtonClick != null) {
            if (selectExtension.getSelections().size() > 0) {
                onMainButtonClick.onMainButtonClick(new ArrayList(selectExtension.getSelectedItems()));
            } else {
                if (onNonSelect != null) {
                    onNonSelect.onNonSelect();
                }
            }
        }

        if (isShowing()) {
            dismiss();
        }
    }

    protected abstract void initItems(Context context, FastItemAdapter<BottomDialogItem2> adapter);

    public void setOnMainButtonClick(OnMainButtonClick onMainButtonClick) {
        this.onMainButtonClick = onMainButtonClick;
    }

    public void setOnNonSelect(OnNonSelect onNonSelect) {
        this.onNonSelect = onNonSelect;
    }

    @FunctionalInterface
    public interface OnMainButtonClick {
        void onMainButtonClick(List<BottomDialogItem2> list);
    }

    @FunctionalInterface
    public interface OnNonSelect {
        void onNonSelect();
    }
}
