package com.iqitco.elepur.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.login.LoginActivity
import kotlinx.android.synthetic.main.visitor_dialog.*


/**
 * date 7/27/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class VisitorDialog(activity: Activity) : Dialog(activity, R.style.Theme_AppCompat_Light_Dialog_Alert) {

    init {
        setContentView(R.layout.visitor_dialog)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        loginBtn.setOnClickListener { activity.startActivity(Intent(activity, LoginActivity::class.java)) }
        createNewAccountBtn.setOnClickListener { activity.startActivity(Intent(activity, LoginActivity::class.java)) }
        cancelBtn.setOnClickListener { dismiss() }
    }

}