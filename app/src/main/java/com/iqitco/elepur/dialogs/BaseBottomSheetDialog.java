package com.iqitco.elepur.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.iqitco.elepur.R;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.iqitco.elepur.items.SpinnerDividerItemDecoration;
import com.iqitco.elepur.items.on_click.MultiCheckBoxOnClickListener;
import com.iqitco.elepur.items.on_click.SingleCheckBoxOnClickListener;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.select.SelectExtension;

import java.util.ArrayList;
import java.util.List;


/**
 * date 5/29/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
@SuppressWarnings("unchecked")
public abstract class BaseBottomSheetDialog extends BottomSheetDialog {

    private FastItemAdapter<BottomDialogItem1> adapter = new FastItemAdapter<>();
    private OnMainButtonClick onMainButtonClick;
    private OnNonSelect onNonSelect;
    private SelectExtension<BottomDialogItem1> selectExtension;
    private boolean multiSelect;
    private Button mainBtn;

    public BaseBottomSheetDialog(@NonNull Context context) {
        super(context);
        init(context, false);
    }

    public BaseBottomSheetDialog(@NonNull Context context, boolean multiSelect) {
        super(context);
        init(context, multiSelect);
    }

    public BaseBottomSheetDialog(@NonNull Context context, int theme) {
        super(context, theme);
        init(context, false);
    }

    public BaseBottomSheetDialog(@NonNull Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context, false);
    }

    private void init(Context context, boolean multiSelect) {
        this.multiSelect = multiSelect;
        setContentView(R.layout.bottom_dialog_base);
        setCanceledOnTouchOutside(true);
        selectExtension = new SelectExtension<>();
        selectExtension.withSelectable(true);
        selectExtension.init(adapter);
        mainBtn = findViewById(R.id.mainBtn);
        if (multiSelect) {
            selectExtension.withMultiSelect(true);
            adapter.withOnClickListener(new MultiCheckBoxOnClickListener(selectExtension));
        } else {
            adapter.withOnClickListener(new SingleCheckBoxOnClickListener(selectExtension));
        }

        initItems(context, adapter);
        RecyclerView recyclerView = findViewById(R.id.list);
        assert recyclerView != null;
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new SpinnerDividerItemDecoration(getContext()));
        mainBtn.setOnClickListener(this::onMainBtnClick);
    }

    public int getItemCount() {
        return adapter.getItemCount();
    }

    public List<Integer> getSelectedList() {
        return new ArrayList<>(selectExtension.getSelections());
    }

    public List<Integer> getSelectionIds() {
        List<Integer> list = new ArrayList<>();
        for (BottomDialogItem1 item : selectExtension.getSelectedItems()) {
            list.add((int) item.getIdentifier());
        }
        return list;
    }

    private void onMainBtnClick(View view) {
        if (onMainButtonClick != null) {
            if (selectExtension.getSelections().size() > 0) {
                onMainButtonClick.onMainButtonClick(new ArrayList<>(selectExtension.getSelectedItems()));
            } else {
                if (onNonSelect != null) {
                    onNonSelect.onNonSelect();
                }
            }
        }

        if (isShowing()) {
            dismiss();
        }
    }

    public void setSelectedIds(List<Integer> ids) {
        if (ids != null && ids.size() > 0) {
            for (int i = 0; i < adapter.getItemCount(); i++) {
                int id = (int) adapter.getItem(i).getIdentifier();
                if (ids.contains(id)) {
                    selectExtension.select(i);
                }
            }
            onMainBtnClick(null);
        } else {
            if (onNonSelect != null) {
                onNonSelect.onNonSelect();
            }
        }
    }

    protected abstract void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter);

    public void setOnMainButtonClick(OnMainButtonClick onMainButtonClick) {
        this.onMainButtonClick = onMainButtonClick;
    }

    public void setOnNonSelect(OnNonSelect onNonSelect) {
        this.onNonSelect = onNonSelect;
    }

    public Button getMainBtn() {
        return mainBtn;
    }

    @FunctionalInterface
    public interface OnMainButtonClick {
        void onMainButtonClick(List<BottomDialogItem1> list);
    }

    @FunctionalInterface
    public interface OnNonSelect {
        void onNonSelect();
    }
}
