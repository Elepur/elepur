package com.iqitco.elepur.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.iqitco.elepur.R;
import com.iqitco.elepur_api.modules.PopAdv;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * date 6/30/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class MainAdvDialog extends Dialog {

    private static final String TAG = MainAdvDialog.class.getSimpleName();

    private PopAdv popAdv;
    private OnPopAdvClick onPopAdvClick;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);

    public MainAdvDialog(@NonNull Context context, PopAdv popAdv) {
        super(context, R.style.Theme_AppCompat_Light_Dialog_Alert);
        setContentView(R.layout.main_adv_dialog);
        this.popAdv = popAdv;
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RoundedImageView image = findViewById(R.id.image);
        ImageButton closeBtn = findViewById(R.id.closeBtn);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        String imageUrl;
        if (Locale.getDefault().toString().equalsIgnoreCase("ar")) {
            imageUrl = popAdv.getPicAr();
        } else {
            imageUrl = popAdv.getPicEn();
        }
        Glide.with(context)
                .load(imageUrl)
                .apply(new RequestOptions()
                        .centerCrop()
                        .fallback(R.drawable.img_place_holder)
                        .placeholder(R.drawable.img_place_holder)
                        .error(R.drawable.img_place_holder))
                .into(image);
        closeBtn.setOnClickListener(v -> dismiss());
        findViewById(R.id.titleTv).setOnClickListener(v -> dismiss());
        findViewById(R.id.layout1).setOnClickListener(v -> {
            if (onPopAdvClick != null) {
                onPopAdvClick.onPopAdvClick(popAdv);
            }
        });
    }

    public void setOnPopAdvClick(OnPopAdvClick onPopAdvClick) {
        this.onPopAdvClick = onPopAdvClick;
    }

    @Override
    public void show() {
        try {
            Date startDate = simpleDateFormat.parse(popAdv.getDateStart());
            Date endDate = simpleDateFormat.parse(popAdv.getDateEnd());
            Calendar calendar = Calendar.getInstance();
            if (startDate.getTime() <= calendar.getTimeInMillis() && calendar.getTimeInMillis() <= endDate.getTime()) {
                super.show();
            }
        } catch (Exception e) {
            Log.e(TAG, "error showing popAdv date", e);
        }

    }

    public interface OnPopAdvClick {
        void onPopAdvClick(PopAdv popAdv);
    }
}
