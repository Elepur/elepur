package com.iqitco.elepur.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.iqitco.elepur.R;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

/**
 * date 5/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class SelectImageDialog extends BaseBottomSheetDialog {

    public static final long FROM_GALLERY = 1;
    public static final long FROM_CAMERA = 2;

    public SelectImageDialog(@NonNull Context context) {
        super(context);
    }

    public SelectImageDialog(@NonNull Context context, int theme) {
        super(context, theme);
    }

    public SelectImageDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
        adapter.add(new BottomDialogItem1(context.getString(R.string.title_from_gallery)).withIdentifier(FROM_GALLERY));
        adapter.add(new BottomDialogItem1(context.getString(R.string.title_from_camera)).withIdentifier(FROM_CAMERA));
    }
}
