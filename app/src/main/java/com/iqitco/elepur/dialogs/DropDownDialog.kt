package com.iqitco.elepur.dialogs

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.iqitco.elepur.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.select.SelectExtension
import io.reactivex.Observable
import kotlinx.android.synthetic.main.drop_down_dialog.*

/**
 * date 8/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class DropDownDialog private constructor(
        context: Context,
        multiSelect: Boolean,
        private val canSelectNon: Boolean,
        private val images: Boolean) : BottomSheetDialog(context) {

    private val adapter = FastItemAdapter<DropDownViewHolder>()
    private val selectExtension = SelectExtension<DropDownViewHolder>()
    private var onSelectCallBack: ((String, List<Item>) -> Unit)? = null
    private var onSelectNonCallBack: (() -> Unit)? = null

    init {
        setContentView(R.layout.drop_down_dialog)
        setCanceledOnTouchOutside(true)
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = adapter
        mainBtn.isEnabled = canSelectNon
        mainBtn.setOnClickListener(this::onMainClick)
        selectExtension.withSelectable(true)
        selectExtension.init(adapter)
        if (multiSelect) {
            selectExtension.withMultiSelect(true)
            adapter.withOnClickListener(this::onMultiSelect)
        } else {
            adapter.withOnClickListener(this::onSingleSelect)
        }
    }

    fun addItem(item: Item) {
        adapter.add(DropDownViewHolder(item, images).withIdentifier(item.id))
    }

    fun count(): Int {
        return adapter.itemCount
    }

    fun clearItems() {
        adapter.clear()
    }

    private fun onMainClick(view: View?) {
        if (selectExtension.selections.isNotEmpty()) {
            onSelect()
        } else {
            onSelectNon()
        }
        dismiss()
    }

    private fun onMultiSelect(v: View?, adapter: IAdapter<DropDownViewHolder>, item: DropDownViewHolder, position: Int): Boolean {
        selectExtension.toggleSelection(position)
        if (!canSelectNon) {
            mainBtn.isEnabled = selectExtension.selections.isNotEmpty()
        }
        return true
    }

    private fun onSingleSelect(v: View?, adapter: IAdapter<DropDownViewHolder>, item: DropDownViewHolder, position: Int): Boolean {
        val selection = selectExtension.selections
        if (!item.isSelected) {
            if (!selection.isEmpty()) {
                selectExtension.deselect()
            }
            selectExtension.select(position)
        } else {
            if (selection.size == 1) {
                val id = selection.iterator().next()
                if (id == position) {
                    selectExtension.deselect()
                }
            }
        }
        if (!canSelectNon) {
            mainBtn.isEnabled = selectExtension.selections.isNotEmpty()
        }
        return true
    }

    private fun onSelect() {
        val items = Observable.just(selectExtension.selectedItems)
                .flatMapIterable { list -> list }
                .map { fastItem -> fastItem.item }
                .toList()
                .blockingGet()
        val builder = StringBuilder()
        builder.append(items[0].title)
        for (i in 1 until items.size) {
            builder.append(", ").append(items[i].title)
        }
        onSelectCallBack?.invoke(builder.toString(), items)
    }

    fun onSelect(onSelectCallBack: (String, List<Item>) -> Unit) {
        this.onSelectCallBack = onSelectCallBack
    }

    private fun onSelectNon() {
        onSelectNonCallBack?.invoke()
    }

    fun onSelectNon(onSelectNonCallBack: () -> Unit) {
        this.onSelectNonCallBack = onSelectNonCallBack
    }

    fun getSelectedIds(): MutableList<Int> {
        return Observable.just(selectExtension.selectedItems)
                .flatMapIterable { list -> list }
                .map { fastItem -> fastItem.item.id.toInt() }
                .toList()
                .blockingGet()
    }

    fun getSelectedId(): Int? {
        return try {
            getSelectedIds()[0]
        } catch (ex: Exception) {
            null
        }
    }

    fun setSelectedIds(ids: List<Int>) {
        selectExtension.selectedItems.clear()
        for (i in 0 until adapter.itemCount) {
            if (ids.contains(adapter.getItem(i).item.id.toInt())){
                selectExtension.select(i)
            }
        }
        mainBtn.isEnabled = canSelectNon || selectExtension.selections.isNotEmpty()
        onMainClick(null)
    }

    data class Item(val id: Long, val title: String, val imageUrl: String?) {
        constructor(id: Long, title: String) : this(id, title, null)
    }

    class Builder(val context: Context) {

        private var withMultiSelect = false
        private var withNonSelect = false
        private var withImages = false

        fun withMultiSelect(flag: Boolean): Builder {
            withMultiSelect = flag
            return this
        }

        fun withNonSelect(flag: Boolean): Builder {
            withNonSelect = flag
            return this
        }

        fun withImages(flag: Boolean): Builder {
            this.withImages = flag
            return this
        }

        fun build(): DropDownDialog {
            return DropDownDialog(context, withMultiSelect, withNonSelect, withImages)
        }
    }

    private class DropDownViewHolder(val item: DropDownDialog.Item, val withImage: Boolean) : AbstractItem<DropDownViewHolder, DropDownViewHolder.ViewHolder>() {

        override fun getType(): Int {
            return R.id.drop_down_item
        }

        override fun getViewHolder(v: View): ViewHolder {
            return ViewHolder(v)
        }

        override fun getLayoutRes(): Int {
            return R.layout.drop_down_item
        }

        inner class ViewHolder(view: View) : FastAdapter.ViewHolder<DropDownViewHolder>(view) {

            private val image = view.findViewById<ImageView>(R.id.image)
            private val checkbox = view.findViewById<CheckBox>(R.id.checkbox)
            private val textTv = view.findViewById<TextView>(R.id.textTv)

            init {
                image.visibility = if (withImage) View.VISIBLE else View.GONE
            }

            override fun unbindView(item: DropDownViewHolder) {

            }

            override fun bindView(item: DropDownViewHolder, payloads: MutableList<Any>) {
                textTv.text = item.item.title
                checkbox.isChecked = item.isSelected
                if (withImage) {
                    if (!item.item.imageUrl.isNullOrEmpty()) {
                        if (item.item.imageUrl!!.startsWith("#")) {
                            image.setImageDrawable(ColorDrawable(Color.parseColor(item.item.imageUrl)))
                        } else {
                            Glide.with(image).load(item.item.imageUrl).into(image)
                        }
                    }
                }
            }
        }

    }
}


