package com.iqitco.elepur.dialogs;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.create_account_customer.CreateAccountCustomerActivity;
import com.iqitco.elepur.activities.create_account_store.CreateAccountStoreActivity;
import com.iqitco.elepur.activities.home.HomeSellerActivity;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.login_vistor.LoginVisitorContract;
import com.iqitco.elepur_api.contracts.login_vistor.LoginVisitorPresenter;
import com.master.permissionhelper.PermissionHelper;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

/**
 * date 5/26/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class RegistrationDialog extends Dialog implements LoginVisitorContract.View {

    private static final String TAG = RegistrationDialog.class.getSimpleName();

    private Button registrationUserBtn;
    private Button registrationStoreBtn;
    private Button registrationSkipBtn;
    private BaseActivity activity;
    private PermissionHelper permissionHelper;
    private LoginVisitorContract.Presenter presenter;
    private boolean openHomeFlag;

    public RegistrationDialog(@NonNull Context context) {
        this(context, true);
    }

    public RegistrationDialog(@NonNull Context context, boolean openHomeFlag) {
        super(context, R.style.Theme_AppCompat_Light_Dialog_Alert);
        this.openHomeFlag = openHomeFlag;
        activity = (BaseActivity) context;
        presenter = new LoginVisitorPresenter(this);
        setContentView(R.layout.registration_dialog);
        /*getWindow().setBackgroundDrawableResource(R.color.dark_layer);*/
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        registrationUserBtn = findViewById(R.id.registrationUserBtn);
        registrationStoreBtn = findViewById(R.id.registrationStoreBtn);
        registrationSkipBtn = findViewById(R.id.registrationSkipBtn);

        registrationUserBtn.setOnClickListener(this::onUserClick);
        registrationStoreBtn.setOnClickListener(this::onStoreBtnClick);
        registrationSkipBtn.setOnClickListener(this::onSkipClick);
    }

    private void onUserClick(View view) {
        dismiss();
        activity.startActivity(new Intent(activity, CreateAccountCustomerActivity.class));
    }

    private void onSkipClick(View view) {
        if (openHomeFlag) {
            presenter.loginVisitor();
        } else {
            dismiss();
        }
    }

    private void onStoreBtnClick(View view) {
        dismiss();
        permissionHelper = new PermissionHelper(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        permissionHelper.request(new PermissionHelper.PermissionCallback() {
            @Override
            public void onPermissionGranted() {
                activity.startActivity(new Intent(activity, CreateAccountStoreActivity.class));
            }

            @Override
            public void onIndividualPermissionGranted(String[] grantedPermission) {

            }

            @Override
            public void onPermissionDenied() {

            }

            @Override
            public void onPermissionDeniedBySystem() {

            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public String getNotificationId() {
        return SecurePrefManager.with(getContext()).get(Constant.NOTIFICATION_ID).defaultValue("").go();
    }

    @Override
    public void onSuccess(boolean flag) {
        dismiss();
        if (flag) {
            activity.finish();
            activity.startActivity(new Intent(activity, HomeSellerActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    @Override
    public void showLoading() {
        activity.showLoading();
    }

    @Override
    public void hideLoading() {
        activity.hideLoading();
    }

    @Override
    public void showUserMessage(String message) {
        activity.showUserMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        activity.showErrorMessage(message);
    }
}
