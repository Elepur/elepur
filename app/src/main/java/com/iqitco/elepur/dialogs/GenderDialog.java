package com.iqitco.elepur.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.iqitco.elepur.R;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class GenderDialog extends BaseBottomSheetDialog {

    public GenderDialog(@NonNull Context context) {
        super(context);
    }

    public GenderDialog(@NonNull Context context, int theme) {
        super(context, theme);
    }

    public GenderDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
        adapter.add(new BottomDialogItem1(context.getString(R.string.title_male)).withIdentifier(1));
        adapter.add(new BottomDialogItem1(context.getString(R.string.title_female)).withIdentifier(2));
    }
}
