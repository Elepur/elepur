package com.iqitco.elepur.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.iqitco.elepur.R
import com.iqitco.elepur_api.modules.SellerProfile
import kotlinx.android.synthetic.main.store_location_dialog.*
import android.content.Intent
import android.net.Uri


/**
 * date 7/27/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class StoreLocationDialog(activity: Activity, sellerProfile: SellerProfile) : Dialog(activity, R.style.Theme_AppCompat_Light_Dialog_Alert) {

    init {
        setContentView(R.layout.store_location_dialog)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        addressDesc.text = sellerProfile.location
        openMapBtn.setOnClickListener {

            val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("geo:${sellerProfile.locationLaut},${sellerProfile.locationLong}?q=${sellerProfile.locationLaut},${sellerProfile.locationLong}(${sellerProfile.shopName})")
            )

            activity.startActivity(intent)
        }
        cancelBtn.setOnClickListener { dismiss() }
    }

}