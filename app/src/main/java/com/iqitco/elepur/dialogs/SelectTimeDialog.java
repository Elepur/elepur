package com.iqitco.elepur.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.iqitco.elepur.util.DateUtility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * date 6/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class SelectTimeDialog extends BottomSheetDialog {

    private Button mainBtn;
    private TextView endTimeLbl;
    private SeekBar endSeekBar;
    private TextView openTimeLbl;
    private SeekBar openSeekBar;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
    private Calendar openTime;
    private Calendar closeTime;

    private OnTimesSelect onTimesSelect;

    private int[] openHours = new int[]{
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            0,
    };

    private int[] closeHours = new int[]{
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            0,
            1,
            2,
    };

    public SelectTimeDialog(@NonNull Context context) {
        super(context);
        init(context);
    }

    public SelectTimeDialog(@NonNull Context context, int theme) {
        super(context, theme);
        init(context);
    }

    public SelectTimeDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context);
    }

    private void init(Context context) {
        setContentView(R.layout.bottom_select_time_dialog);
        mainBtn = findViewById(R.id.mainBtn);

        openTimeLbl = findViewById(R.id.openTimeLbl);
        endTimeLbl = findViewById(R.id.endTimeLbl);

        openSeekBar = findViewById(R.id.openSeekBar);
        endSeekBar = findViewById(R.id.endSeekBar);

        openSeekBar.setMax(openHours.length - 1);
        endSeekBar.setMax(closeHours.length - 1);


        openTime = Calendar.getInstance();
        openTime.set(Calendar.MINUTE, 0);
        openTime.set(Calendar.SECOND, 0);
        openTime.set(Calendar.HOUR_OF_DAY, openHours[0]);
        openTimeLbl.setText(dateFormat.format(openTime.getTime()));

        closeTime = Calendar.getInstance();
        closeTime.set(Calendar.MINUTE, 0);
        closeTime.set(Calendar.SECOND, 0);
        closeTime.set(Calendar.HOUR_OF_DAY, closeHours[0]);
        endTimeLbl.setText(dateFormat.format(closeTime.getTime()));

        openSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                openTime.set(Calendar.HOUR_OF_DAY, openHours[progress]);
                openTimeLbl.setText(dateFormat.format(openTime.getTime()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        endSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                closeTime.set(Calendar.HOUR_OF_DAY, closeHours[progress]);
                endTimeLbl.setText(dateFormat.format(closeTime.getTime()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mainBtn.setOnClickListener(v -> {
            if (onTimesSelect != null) {
                onTimesSelect.onTimesSelect(openTime, closeTime, getText());
            }
        });
    }

    private String getText() {
        return dateFormat.format(openTime.getTime()) + "-" + dateFormat.format(closeTime.getTime());
    }

    public void setTime(String fromTime, String toTime) {
        openTime = DateUtility.getCalendarFromTime(fromTime);
        closeTime = DateUtility.getCalendarFromTime(toTime);
        openSeekBar.setProgress(openTime.get(Calendar.HOUR_OF_DAY));
        endSeekBar.setProgress(closeTime.get(Calendar.HOUR_OF_DAY));
        mainBtn.callOnClick();
    }

    public void setOnTimesSelect(OnTimesSelect onTimesSelect) {
        this.onTimesSelect = onTimesSelect;
    }

    @FunctionalInterface
    public interface OnTimesSelect {
        void onTimesSelect(Calendar openTime, Calendar closeTime, String text);
    }
}
