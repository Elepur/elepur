package com.iqitco.elepur.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.iqitco.elepur.BuildConfig;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.splash.SplashActivity;

/**
 * date 14/10/2017
 *
 * @author Mohammad Al-Najjar
 * @version 1.0
 * @since 1.0
 */
public class FireBaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    public static final String TAG = FireBaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            String title = remoteMessage.getData().get("title");
            if (title == null) {
                title = getString(R.string.app_name);
            }


            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Intent[] intents = new Intent[]{
                    new Intent(this, SplashActivity.class)
            };
            PendingIntent pendingIntent = PendingIntent.getActivities(this, 0 /* Request code */, intents, PendingIntent.FLAG_UPDATE_CURRENT);

            int color = getResources().getColor(R.color.primary);
            String colorStr = remoteMessage.getData().get("color");
            if (colorStr != null && !colorStr.isEmpty()) {
                try {
                    color = Color.parseColor(colorStr);
                } catch (Exception e) {
                    /*NOTHING*/
                }
            }

            String body = remoteMessage.getData().get("body");

            NotificationCompat.Builder notificationBuilder = initChannels()

                    .setSmallIcon(R.drawable.ic_black_icon)
                    .setColor(color)
                    /*.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))*/
                    .setContentTitle(title)
                    .setContentText(body)
                    /*.setStyle(new NotificationCompat.BigTextStyle().bigText(text))*/
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    /*.setContentIntent(pendingIntent)*/;

            NotificationManagerCompat.from(this).notify(0, notificationBuilder.build());
        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }

    }

    public NotificationCompat.Builder initChannels() {
        if (Build.VERSION.SDK_INT < 26) {
            return new NotificationCompat.Builder(this);
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String id = BuildConfig.APPLICATION_ID;
        NotificationChannel channel = new NotificationChannel(id, BuildConfig.APPLICATION_ID, NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription(BuildConfig.APPLICATION_ID);
        notificationManager.createNotificationChannel(channel);
        return new NotificationCompat.Builder(this, id);
    }

}