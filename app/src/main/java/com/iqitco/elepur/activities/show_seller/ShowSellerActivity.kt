package com.iqitco.elepur.activities.show_seller

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity
import com.iqitco.elepur.dialogs.StoreLocationDialog
import com.iqitco.elepur.items.ProductItem
import com.iqitco.elepur.util.DateUtility
import com.iqitco.elepur.util.DrawableUtility
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.show_seller.ShowSellerContract
import com.iqitco.elepur_api.contracts.show_seller.ShowSellerPresenter
import com.iqitco.elepur_api.modules.MainPageSection
import com.iqitco.elepur_api.modules.RateResult
import com.iqitco.elepur_api.modules.SellerProduct
import com.iqitco.elepur_api.modules.SellerProfile
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.show_seller_activity.*
import java.util.*

/**
 * date 7/25/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ShowSellerActivity : BaseActivity(), ShowSellerContract.View, TabLayout.OnTabSelectedListener {

    private val itemsAdapter = FastItemAdapter<ProductItem>()
    private lateinit var presenter: ShowSellerContract.Presenter
    private lateinit var profile: SellerProfile
    private lateinit var endlessListener: EndlessRecyclerOnScrollListener
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var mapDialog: StoreLocationDialog
    private var sellerId: Int? = null
    private var customerId: Int? = null
    private var categoryId: Int = 0
    private var confirmCallDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            sellerId = intent.getIntExtra(Constant.SELLER_ID, -1)
            if (sellerId == -1) {
                throw Exception("sellerId cannot be null")
            }
        } catch (ex: Exception) {
            throw Exception("sellerId cannot be null")
        }
        customerId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
        setContentView(R.layout.show_seller_activity)
        tabLayout.addOnTabSelectedListener(this)
        itemsAdapter.withOnClickListener(this::onItemClick)
        layoutManager = LinearLayoutManager(this)
        list.layoutManager = layoutManager
        list.adapter = itemsAdapter

        emojiBar.sadEmoji!!.setOnClickListener {
            showVisitorDialog {
                if (it == 2) {
                    presenter.rateSad()
                }
            }
        }
        emojiBar.happyEmoji!!.setOnClickListener {
            showVisitorDialog {
                if (it == 2) {
                    presenter.rateHappy()
                }
            }
        }
        emojiBar.loveEmoji!!.setOnClickListener {
            showVisitorDialog {
                if (it == 2) {
                    presenter.favorite()
                }
            }
        }
        callSellerBtn.setOnClickListener {
            showVisitorDialog {
                if (confirmCallDialog == null) {
                    confirmCallDialog = MaterialDialog.Builder(this)
                            .content(getString(R.string.message_call_seller_f, profile.phone))
                            .positiveText(R.string.btn_call)
                            .negativeText(R.string.btn_cancel)
                            .onPositive(this::onCallClick)
                            .autoDismiss(true)
                            .build()
                }

                if (!confirmCallDialog!!.isShowing) {
                    confirmCallDialog!!.show()
                }
            }
        }
        mapBtn.setOnClickListener {
            showVisitorDialog {
                if (!mapDialog.isShowing) {
                    mapDialog.show()
                }
            }
        }

        presenter = ShowSellerPresenter(this)
        presenter.onCreate()
        endlessListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                presenter.getAdv(currentPage + 1)
            }
        }

        list.addOnScrollListener(endlessListener)
        endlessListener.disable()
    }

    private fun onCallClick(dialog: MaterialDialog, which: DialogAction) {
        presenter.insertCall()
    }

    private fun onItemClick(view: View?, adapter: IAdapter<ProductItem>?, item: ProductItem, position: Int): Boolean {
        startActivity(
                Intent(this, ShowAdsActivity::class.java)
                        .putExtra(Constant.ADV_ID, item.o.idAdv)
                        .putExtra(Constant.SELLER_ID, item.o.shopId)
        )
        return true
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        categoryId = tab!!.tag.toString().toInt()
        /*endlessListener.disable()*/
        endlessListener.resetPageCount()
    }

    override fun publishRate(result: RateResult) {
        emojiBar.sadEmoji!!.counterTv.text = result.sad.toString()
        emojiBar.happyEmoji!!.counterTv.text = result.happy.toString()
        emojiBar.loveEmoji!!.counterTv.text = result.fav.toString()

        emojiBar.sadEmoji!!.isSelected = false
        emojiBar.happyEmoji!!.isSelected = false
        emojiBar.loveEmoji!!.isSelected = false

        when {
            result.checkSad == 1 -> emojiBar.sadEmoji!!.isSelected = true
            result.checkHappy == 1 -> emojiBar.happyEmoji!!.isSelected = true
        }
        if (result.checkFav == 1) {
            emojiBar.loveEmoji!!.isSelected = true
        }
    }

    override fun onInsertCall(flag: Boolean) {
        if (flag) {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:" + profile.phone)
            startActivity(dialIntent)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun publishSellerProfile(profile: SellerProfile) {
        this.profile = profile
        mapDialog = StoreLocationDialog(this, profile)
        presenter.getSellerCategory(profile.sellerCategory!!)

        sellerNameTv.text = profile.shopName
        toolbar.titleTv.text = profile.shopName
        sellerKindTv.text = profile.kind

        sellerBlue.visibility = if (profile.blue == 1) View.VISIBLE else View.INVISIBLE

        Glide.with(this)
                .load(profile.picCover)
                .apply(RequestOptions()
                        .centerCrop()
                        .fallback(R.drawable.img_place_holder)
                        .placeholder(R.drawable.img_place_holder)
                        .error(R.drawable.img_place_holder))
                .into(sellerImage)

        Glide.with(this)
                .load(profile.picLogo)
                .into(sellerLogo)

        emojiBar.sadEmoji!!.counterTv.text = profile.rateSad.toString()
        emojiBar.happyEmoji!!.counterTv.text = profile.rateHappy.toString()
        emojiBar.loveEmoji!!.counterTv.text = profile.favourite.toString()

        emojiBar.sadEmoji!!.isSelected = profile.rateSadC == 1
        emojiBar.happyEmoji!!.isSelected = profile.rateHappyC == 1
        emojiBar.loveEmoji!!.isSelected = profile.favouriteC == 1

        timeTv.text = "${DateUtility.convertTime(profile.timeOpen)} - ${DateUtility.convertTime(profile.timeClose)}"
        val color: Int = if (profile.time == 0) {
            context.resources.getColor(R.color.border)
        } else {
            context.resources.getColor(R.color.textColorPrimary)
        }
        timeTv.setTextColor(color)
        clock1.setImageDrawable(DrawableUtility.getTintDrawable(context, R.drawable.ic_clock1, color))

        locationTv.text = "${profile.state} - ${profile.area}"
        tabLayout.setScrollPosition(0, 0.0F, true)

        val role = SecurePrefManager.with(this).get(Constant.ROLL).defaultValue("").go()
        if (role.equals(Constant.USER_ROLE_SELLER, true)){
            emojiBar.sadEmoji!!.isSelected = true
            emojiBar.happyEmoji!!.isSelected = true
            emojiBar.loveEmoji!!.isSelected = true
        }

    }

    override fun publishSellerCategory(list: List<MainPageSection>) {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_all_products)).setTag(0).setCustomView(R.layout.tab_item_view))
        for (category in list) {
            val title = if (Locale.getDefault().toString().equals("ar", true)) category.categoryNameAr else category.categoryName
            tabLayout.addTab(tabLayout.newTab().setText(title).setTag(category.id!!).setCustomView(R.layout.tab_item_view))
        }
    }

    override fun getSellerId(): Int {
        return sellerId!!
    }

    override fun getCustomerId(): Int {
        return customerId!!
    }

    override fun getCategoryId(): Int {
        return categoryId
    }

    override fun publishSellerAdv(list: List<SellerProduct>, pageNumber: Int) {
        if (pageNumber == 1) {
            itemsAdapter.clear()
        }
        for (product in list) {
            itemsAdapter.add(ProductItem.getProductItem(product, 1, sellerId!!))
        }
        /*if (pageNumber == 1) {
            endlessListener.enable()
        }*/
        itemsAdapter.notifyAdapterDataSetChanged()
    }
}