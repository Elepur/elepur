package com.iqitco.elepur.activities.home;

import android.view.Gravity;
import android.view.View;

import com.iqitco.elepur.activities.BaseFragment;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BaseHomeFragment extends BaseFragment {


    protected HomeSellerActivity getHomeSellerActivity(){
        return (HomeSellerActivity) getActivity();
    }

    protected void onMenuClick(View view) {
        getHomeSellerActivity().getDrawerLayout().openDrawer(Gravity.LEFT);
    }

}
