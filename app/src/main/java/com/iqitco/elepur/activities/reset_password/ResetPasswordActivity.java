package com.iqitco.elepur.activities.reset_password;

import android.os.Bundle;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.widget.CustomEditTextGrayView;
import com.iqitco.elepur_api.contracts.reset_password.ResetPasswordContract;
import com.iqitco.elepur_api.contracts.reset_password.ResetPasswordPresenter;

/**
 * date 6/5/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ResetPasswordActivity extends BaseActivity implements ResetPasswordContract.View {

    private CustomEditTextGrayView emailEdt;
    private Button sendBtn;
    private ResetPasswordContract.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password_activity);
        presenter = new ResetPasswordPresenter(this);
        emailEdt = findViewById(R.id.emailEdt);
        sendBtn = findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(v -> presenter.resetPassword());
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public String getEmail() {
        return emailEdt.getText().toString().trim().toLowerCase();
    }

    @Override
    public void errorEmail(boolean flag) {
        emailEdt.showError(flag);
    }

    @Override
    public void onSuccess() {
        new MaterialDialog.Builder(this)
                .content(R.string.message_success_reset_password)
                .positiveText(R.string.btn_close)
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    onBackPressed();
                })
                .show();
    }
}
