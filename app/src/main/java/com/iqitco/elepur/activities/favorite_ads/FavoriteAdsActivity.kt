package com.iqitco.elepur.activities.favorite_ads

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity
import com.iqitco.elepur.items.ProductItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.favorite_ads.FavoriteAdsContract
import com.iqitco.elepur_api.contracts.favorite_ads.FavoriteAdsPresenter
import com.iqitco.elepur_api.modules.Ads
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.favorite_ads_activity.*

/**
 * date 7/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class FavoriteAdsActivity : BaseActivity(), FavoriteAdsContract.View {

    private val adapter = FastItemAdapter<ProductItem>()
    private val adapterGrid = FastItemAdapter<ProductItem>()
    private lateinit var endlessListener: EndlessRecyclerOnScrollListener
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var presenter: FavoriteAdsContract.Presenter
    private var customerId = -1
    private var isGridList = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.favorite_ads_activity)
        customerId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()

        initSwipeRefresh()

        layoutManager = GridLayoutManager(this, 1)
        list.layoutManager = layoutManager
        list.adapter = adapter

        presenter = FavoriteAdsPresenter(this)
        presenter.onCreate()
        endlessListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                presenter.getCustomerFavAds(currentPage + 1)
            }
        }

        list.addOnScrollListener(endlessListener)
        endlessListener.enable()

        listViewBtn.setOnClickListener(this::changeShow)

        adapter.withOnClickListener(this::onItemClick)
        adapterGrid.withOnClickListener(this::onItemClick)
    }

    private fun onItemClick(v: View?, adapter: IAdapter<ProductItem>?, item: ProductItem, position: Int): Boolean {
        startActivity(
                Intent(this, ShowAdsActivity::class.java)
                        .putExtra(Constant.ADV_ID, item.o.idAdv)
                        .putExtra(Constant.SELLER_ID, item.o.shopId)
        )
        return true
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    private fun initSwipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener {
            endlessListener.disable()
            endlessListener.resetPageCount()
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.accent)
    }

    private fun changeShow(view: View) {
        list.recycledViewPool.clear()
        if (layoutManager.spanCount == 1) {
            layoutManager.spanCount = 2
            listViewBtn.animate().rotation(90f).start()
            isGridList = true
            list.swapAdapter(adapterGrid, false)
            adapterGrid.notifyAdapterDataSetChanged()
            adapterGrid.notifyDataSetChanged()
        } else {
            layoutManager.spanCount = 1
            listViewBtn.animate().rotation(0f).start()
            isGridList = false
            list.swapAdapter(adapter, false)
            adapter.notifyAdapterDataSetChanged()
            adapter.notifyDataSetChanged()
        }

    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun getId(): Int {
        return customerId
    }

    override fun publishAds(list: List<Ads>, pageNumber: Int) {
        if (pageNumber == 1) {
            adapter.clear()
            adapterGrid.clear()
        }
        for (ads in list) {
            adapter.add(ProductItem(ads, 1).withIdentifier(ads.rowNum.toLong()))
            adapterGrid.add(ProductItem(ads, 2).withIdentifier(ads.rowNum.toLong()))
        }
        if (pageNumber == 1) {
            endlessListener.enable()
        }

        if (adapter.itemCount > 0) {
            emptyLayout.visibility = View.INVISIBLE
        } else {
            emptyLayout.visibility = View.VISIBLE
        }
    }
}