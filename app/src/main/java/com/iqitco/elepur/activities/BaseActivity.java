package com.iqitco.elepur.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.akexorcist.localizationactivity.LocalizationActivity;
import com.iqitco.elepur.ElepurApp;
import com.iqitco.elepur.R;
import com.iqitco.elepur.dialogs.VisitorDialog;
import com.iqitco.elepur_api.Constant;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.util.Locale;

/**
 * date 5/26/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BaseActivity extends LocalizationActivity {

    private MaterialDialog progressDialog;
    public static final int PERMISSION_REQUEST_CODE = 30;
    private VisitorDialog visitorDialog;
    protected boolean isLocalAr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        SecurePrefManager securePrefManager = SecurePrefManager.with(this);
        String language = securePrefManager.get(Constant.LANGUAGE).defaultValue(Locale.getDefault().getLanguage()).go();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        setDefaultLanguage(locale);
        setLanguage(locale);
        super.onCreate(savedInstanceState);

        progressDialog = new MaterialDialog.Builder(this)
                .progress(true, 0)
                .content(R.string.title_please_wait)
                .build();
        isLocalAr = Locale.getDefault().toString().equalsIgnoreCase("ar");
    }


    public void showLoading() {
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void hideLoading() {
        progressDialog.dismiss();
    }

    public void showUserMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showErrorMessage(String message) {
        if (message != null) {
            Log.e("error", message);
        }
    }

    public Context getContext() {
        return this;
    }

    public ElepurApp getElepurApp() {
        return (ElepurApp) getApplication();
    }

    public void changeStatusBarColor(int color, boolean isLightTheme) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
            if (isLightTheme) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                }
            }
        }
    }

    public MaterialDialog getProgressDialog() {
        return progressDialog;
    }

    public void changeLanguage(View view) {
        SecurePrefManager securePrefManager = SecurePrefManager.with(this);
        String language = securePrefManager.get(Constant.LANGUAGE).defaultValue(Locale.getDefault().getLanguage()).go();
        String newLang;
        if (language.equalsIgnoreCase("ar")) {
            newLang = "en";
        } else {
            newLang = "ar";
        }
        securePrefManager.set(Constant.LANGUAGE).value(newLang).go();

        PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void showVisitorDialog(OnNotVisitorClick onNotVisitorClick) {
        String role = SecurePrefManager.with(this).get(Constant.ROLL).defaultValue("").go();
        int roleId;
        if (role.equalsIgnoreCase(Constant.USER_ROLE_SELLER)) {
            roleId = 3;
        } else if (role.equalsIgnoreCase(Constant.USER_ROLE_CUSTOMER)) {
            roleId = 2;
        } else {
            roleId = 1;
        }

        if (roleId == 1) {
            if (visitorDialog == null) {
                visitorDialog = new VisitorDialog(this);
            }

            if (!visitorDialog.isShowing()) {
                visitorDialog.show();
            }
        } else {
            if (onNotVisitorClick != null) {
                onNotVisitorClick.onNotVisitorClick(roleId);
            }
        }
    }

    @FunctionalInterface
    public interface OnNotVisitorClick {

        void onNotVisitorClick(int roleId);

    }
}
