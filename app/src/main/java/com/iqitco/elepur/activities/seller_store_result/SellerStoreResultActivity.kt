package com.iqitco.elepur.activities.seller_store_result

import android.graphics.Color
import android.os.Bundle
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.seller_store_result.SellerStoreResultContract
import com.iqitco.elepur_api.contracts.seller_store_result.SellerStoreResultPresenter
import com.iqitco.elepur_api.modules.StoreResult
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.seller_store_result_activity.*
import kotlinx.android.synthetic.main.store_result_emoji_view.view.*
import kotlinx.android.synthetic.main.store_result_info_view.view.*

/**
 * date 8/4/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class SellerStoreResultActivity : BaseActivity(), SellerStoreResultContract.View {

    private lateinit var presenter: SellerStoreResultContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.seller_store_result_activity)
        presenter = SellerStoreResultPresenter(this)
        successOrdersElegoResult.contentTv.setTextColor(Color.WHITE)
        successOrdersElegoResult.contentTv.setBackgroundResource(R.drawable.store_result_bottom_green)
        toolbar.titleTv.text = getString(R.string.title_store_result)
        emojisResult.emojisBar.sadEmoji!!.isSelected = true
        emojisResult.emojisBar.happyEmoji!!.isSelected = true
        emojisResult.emojisBar.loveEmoji!!.isSelected = true
        presenter.onCreate()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun getId(): Int {
        return SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
    }

    override fun publishResult(result: StoreResult) {
        callsResult.contentTv.text = result.calls!!.toString()
        successOrdersResult.contentTv.text = "0"
        viewsResult.contentTv.text = result.seen!!.toString()
        currentProductsResult.contentTv.text = result.adv!!.toString()
        successOrdersElegoResult.contentTv.text = getString(R.string.title_jod_price_f, result.elego!!)
        emojisResult.emojisBar.sadEmoji!!.counterTv.text = result.sad!!.toString()
        emojisResult.emojisBar.happyEmoji!!.counterTv.text = result.happy!!.toString()
        emojisResult.emojisBar.loveEmoji!!.counterTv.text = result.fav!!.toString()
    }
}