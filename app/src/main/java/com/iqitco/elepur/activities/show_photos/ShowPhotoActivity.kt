package com.iqitco.elepur.activities.show_photos

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.chrisbanes.photoview.PhotoView
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.adapters.ViewPagerViewAdapter
import com.iqitco.elepur.items.ShowPhotoItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.modules.FullProduct
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import kotlinx.android.synthetic.main.show_photo_activity.*

/**
 * date 7/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ShowPhotoActivity : BaseActivity(), ViewPager.OnPageChangeListener {

    private val photoListAdapter = FastItemAdapter<ShowPhotoItem>()
    private lateinit var photoPagerAdapter: ViewPagerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_photo_activity)
        val photoList: ArrayList<FullProduct.Pics>
        try {
            photoList = intent.getParcelableArrayListExtra(Constant.PHOTOS)
        } catch (ex: Exception) {
            throw Exception("Photo list cannot be null")
        }

        val position: Int
        try {
            position = intent.getIntExtra(Constant.POSITION, -1)
            if (position == -1) {
                throw Exception("Position cannot be null")
            }
        } catch (ex: Exception) {
            throw Exception("Position cannot be null")
        }

        toolbar.backBtn.setOnClickListener(null)
        toolbar.secondBtn.setImageResource(R.drawable.ic_close_x)
        toolbar.secondBtn.setOnClickListener { onBackPressed() }
        photoPagerAdapter = ViewPagerViewAdapter()
        for (i in 0 until photoList.size) {
            val photo = photoList[i]
            val imageView = PhotoView(this)
            imageView.scaleX = resources.getInteger(R.integer.scale_x).toFloat()
            Glide.with(this)
                    .load(photo.pics)
                    .apply(RequestOptions()
                            .fallback(R.drawable.img_place_holder)
                            .placeholder(R.drawable.img_place_holder)
                            .error(R.drawable.img_place_holder))
                    .into(imageView)
            photoPagerAdapter.add(imageView)
            photoListAdapter.add(ShowPhotoItem(photo.pics!!).withIdentifier(i.toLong()))
        }
        photoListAdapter.withOnClickListener(this::onPhotoItemClick)
        list.adapter = photoListAdapter
        list.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)


        photoViewPager.adapter = photoPagerAdapter
        photoViewPager.isCycle = true
        photoViewPager.currentItem = position
        photoViewPager.addOnPageChangeListener(this)

        toolbar.titleTv.text = getString(R.string.title_photo_count_f, position + 1, photoPagerAdapter.count)
    }

    private fun onPhotoItemClick(view: View?, adapter: IAdapter<ShowPhotoItem>?, item: ShowPhotoItem?, position: Int): Boolean {
        photoViewPager.currentItem = position
        return true
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        toolbar.titleTv.text = getString(R.string.title_photo_count_f, position + 1, photoPagerAdapter.count)
    }
}