package com.iqitco.elepur.activities.products

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.dialogs.DropDownDialog
import com.iqitco.elepur.util.getNumber
import com.iqitco.elepur.widget.SelectImageSquareView
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.products.AddProductContract
import com.iqitco.elepur_api.contracts.products.AddProductPresenter
import com.iqitco.elepur_api.modules.*
import com.iqitco.elepur_api.modules.request.OtherPic
import com.iqitco.elepur_api.modules.request.RequestAddAdv
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.add_product_activity.*

/**
 * date 8/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class AddProductActivity : BaseActivity(), AddProductContract.View {

    private lateinit var presenter: AddProductContract.Presenter
    private lateinit var requestAddAdv: RequestAddAdv
    private var userId = 0
    private var guaranteeTypeId = 0
    private var categoryId = 0
    private var brandId = 0
    private var guaranteeDialog: DropDownDialog? = null

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            enableMainBtn()
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_product_activity)
        userId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(0).go()
        presenter = AddProductPresenter(this)
        presenter.onCreate()
        colorDDL.setDialog(DropDownDialog.Builder(this).withMultiSelect(true).withImages(true).build())
        kindDDL.setDialog(DropDownDialog.Builder(this).withMultiSelect(true).build())
        freeGiftsDDL.setDialog(DropDownDialog.Builder(this).withMultiSelect(true).build())

        guaranteeDDL.onSelect { _, list ->
            guaranteeTypeId = list[0].id.toInt()
            presenter.getGuaranteeList()
        }
        categoryDDL.onSelect { _, list ->
            categoryId = list[0].id.toInt()
            presenter.getBrands()
            presenter.getKind()
            enableMainBtn()
        }

        brandDDL.onSelect { _, list ->
            brandId = list[0].id.toInt()
            presenter.getSubBrands()
            enableMainBtn()
        }

        statsDDL.addItem(DropDownDialog.Item(1, getString(R.string.title_new)))
        statsDDL.addItem(DropDownDialog.Item(2, getString(R.string.title_old)))
        statsDDL.onSelect { _, _ -> enableMainBtn() }
        subBrandDDL.onSelect { _, _ -> enableMainBtn() }
        kindDDL.onSelect { _, _ -> enableMainBtn() }
        colorDDL.onSelect { _, _ -> enableMainBtn() }
        freeGiftsDDL.onSelect { _, _ -> enableMainBtn() }
        paymentDDL.onSelect { _, _ -> enableMainBtn() }
        mainImage.setOnSelectImage { enableMainBtn() }
        otherImage1.setOnSelectImage { enableMainBtn() }
        otherImage2.setOnSelectImage { enableMainBtn() }
        otherImage3.setOnSelectImage { enableMainBtn() }
        otherImage4.setOnSelectImage { enableMainBtn() }
        otherImage5.setOnSelectImage { enableMainBtn() }

        priceEdt.addTextChangedListener(textWatcher)
        spaceEdt.addTextChangedListener(textWatcher)
        quantityEdt.addTextChangedListener(textWatcher)
        descEdt.addTextChangedListener(textWatcher)
        mainBtn.setOnClickListener {
            presenter.addAdv()
        }
        mainBtn.isEnabled = false
    }

    fun enableMainBtn() {
        mainBtn.isEnabled = checkValues()
    }

    private fun checkValues(): Boolean {
        requestAddAdv = RequestAddAdv()

        requestAddAdv.categoryId = categoryDDL.getSelectedId()
        if (requestAddAdv.categoryId == null) {
            return false
        }
        requestAddAdv.brandId = brandDDL.getSelectedId()
        if (requestAddAdv.brandId == null) {
            return false
        }
        requestAddAdv.subBrandId = subBrandDDL.getSelectedId()
        if (requestAddAdv.subBrandId == null) {
            return false
        }
        requestAddAdv.advKindId = kindDDL.getSelectedId()
        if (requestAddAdv.advKindId == null) {
            requestAddAdv.advKindId = 1
        }
        requestAddAdv.price = priceEdt.getNumber()
        if (requestAddAdv.price == null) {
            return false
        }
        if (statsDDL.getSelectedId() == null) {
            return false
        } else {
            requestAddAdv.nO = if (statsDDL.getSelectedId() == 1) "n" else "o"
        }
        if (guaranteeDialog == null) {
            return false
        }
        requestAddAdv.grantyId = guaranteeDialog!!.getSelectedId()
        if (requestAddAdv.grantyId == null) {
            return false
        }
        requestAddAdv.capacity = spaceEdt.getNumber()
        if (requestAddAdv.capacity == null) {
            return false
        }
        requestAddAdv.amount = quantityEdt.getNumber()
        if (requestAddAdv.amount == null) {
            return false
        }
        requestAddAdv.colors = colorDDL.getSelectedIds()
        if (requestAddAdv.colors!!.isEmpty()) {
            return false
        }
        requestAddAdv.freeGift = freeGiftsDDL.getSelectedIds()
        /*if (requestAddAdv.freeGift!!.isEmpty()) {
            return false
        }*/
        requestAddAdv.paymentId = paymentDDL.getSelectedId()
        if (requestAddAdv.paymentId == null) {
            return false
        }
        requestAddAdv.mainPic = mainImage.base64
        if (requestAddAdv.mainPic == null) {
            return false
        }
        requestAddAdv.mainPicExt = "png"

        val otherPic1 = otherImage1.base64 ?: return false
        val otherPic2 = otherImage2.base64 ?: return false

        requestAddAdv.otherPic = arrayListOf()
        requestAddAdv.otherPic!!.add(OtherPic(otherPic1, "png"))
        requestAddAdv.otherPic!!.add(OtherPic(otherPic2, "png"))

        for (otherImage in listOf<SelectImageSquareView>(otherImage3, otherImage4, otherImage5)) {
            val otherPic = otherImage.base64
            if (!otherPic.isNullOrEmpty()) {
                requestAddAdv.otherPic!!.add(OtherPic(otherPic, "png"))
            }
        }

        requestAddAdv.dECS = descEdt.text!!.toString().trim()
        requestAddAdv.sallerId = userId
        return true
    }

    override fun publishCategories(list: List<MainPageSection>) {
        categoryDDL.clearItems()
        for (category in list) {
            val title = if (isLocalAr) category.categoryNameAr else category.categoryName
            categoryDDL.addItem(DropDownDialog.Item(category.id.toLong(), title))
        }
    }

    override fun publishColors(list: List<Color>) {
        colorDDL.clearItems()
        for (color in list) {
            val title = if (isLocalAr) color.nameAr else color.nameAr
            colorDDL.addItem(DropDownDialog.Item(color.id.toLong(), title, color.tag))
        }
    }

    override fun publishBrands(list: List<Brand>) {
        brandDDL.clearItems()
        val set = HashSet<Int>()
        for (brand in list) {
            if (set.add(brand.id)) {
                val title = if (isLocalAr) brand.brandsNameAr else brand.brandsNameEn
                brandDDL.addItem(DropDownDialog.Item(brand.id.toLong(), title))
            }
        }
    }

    override fun publishFreeGifts(list: List<FreeGifts>) {
        freeGiftsDDL.clearItems()
        for (gift in list) {
            val title = if (isLocalAr) gift.kindNameAr else gift.kindNameEn
            freeGiftsDDL.addItem(DropDownDialog.Item(gift.id!!.toLong(), title!!))
        }
    }

    override fun publishGuaranteeType(list: List<GuaranteeType>) {
        guaranteeDDL.clearItems()
        for (guaranteeType in list) {
            val title = if (isLocalAr) guaranteeType.nameAr else guaranteeType.nameEn
            guaranteeDDL.addItem(DropDownDialog.Item(guaranteeType.id!!.toLong(), title!!))
        }
    }

    override fun publishPaymentTypes(list: List<PaymentsType>) {
        paymentDDL.clearItems()
        for (payment in list) {
            val title = if (isLocalAr) payment.nameAr!! else payment.nameEn!!
            paymentDDL.addItem(DropDownDialog.Item(payment.id!!.toLong(), title))
        }
    }

    override fun publishGuarantee(list: List<Guarantee>) {
        if (guaranteeDialog != null && guaranteeDialog!!.isShowing) {
            guaranteeDialog!!.dismiss()
        }
        guaranteeDialog = DropDownDialog.Builder(this).withImages(true).build()
        guaranteeDialog!!.onSelect { s, _ -> guaranteeDDL.setText(s) }
        for (guarantee in list) {
            guaranteeDialog!!.addItem(DropDownDialog.Item(guarantee.id!!.toLong(), guarantee.name!!, guarantee.link!!))
        }
        guaranteeDialog!!.show()
    }

    override fun publishKind(list: List<ProductKind>) {
        kindDDL.clearItems()
        for (kind in list) {
            val title = if (isLocalAr) kind.kindNameAr else kind.kindNameEn
            kindDDL.addItem(DropDownDialog.Item(kind.id.toLong(), title))
        }
        kindDDL.visibility = if (list.isNotEmpty()) View.VISIBLE else View.GONE
    }

    override fun publishSubBrands(list: List<SubBrand>) {
        subBrandDDL.clearItems()
        for (subBrand in list) {
            val title = if (isLocalAr) subBrand.nameAr else subBrand.nameEn
            subBrandDDL.addItem(DropDownDialog.Item(subBrand.id.toLong(), title))
        }
    }

    override fun getGuaranteeTypeId(): Int {
        return guaranteeTypeId
    }

    override fun getCategoryId(): Int {
        return categoryId
    }

    override fun getBrandId(): Int {
        return brandId
    }

    override fun getRequestAddAdv(): RequestAddAdv {
        return requestAddAdv
    }

    override fun onAddSuccess(flag: Boolean) {
        if (flag) {
            showUserMessage(getString(R.string.message_add_adv_done))
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mainImage.onActivityResult(requestCode, resultCode, data)
        otherImage1.onActivityResult(requestCode, resultCode, data)
        otherImage2.onActivityResult(requestCode, resultCode, data)
        otherImage3.onActivityResult(requestCode, resultCode, data)
        otherImage4.onActivityResult(requestCode, resultCode, data)
        otherImage5.onActivityResult(requestCode, resultCode, data)
    }
}