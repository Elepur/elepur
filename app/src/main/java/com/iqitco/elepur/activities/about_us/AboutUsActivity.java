package com.iqitco.elepur.activities.about_us;

import android.os.Bundle;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AboutUsActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us_activity);
    }
}
