package com.iqitco.elepur.activities.home.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.home.BaseHomeFragment

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class MainPageContainerFragment : BaseHomeFragment() {

    private val backStackStates = ArrayList<BackStackStates>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_page_container_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        childFragmentManager.beginTransaction()
                .add(R.id.fragmentContainer2, MainPageFragment())
                .commit()
        for (i in 0 until backStackStates.size) {
            val fragmentState = backStackStates[i]
            val fragment = fragmentState.javaClass.newInstance()
            val method = fragmentState.javaClass.getMethod("setArguments", Bundle::class.java)
            method.invoke(fragment, fragmentState.arguments!!)
            childFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer2, fragment as Fragment)
                    .addToBackStack(fragment.javaClass.simpleName!!)
                    .commit()
        }
    }

    fun returnToHome() {
        childFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        backStackStates.clear()
    }

    fun changeFragment(fragment: Fragment) {
        fragment.arguments
        backStackStates.add(BackStackStates(
                fragment.arguments,
                fragment.javaClass
        ))
        childFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer2, fragment)
                .addToBackStack(fragment.javaClass.simpleName!!)
                .commit()
    }

    fun removeFragment() {
        childFragmentManager.popBackStackImmediate()
        backStackStates.removeAt(backStackStates.size - 1)
    }

    private inner class BackStackStates(
            internal val arguments: Bundle?,
            internal val javaClass: Class<Any>)

}