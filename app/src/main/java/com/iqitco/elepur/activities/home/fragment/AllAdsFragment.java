package com.iqitco.elepur.activities.home.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.home.AllAdsFilterActivity;
import com.iqitco.elepur.activities.home.BaseHomeFragment;
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity;
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.iqitco.elepur.items.ProductItem;
import com.iqitco.elepur.widget.CustomToolbarView;
import com.iqitco.elepur.widget.FilterButtonView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.home.AllAdsContract;
import com.iqitco.elepur_api.contracts.home.AllAdsPresenter;
import com.iqitco.elepur_api.modules.Ads;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;

import java.util.List;
import java.util.Locale;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@SuppressWarnings("unchecked")
public class AllAdsFragment extends BaseHomeFragment implements AllAdsContract.View {

    public static final String TAG = AllAdsFragment.class.getSimpleName();

    private CustomToolbarView toolbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AllAdsContract.Presenter presenter;
    private BaseBottomSheetDialog stateDialog;
    private BaseBottomSheetDialog areaDialog;

    private FilterButtonView selectStateBtn;
    private FilterButtonView selectAreaBtn;
    private FilterButtonView resultFilterBtn;

    //endless scroll
    private FastItemAdapter<ProductItem> adsAdapter = new FastItemAdapter<>();
    private FastItemAdapter<ProductItem> adsAdapterGrid = new FastItemAdapter<>();
    private GridLayoutManager layoutManager;
    private EndlessRecyclerOnScrollListener endlessListener;
    private boolean isGridList = false;

    private Integer stateId;
    private Integer areaId;
    private Integer sortColumn = 0;
    private String sortDirection = "desc";


    private ConstraintLayout listViewBtn;
    private ConstraintLayout sortBtn;
    private RecyclerView recyclerView;

    private BaseBottomSheetDialog sortingDialog;
    private ConstraintLayout filtersLayout;
    private boolean showFilters = true;
    private Context context;
    private AllAdsFilterActivity.Result filterResult = new AllAdsFilterActivity.Result();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_ads_fragment, container, false);
        context = getContext();

        getBaseActivity().changeStatusBarColor(getResources().getColor(R.color.colorPrimary), false);
        filtersLayout = view.findViewById(R.id.filtersLayout);
        filtersLayout.setVisibility(showFilters ? View.VISIBLE : View.GONE);
        listViewBtn = view.findViewById(R.id.listViewBtn);
        sortBtn = view.findViewById(R.id.sortBtn);
        listViewBtn.setOnClickListener(this::changeShow);
        sortBtn.setOnClickListener(this::showSortingDialog);

        toolbar = view.findViewById(R.id.toolbar);
        toolbar.getTitleTv().setText(R.string.title_bottom_nav_all_ads);
        toolbar.getSecondBtn().setImageResource(R.drawable.ic_search);
        if (showFilters) {
            toolbar.getBackBtn().setOnClickListener(this::onMenuClick);
            toolbar.getBackBtn().setImageResource(R.drawable.ic_menu);
        }

        selectStateBtn = view.findViewById(R.id.selectStateBtn);
        selectAreaBtn = view.findViewById(R.id.selectAreaBtn);
        resultFilterBtn = view.findViewById(R.id.resultFilterBtn);

        selectStateBtn.setOnClickListener(v -> stateDialog.show());
        selectAreaBtn.setOnClickListener(v -> areaDialog.show());
        resultFilterBtn.setOnClickListener(v -> startActivityForResult(
                new Intent(getActivity(), AllAdsFilterActivity.class)
                        .putExtra(AllAdsFilterActivity.RESULTS, filterResult),
                AllAdsFilterActivity.REQUEST_CODE)
        );
        selectAreaBtn.setEnabled(false);

        initSwipeRefresh(view);

        recyclerView = view.findViewById(R.id.list);

        if (isGridList) {
            layoutManager = new GridLayoutManager(getContext(), 2);
            listViewBtn.animate().rotation(90).start();
            recyclerView.setAdapter(adsAdapterGrid);
        } else {
            layoutManager = new GridLayoutManager(getContext(), 1);
            recyclerView.setAdapter(adsAdapter);
        }
        recyclerView.setLayoutManager(layoutManager);

        if (presenter == null) {
            presenter = new AllAdsPresenter(this);
            presenter.onCreate();
            endlessListener = new EndlessRecyclerOnScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int currentPage) {
                    presenter.getAllAds(currentPage + 1);
                }
            };
        }

        adsAdapter.withOnClickListener(this::onItemClick);
        adsAdapterGrid.withOnClickListener(this::onItemClick);
        recyclerView.addOnScrollListener(endlessListener);
        endlessListener.enable();
        return view;
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

    public boolean onItemClick(View v, IAdapter<ProductItem> adapter, ProductItem item, int position) {
        startActivity(
                new Intent(getActivity(), ShowAdsActivity.class)
                .putExtra(Constant.ADV_ID, item.getO().getIdAdv())
                .putExtra(Constant.SELLER_ID, item.getO().getShopId())
        );
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == AllAdsFilterActivity.REQUEST_CODE) {
            if (data.getExtras() != null) {
                filterResult = data.getExtras().getParcelable(AllAdsFilterActivity.RESULTS);
                endlessListener.disable();
                endlessListener.resetPageCount();
            }
        }
    }

    private void showSortingDialog(View view) {
        if (sortingDialog == null) {
            sortingDialog = new BaseBottomSheetDialog(getContext()) {
                @Override
                protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                    adapter.add(new BottomDialogItem1(getString(R.string.title_sorting_new_old)).withIdentifier(1));
                    adapter.add(new BottomDialogItem1(getString(R.string.title_sorting_old_new)).withIdentifier(2));
                    adapter.add(new BottomDialogItem1(getString(R.string.title_sorting_high_low_price)).withIdentifier(3));
                    adapter.add(new BottomDialogItem1(getString(R.string.title_sorting_low_high_price)).withIdentifier(4));
                    adapter.add(new BottomDialogItem1(getString(R.string.title_sorting_high_fav)).withIdentifier(5));
                }
            };
            sortingDialog.setOnMainButtonClick(this::onSortingSelect);
            sortingDialog.setOnNonSelect(this::onSortingSelectNon);
        }

        if (!sortingDialog.isShowing()) {
            sortingDialog.show();
        }
    }

    private void onSortingSelectNon() {
        sortColumn = 0;
        sortDirection = "desc";
        sortingDialog.dismiss();
        endlessListener.disable();
        endlessListener.resetPageCount();
    }

    private void onSortingSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        long id = item.getIdentifier();
        if (id == 1) {
            sortColumn = 0;
            sortDirection = "desc";
        } else if (id == 2) {
            sortColumn = 0;
            sortDirection = "asc";
        } else if (id == 3) {
            sortColumn = 1;
            sortDirection = "desc";
        } else if (id == 4) {
            sortColumn = 1;
            sortDirection = "asc";
        } else if (id == 5) {
            sortColumn = 3;
            sortDirection = "desc";
        } else {
            sortColumn = 0;
            sortDirection = "desc";
        }
        sortingDialog.dismiss();
        endlessListener.disable();
        endlessListener.resetPageCount();
    }

    private void changeShow(View view) {
        recyclerView.getRecycledViewPool().clear();
        if (layoutManager.getSpanCount() == 1) {
            layoutManager.setSpanCount(2);
            listViewBtn.animate().rotation(90).start();
            isGridList = true;
            recyclerView.swapAdapter(adsAdapterGrid, false);
            adsAdapterGrid.notifyAdapterDataSetChanged();
            adsAdapterGrid.notifyDataSetChanged();
        } else {
            layoutManager.setSpanCount(1);
            listViewBtn.animate().rotation(0).start();
            isGridList = false;
            recyclerView.swapAdapter(adsAdapter, false);
            adsAdapter.notifyAdapterDataSetChanged();
            adsAdapter.notifyDataSetChanged();
        }

    }

    private void initSwipeRefresh(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            endlessListener.disable();
            endlessListener.resetPageCount();
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.accent);
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void publishAdv(List<Ads> list, int pageNumber) {
        if (pageNumber == 1) {
            adsAdapterGrid.clear();
            adsAdapter.clear();
        }
        for (Ads ads : list) {
            adsAdapterGrid.add(new ProductItem(ads, 2).withIdentifier(ads.getRowNum()));
            adsAdapter.add(new ProductItem(ads, 1).withIdentifier(ads.getRowNum()));
        }
        if (pageNumber == 1) {
            endlessListener.enable();
        }
    }

    @Override
    public void publishState(List<State> list) {
        stateDialog = new BaseBottomSheetDialog(getContext()) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (State state : list) {
                    String title;
                    if (Locale.getDefault().toString().equals("ar")) {
                        title = state.getNameStateAr();
                    } else {
                        title = state.getNameStateEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(state.getId()));
                }
            }
        };
        stateDialog.setOnMainButtonClick(this::onStateSelect);
        stateDialog.setOnNonSelect(this::onNonStateSelect);
    }

    private void onStateSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        stateId = (int) item.getIdentifier();
        presenter.getAreas();
        stateDialog.dismiss();
        selectAreaBtn.setEnabled(true);
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllAds(1);
    }

    private void onNonStateSelect() {
        stateId = 0;
        areaId = 0;
        selectAreaBtn.setEnabled(false);
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllAds(1);
        stateDialog.dismiss();
    }

    @Override
    public void publishArea(List<StateArea> list) {
        areaDialog = new BaseBottomSheetDialog(getContext()) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (StateArea area : list) {
                    String title;
                    if (Locale.getDefault().toString().equals("ar")) {
                        title = area.getNameAreaAr();
                    } else {
                        title = area.getNameAreaEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(area.getId()));
                }
            }
        };
        areaDialog.setOnMainButtonClick(this::onAreaSelect);
        areaDialog.setOnNonSelect(this::onNonAreaSelect);
    }

    private void onAreaSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        areaId = (int) item.getIdentifier();
        areaDialog.dismiss();
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllAds(1);
    }

    private void onNonAreaSelect() {
        areaId = 0;
        areaDialog.dismiss();
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllAds(1);
    }

    @Override
    public Integer getStateId() {
        return stateId;
    }

    @Override
    public Integer getAreaId() {
        return areaId;
    }

    @Override
    public Integer getSortColumn() {
        return sortColumn;
    }

    @Override
    public String getSortDirection() {
        return sortDirection;
    }

    @Override
    public List<Integer> getBrandsIds() {
        return filterResult.getBrandsIds();
    }

    @Override
    public List<Integer> getSubBrands() {
        return filterResult.getSubBrandsIds();
    }

    @Override
    public List<Integer> getColors() {
        return filterResult.getColorsIds();
    }

    @Override
    public List<Integer> getKind() {
        return filterResult.getKindsIds();
    }

    @Override
    public String getNewOldFlag() {
        return filterResult.getNewOldFlag();
    }

    @Override
    public Boolean getElegoFlag() {
        return filterResult.isElegoFlag();
    }

    @Override
    public Integer getLowestPrice() {
        return filterResult.getPriceStart();
    }

    @Override
    public Integer getHighestPrice() {
        return filterResult.getPriceEnd();
    }

    public boolean isShowFilters() {
        return showFilters;
    }

    public void setShowFilters(boolean showFilters) {
        this.showFilters = showFilters;
    }
}
