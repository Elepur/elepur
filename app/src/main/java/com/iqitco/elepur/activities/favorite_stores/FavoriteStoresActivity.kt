package com.iqitco.elepur.activities.favorite_stores

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.items.StoreItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.favorite_stores.FavoriteStoresContract
import com.iqitco.elepur_api.contracts.favorite_stores.FavoriteStoresPresenter
import com.iqitco.elepur_api.modules.Seller
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.favorite_stores_activity.*

/**
 * date: 2018-07-05
 *
 * @author Mohammad Al-Najjar
 */
class FavoriteStoresActivity : BaseActivity(), FavoriteStoresContract.View {

    private val adapter = FastItemAdapter<StoreItem>()
    private lateinit var endlessListener: EndlessRecyclerOnScrollListener
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var presenter: FavoriteStoresContract.Presenter
    private var customerId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.favorite_stores_activity)

        customerId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()

        initSwipeRefresh()

        layoutManager = LinearLayoutManager(this)
        list.layoutManager = layoutManager
        list.adapter = adapter

        presenter = FavoriteStoresPresenter(this)
        presenter.onCreate()
        endlessListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                presenter.getCustomerFavSellers(currentPage)
            }
        }

        list.addOnScrollListener(endlessListener)
        endlessListener.enable()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    private fun initSwipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener {
            endlessListener.disable()
            endlessListener.resetPageCount()
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.accent)
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun getId(): Int {
        return customerId
    }

    override fun publishSellers(list: List<Seller>, pageNumber: Int) {
        if (pageNumber == 1) {
            adapter.clear()
        }
        for (seller in list) {
            adapter.add(
                    StoreItem(seller)
                            .withIdentifier(seller.rowNum.toLong())
            )
        }
        if (pageNumber == 1) {
            endlessListener.enable()
        }

        if (adapter.itemCount > 0) {
            emptyLayout.visibility = View.INVISIBLE
        } else {
            emptyLayout.visibility = View.VISIBLE
        }

    }
}