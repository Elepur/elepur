package com.iqitco.elepur.activities.blocked_account;

import android.os.Bundle;
import android.view.View;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;

/**
 * date: 2018-06-13
 *
 * @author Mohammad Al-Najjar
 */
public class BlockedAccountActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blocked_account_activity);
        changeStatusBarColor(getResources().getColor(R.color.clockRed), false);
        findViewById(R.id.logoutBtn).setOnClickListener(this::onLogoutClick);
    }

    private void onLogoutClick(View view) {
        getElepurApp().logoutUser(this);
    }
}
