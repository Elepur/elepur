package com.iqitco.elepur.activities.notification

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.util.DateUtility
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.notification.ShowNotificationContract
import com.iqitco.elepur_api.contracts.notification.ShowNotificationPresenter
import com.iqitco.elepur_api.modules.UserNotification
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.show_notification_activity.*

/**
 * date: 2018-08-02
 *
 * @author Mohammad Al-Najjar
 */
class ShowNotificationActivity : BaseActivity(), ShowNotificationContract.View {

    private lateinit var role: String
    private var userId: Int = 0
    private lateinit var notification: UserNotification
    private lateinit var presenter: ShowNotificationContract.Presenter
    private var confirmDeleteDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            notification = intent.getParcelableExtra(Constant.NOTIFICATION_ID)
        } catch (ex: Exception) {
            throw Exception("Notification id cannot be null")
        }
        setContentView(R.layout.show_notification_activity)
        toolbar.titleTv.text = getString(R.string.title_profile_notification)
        role = SecurePrefManager.with(context).get(Constant.ROLL).defaultValue("").go()
        userId = SecurePrefManager.with(context).get(Constant.ID_ID).defaultValue(0).go()

        try {
            var resId = 0
            when (notification.type!!) {
                1 -> resId = R.drawable.ic_notification_elego;
                2 -> resId = R.drawable.ic_profile_elego
                3 -> resId = R.drawable.ic_profile_notification
            }
            image.setImageResource(resId);
        } catch (ex: Exception) {
            /*NOTHING*/
        }
        headerTv.text = notification.header
        textTv.text = notification.message
        dateTv.text = DateUtility.formatNotificationDate(notification.date)
        presenter = ShowNotificationPresenter(this)
        if (notification.seen == 0) {
            presenter.updateSeenNotification()
        }

        deleteBtn.setOnClickListener(this::showDeleteDialog)

        deleteBtn.visibility = if (notification.typeCsv != null && notification.typeCsv == 0) View.VISIBLE else View.GONE
    }

    private fun showDeleteDialog(view: View) {
        if (confirmDeleteDialog == null) {
            confirmDeleteDialog = MaterialDialog.Builder(this)
                    .content(R.string.message_confirm_delete_notification)
                    .positiveText(R.string.btn_delete)
                    .negativeText(R.string.btn_cancel)
                    .positiveColor(Color.WHITE)
                    .negativeColor(Color.WHITE)
                    .contentColor(Color.WHITE)
                    .autoDismiss(false)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .backgroundColor(resources.getColor(R.color.border))
                    .onPositive { _, _ -> presenter.deleteNotification() }
                    .onNegative { dialog, _ -> dialog.dismiss() }
                    .build()
        }

        if (!confirmDeleteDialog!!.isShowing) {
            confirmDeleteDialog!!.show()
        }
    }

    override fun getNotificationId(): Int {
        return notification.idN!!
    }

    override fun getUserId(): Int {
        return userId
    }

    override fun getRole(): String? {
        return role
    }

    override fun onDeleteSuccess(flag: Boolean) {
        confirmDeleteDialog!!.dismiss()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onUpdateSuccess(flag: Boolean) {

    }
}