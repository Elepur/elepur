package com.iqitco.elepur.activities.landing_activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.iqitco.elepur.BuildConfig;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.home.HomeSellerActivity;
import com.iqitco.elepur.activities.login.LoginActivity;
import com.iqitco.elepur.dialogs.RegistrationDialog;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.login_vistor.LoginVisitorContract;
import com.iqitco.elepur_api.contracts.login_vistor.LoginVisitorPresenter;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

/**
 * date 5/26/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class LandingActivity extends BaseActivity implements LoginVisitorContract.View {

    private RegistrationDialog dialog;
    private LoginVisitorContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_activity);
        presenter = new LoginVisitorPresenter(this);
        TextView versionTv = findViewById(R.id.versionTv);
        versionTv.setText(BuildConfig.VERSION_NAME);
        findViewById(R.id.skipBtn).setOnClickListener(this::onSkipBtnClick);
        findViewById(R.id.registrationBtn).setOnClickListener(this::onRegistrationBtnClick);
        findViewById(R.id.loginBtn).setOnClickListener(this::onLoginBtnClick);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void onLoginBtnClick(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void onRegistrationBtnClick(View view) {
        dialog = new RegistrationDialog(LandingActivity.this);
        dialog.show();
    }

    private void onSkipBtnClick(View view) {
        presenter.loginVisitor();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        dialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public String getNotificationId() {
        return SecurePrefManager.with(this).get(Constant.NOTIFICATION_ID).defaultValue("").go();
    }

    @Override
    public void onSuccess(boolean flag) {
        if (flag) {
            SecurePrefManager.with(this).set(Constant.IS_VISITOR).value(true).go();
            finish();
            startActivity(new Intent(this, HomeSellerActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}
