package com.iqitco.elepur.activities.delivery_info;

import android.os.Bundle;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.widget.CustomToolbarView;
import com.iqitco.elepur_api.Constant;

/**
 * date 6/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class DeliveryInfoActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_info_activity);
        String title = getIntent().getStringExtra(Constant.TITLE);
        if (title != null){
            CustomToolbarView toolbarView = findViewById(R.id.toolbar);
            toolbarView.getTitleTv().setText(title);
        }
    }
}
