package com.iqitco.elepur.activities.reports_adv

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.reports_adv.ReportsAdvContract
import com.iqitco.elepur_api.contracts.reports_adv.ReportsAdvPresenter
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.reports_adv_activity.*

/**
 * date 7/22/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ReportsAdvActivity : BaseActivity(), ReportsAdvContract.View {

    private var advId: Int? = null
    private var customerId: Int? = null
    private lateinit var presenter: ReportsAdvContract.Presenter
    private val onTextChange = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            mainBtn.isEnabled = titleEdt.text.toString().isNotEmpty() && opinionEdt.text.toString().isNotEmpty()
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ReportsAdvPresenter(this)
        setContentView(R.layout.reports_adv_activity)
        try {
            advId = intent.getIntExtra(Constant.ADV_ID, -1)
            if (advId == -1) {
                throw Exception("advId cannot be null")
            }
        } catch (ex: Exception) {
            throw Exception("advId cannot be null")
        }
        customerId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
        titleEdt.addTextChangedListener(onTextChange)
        opinionEdt.addTextChangedListener(onTextChange)
        mainBtn.setOnClickListener {
            presenter.reportAdv()
        }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun getHeader(): String {
        return titleEdt.text.toString().trim()
    }

    override fun getMessage(): String {
        return opinionEdt.text.toString().trim()
    }

    override fun getAdvId(): Int {
        return advId!!
    }

    override fun getCustomerId(): Int {
        return customerId!!
    }

    override fun onReportSuccess() {
        Toast.makeText(this, R.string.message_report_send, Toast.LENGTH_LONG).show()
        finish()
    }
}