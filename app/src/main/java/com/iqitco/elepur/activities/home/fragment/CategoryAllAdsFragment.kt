package com.iqitco.elepur.activities.home.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.home.AllAdsFilterActivity
import com.iqitco.elepur.activities.home.BaseHomeFragment
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog
import com.iqitco.elepur.items.BottomDialogItem1
import com.iqitco.elepur.items.ProductItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.home.CategoryAllAdsContract
import com.iqitco.elepur_api.contracts.home.CategoryAllAdsPresenter
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.all_ads_fragment.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class CategoryAllAdsFragment : BaseHomeFragment(), CategoryAllAdsContract.View {

    companion object {
        fun newInstance(categoryId: Int, brandsList: ArrayList<Int>): Fragment {
            val fragment = CategoryAllAdsFragment()
            fragment.arguments = Bundle()
            fragment.arguments!!.putInt(Constant.CATEGORY_ID, categoryId)
            fragment.arguments!!.putIntegerArrayList(Constant.BRANDS_LIST, brandsList)
            return fragment
        }
    }

    private lateinit var presenter: CategoryAllAdsContract.Presenter

    private var stateDialog: BaseBottomSheetDialog? = null
    private var areaDialog: BaseBottomSheetDialog? = null
    private var sortingDialog: BaseBottomSheetDialog? = null

    private val adsAdapter = FastItemAdapter<ProductItem>()
    private val adsAdapterGrid = FastItemAdapter<ProductItem>()
    private lateinit var layoutManager: GridLayoutManager
    private var endlessListener: EndlessRecyclerOnScrollListener? = null
    private var isGridList = false

    private var stateId: Int = 0
    private var areaId: Int = 0
    private var sortColumn: Int = 0
    private var sortDirectionValue = "desc"
    private var filterResult = AllAdsFilterActivity.Result()

    private var categoryId: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            categoryId = arguments!!.getInt(Constant.CATEGORY_ID)
        } catch (ex: Exception) {
            throw Exception("categoryId cannot be null")
        }
        try {
            val listParcelable = arguments!!.getIntegerArrayList(Constant.BRANDS_LIST)!!
            filterResult.brandsIds = ArrayList<Int>()
            for (item in listParcelable) {
                filterResult.brandsIds.add(item)
            }
        } catch (ex: Exception) {
            throw Exception("brand list cannot be null", ex)
        }
        return inflater.inflate(R.layout.all_ads_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.titleTv.text = getString(R.string.title_bottom_nav_all_ads)
        if (isGridList) {
            layoutManager = GridLayoutManager(context, 2)
            listViewBtn.animate().rotation(90f).start()
            list.adapter = adsAdapterGrid
        } else {
            layoutManager = GridLayoutManager(context, 1)
            list.adapter = adsAdapter
        }
        list.layoutManager = layoutManager

        listViewBtn.setOnClickListener(this::changeShow)
        sortBtn.setOnClickListener(this::showSortDialog)
        selectStateBtn.setOnClickListener { stateDialog!!.show() }
        selectAreaBtn.setOnClickListener { areaDialog!!.show() }
        resultFilterBtn.setOnClickListener {
            startActivityForResult(
                    Intent(context!!, AllAdsFilterActivity::class.java)
                            .putExtra(AllAdsFilterActivity.RESULTS, filterResult),
                    AllAdsFilterActivity.REQUEST_CODE
            )
        }
        selectAreaBtn.isEnabled = false
        swipeRefreshLayout.setOnRefreshListener {
            endlessListener!!.disable()
            endlessListener!!.resetPageCount()
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.accent)

        presenter = CategoryAllAdsPresenter(this)
        presenter.onCreate()
        endlessListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                presenter.getAllAds(currentPage + 1)
            }
        }

        adsAdapter.withOnClickListener(this::onItemClick)
        adsAdapterGrid.withOnClickListener(this::onItemClick)

        list.addOnScrollListener(endlessListener as EndlessRecyclerOnScrollListener)
        endlessListener!!.enable()
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    private fun onItemClick(v: View?, adapter: IAdapter<ProductItem>?, item: ProductItem?, position: Int?): Boolean {
        startActivity(
                Intent(activity, ShowAdsActivity::class.java)
                        .putExtra(Constant.ADV_ID, item!!.o.idAdv)
                        .putExtra(Constant.SELLER_ID, item.o.shopId)

        )
        return true
    }

    private fun changeShow(view: View) {
        list.recycledViewPool.clear()
        if (layoutManager.spanCount == 1) {
            layoutManager.spanCount = 2
            listViewBtn.animate().rotation(90F).start()
            isGridList = true
            list.swapAdapter(adsAdapterGrid, false)
        } else {
            layoutManager.spanCount = 1
            listViewBtn.animate().rotation(0F).start()
            isGridList = false
            list.swapAdapter(adsAdapter, false)
        }
        (list.adapter as FastItemAdapter<ProductItem>).notifyAdapterDataSetChanged()
        (list.adapter as FastItemAdapter<ProductItem>).notifyDataSetChanged()
    }

    private fun showSortDialog(view: View) {
        if (sortingDialog == null) {
            sortingDialog = object : BaseBottomSheetDialog(context!!) {
                override fun initItems(context: Context, adapter: FastItemAdapter<BottomDialogItem1>) {
                    adapter.add(BottomDialogItem1(getString(R.string.title_sorting_new_old)).withIdentifier(1))
                    adapter.add(BottomDialogItem1(getString(R.string.title_sorting_old_new)).withIdentifier(2))
                    adapter.add(BottomDialogItem1(getString(R.string.title_sorting_high_low_price)).withIdentifier(3))
                    adapter.add(BottomDialogItem1(getString(R.string.title_sorting_low_high_price)).withIdentifier(4))
                    adapter.add(BottomDialogItem1(getString(R.string.title_sorting_high_fav)).withIdentifier(5))
                }
            }
            sortingDialog!!.setOnMainButtonClick(this::onSortSelect)
            sortingDialog!!.setOnNonSelect(this::onSortNonSelect)
        }


        if (!sortingDialog!!.isShowing) {
            sortingDialog!!.show()
        }
    }

    private fun onSortNonSelect() {
        sortColumn = 0
        sortDirectionValue = "desc"
        sortingDialog!!.dismiss()
        endlessListener!!.disable()
        endlessListener!!.resetPageCount()
    }

    private fun onSortSelect(it: List<BottomDialogItem1>) {
        val item = it[0]
        val id = item.identifier
        when (id) {
            1L -> {
                sortColumn = 0
                sortDirectionValue = "desc"
            }
            2L -> {
                sortColumn = 0
                sortDirectionValue = "asc"
            }
            3L -> {
                sortColumn = 1
                sortDirectionValue = "desc"
            }
            4L -> {
                sortColumn = 1
                sortDirectionValue = "asc"
            }
            5L -> {
                sortColumn = 3
                sortDirectionValue = "desc"
            }
            else -> {
                sortColumn = 0
                sortDirectionValue = "desc"
            }
        }
        sortingDialog!!.dismiss()
        endlessListener!!.disable()
        endlessListener!!.resetPageCount()
    }

    override fun onDestroyView() {
        presenter.onDestroy()
        super.onDestroyView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == AllAdsFilterActivity.REQUEST_CODE) {
            if (data!!.extras != null) {
                filterResult = data.extras!!.getParcelable(AllAdsFilterActivity.RESULTS)
                endlessListener!!.disable()
                endlessListener!!.resetPageCount()
            }
        }
    }

    override fun publishAds(list: List<Ads>, pageNumber: Int) {
        if (pageNumber == 1) {
            adsAdapterGrid.clear()
            adsAdapter.clear()
        }
        for (ads in list) {
            adsAdapterGrid.add(ProductItem(ads, 2).withIdentifier(ads.rowNum!!.toLong()))
            adsAdapter.add(ProductItem(ads, 1).withIdentifier(ads.rowNum!!.toLong()))
        }
        if (pageNumber == 1) {
            endlessListener!!.enable()
        }
    }

    override fun publishStates(list: List<State>) {
        stateDialog = object : BaseBottomSheetDialog(context!!) {
            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>) {
                for (state in list) {
                    val title = if (Locale.getDefault().toString().equals("ar", true)) state.nameStateAr else state.nameStateEn
                    adapter.add(BottomDialogItem1(title).withIdentifier(state.id.toLong()))
                }
            }
        }
        stateDialog!!.setOnMainButtonClick(this::onStateSelect)
        stateDialog!!.setOnNonSelect(this::onStateNonSelect)
    }

    private fun onStateNonSelect() {
        stateId = 0
        areaId = 0
        selectAreaBtn.isEnabled = false
        endlessListener!!.disable()
        endlessListener!!.resetPageCount()
        presenter.getAllAds(1)
        stateDialog!!.dismiss()
    }

    private fun onStateSelect(it: List<BottomDialogItem1>) {
        val item = it[0]
        stateId = item.identifier.toInt()
        presenter.getAreas()
        stateDialog!!.dismiss()
        selectAreaBtn.isEnabled = true
        endlessListener!!.disable()
        endlessListener!!.resetPageCount()
        presenter.getAllAds(1)
    }

    override fun publishArea(list: List<StateArea>) {
        areaDialog = object : BaseBottomSheetDialog(context!!) {
            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>) {
                for (area in list) {
                    adapter.add(BottomDialogItem1(
                            if (Locale.getDefault().toString().equals("ar", true)) area.nameAreaAr else area.nameAreaEn
                    ).withIdentifier(area.id.toLong()))
                }
            }
        }
        areaDialog!!.setOnMainButtonClick(this::onAreaSelect)
        areaDialog!!.setOnNonSelect(this::onAreaNonSelect)
    }

    private fun onAreaNonSelect() {
        areaId = 0
        areaDialog!!.dismiss()
        endlessListener!!.disable()
        endlessListener!!.resetPageCount()
        presenter.getAllAds(1)
    }

    private fun onAreaSelect(it: List<BottomDialogItem1>) {
        val item = it[0]
        areaId = item.identifier.toInt()
        areaDialog!!.dismiss()
        endlessListener!!.disable()
        endlessListener!!.resetPageCount()
        presenter.getAllAds(1)
    }

    override fun getStateId(): Int {
        return stateId
    }

    override fun getAreaId(): Int {
        return areaId
    }

    override fun getSortColumn(): Int {
        return sortColumn
    }

    override fun getSortDirection(): String {
        return sortDirectionValue
    }

    override fun getBrandsIds(): List<Int> {
        return filterResult.brandsIds
    }

    override fun getSubBrands(): List<Int> {
        return filterResult.subBrandsIds
    }

    override fun getColors(): List<Int> {
        return filterResult.colorsIds
    }

    override fun getKind(): List<Int> {
        return filterResult.kindsIds
    }

    override fun getAdvDepartment(): Int {
        return categoryId!!
    }

    override fun getNewOldFlag(): String? {
        return filterResult.newOldFlag
    }

    override fun getElegoFlag(): Boolean {
        return filterResult.isElegoFlag
    }

    override fun getLowestPrice(): Int {
        return filterResult.priceStart
    }

    override fun getHighestPrice(): Int {
        return filterResult.priceEnd
    }
}