package com.iqitco.elepur.activities.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.reset_password.ResetPasswordActivity;
import com.iqitco.elepur.dialogs.RegistrationDialog;
import com.iqitco.elepur.widget.CustomEditTextGrayView;
import com.iqitco.elepur.widget.FacebookBtnView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.login.LoginContract;
import com.iqitco.elepur_api.contracts.login.LoginPresenter;
import com.iqitco.elepur_api.modules.UserData;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import org.json.JSONObject;

/**
 * date 6/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class LoginActivity extends BaseActivity implements LoginContract.View, FacebookCallback<LoginResult> {

    public static final String TAG = LoginActivity.class.getSimpleName();

    private LoginContract.Presenter presenter;
    private CustomEditTextGrayView emailEdt;
    private CustomEditTextGrayView passwordEdt;
    private Button loginBtn;
    private FacebookBtnView facebookBtn;
    private TextView forgetPasswordBtn;
    private TextView createNewAccountBtn;
    private RegistrationDialog registrationDialog;

    private AccessToken facebookAccessToken;
    private CallbackManager callbackManager;

    private String email;
    private String name;
    private String picBase64;
    private String gender;
    private String facebookId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        presenter = new LoginPresenter(this);
        emailEdt = findViewById(R.id.emailEdt);
        passwordEdt = findViewById(R.id.passwordEdt);
        loginBtn = findViewById(R.id.loginBtn);
        facebookBtn = findViewById(R.id.facebookBtn);
        forgetPasswordBtn = findViewById(R.id.forgetPasswordBtn);
        createNewAccountBtn = findViewById(R.id.createNewAccountBtn);

        registrationDialog = new RegistrationDialog(this);

        createNewAccountBtn.setOnClickListener(v -> registrationDialog.show());
        forgetPasswordBtn.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class)));
        loginBtn.setOnClickListener(v -> {
            email = emailEdt.getText().toString().trim();
            presenter.login();
        });

        callbackManager = new CallbackManagerImpl();
        facebookBtn.getFacebookBtn().registerCallback(callbackManager, this);
        LoginManager.getInstance().logOut();
        /**/

    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /**
     * FacebookCallback
     */
    @Override
    public void onSuccess(LoginResult loginResult) {
        facebookAccessToken = loginResult.getAccessToken();
        getUserProfile();
    }

    /**
     * FacebookCallback
     */
    @Override
    public void onCancel() {

    }

    /**
     * FacebookCallback
     */
    @Override
    public void onError(FacebookException error) {

    }

    /**
     * FacebookCallback
     */
    private void getUserProfile() {
        GraphRequest request = GraphRequest.newMeRequest(facebookAccessToken, this::onFacebookGetDataCompleted);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender, cover, picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * FacebookCallback
     */
    public void onFacebookGetDataCompleted(JSONObject object, GraphResponse response) {
        try {
            name = object.getString("name");
        } catch (Exception e) {
            /*NOTHING*/
        }

        try {
            facebookId = object.getString("id");
        } catch (Exception e) {
            /*NOTHING*/
        }

        try {
            email = object.getString("email");
        } catch (Exception e) {
            /*NOTHING*/
        }

        try {
            String gender = object.getString("gender");
            if (gender.equalsIgnoreCase("male")) {
                this.gender = "M";
            } else {
                this.gender = "F";
            }
        } catch (Exception e) {
            /*NOTHING*/
        }

        try {
            JSONObject picture = object.getJSONObject("picture");
            JSONObject data = picture.getJSONObject("data");
            picBase64 = data.getString("url");
            Log.d(TAG, String.format("facebook pic: %s", picBase64));
        } catch (Exception e) {
            /*NOTHING*/
        }

        SecurePrefManager securePrefManager = SecurePrefManager.with(this);
        securePrefManager.set(Constant.IS_FACEBOOK_USER).value(true).go();
        securePrefManager.set(Constant.FACEBOOK_ID).value(facebookId).go();
        securePrefManager.set(Constant.FACEBOOK_USER_AUTH).value(facebookAccessToken.getToken()).go();

        presenter.loginFacebook();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        registrationDialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public String getEmail() {
        if (email != null) {
            return email.toLowerCase();
        }
        return "";
    }

    @Override
    public void errorEmail(boolean flag) {
        emailEdt.showError(flag);
    }

    @Override
    public String getPassword() {
        return passwordEdt.getText().toString().trim();
    }

    @Override
    public void errorPassword(boolean flag) {
        passwordEdt.showError(flag);
    }

    @Override
    public String getNotificationId() {
        return SecurePrefManager.with(this).get(Constant.NOTIFICATION_ID).defaultValue("").go();
    }

    @Override
    public void publishData(UserData body) {
        getElepurApp().loginUser(this, body);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public String getFacebookId() {
        return facebookId;
    }

    @Override
    public String getPic() {
        return picBase64;
    }
}
