package com.iqitco.elepur.activities.home.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.home.BaseHomeFragment;
import com.iqitco.elepur.activities.show_seller.ShowSellerActivity;
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.iqitco.elepur.items.StoreItem;
import com.iqitco.elepur.util.AnimationUtil;
import com.iqitco.elepur.widget.CustomToolbarView;
import com.iqitco.elepur.widget.FilterButtonView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.home.AllStoreContract;
import com.iqitco.elepur_api.contracts.home.AllStorePresenter;
import com.iqitco.elepur_api.modules.Seller;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;

import java.util.List;
import java.util.Locale;

/**
 * date 6/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AllStoresFragment extends BaseHomeFragment implements AllStoreContract.View {

    public static final String TAG = AllStoresFragment.class.getSimpleName();

    private CustomToolbarView toolbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FilterButtonView selectStateBtn;
    private FilterButtonView selectAreaBtn;
    private FilterButtonView resultFilterBtn;
    private ConstraintLayout searchLayout;
    private EditText searchEdt;

    private FastItemAdapter<StoreItem> adapter = new FastItemAdapter<>();
    private EndlessRecyclerOnScrollListener endlessListener;
    private LinearLayoutManager layoutManager;

    private BaseBottomSheetDialog stateDialog;
    private BaseBottomSheetDialog areaDialog;

    private AllStoreContract.Presenter presenter;

    private Integer stateId = 0;
    private Integer areaId = 0;

    private boolean isSearchShowing = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_stores_fragment, container, false);
        getHomeSellerActivity().changeStatusBarColor(getResources().getColor(R.color.colorPrimary), false);
        searchLayout = view.findViewById(R.id.searchLayout);
        searchEdt = view.findViewById(R.id.searchEdt);
        view.findViewById(R.id.clearBtn).setOnClickListener(v -> searchEdt.setText(""));
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.getTitleTv().setText(R.string.title_bottom_nav_all_stores);
        toolbar.getBackBtn().setOnClickListener(this::onMenuClick);
        toolbar.getBackBtn().setImageResource(R.drawable.ic_menu);
        toolbar.getSecondBtn().setImageResource(R.drawable.ic_search);
        toolbar.getSecondBtn().setOnClickListener(this::onSearchClick);
        selectStateBtn = view.findViewById(R.id.selectStateBtn);
        selectAreaBtn = view.findViewById(R.id.selectAreaBtn);
        resultFilterBtn = view.findViewById(R.id.resultFilterBtn);
        resultFilterBtn.setOnClickListener(this::onSearchClick);
        selectStateBtn.setOnClickListener(v -> stateDialog.show());
        selectAreaBtn.setOnClickListener(v -> areaDialog.show());
        selectAreaBtn.setEnabled(false);

        initSwipeRefresh(view);

        RecyclerView recyclerView = view.findViewById(R.id.list);
        layoutManager = new LinearLayoutManager(getContext());
        adapter.withOnClickListener((a, b, i, d) -> {
            startActivity(
                    new Intent(getContext(), ShowSellerActivity.class).putExtra(Constant.SELLER_ID, i.getSeller().getIdSeller())
            );
            return true;
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        if (presenter == null) {
            presenter = new AllStorePresenter(this);
            presenter.onCreate();
        }

        endlessListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                presenter.getAllSellers(currentPage + 1);
            }
        };
        recyclerView.addOnScrollListener(endlessListener);
        endlessListener.enable();

        searchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                endlessListener.resetPageCount();
            }
        });
        return view;
    }

    private void onSearchClick(View view) {
        if (isSearchShowing) {
            AnimationUtil.collapse(searchLayout);
            isSearchShowing = false;
        } else {
            AnimationUtil.expand(searchLayout);
            isSearchShowing = true;
        }
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private void initSwipeRefresh(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            endlessListener.disable();
            endlessListener.resetPageCount();
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.accent);
    }

    @Override
    public void publishAllSellers(List<Seller> list, int pageNumber) {
        if (pageNumber == 1) {
            adapter.clear();
        }
        for (Seller seller : list) {
            adapter.add(
                    new StoreItem(seller)
                            .withIdentifier(seller.getRowNum())
            );
        }
        if (pageNumber == 1) {
            endlessListener.enable();
        }
    }

    @Override
    public void publishState(List<State> list) {
        stateDialog = new BaseBottomSheetDialog(getContext()) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (State state : list) {
                    String title;
                    if (Locale.getDefault().toString().equals("ar")) {
                        title = state.getNameStateAr();
                    } else {
                        title = state.getNameStateEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(state.getId()));
                }
            }
        };
        stateDialog.setOnMainButtonClick(this::onStateSelect);
        stateDialog.setOnNonSelect(this::onNonStateSelect);
    }

    private void onStateSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        stateId = (int) item.getIdentifier();
        presenter.getAreas();
        stateDialog.dismiss();
        selectAreaBtn.setEnabled(true);
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllSellers(1);
    }

    private void onNonStateSelect() {
        stateId = 0;
        areaId = 0;
        selectAreaBtn.setEnabled(false);
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllSellers(1);
        stateDialog.dismiss();
    }

    @Override
    public void publishArea(List<StateArea> list) {
        areaDialog = new BaseBottomSheetDialog(getContext()) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (StateArea area : list) {
                    String title;
                    if (Locale.getDefault().toString().equals("ar")) {
                        title = area.getNameAreaAr();
                    } else {
                        title = area.getNameAreaEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(area.getId()));
                }
            }
        };
        areaDialog.setOnMainButtonClick(this::onAreaSelect);
        areaDialog.setOnNonSelect(this::onNonAreaSelect);
    }

    private void onAreaSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        areaId = (int) item.getIdentifier();
        areaDialog.dismiss();
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllSellers(1);
    }

    private void onNonAreaSelect() {
        areaId = 0;
        areaDialog.dismiss();
        endlessListener.disable();
        endlessListener.resetPageCount();
        presenter.getAllSellers(1);
    }

    @Override
    public Integer getStateId() {
        return stateId;
    }

    @Override
    public Integer getAreaId() {
        return areaId;
    }

    @Override
    public String getSearchStr() {
        return searchEdt.getText().toString().trim();
    }
}
