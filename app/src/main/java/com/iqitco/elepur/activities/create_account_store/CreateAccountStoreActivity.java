package com.iqitco.elepur.activities.create_account_store;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.delivery_info.DeliveryInfoActivity;
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog;
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog2;
import com.iqitco.elepur.dialogs.SelectTimeDialog;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.iqitco.elepur.items.BottomDialogItem2;
import com.iqitco.elepur.widget.CheckboxInputView;
import com.iqitco.elepur.widget.CustomEditTextView;
import com.iqitco.elepur.widget.SelectImageView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.create_account_store.CreateAccountStoreContract;
import com.iqitco.elepur_api.contracts.create_account_store.CreateAccountStorePresenter;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.State;
import com.iqitco.elepur_api.modules.StateArea;
import com.iqitco.elepur_api.modules.UserData;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * date 6/1/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CreateAccountStoreActivity extends BaseActivity implements CreateAccountStoreContract.View, GoogleApiClient.ConnectionCallbacks {

    public static final String TAG = CreateAccountStoreActivity.class.getSimpleName();

    private SelectImageView logoSelectImage;
    private SelectImageView frontSelectImage;
    private SelectImageView licenseSelectImage;
    private CustomEditTextView nameArEdt;
    private CustomEditTextView nameEnEdt;
    private CustomEditTextView ownerNameEdt;
    private CustomEditTextView emailEdt;
    private CustomEditTextView passwordEdt;
    private CustomEditTextView telephoneEdt;
    private CustomEditTextView stateEdt;
    private CustomEditTextView areaEdt;
    private CustomEditTextView addressEdt;
    private CustomEditTextView descArEdt;
    private CustomEditTextView descEnEdt;
    private CustomEditTextView workTimeEdt;
    private CustomEditTextView jobEdt;
    private CheckboxInputView serviceCheck;
    private Button registrationBtn;
    private TextView acceptBtn;

    private BaseBottomSheetDialog stateDialog;
    private BaseBottomSheetDialog areaDialog;
    private BaseBottomSheetDialog2 professionsDialog;

    private CreateAccountStoreContract.Presenter presenter;

    private Integer stateId;
    private Integer areaId;
    private List<Integer> professionId;

    private Calendar openTimeCalendar = Calendar.getInstance();
    private Calendar closeTimeCalendar = Calendar.getInstance();

    /*google play service client for location*/
    private GoogleApiClient googleApiClient;
    private static final int GPS_RESULT_CODE = 1020;
    private String longitude;
    private String latitude;

    private SelectTimeDialog selectTimeDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGoogleApiClient();
        setContentView(R.layout.create_account_store_activity);
        presenter = new CreateAccountStorePresenter(this);
        presenter.onCreate();
        initViews();

    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this::onConnectionFailed)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();
    }


    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google play service connection failed: " + connectionResult.getErrorMessage());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (checkGps()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                longitude = String.valueOf(location.getLongitude());
                latitude = String.valueOf(location.getLatitude());
                Log.d(TAG, String.format("longitude: %s", longitude));
                Log.d(TAG, String.format("latitude: %s", latitude));
            }
        } else {
            openSettings();
        }
    }

    private boolean checkGps() {
        return ((LocationManager) getSystemService(Context.LOCATION_SERVICE))
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void openSettings() {
        new MaterialDialog.Builder(this)
                .content(R.string.title_open_gps)
                .positiveText(android.R.string.yes)
                .negativeText(android.R.string.no)
                .onPositive((dialog, which) -> startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_RESULT_CODE))
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        logoSelectImage.onActivityResult(requestCode, resultCode, data);
        frontSelectImage.onActivityResult(requestCode, resultCode, data);
        licenseSelectImage.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews() {
        logoSelectImage = findViewById(R.id.logoSelectImage);
        frontSelectImage = findViewById(R.id.frontSelectImage);
        licenseSelectImage = findViewById(R.id.licenseSelectImage);
        nameArEdt = findViewById(R.id.nameArEdt);
        nameEnEdt = findViewById(R.id.nameEnEdt);
        ownerNameEdt = findViewById(R.id.ownerNameEdt);
        emailEdt = findViewById(R.id.emailEdt);
        passwordEdt = findViewById(R.id.passwordEdt);
        telephoneEdt = findViewById(R.id.telephoneEdt);
        stateEdt = findViewById(R.id.stateEdt);
        areaEdt = findViewById(R.id.areaEdt);
        addressEdt = findViewById(R.id.addressEdt);
        descArEdt = findViewById(R.id.descArEdt);
        descEnEdt = findViewById(R.id.descEnEdt);
        workTimeEdt = findViewById(R.id.workTimeEdt);
        jobEdt = findViewById(R.id.jobEdt);
        serviceCheck = findViewById(R.id.serviceCheck);
        registrationBtn = findViewById(R.id.registrationBtn);
        acceptBtn = findViewById(R.id.acceptBtn);
        acceptBtn.setOnClickListener(v -> startActivity(new Intent(CreateAccountStoreActivity.this, DeliveryInfoActivity.class).putExtra(Constant.TITLE, getString(R.string.title_terms))));

        stateEdt.setInputType(InputType.TYPE_NULL);
        stateEdt.setOnKeyListener(null);
        stateEdt.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction())
                stateDialog.show();
            return false;
        });
        stateEdt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                stateDialog.show();
            }
        });

        areaEdt.setInputType(InputType.TYPE_NULL);
        areaEdt.setOnKeyListener(null);
        areaEdt.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction()) {
                if (areaDialog != null) {
                    areaDialog.show();
                }
            }
            return false;
        });
        areaEdt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                if (areaDialog != null) {
                    areaDialog.show();
                }
            }
        });

        jobEdt.setInputType(InputType.TYPE_NULL);
        jobEdt.setOnKeyListener(null);
        jobEdt.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction())
                professionsDialog.show();
            return false;
        });
        jobEdt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                professionsDialog.show();
            }
        });

        selectTimeDialog = new SelectTimeDialog(getContext());
        selectTimeDialog.setOnTimesSelect((openTime, closeTime, text) -> {
            openTimeCalendar = openTime;
            closeTimeCalendar = closeTime;
            workTimeEdt.setText(text);
            selectTimeDialog.dismiss();
        });

        workTimeEdt.setInputType(InputType.TYPE_NULL);
        workTimeEdt.setOnKeyListener(null);
        workTimeEdt.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction()) {
                selectTimeDialog.show();
            }
            return false;
        });

        registrationBtn.setOnClickListener(v -> presenter.registration());
    }

    @Override
    public void publishState(List<State> list) {
        stateDialog = new BaseBottomSheetDialog(this) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (State state : list) {
                    String title;
                    if (Locale.getDefault().toString().equals("ar")) {
                        title = state.getNameStateAr();
                    } else {
                        title = state.getNameStateEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(state.getId()));
                }
            }
        };
        stateDialog.setOnMainButtonClick(this::onStateSelect);
    }

    private void onStateSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        stateId = (int) item.getIdentifier();
        stateEdt.setText(item.getTitle());
        presenter.getStateArea();
        stateDialog.dismiss();
        areaEdt.setText("");
        areaId = null;
    }

    @Override
    public void publishArea(List<StateArea> list) {
        areaDialog = new BaseBottomSheetDialog(this) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (StateArea area : list) {
                    String title;
                    if (Locale.getDefault().toString().equals("ar")) {
                        title = area.getNameAreaAr();
                    } else {
                        title = area.getNameAreaEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(area.getId()));
                }
            }
        };
        areaDialog.setOnMainButtonClick(this::onAreaSelect);
    }

    private void onAreaSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        areaId = (int) item.getIdentifier();
        areaEdt.setText(item.getTitle());
        areaDialog.dismiss();
    }

    @Override
    public void publishProfessions(List<MainPageSection> list) {
        professionsDialog = new BaseBottomSheetDialog2(this, true) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem2> adapter) {
                for (MainPageSection section : list) {

                    String title;
                    String pic;
                    if (Locale.getDefault().toString().equals("ar")) {
                        title = section.getCategoryNameAr();
                        pic = section.getPicAr();
                    } else {
                        title = section.getCategoryName();
                        pic = section.getPicEn();
                    }
                    adapter.add(new BottomDialogItem2(title, pic).withIdentifier(section.getId()));
                }
            }
        };
        professionsDialog.setOnMainButtonClick(this::onProfessionSelect);
    }

    private void onProfessionSelect(List<BottomDialogItem2> list) {
        StringBuilder builder = new StringBuilder();
        builder.append(list.get(0).getTitle());
        for (int i = 1; i < list.size(); i++) {
            builder.append(", ").append(list.get(i).getTitle());
        }
        jobEdt.setText(builder.toString());
        professionsDialog.dismiss();
    }

    @Override
    public Integer getStateId() {
        return stateId;
    }

    @Override
    public void errorStateId(boolean flag) {
        stateEdt.showError(flag);
    }

    @Override
    public Integer getAreaId() {
        return areaId;
    }

    @Override
    public void errorAreaId(boolean flag) {
        areaEdt.showError(flag);
    }

    @Override
    public String getShopNameAr() {
        return nameArEdt.getText().toString().trim();
    }

    @Override
    public void errorShopNameAr(boolean flag) {
        nameArEdt.showError(flag);
    }

    @Override
    public String getShopNameEn() {
        return nameEnEdt.getText().toString().trim();
    }

    @Override
    public void errorShopNameEn(boolean flag) {
        nameEnEdt.showError(flag);
    }

    @Override
    public String getOwnerName() {
        return ownerNameEdt.getText().toString().trim();
    }

    @Override
    public void errorOwnerName(boolean flag) {
        ownerNameEdt.showError(flag);
    }

    @Override
    public String getEmail() {
        return emailEdt.getText().toString().trim().toLowerCase();
    }

    @Override
    public void errorEmail(boolean flag) {
        emailEdt.showError(flag);
    }

    @Override
    public String getPassword() {
        return passwordEdt.getText().toString().trim();
    }

    @Override
    public void errorPassword(boolean flag) {
        passwordEdt.showError(flag);
    }

    @Override
    public String getTelephone() {
        return telephoneEdt.getText().toString().trim();
    }

    @Override
    public void errorTelephone(boolean flag) {
        telephoneEdt.showError(flag);
    }

    @Override
    public String getLocation() {
        return addressEdt.getText().toString().trim();
    }

    @Override
    public void errorLocation(boolean flag) {
        addressEdt.showError(flag);
    }

    @Override
    public String getDescriptionAr() {
        return descArEdt.getText().toString().trim();
    }

    @Override
    public void errorDescriptionAr(boolean flag) {
        descArEdt.showError(flag);
    }

    @Override
    public String getDescriptionEn() {
        return descEnEdt.getText().toString().trim();
    }

    @Override
    public void errorDescriptionEn(boolean flag) {
        descEnEdt.showError(flag);
    }

    @Override
    public String getLogoBase64() {
        return logoSelectImage.getBase64();
    }

    @Override
    public void errorLogo(boolean flag) {
        logoSelectImage.showError(flag);
    }

    @Override
    public String getCoverBase64() {
        return frontSelectImage.getBase64();
    }

    @Override
    public void errorCover(boolean flag) {
        frontSelectImage.showError(flag);
    }

    @Override
    public String getLicenseBase64() {
        return licenseSelectImage.getBase64();
    }

    @Override
    public void errorLicense(boolean flag) {
        licenseSelectImage.showError(flag);
    }

    @Override
    public void showNotValidEmail(boolean flag) {
        emailEdt.showError(flag);
    }

    @Override
    public void showNotValidPassword(boolean flag) {
        passwordEdt.showError(flag);
    }

    @Override
    public List<Integer> getCategories() {
        return professionsDialog.getSelectedList();
    }

    @Override
    public void errorCategories(boolean flag) {
        jobEdt.showError(flag);
    }

    @Override
    public String getLatitude() {
        return latitude;
    }

    @Override
    public String getLongitude() {
        return longitude;
    }

    @Override
    public Calendar getOpenTime() {
        if (TextUtils.isEmpty(workTimeEdt.getText().toString().trim())) {
            return null;
        } else {
            return openTimeCalendar;
        }
    }

    @Override
    public void errorOpenTime(boolean flag) {
        workTimeEdt.showError(flag);
    }

    @Override
    public Calendar getCloseTime() {
        if (TextUtils.isEmpty(workTimeEdt.getText().toString().trim())) {
            return null;
        } else {
            return closeTimeCalendar;
        }
    }

    @Override
    public void errorCloseTime(boolean flag) {
        workTimeEdt.showError(flag);
    }

    @Override
    public boolean getDeliveryCheck() {
        return serviceCheck.getCheckBox().isChecked();
    }

    @Override
    public void publishSellerData(UserData body) {
        getElepurApp().loginUser(this, body);
    }

    @Override
    public String getNotificationId() {
        return SecurePrefManager.with(this).get(Constant.NOTIFICATION_ID).defaultValue("").go();
    }
}
