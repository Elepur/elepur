package com.iqitco.elepur.activities.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.about_us.AboutUsActivity;
import com.iqitco.elepur.activities.all_ads_by_brand.AllAdsByBrandActivity;
import com.iqitco.elepur.activities.contact_us.ContactUsActivity;
import com.iqitco.elepur.activities.edit_seller_profile.EditSellerProfileActivity;
import com.iqitco.elepur.activities.favorite_ads.FavoriteAdsActivity;
import com.iqitco.elepur.activities.favorite_stores.FavoriteStoresActivity;
import com.iqitco.elepur.activities.home.fragment.AllAdsFragment;
import com.iqitco.elepur.activities.home.fragment.AllStoresFragment;
import com.iqitco.elepur.activities.home.fragment.MainPageContainerFragment;
import com.iqitco.elepur.activities.home.fragment.SellerProfileFragment;
import com.iqitco.elepur.activities.home.fragment.VisitorProfileFragment;
import com.iqitco.elepur.activities.products.AddProductActivity;
import com.iqitco.elepur.activities.seller_store_result.SellerStoreResultActivity;
import com.iqitco.elepur.activities.settings.SettingsActivity;
import com.iqitco.elepur.activities.share_opinion.ShareOpinionActivity;
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity;
import com.iqitco.elepur.activities.store_products_status.StoreProductsStatusActivity;
import com.iqitco.elepur.dialogs.MainAdvDialog;
import com.iqitco.elepur.dialogs.RegistrationDialog;
import com.iqitco.elepur.widget.BottomNavView;
import com.iqitco.elepur.widget.NavDrawerButtonView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.home.HomeContract;
import com.iqitco.elepur_api.contracts.home.HomePresenter;
import com.iqitco.elepur_api.modules.PopAdv;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.util.ArrayList;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class HomeSellerActivity extends BaseActivity implements HomeContract.View {

    public static final String TAG = HomeSellerActivity.class.getSimpleName();

    private DrawerLayout drawerLayout;
    private BottomNavView bottomNav;
    private Fragment[] fragments;
    private boolean doubleBackToExitPressedOnce = false;
    private NavDrawerButtonView loginBtn;
    private NavDrawerButtonView aboutUsBtn;
    private NavDrawerButtonView shareBtn;
    private NavDrawerButtonView callUsBtn;
    private NavDrawerButtonView settingsBtn;
    private RegistrationDialog registrationDialog;

    private MaterialDialog logoutDialog;
    private HomeContract.Presenter presenter;
    private MainAdvDialog mainAdvDialog;

    /**
     * used to check id user is logged in
     */
    private Integer loginId;

    /**
     * used to check the current user role
     */
    private String role;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_seller_activity);

        role = SecurePrefManager.with(this).get(Constant.ROLL).defaultValue("").go();
        presenter = new HomePresenter(this);
        bottomNav = findViewById(R.id.bottomNav);
        loginBtn = findViewById(R.id.loginBtn);
        aboutUsBtn = findViewById(R.id.aboutUsBtn);
        shareBtn = findViewById(R.id.shareBtn);
        callUsBtn = findViewById(R.id.callUsBtn);
        settingsBtn = findViewById(R.id.settingsBtn);
        settingsBtn.setOnClickListener(this::onSettingsClick);
        loginBtn.setOnClickListener(this::onLoginClick);
        callUsBtn.setOnClickListener(v -> startActivity(new Intent(this, ContactUsActivity.class)));
        shareBtn.setOnClickListener(v -> startActivity(new Intent(this, ShareOpinionActivity.class)));
        aboutUsBtn.setOnClickListener(v -> startActivity(new Intent(this, AboutUsActivity.class)));
        drawerLayout = findViewById(R.id.drawerLayout);
        initDownMenu();
        initFragments();

        loginId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go();
        if (loginId != -1) {
            loginBtn.getImage().setImageResource(R.drawable.ic_signout);
            loginBtn.getTextTv().setText(R.string.title_sign_out);
        }

        registrationDialog = new RegistrationDialog(this, false);

        bottomNav.setSelected(2);

        if (!role.equalsIgnoreCase("seller")) {
            bottomNav.changeButtonIcon(3, R.drawable.ic_profile);
            bottomNav.changeButtonText(3, R.string.title_profile);
        }

        presenter.onCreate();
        if (getIntent().getData() != null) {
            String[] split = getIntent().getData().toString().split("/");
            try {
                Integer adId = Integer.parseInt(split[split.length - 2]);
                Integer sellerId = Integer.parseInt(split[split.length - 1]);
                startActivity(
                        new Intent(this, ShowAdsActivity.class)
                                .putExtra(Constant.ADV_ID, adId)
                                .putExtra(Constant.SELLER_ID, sellerId)
                );
            } catch (Exception e) {
                Log.e(TAG, "error", e);
            }
        }
    }

    public void loadBrandAds(int brandId) {
        startActivity(
                new Intent(this, AllAdsByBrandActivity.class)
                        .putExtra(AllAdsByBrandActivity.BRAND_ID, brandId)
        );
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initDownMenu() {
        bottomNav.setOnNavClick(this::onNavClick);
        if (role.equalsIgnoreCase("seller")) {
            bottomNav.addDownMenuItem(R.string.title_add_ads, R.drawable.ic_add_ads, V -> startActivity(new Intent(HomeSellerActivity.this, AddProductActivity.class)));
            bottomNav.addDownMenuItem(R.string.title_ads_result, R.drawable.ic_ads_result, v -> startActivity(new Intent(HomeSellerActivity.this, StoreProductsStatusActivity.class)));
            bottomNav.addDownMenuItem(R.string.title_store_result, R.drawable.ic_store_results, v -> startActivity(new Intent(HomeSellerActivity.this, SellerStoreResultActivity.class)));
            bottomNav.addDownMenuItem(R.string.title_edit_store, R.drawable.ic_edit_store, v -> startActivity(new Intent(HomeSellerActivity.this, EditSellerProfileActivity.class)));
            bottomNav.setMainBtnIcon(getResources().getDrawable(R.drawable.ic_bottom_nav_plus));
        } else {
            bottomNav.addDownMenuItem(R.string.title_fav_ads, R.drawable.ic_fav_ads, v -> showVisitorDialog(roleId -> startActivity(new Intent(HomeSellerActivity.this, FavoriteAdsActivity.class))));
            bottomNav.addDownMenuItem(R.string.title_fav_store, R.drawable.ic_fav_store, v -> showVisitorDialog(roleId -> startActivity(new Intent(HomeSellerActivity.this, FavoriteStoresActivity.class))));
            bottomNav.setMainBtnIcon(getResources().getDrawable(R.drawable.ic_hart));
        }

    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    private void initFragments() {
        fragments = new Fragment[4];
        fragments[0] = new AllAdsFragment();
        fragments[1] = new AllStoresFragment();
        fragments[2] = new MainPageContainerFragment();
        if (role.equalsIgnoreCase(Constant.USER_ROLE_SELLER)) {
            fragments[3] = new SellerProfileFragment();
        } else {
            fragments[3] = new VisitorProfileFragment();
        }
    }

    public void onNavClick(int btnIndex, Bundle arguments) {
        if (btnIndex == 0) {
            AllAdsFragment alladsfragment = (AllAdsFragment) fragments[0];
            alladsfragment.getBrandsIds().clear();
        } else if (btnIndex == 2) {
            MainPageContainerFragment containerFragment = (MainPageContainerFragment) fragments[2];
            if (containerFragment.isAdded()) {
                containerFragment.returnToHome();
                return;
            }
        }
        fragments[btnIndex].setArguments(arguments);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragments[btnIndex])
                .commit();
    }

    public void onNavClick(int btnIndex) {
        onNavClick(btnIndex, null);
    }

    private void onSettingsClick(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void onLoginClick(View view) {
        if (loginId != -1) {
            if (logoutDialog == null) {
                logoutDialog = new MaterialDialog.Builder(this)
                        .content(R.string.message_logout)
                        .positiveText(R.string.btn_logout)
                        .negativeText(R.string.btn_cancel)
                        .autoDismiss(false)
                        .canceledOnTouchOutside(false)
                        .cancelable(false)
                        .onPositive((dialog, which) -> {
                            dialog.dismiss();
                            presenter.logout();
                        })
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .build();
            }

            if (!logoutDialog.isShowing()) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                }
                logoutDialog.show();
            }
        } else {
            if (!registrationDialog.isShowing()) {
                registrationDialog.show();
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }

        if (bottomNav.isOpen()) {
            bottomNav.close();
            return;
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment instanceof MainPageContainerFragment) {
            MainPageContainerFragment mainPageContainerFragment = (MainPageContainerFragment) fragment;
            if (mainPageContainerFragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                mainPageContainerFragment.removeFragment();
                return;
            }
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.message_exit_app), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    public Integer getId() {
        return SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(1).go();
    }

    @Override
    public String getRole() {
        return SecurePrefManager.with(this).get(Constant.ROLL).defaultValue("").go();
    }

    @Override
    public void onSuccess() {
        getElepurApp().logoutUser(this);
    }

    @Override
    public void publishMainAdv(PopAdv popAdv) {
        if (mainAdvDialog == null) {
            mainAdvDialog = new MainAdvDialog(this, popAdv);
            mainAdvDialog.setOnPopAdvClick(popAdv1 -> {
                redirectLink(popAdv1.getLinkType(), popAdv1.getLink());
                mainAdvDialog.dismiss();
            });
        } else {
            return;
        }

        if (!mainAdvDialog.isShowing()) {
            mainAdvDialog.show();
        }
    }

    public void redirectLink(Integer linkType, String link) {
        if (linkType == null || linkType == 3) {
            return;
        }
        try {
            switch (linkType) {
                case 1:
                    /*all ads*/
                    onNavClick(0);
                    bottomNav.setSelected(0, false);
                    break;
                case 2:
                    /*all store*/
                    onNavClick(1);
                    bottomNav.setSelected(1, false);
                    break;
                case 4:

                    break;
                case 5: {
                    /*all ads, category*/
                    Bundle bundle = new Bundle();
                    AllAdsFilterActivity.Result result = new AllAdsFilterActivity.Result();
                    result.setCategoryId(Integer.parseInt(link));
                    bundle.putParcelable(AllAdsFilterActivity.RESULTS, result);
                    onNavClick(0, bundle);
                    bottomNav.setSelected(0, false);
                }
                break;
                case 6: {
                    /*all ads, brand*/
                    Bundle bundle = new Bundle();
                    AllAdsFilterActivity.Result result = new AllAdsFilterActivity.Result();
                    result.setBrandsIds(new ArrayList<>());
                    result.getBrandsIds().add(Integer.parseInt(link));
                    bundle.putParcelable(AllAdsFilterActivity.RESULTS, result);
                    onNavClick(0, bundle);
                    bottomNav.setSelected(0, false);
                }
                break;
                case 7: {
                    /*all ads, sub brand*/
                    Bundle bundle = new Bundle();
                    AllAdsFilterActivity.Result result = new AllAdsFilterActivity.Result();
                    result.setSubBrandsIds(new ArrayList<>());
                    result.getSubBrandsIds().add(Integer.parseInt(link));
                    bundle.putParcelable(AllAdsFilterActivity.RESULTS, result);
                    onNavClick(0, bundle);
                    bottomNav.setSelected(0, false);
                }
                break;
                case 8: {
                    /*all ads, kind*/
                    Bundle bundle = new Bundle();
                    AllAdsFilterActivity.Result result = new AllAdsFilterActivity.Result();
                    result.setKindsIds(new ArrayList<>());
                    result.getKindsIds().add(Integer.parseInt(link));
                    bundle.putParcelable(AllAdsFilterActivity.RESULTS, result);
                    onNavClick(0, bundle);
                    bottomNav.setSelected(0, false);
                }
                break;
            }
        } catch (Exception e) {
            /*NOTHING*/
        }
    }
}
