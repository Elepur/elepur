package com.iqitco.elepur.activities.contact_us;

import android.os.Bundle;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;

/**
 * date 6/20/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ContactUsActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us_activity);

    }
}
