package com.iqitco.elepur.activities.update_customer_profile;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.dialogs.GenderDialog;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.iqitco.elepur.util.DateUtility;
import com.iqitco.elepur.util.InitUtility;
import com.iqitco.elepur.widget.CustomEditTextView;
import com.iqitco.elepur.widget.SelectImageView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.update_customer_profile.UpdateCustomerProfileContract;
import com.iqitco.elepur_api.contracts.update_customer_profile.UpdateCustomerProfilePresenter;
import com.iqitco.elepur_api.modules.CustomerProfile;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * date: 2018-07-02
 *
 * @author Mohammad Al-Najjar
 */
public class UpdateCustomerProfileActivity extends BaseActivity implements UpdateCustomerProfileContract.View {

    private SelectImageView customerSelectImage;
    private CustomEditTextView nameEdt;
    private CustomEditTextView emailEdt;
    private CustomEditTextView passwordEdt;
    private CustomEditTextView genderEdt;
    private CustomEditTextView dayOfBirthEdt;
    private Button registrationBtn;
    private GenderDialog genderDialog;
    private UpdateCustomerProfileContract.Presenter presenter;
    private int loginId;
    private String gender;
    private boolean isFacebookUser;
    private SecurePrefManager securePrefManager;
    private Calendar birthday = Calendar.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_customer_profile_activity);

        securePrefManager = SecurePrefManager.with(this);
        loginId = securePrefManager.get(Constant.ID_ID).defaultValue(-1).go();
        isFacebookUser = securePrefManager.get(Constant.IS_FACEBOOK_USER).defaultValue(false).go();

        genderDialog = new GenderDialog(this);
        genderDialog.setOnMainButtonClick(this::onGenderSelect);
        genderDialog.setOnNonSelect(this::onNonGenderSelect);

        customerSelectImage = findViewById(R.id.customerSelectImage);
        nameEdt = findViewById(R.id.nameEdt);
        emailEdt = findViewById(R.id.emailEdt);
        emailEdt.setVisibility(isFacebookUser ? View.GONE : View.VISIBLE);
        passwordEdt = findViewById(R.id.passwordEdt);
        genderEdt = findViewById(R.id.genderEdt);
        dayOfBirthEdt = findViewById(R.id.dayOfBirthEdt);
        registrationBtn = findViewById(R.id.registrationBtn);
        registrationBtn.setOnClickListener(v -> presenter.updateCustomerProfile());
        presenter = new UpdateCustomerProfilePresenter(this);
        presenter.onCreate();


        genderEdt.setInputType(InputType.TYPE_NULL);
        genderEdt.setOnKeyListener(null);
        genderEdt.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction())
                genderDialog.show();
            return false;
        });
        genderEdt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                genderDialog.show();
            }
        });

        passwordEdt.setInputType(InputType.TYPE_NULL);
        passwordEdt.setOnKeyListener(null);
        passwordEdt.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction()) {
                startActivityForResult(new Intent(UpdateCustomerProfileActivity.this, UpdatePasswordActivity.class), 2);
            }
            return false;
        });
        passwordEdt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                startActivityForResult(new Intent(UpdateCustomerProfileActivity.this, UpdatePasswordActivity.class), 2);
            }
        });

        InitUtility.initDateInput(dayOfBirthEdt, birthday, getFragmentManager(), null);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void onGenderSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        genderEdt.setText(item.getTitle());
        gender = item.getIdentifier() == 1 ? "M" : "F";
        genderDialog.dismiss();
    }

    private void onNonGenderSelect() {
        genderEdt.setText(null);
        gender = null;
        genderDialog.dismiss();
    }

    @Override
    public Integer getId() {
        return loginId;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK) {
            Toast.makeText(this, R.string.message_update_profile_done, Toast.LENGTH_LONG).show();
            return;
        }
        customerSelectImage.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void publishCustomerProfile(CustomerProfile profile) {
        nameEdt.setText(profile.getName());
        emailEdt.setText(profile.getEmail());
        passwordEdt.setText("*************");
        try {
            SimpleDateFormat dateParser = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            birthday.setTime(dateParser.parse(profile.getBirth()));
            dayOfBirthEdt.setText(DateUtility.formatDate(birthday, DateUtility.API_IN_DATE_FORMAT));
        } catch (Exception e) {
            /*NOTHING*/
        }
        List<Integer> selectedIds = new ArrayList<>();
        if (profile.getGender() != null && profile.getGender().length() > 0) {
            selectedIds.add(profile.getGender().equalsIgnoreCase("m") ? 1 : 2);
        }
        genderDialog.setSelectedIds(selectedIds);

        if (isFacebookUser) {
            String facebookId = securePrefManager.get(Constant.FACEBOOK_ID).defaultValue("").go();
            customerSelectImage.loadImage("http://graph.facebook.com/" + facebookId + "/picture?type=large");
        } else {
            customerSelectImage.loadImage(profile.getPic());
        }
    }

    @Override
    public String getBase64Pic() {
        return customerSelectImage.getBase64();
    }

    @Override
    public String getName() {
        return nameEdt.getText().toString().trim();
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public Calendar getBirthDay() {
        return birthday;
    }

    @Override
    public String getNotificationId() {
        return SecurePrefManager.with(this).get(Constant.NOTIFICATION_ID).defaultValue("").go();
    }

    @Override
    public void onUpdateSuccess() {
        setResult(RESULT_OK);
        finish();
    }
}
