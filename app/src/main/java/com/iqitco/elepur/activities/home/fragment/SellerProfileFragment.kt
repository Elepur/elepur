package com.iqitco.elepur.activities.home.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.home.BaseHomeFragment
import com.iqitco.elepur.dialogs.StoreLocationDialog
import com.iqitco.elepur.items.ProductItem
import com.iqitco.elepur.util.DateUtility
import com.iqitco.elepur.util.DrawableUtility
import com.iqitco.elepur.util.onProductItemItemClick
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.sellerProfile.SellerProfileContract
import com.iqitco.elepur_api.contracts.sellerProfile.SellerProfilePresenter
import com.iqitco.elepur_api.modules.MainPageSection
import com.iqitco.elepur_api.modules.SellerProduct
import com.iqitco.elepur_api.modules.SellerProfile
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.show_seller_activity.*
import java.util.*

/**
 * date 9/8/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class SellerProfileFragment : BaseHomeFragment(), SellerProfileContract.View, TabLayout.OnTabSelectedListener {

    /**
     * Seller profile after retrieve from api
     */
    private lateinit var profile: SellerProfile

    /**
     * the presenter for this fragment
     */
    private val presenter by lazy {
        SellerProfilePresenter(this)
    }

    /**
     * seller id who logged in
     */
    private val sellerId by lazy {
        SecurePrefManager.with(context).get(Constant.ID_ID).defaultValue(-1).go()
    }

    /**
     * map dialog when user click on show store location button
     */
    private val mapDialog: StoreLocationDialog by lazy {
        StoreLocationDialog(activity!!, profile)
    }

    /**
     * layout manager for items list
     */
    private lateinit var layoutManager: LinearLayoutManager

    /**
     * listener for list when load more
     */
    private lateinit var endlessListener: EndlessRecyclerOnScrollListener

    /**
     * products list adapter
     */
    private val itemsAdapter = FastItemAdapter<ProductItem>()

    /**
     * selected category id when user click on category in tabs
     */
    private var categoryId: Int = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.show_seller_activity, container, false)
        view.scaleX = 1F
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreate()

        mapBtn.setOnClickListener {
            if (!mapDialog.isShowing) {
                mapDialog.show()
            }
        }
        toolbar.backBtn.setOnClickListener(this::onMenuClick)
        toolbar.backBtn.setImageResource(R.drawable.ic_menu)
        tabLayout.addOnTabSelectedListener(this)
        itemsAdapter.withOnClickListener(this::onProductItemItemClick)

        layoutManager = LinearLayoutManager(context)
        list.layoutManager = layoutManager
        list.adapter = itemsAdapter
        endlessListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                presenter.getAdv(currentPage + 1)
            }
        }
        endlessListener.disable()
        list.addOnScrollListener(endlessListener)
    }

    /**
     * return seller id to presenter
     */
    override fun getSellerId(): Int {
        return sellerId
    }

    /**
     * return category id to presenter
     */
    override fun getCategoryId(): Int {
        return categoryId
    }

    /**
     * published seller category after call TODO
     */
    override fun publishSellerCategory(list: List<MainPageSection>) {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_all_products)).setTag(0).setCustomView(R.layout.tab_item_view))
        for (category in list) {
            val title = if (Locale.getDefault().toString().equals("ar", true)) category.categoryNameAr else category.categoryName
            tabLayout.addTab(tabLayout.newTab().setText(title).setTag(category.id!!).setCustomView(R.layout.tab_item_view))
        }
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        categoryId = tab!!.tag.toString().toInt()
        /*endlessListener.disable()*/
        endlessListener.resetPageCount()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    /**
     * published seller profile after call TODO
     */
    override fun publishSellerProfile(profile: SellerProfile) {
        this.profile = profile
        if (profile.authorize != null && profile.authorize == 0) {
            soldLayout.visibility = View.VISIBLE
        }
        presenter.getSellerCategories(profile.sellerCategory!!)

        sellerNameTv.text = profile.shopName
        toolbar.titleTv.text = profile.shopName
        sellerKindTv.text = profile.kind

        sellerBlue.visibility = if (profile.blue == 1) View.VISIBLE else View.INVISIBLE

        Glide.with(this)
                .load(profile.picCover)
                .apply(RequestOptions()
                        .centerCrop()
                        .fallback(R.drawable.img_place_holder)
                        .placeholder(R.drawable.img_place_holder)
                        .error(R.drawable.img_place_holder))
                .into(sellerImage)

        Glide.with(this)
                .load(profile.picLogo)
                .into(sellerLogo)

        emojiBar.sadEmoji!!.counterTv.text = profile.rateSad.toString()
        emojiBar.happyEmoji!!.counterTv.text = profile.rateHappy.toString()
        emojiBar.loveEmoji!!.counterTv.text = profile.favourite.toString()

        emojiBar.sadEmoji!!.isSelected = profile.rateSadC == 1
        emojiBar.happyEmoji!!.isSelected = profile.rateHappyC == 1
        emojiBar.loveEmoji!!.isSelected = profile.favouriteC == 1

        timeTv.text = "${DateUtility.convertTime(profile.timeOpen)} - ${DateUtility.convertTime(profile.timeClose)}"
        val color: Int = if (profile.time == 0) {
            context!!.resources.getColor(R.color.border)
        } else {
            context!!.resources.getColor(R.color.textColorPrimary)
        }
        timeTv.setTextColor(color)
        clock1.setImageDrawable(DrawableUtility.getTintDrawable(context, R.drawable.ic_clock1, color))

        locationTv.text = "${profile.state} - ${profile.area}"
        tabLayout.setScrollPosition(0, 0.0F, true)

        val role = SecurePrefManager.with(context).get(Constant.ROLL).defaultValue("").go()
        if (role.equals(Constant.USER_ROLE_SELLER, true)) {
            emojiBar.sadEmoji!!.isSelected = true
            emojiBar.happyEmoji!!.isSelected = true
            emojiBar.loveEmoji!!.isSelected = true
        }
    }

    /**
     * published seller adv after call TODO
     */
    override fun publishSellerAdv(list: List<SellerProduct>, pageNumber: Int) {
        if (pageNumber == 1) {
            itemsAdapter.clear()
        }
        for (product in list) {
            itemsAdapter.add(ProductItem.getProductItem(product, 1, sellerId!!))
        }
        /*if (pageNumber == 1) {
            endlessListener.enable()
        }*/
        itemsAdapter.notifyAdapterDataSetChanged()
    }
}