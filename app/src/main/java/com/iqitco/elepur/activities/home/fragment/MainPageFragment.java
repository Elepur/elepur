package com.iqitco.elepur.activities.home.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.akexorcist.localizationactivity.LanguageSetting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.home.BaseHomeFragment;
import com.iqitco.elepur.activities.main_search.MainSearchActivity;
import com.iqitco.elepur.activities.notification.NotificationActivity;
import com.iqitco.elepur.adapters.ViewPagerViewAdapter;
import com.iqitco.elepur.items.ItemDecorationAlbumColumns;
import com.iqitco.elepur.items.MainPageBrandItem;
import com.iqitco.elepur.items.MainPageSectionItem;
import com.iqitco.elepur.widget.HomeToolbarView;
import com.iqitco.elepur.widget.MainSlideView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.home.MainPageContract;
import com.iqitco.elepur_api.contracts.home.MainPagePresenter;
import com.iqitco.elepur_api.modules.BrandPic;
import com.iqitco.elepur_api.modules.ElegoMain;
import com.iqitco.elepur_api.modules.GreenLight;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.SecondAdv;
import com.iqitco.elepur_api.modules.Slides;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class MainPageFragment extends BaseHomeFragment implements MainPageContract.View {

    private HomeToolbarView toolbar;
    private ViewPagerViewAdapter pagerViewAdapter;
    private FastItemAdapter<MainPageSectionItem> sectionsAdapter = new FastItemAdapter<>();
    private FastItemAdapter<MainPageBrandItem> brandAdapter = new FastItemAdapter<>();
    private RecyclerView sectionsRecyclerView;
    private RecyclerView brandList;
    private MainPageContract.Presenter presenter;
    private ImageView secondAdvImage;
    private ImageView elegoMainImage;
    private ConstraintLayout secondAdvBtn;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH);

    private String role;
    private Integer userId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_page_fragment, container, false);
        secondAdvBtn = view.findViewById(R.id.secondAdvBtn);
        getHomeSellerActivity().changeStatusBarColor(getResources().getColor(R.color.colorPrimary), false);
        initViews(view);
        role = SecurePrefManager.with(getContext()).get(Constant.ROLL).defaultValue("").go();
        userId = SecurePrefManager.with(getContext()).get(Constant.ID_ID).defaultValue(0).go();
        presenter = new MainPagePresenter(this);
        presenter.onCreate();
        return view;
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

    private void initViews(View view) {
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.getMenuBtn().setOnClickListener(this::onMenuClick);
        toolbar.getNotificationBtn().setOnClickListener(this::onNotificationBtnClick);
        toolbar.getSearchBtn().setOnClickListener(v -> startActivity(new Intent(getActivity(), MainSearchActivity.class)));
        sectionsRecyclerView = view.findViewById(R.id.list);
        sectionsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        sectionsRecyclerView.addItemDecoration(new ItemDecorationAlbumColumns(
                getContext().getResources().getDimensionPixelSize(R.dimen.mainCategorySpacing),
                2
        ));
        sectionsRecyclerView.setNestedScrollingEnabled(false);
        sectionsRecyclerView.setAdapter(sectionsAdapter);

        brandList = view.findViewById(R.id.brandList);
        brandList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        brandList.setNestedScrollingEnabled(true);
        brandList.setAdapter(brandAdapter);
        pagerViewAdapter = new ViewPagerViewAdapter();

        brandAdapter.withOnClickListener(this::onBrandClick);
        sectionsAdapter.withOnClickListener(this::onCategorySelect);

        secondAdvImage = view.findViewById(R.id.secondAdvImage);
        elegoMainImage = view.findViewById(R.id.elegoMainImage);

    }

    private boolean onCategorySelect(View v, IAdapter<MainPageSectionItem> adapter, MainPageSectionItem item, int position) {
        MainPageContainerFragment parentFragment = (MainPageContainerFragment) getParentFragment();
        assert parentFragment != null;
        Fragment fragment = new CategoryBrandsFragment();
        fragment.setArguments(new Bundle());
        int categoryId = (int) item.getIdentifier();
        assert fragment.getArguments() != null;
        fragment.getArguments().putInt(Constant.CATEGORY_ID, categoryId);
        parentFragment.changeFragment(fragment);
        return true;
    }

    private void onNotificationBtnClick(View view) {
        startActivity(new Intent(getContext(), NotificationActivity.class));
    }

    private boolean onBrandClick(View v, IAdapter<MainPageBrandItem> adapter, MainPageBrandItem item, int position) {
        getHomeSellerActivity().loadBrandAds(item.getBrandPic().getId());
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getGreenLight();
    }

    @Override
    public void publishSlides(List<Slides> list) {
        long currentDateInMillis = Calendar.getInstance().getTimeInMillis();
        for (Slides slides : list) {
            /*date format 5/29/2018 2:56:24 AM*/
            try {
                long startDateInMillis = dateFormat.parse(slides.getDate()).getTime();
                long endDateInMillis = dateFormat.parse(slides.getDateExpire()).getTime();
                if (startDateInMillis <= currentDateInMillis && currentDateInMillis <= endDateInMillis) {
                    MainSlideView imageView = new MainSlideView(getContext(), slides);
                    imageView.setOnSlideClick(slide -> getHomeSellerActivity().redirectLink(slide.getLinkType(), slide.getLink()));
                    pagerViewAdapter.add(imageView);
                }
            } catch (Exception e) {
                /*NOTHING*/
            }
        }
        toolbar.getPager().setAdapter(pagerViewAdapter);
        toolbar.getPager().setCycle(true);
        toolbar.getPager().setInterval(6000);
        toolbar.getPager().startAutoScroll();
    }

    @Override
    public void publishGreenLight(GreenLight greenLight) {
        toolbar.getNotificationGreenLight().setVisibility(greenLight.getNotify() == 1 ? View.VISIBLE : View.INVISIBLE);
        toolbar.getTicketGreenLight().setVisibility(greenLight.getReceipt() == 1 ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void publishMainSections(List<MainPageSection> list) {
        sectionsAdapter.clear();
        for (MainPageSection mainPageSection : list) {
            sectionsAdapter.add(new MainPageSectionItem(mainPageSection).withIdentifier(mainPageSection.getId()));
        }
    }

    @Override
    public void publishBrandPix(List<BrandPic> list) {
        brandAdapter.clear();
        for (BrandPic brandPic : list) {
            brandAdapter.add(new MainPageBrandItem(brandPic));
        }
    }

    @Override
    public void publishSecondAdv(List<SecondAdv> list) {
        if (list.size() == 0) return;
        SecondAdv secondAdv = list.get(0);
        String link;
        if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
            link = secondAdv.getButtounSecondAdvAr();
        } else {
            link = secondAdv.getButtounSecondAdvEn();
        }
        secondAdvBtn.setOnClickListener(v -> getHomeSellerActivity().redirectLink(secondAdv.getTypeLink(), secondAdv.getButtounSecondAdvLink()));
        Glide.with(getContext())
                .load(link)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        elegoMainImage.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(elegoMainImage);
    }

    @Override
    public void publishElegoMain(List<ElegoMain> list) {
        if (list.size() == 0) return;
        ElegoMain elegoMain = list.get(0);
        String link;
        if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
            link = elegoMain.getButtounMainElegoAr();
        } else {
            link = elegoMain.getButtounMainElegoEn();
        }
        Glide.with(getContext())
                .load(link)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        secondAdvImage.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(secondAdvImage);
    }
}
