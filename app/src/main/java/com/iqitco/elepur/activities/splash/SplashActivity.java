package com.iqitco.elepur.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.blocked_account.BlockedAccountActivity;
import com.iqitco.elepur.activities.home.HomeSellerActivity;
import com.iqitco.elepur.activities.landing_activity.LandingActivity;
import com.iqitco.elepur_api.Constant;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * date 5/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class SplashActivity extends BaseActivity {

    private Handler handler = new Handler();
    private static boolean isRun;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isRun = false;
        setContentView(R.layout.splash_activity);

        try {
            GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.newg);
            GifImageView gifView = findViewById(R.id.gifImage);
            gifView.setImageDrawable(gifFromResource);
            handler.postDelayed(this::startActivity, gifFromResource.getDuration() + 200);
        } catch (Exception e) {

        }

        findViewById(R.id.skipBtn).setOnClickListener(v -> {
            startActivity();
        });


    }

    private void startActivity() {
        if (isRun) {
            return;
        }
        isRun = true;
        finish();
        SecurePrefManager securePrefManager = SecurePrefManager.with(this);
        Integer loginId = securePrefManager.get(Constant.ID_ID).defaultValue(-1).go();
        boolean isVisitor = securePrefManager.get(Constant.IS_VISITOR).defaultValue(false).go();
        /*String email = "";*/
        Class nextActivity;
        if (isVisitor) {
            nextActivity = HomeSellerActivity.class;
        } else if (loginId == -1) {
            nextActivity = LandingActivity.class;
        } else {
            boolean isBlocked = SecurePrefManager.with(this).get(Constant.BLOCK).defaultValue(false).go();
            if (isBlocked) {
                nextActivity = BlockedAccountActivity.class;
            } else {
                nextActivity = HomeSellerActivity.class;
            }
        }
        startActivity(new Intent(this, nextActivity).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onBackPressed() {
        isRun = true;
        super.onBackPressed();
    }
}
