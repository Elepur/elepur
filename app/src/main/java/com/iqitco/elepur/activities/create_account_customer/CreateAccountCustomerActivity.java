package com.iqitco.elepur.activities.create_account_customer;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.delivery_info.DeliveryInfoActivity;
import com.iqitco.elepur.dialogs.GenderDialog;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.iqitco.elepur.util.InitUtility;
import com.iqitco.elepur.widget.CustomEditTextView;
import com.iqitco.elepur.widget.SelectImageView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.create_account_customer.CreateAccountCustomerContract;
import com.iqitco.elepur_api.contracts.create_account_customer.CreateAccountCustomerPresenter;
import com.iqitco.elepur_api.modules.UserData;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CreateAccountCustomerActivity extends BaseActivity implements CreateAccountCustomerContract.View {

    private SelectImageView customerSelectImage;
    private CustomEditTextView nameEdt;
    private CustomEditTextView emailEdt;
    private CustomEditTextView passwordEdt;
    private CustomEditTextView genderEdt;
    private CustomEditTextView dayOfBirthEdt;
    private TextView acceptBtn;
    private Button registrationBtn;
    private GenderDialog dialog;

    private String gender;
    private Calendar birthday = Calendar.getInstance();
    private CreateAccountCustomerContract.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_account_customer_activity);
        setTitle(R.string.title_create_customer_account);
        initViews();
        presenter = new CreateAccountCustomerPresenter(this);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initViews() {

        dialog = new GenderDialog(this);
        dialog.setOnMainButtonClick(this::onGenderSelect);
        dialog.setOnNonSelect(this::onNonGenderSelect);

        customerSelectImage = findViewById(R.id.customerSelectImage);
        nameEdt = findViewById(R.id.nameEdt);
        emailEdt = findViewById(R.id.emailEdt);
        passwordEdt = findViewById(R.id.passwordEdt);
        genderEdt = findViewById(R.id.genderEdt);
        dayOfBirthEdt = findViewById(R.id.dayOfBirthEdt);
        acceptBtn = findViewById(R.id.acceptBtn);
        acceptBtn.setOnClickListener(v -> startActivity(new Intent(CreateAccountCustomerActivity.this, DeliveryInfoActivity.class).putExtra(Constant.TITLE, getString(R.string.title_terms))));
        registrationBtn = findViewById(R.id.registrationBtn);
        registrationBtn.setOnClickListener(v -> presenter.registration());

        genderEdt.setInputType(InputType.TYPE_NULL);
        genderEdt.setOnKeyListener(null);
        genderEdt.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction())
                dialog.show();
            return false;
        });
        genderEdt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                dialog.show();
            }
        });

        InitUtility.initDateInput(dayOfBirthEdt, birthday, getFragmentManager(), null);

    }

    private void onGenderSelect(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        genderEdt.setText(item.getTitle());
        gender = item.getIdentifier() == 1 ? "M" : "F";
        dialog.dismiss();
    }

    private void onNonGenderSelect() {
        genderEdt.setText(null);
        gender = null;
        dialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        customerSelectImage.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public String getName() {
        return nameEdt.getText().toString().trim();
    }

    @Override
    public String getEmail() {
        return emailEdt.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return passwordEdt.getText().toString().trim();
    }

    @Override
    public String getPic() {
        return customerSelectImage.getBase64();
    }

    @Override
    public String getPicFormat() {
        return customerSelectImage.getExt();
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public String getBirth() {
        if (dayOfBirthEdt.getText().toString().trim().length() == 0) {
            return null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        return dateFormat.format(birthday.getTime());
    }

    @Override
    public void errorName(boolean flag) {
        nameEdt.showError(flag);
    }

    @Override
    public void errorEmail(boolean flag) {
        emailEdt.showError(flag);
    }

    @Override
    public void showNotValidPassword(boolean flag) {
        passwordEdt.showError(flag);
    }

    @Override
    public void showNotValidEmail(boolean flag) {
        emailEdt.showError(flag);
    }

    @Override
    public void emptyBirthday(boolean flag) {
        dayOfBirthEdt.showError(flag);
    }

    @Override
    public void emptyGender(boolean flag) {
        genderEdt.showError(flag);
    }

    @Override
    public void emptyPassword(boolean flag) {
        passwordEdt.showError(flag);
    }

    @Override
    public void publishCustomerData(UserData body) {
        getElepurApp().loginUser(this, body);
    }

    @Override
    public String getNotificationId() {
        return SecurePrefManager.with(this).get(Constant.NOTIFICATION_ID).defaultValue("").go();
    }
}
