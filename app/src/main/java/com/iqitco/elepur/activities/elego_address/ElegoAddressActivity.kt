package com.iqitco.elepur.activities.elego_address

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.activities.map.MapActivity
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog
import com.iqitco.elepur.items.BottomDialogItem1
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.elego_address.ElegoAddressContract
import com.iqitco.elepur_api.contracts.elego_address.ElegoAddressPresenter
import com.iqitco.elepur_api.modules.CustomerAddress
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea
import com.master.permissionhelper.PermissionHelper
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.elego_address_activity.*
import java.util.*

/**
 * date 7/8/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ElegoAddressActivity : BaseActivity(), ElegoAddressContract.View {

    private lateinit var presenter: ElegoAddressContract.Presenter
    private var statesDialog: BaseBottomSheetDialog? = null
    private var areaDialog: BaseBottomSheetDialog? = null
    private var addressDialog: BaseBottomSheetDialog? = null
    private var stateId = 0
    private var areaId = 0
    private var latitude = ""
    private var longitude = ""
    private var permissionHelper: PermissionHelper? = null

    private var address: CustomerAddress? = null
    private var confirmDeleteDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.elego_address_activity)
        addressesSpinner.visibility = View.GONE
        areaSpinner.isEnabled = false
        presenter = ElegoAddressPresenter(this)
        presenter.onCreate()

        stateSpinner.setOnClickListener {
            if (statesDialog != null && !statesDialog!!.isShowing) {
                statesDialog!!.show()
            }
        }

        areaSpinner.setOnClickListener {
            if (areaDialog != null && !areaDialog!!.isShowing && stateId > 0) {
                areaDialog!!.show()
            }
        }

        addressesSpinner.setOnClickListener {
            if (addressDialog != null && !addressDialog!!.isShowing) {
                addressDialog!!.show()
            }
        }

        mainBtn.setOnClickListener {
            if (checkAllField()) {
                return@setOnClickListener
            }

            if (address == null) {
                presenter.addCustomerAddress()
            } else {
                presenter.updateCustomerLocation()
            }
        }

        deleteBtn.setOnClickListener(this::onDeleteClick)

        openMapBtn.setOnClickListener {
            permissionHelper = PermissionHelper(this, arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 100)
            permissionHelper!!.request(object : PermissionHelper.PermissionCallback {
                override fun onPermissionGranted() {
                    val intent = Intent(this@ElegoAddressActivity, MapActivity::class.java)
                    try {
                        intent.putExtra(MapActivity.LAT, latitude)
                        intent.putExtra(MapActivity.LONG, longitude)
                    } catch (ex: Exception) {

                    }
                    startActivityForResult(intent, MapActivity.REQUEST_CODE)
                }

                override fun onIndividualPermissionGranted(grantedPermission: Array<String>) {

                }

                override fun onPermissionDenied() {

                }

                override fun onPermissionDeniedBySystem() {

                }
            })
        }

        nameEdt.requestFocus()
    }

    /**
     * return true if there is error, otherwise false
     */
    private fun checkAllField(): Boolean {
        nameEdt.showError(getName().isEmpty())
        stateSpinner.setError(stateId <= 0)
        areaSpinner.setError(areaId <= 0)
        mobileEdt.showError(getMobile().isEmpty())
        descEdt.showError(getAddressDescription().isEmpty())
        if (latitude.isEmpty() || longitude.isEmpty()) {
            openMapBtn.setBackgroundResource(R.drawable.edit_text_small_rounded_white)
        } else {
            openMapBtn.setBackgroundResource(R.drawable.small_rounded_white)
        }

        return nameEdt.isFlag ||
                stateSpinner.flag ||
                areaSpinner.flag ||
                mobileEdt.isFlag ||
                descEdt.isFlag ||
                latitude.isEmpty() ||
                longitude.isEmpty()
    }

    override fun getAddressId(): Int? {
        return address!!.locationId
    }

    private fun onDeleteClick(view: View) {
        if (confirmDeleteDialog == null) {
            confirmDeleteDialog = MaterialDialog.Builder(this)
                    .backgroundColor(resources.getColor(R.color.border))
                    .contentColor(Color.WHITE)
                    .positiveColor(Color.WHITE)
                    .negativeColor(Color.WHITE)
                    .content(R.string.message_confirm_address_delete)
                    .positiveText(R.string.btn_delete)
                    .negativeText(R.string.btn_cancel)
                    .cancelable(false)
                    .autoDismiss(false)
                    .canceledOnTouchOutside(false)
                    .onPositive { dialog, _ ->
                        dialog.dismiss()
                        presenter.deleteAddress()
                    }
                    .onNegative { dialog, _ ->
                        dialog.dismiss()
                    }
                    .build()
        }

        if (!confirmDeleteDialog!!.isShowing) {
            confirmDeleteDialog!!.show()
        }
    }

    private fun initEditAddress() {
        try {
            address = intent.getParcelableExtra(Constant.ADDRESS)
            nameEdt.setText(address!!.locationName)
            statesDialog!!.setSelectedIds(listOf(address!!.stateId))
            mobileEdt.setText(address!!.phone)
            descEdt.setText(address!!.location)
            deleteBtn.visibility = View.VISIBLE
            toolbar.titleTv.setText(R.string.title_edit_address)
            mainBtn.setText(R.string.btn_save_edit)
            try {
                latitude = address!!.latitude!!.toDouble().toString()
                longitude = address!!.longitude!!.toDouble().toString()
            } catch (ex: Exception) {

            }
        } catch (ex: Exception) {
            /*NOTHING*/
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 100) {
            permissionHelper!!.onRequestPermissionsResult(requestCode, permissions as Array<String>, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MapActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                latitude = data!!.getStringExtra(MapActivity.LAT)
                longitude = data.getStringExtra(MapActivity.LONG)
                try {
                    val address: String? = data.getStringExtra(MapActivity.ADDRESS)
                    if (address != null && !address.isBlank()) {
                        if (descEdt.text.toString().isBlank()) {
                            descEdt.setText(address)
                        }
                    }
                } catch (ex: Exception) {
                    Toast.makeText(this, R.string.message_location_not_selected, Toast.LENGTH_LONG).show()
                }
            } else if (resultCode == -1) {
                Toast.makeText(this, R.string.message_location_not_selected, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun publishStatus(list: List<State>) {
        statesDialog = object : BaseBottomSheetDialog(this, false) {

            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>?) {
                for (state in list) {
                    val title = if (Locale.getDefault().toString() == "ar") state.nameStateAr else state.nameStateEn
                    adapter!!.add(BottomDialogItem1(title).withIdentifier(state.id.toLong()))
                }
            }
        }
        statesDialog!!.setOnNonSelect {
            stateId = 0
            areaId = 0
            stateSpinner.getTextTv().text = getString(R.string.title_state)
            areaSpinner.getTextTv().text = getString(R.string.title_area)
            areaSpinner.isEnabled = false
            statesDialog!!.dismiss()
        }

        statesDialog!!.setOnMainButtonClick {
            stateId = it[0].identifier.toInt()
            areaId = 0
            stateSpinner.getTextTv().text = it[0].title
            areaSpinner.getTextTv().text = getString(R.string.title_area)
            presenter.getStatusAreas()
            statesDialog!!.dismiss()
        }
        initEditAddress()
    }

    override fun publishArea(list: List<StateArea>) {
        areaDialog = object : BaseBottomSheetDialog(this, false) {
            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>?) {
                for (area in list) {
                    val title = if (Locale.getDefault().toString() == "ar") area.nameAreaAr else area.nameAreaEn
                    adapter!!.add(BottomDialogItem1(title).withIdentifier(area.id.toLong()))
                }
            }
        }
        areaSpinner.isEnabled = true

        areaDialog!!.setOnNonSelect {
            areaId = 0
            areaSpinner.getTextTv().text = getString(R.string.title_area)
            areaDialog!!.dismiss()
        }

        areaDialog!!.setOnMainButtonClick {
            areaId = it[0].identifier.toInt()
            areaSpinner.getTextTv().text = it[0].title
            areaDialog!!.dismiss()
        }

        if (address != null) {
            areaDialog!!.setSelectedIds(listOf(address!!.areaId))
        }
    }

    override fun publishAddresses(list: List<CustomerAddress>) {
        if (list.isEmpty()) {
            addressesSpinner.isEnabled = false
            return
        }

        addressesSpinner.isEnabled = true
        addressDialog = object : BaseBottomSheetDialog(this, false) {
            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>?) {
                for (address in list) {
                    adapter!!.add(BottomDialogItem1(address.locationName).withIdentifier(address.locationId!!.toLong()))
                }
            }
        }


    }

    override fun getId(): Int {
        return SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
    }

    override fun getStateId(): Int {
        return stateId
    }

    override fun getAreaId(): Int {
        return areaId
    }

    override fun getName(): String {
        return nameEdt.text.toString().trim()
    }

    override fun getMobile(): String {
        return mobileEdt.text.toString().trim()
    }

    override fun getAddressDescription(): String {
        return descEdt.text.toString().trim()
    }

    override fun onAddSuccess() {
        presenter.getCustomerLocations()
        nameEdt.setText("")
        mobileEdt.setText("")
        descEdt.setText("")
        statesDialog!!.setSelectedIds(null)
        setResult(Activity.RESULT_OK)
        finish()
        Toast.makeText(this, R.string.message_add_address_success, Toast.LENGTH_LONG).show()
    }

    override fun onDeleteSuccess() {
        setResult(Activity.RESULT_OK)
        finish()
        Toast.makeText(this, R.string.message_delete_address_success, Toast.LENGTH_LONG).show()
    }

    override fun onUpdateSuccess() {
        setResult(Activity.RESULT_OK)
        finish()
        Toast.makeText(this, R.string.message_update_address_success, Toast.LENGTH_LONG).show()
    }

    override fun getLatitude(): String {
        return latitude
    }

    override fun getLongitude(): String {
        return longitude
    }
}