package com.iqitco.elepur.activities.share_opinion;

import android.os.Bundle;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.share_opinion.ShareOpinionContract;
import com.iqitco.elepur_api.contracts.share_opinion.ShareOpinionPresenter;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ShareOpinionActivity extends BaseActivity implements ShareOpinionContract.View {

    private ShareOpinionContract.Presenter presenter;
    private EditText titleEdt;
    private EditText opinionEdt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_opinion_activity);
        presenter = new ShareOpinionPresenter(this);
        titleEdt = findViewById(R.id.titleEdt);
        opinionEdt = findViewById(R.id.opinionEdt);
        findViewById(R.id.mainBtn).setOnClickListener(view -> presenter.insertMessage());
    }

    @Override
    public String getMessage() {
        return opinionEdt.getText().toString().trim();
    }

    @Override
    public String getHeader() {
        return titleEdt.getText().toString().trim();
    }

    @Override
    public Integer getLoginId() {
        return SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onSuccess(boolean flag) {
        if (flag) {
            new MaterialDialog.Builder(this)
                    .content(R.string.message_opinion_thanks)
                    .autoDismiss(true)
                    .positiveText(R.string.btn_close)
                    .dismissListener(dialog -> finish())
                    .show();
        }
    }
}
