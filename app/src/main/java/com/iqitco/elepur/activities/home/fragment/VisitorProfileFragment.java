package com.iqitco.elepur.activities.home.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.edit_seller_profile.EditSellerProfileActivity;
import com.iqitco.elepur.activities.elego_address.ElegoAddressListActivity;
import com.iqitco.elepur.activities.favorite_ads.FavoriteAdsActivity;
import com.iqitco.elepur.activities.favorite_stores.FavoriteStoresActivity;
import com.iqitco.elepur.activities.home.BaseHomeFragment;
import com.iqitco.elepur.activities.login.LoginActivity;
import com.iqitco.elepur.activities.notification.NotificationActivity;
import com.iqitco.elepur.activities.update_customer_profile.UpdateCustomerProfileActivity;
import com.iqitco.elepur.items.ProfileItem;
import com.iqitco.elepur.providers.FileProvider;
import com.iqitco.elepur.widget.CustomToolbarView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.home.VisitorProfileContract;
import com.iqitco.elepur_api.contracts.home.VisitorProfilePresenter;
import com.iqitco.elepur_api.modules.CustomerPic;
import com.iqitco.elepur_api.modules.SellerProfile;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Locale;

/**
 * date: 2018-07-01
 *
 * @author Mohammad Al-Najjar
 */
public class VisitorProfileFragment extends BaseHomeFragment implements VisitorProfileContract.View {

    private static final String TAG = VisitorProfileFragment.class.getSimpleName();

    private int loginId;
    private CustomToolbarView toolbar;
    private ImageView backImageLayer;
    private ImageView backImage;
    private ImageView profileImage;
    private TextView usernameTv;
    private VisitorProfileContract.Presenter presenter;
    private String shareLink;

    private String role;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.visitor_profile_fragment, container, false);
        loginId = SecurePrefManager.with(getContext()).get(Constant.ID_ID).defaultValue(-1).go();
        role = SecurePrefManager.with(getContext()).get(Constant.ROLL).defaultValue("").go();

        backImageLayer = view.findViewById(R.id.backImageLayer);
        backImage = view.findViewById(R.id.backImage);
        profileImage = view.findViewById(R.id.profileImage);
        usernameTv = view.findViewById(R.id.usernameTv);

        toolbar = view.findViewById(R.id.toolbar);
        toolbar.getTitleTv().setText(R.string.title_profile);
        toolbar.getBackBtn().setOnClickListener(this::onMenuClick);
        toolbar.getBackBtn().setImageResource(R.drawable.ic_menu);

        getHomeSellerActivity().changeStatusBarColor(getResources().getColor(R.color.colorPrimary2), false);
        FastItemAdapter<ProfileItem> adapter = new FastItemAdapter<>();
        adapter.withOnClickListener(this::onItemClick);
        if (role.equalsIgnoreCase("seller")) {
            adapter.add(new ProfileItem(R.string.title_profile_notification, R.drawable.ic_profile_notification).withIdentifier(1));
            adapter.add(new ProfileItem(R.string.title_settings_change_language1, R.drawable.ic_profile_language).withIdentifier(5));
        } else {
            adapter.add(new ProfileItem(R.string.title_profile_notification, R.drawable.ic_profile_notification).withIdentifier(1));
            adapter.add(new ProfileItem(R.string.title_profile_invite_friends, R.drawable.ic_profile_invite_friends).withIdentifier(2));
            adapter.add(new ProfileItem(R.string.title_profile_fav_adv, R.drawable.ic_profile_fav_adv).withIdentifier(3));
            adapter.add(new ProfileItem(R.string.title_profile_fav_stores, R.drawable.ic_profile_fav_store).withIdentifier(4));
            adapter.add(new ProfileItem(R.string.title_settings_change_language1, R.drawable.ic_profile_language).withIdentifier(5));
            adapter.add(new ProfileItem(R.string.title_profile_elego_address, R.drawable.ic_profile_elego).withIdentifier(6));
        }

        Button mainBtn = view.findViewById(R.id.mainBtn);
        mainBtn.setOnClickListener(this::onMainBtnClick);
        if (loginId > 0) {
            if (role.equalsIgnoreCase("seller")) {
                mainBtn.setText(R.string.title_edit_store);
            } else {
                mainBtn.setText(R.string.btn_edit_my_account);
            }
        }

        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        presenter = new VisitorProfilePresenter(this);
        presenter.onCreate();

        return view;
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

    private void onMainBtnClick(View view) {
        Class c;
        if (loginId > 0) {
            if (role.equalsIgnoreCase("seller")) {
                c = EditSellerProfileActivity.class;
            } else {
                c = UpdateCustomerProfileActivity.class;
            }
        } else {
            c = LoginActivity.class;
        }
        startActivityForResult(new Intent(getContext(), c), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            presenter.onCreate();
            Toast.makeText(getContext(), R.string.message_update_profile_done, Toast.LENGTH_LONG).show();
        }
    }

    private boolean onItemClick(View v, IAdapter<ProfileItem> adapter, ProfileItem item, int position) {
        long id = item.getIdentifier();
        if (id == 1) {
            getBaseActivity().startActivity(new Intent(getContext(), NotificationActivity.class));
        } else if (id == 2) {
            shareWith();
        } else if (id == 3) {
            getBaseActivity().showVisitorDialog(roleId -> getBaseActivity().startActivity(new Intent(getBaseActivity(), FavoriteAdsActivity.class)));
        } else if (id == 4) {
            getBaseActivity().showVisitorDialog(roleId -> getBaseActivity().startActivity(new Intent(getBaseActivity(), FavoriteStoresActivity.class)));
        } else if (id == 5) {
            getBaseActivity().changeLanguage(null);
        } else if (id == 6) {
            getBaseActivity().showVisitorDialog(roleId -> getBaseActivity().startActivity(new Intent(getBaseActivity(), ElegoAddressListActivity.class)));
        }
        return true;
    }

    private void shareWith() {
        try {
            final Intent intent = new Intent(Intent.ACTION_SEND);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_share);
            File file = new File(getContext().getCacheDir(), "img_share.png");
            OutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            Uri photoURI = FileProvider.getUriForFile(getContext(), "com.iqitco.elepur.providers.FileProvider", file);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, shareLink);
            /*intent.putExtra(Intent.EXTRA_STREAM, photoURI);*/
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            getActivity().startActivity(Intent.createChooser(intent, getString(R.string.app_name)));
        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }
    }

    @Override
    public void publishShareLink(String str) {
        shareLink = str;
    }

    @Override
    public Integer getLoginId() {
        return loginId;
    }

    @Override
    public void publishSellerProfile(SellerProfile profile) {
        String name;
        if (Locale.getDefault().toString().equalsIgnoreCase("ar")) {
            name = profile.getShopNameAr();
        } else {
            name = profile.getShopName();
        }
        fillData(name, profile.getPicLogo(), profile.getPicCover());
    }

    @Override
    public void publishCustomerPix(CustomerPic pic) {
        fillData(pic.getName(), pic.getPic(), null);
    }

    @Override
    public String getUserRole() {
        return role;
    }

    private void fillData(String name, String logoImageUrl, String backImageUrl) {
        usernameTv.setText(name);

        SecurePrefManager securePrefManager = SecurePrefManager.with(getContext());
        boolean isFacebookUser = securePrefManager.get(Constant.IS_FACEBOOK_USER).defaultValue(false).go();
        String url;
        if (isFacebookUser) {
            String facebookId = securePrefManager.get(Constant.FACEBOOK_ID).defaultValue("").go();
            String facebookUserAuth = securePrefManager.get(Constant.FACEBOOK_USER_AUTH).defaultValue("").go();
            url = "http://graph.facebook.com/" + facebookId + "/picture?type=large";
        } else {
            url = logoImageUrl;
        }

        Glide.with(getContext())
                .load(url)
                .apply(
                        new RequestOptions()
                                .centerCrop()
                                .fallback(R.drawable.img_profile_default)
                                .placeholder(R.drawable.img_profile_default)
                                .error(R.drawable.img_profile_default)
                )
                .into(profileImage);

        if (backImageUrl == null || backImageUrl.isEmpty()) {
            backImageUrl = logoImageUrl;
        }

        Glide.with(getContext())
                .asBitmap()
                .load(backImageUrl)
                .apply(
                        new RequestOptions()
                                .centerCrop()
                                .fallback(R.drawable.img_profile_background2)
                                .placeholder(R.drawable.img_profile_background2)
                                .error(R.drawable.img_profile_background2)
                )
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap bitmap, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        backImage.setImageBitmap(bitmap);
                        backImage.setColorFilter(Color.parseColor("#b3000000"));
                        backImage.setDrawingCacheEnabled(true);
                        try {
                            Bitmap resource = backImage.getDrawingCache();
                            Palette palette = Palette.from(resource).generate();

                            Palette.Swatch swatch = null;
                            if (palette.getDarkVibrantSwatch() != null) {
                                swatch = palette.getDarkVibrantSwatch();
                            } else if (palette.getVibrantSwatch() != null) {
                                swatch = palette.getVibrantSwatch();
                            } else if (palette.getLightVibrantSwatch() != null) {
                                swatch = palette.getLightVibrantSwatch();
                            } else if (palette.getDarkMutedSwatch() != null) {
                                swatch = palette.getDarkMutedSwatch();
                            } else if (palette.getLightMutedSwatch() != null) {
                                swatch = palette.getLightMutedSwatch();
                            } else if (palette.getDominantSwatch() != null) {
                                swatch = palette.getDominantSwatch();
                            } else if (palette.getMutedSwatch() != null) {
                                swatch = palette.getMutedSwatch();
                            }

                            if (swatch != null) {
                                loadColors(swatch.getRgb());
                            }
                        } catch (Exception e) {
                            /*NOTHING*/
                        }
                        backImage.setDrawingCacheEnabled(false);
                        return true;
                    }
                })
                .into(backImage);
    }

    private void loadColors(int color) {
        int transparent = Color.argb(220, Color.red(color), Color.green(color), Color.blue(color));
        double darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
        boolean isDarkness = darkness > 0.5;
        getBaseActivity().changeStatusBarColor(color, !isDarkness);
    }
}
