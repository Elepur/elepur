package com.iqitco.elepur.activities.settings;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.widget.SettingsSwitchView;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

/**
 * date 6/19/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class SettingsActivity extends BaseActivity {

    private SecurePrefManager securePrefManager;
    private final String GENERAL_FLAG = "generalFlag";
    private final String MANAGEMENT_FLAG = "managementFlag";
    private final String DELIVERY_FLAG = "deliveryFlag";
    private final String SOUND_FLAG = "soundFlag";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        securePrefManager = SecurePrefManager.with(this);

        SettingsSwitchView generalSwitch = findViewById(R.id.generalSwitch);
        SettingsSwitchView managementSwitch = findViewById(R.id.managementSwitch);
        SettingsSwitchView deliverySwitch = findViewById(R.id.deliverySwitch);
        SettingsSwitchView soundSwitch = findViewById(R.id.soundSwitch);
        TextView changeLanguageBtn = findViewById(R.id.changeLanguageBtn);
        TextView clearSearchBtn = findViewById(R.id.clearSearchBtn);


        generalSwitch.getSwitchFlag().setChecked(securePrefManager.get(GENERAL_FLAG).defaultValue(false).go());
        managementSwitch.getSwitchFlag().setChecked(securePrefManager.get(MANAGEMENT_FLAG).defaultValue(false).go());
        deliverySwitch.getSwitchFlag().setChecked(securePrefManager.get(DELIVERY_FLAG).defaultValue(false).go());
        soundSwitch.getSwitchFlag().setChecked(securePrefManager.get(SOUND_FLAG).defaultValue(false).go());

        generalSwitch.setOnSwitchChange((view, isCheck) -> securePrefManager.set(GENERAL_FLAG).value(isCheck).go());
        managementSwitch.setOnSwitchChange((view, isCheck) -> securePrefManager.set(MANAGEMENT_FLAG).value(isCheck).go());
        deliverySwitch.setOnSwitchChange((view, isCheck) -> securePrefManager.set(DELIVERY_FLAG).value(isCheck).go());
        soundSwitch.setOnSwitchChange((view, isCheck) -> securePrefManager.set(SOUND_FLAG).value(isCheck).go());

        changeLanguageBtn.setOnClickListener(this::changeLanguage);
        clearSearchBtn.setOnClickListener(this::onClearClick);

    }

    private void onClearClick(View view) {

    }


}
