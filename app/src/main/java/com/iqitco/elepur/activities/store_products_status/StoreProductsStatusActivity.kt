package com.iqitco.elepur.activities.store_products_status

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.activities.products.EditProductActivity
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog
import com.iqitco.elepur.items.BottomDialogItem1
import com.iqitco.elepur.items.ItemDecorationAlbumColumns
import com.iqitco.elepur.items.ProductStatusItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.store_products_status.StoreProductsStatusContract
import com.iqitco.elepur_api.contracts.store_products_status.StoreProductsStatusPresenter
import com.iqitco.elepur_api.modules.StoreProductStatus
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.store_products_status_activity.*

/**
 * date 8/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class StoreProductsStatusActivity : BaseActivity(), StoreProductsStatusContract.View {

    private val adapter = FastItemAdapter<ProductStatusItem>()
    private lateinit var presenter: StoreProductsStatusContract.Presenter
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var endlessListener: EndlessRecyclerOnScrollListener
    private lateinit var removeDialog: BaseBottomSheetDialog
    private var userId: Int = -1
    private var selectedAdvToBeDeleted = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.store_products_status_activity)
        toolbar.titleTv.text = getString(R.string.title_ads_result)
        presenter = StoreProductsStatusPresenter(this)
        layoutManager = GridLayoutManager(this, 1)
        endlessListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                presenter.getSellerProducts(currentPage + 1)
            }
        }
        adapter.withOnClickListener(this::onItemClick)
        adapter.withEventHook(
                object : ClickEventHook<ProductStatusItem>() {
                    override fun onBindMany(viewHolder: RecyclerView.ViewHolder?): MutableList<View> {
                        val holder = viewHolder as ProductStatusItem.ViewHolder
                        return listOf<View>(holder.editBtn, holder.removeBtn) as MutableList<View>
                    }

                    override fun onClick(v: View?, position: Int, fastAdapter: FastAdapter<ProductStatusItem>?, item: ProductStatusItem?) {
                        when (v!!.id) {
                            R.id.editBtn -> {
                                startActivityForResult(Intent(this@StoreProductsStatusActivity, EditProductActivity::class.java).putExtra(Constant.ADV_ID, item!!.product.advId), 1)
                            }
                            R.id.removeBtn -> {
                                showRemoveDialog(item!!, position)
                            }
                        }
                    }
                }
        )
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager
        list.addItemDecoration(ItemDecorationAlbumColumns(context!!.resources.getDimensionPixelSize(R.dimen.mainCategorySpacing), layoutManager.spanCount))
        list.addOnScrollListener(endlessListener)
        list.adapter = adapter
        endlessListener.enable()
        userId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
        clearBtn.setOnClickListener { searchEdt.setText("") }
        swipeRefreshLayout.setOnRefreshListener {
            refreshData()
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.accent)
        searchEdt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                refreshData()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        refreshData()
    }

    fun refreshData() {
        endlessListener.disable()
        endlessListener.resetPageCount()
    }

    private fun showRemoveDialog(item: ProductStatusItem, position: Int) {
        removeDialog = object : BaseBottomSheetDialog(this) {
            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>?) {
                adapter!!.add(BottomDialogItem1(getString(R.string.btn_remove_product_eleper)).withIdentifier(1))
                adapter.add(BottomDialogItem1(getString(R.string.btn_remove_product_store)).withIdentifier(2))
            }
        }
        removeDialog.setOnNonSelect { removeDialog.dismiss() }
        removeDialog.setOnMainButtonClick {
            val id = it[0].identifier.toInt()
            selectedAdvToBeDeleted = position
            if (id == 1) {
                presenter.removeFromElepur(item.product.advId!!)
            } else {
                presenter.removeFromStore(item.product.advId!!)
            }
            removeDialog.dismiss()
        }
        removeDialog.mainBtn.setBackgroundColor(resources.getColor(R.color.border))
        if (!removeDialog.isShowing) {
            removeDialog.show()
        }
    }

    private fun onItemClick(view: View?, adapter: IAdapter<ProductStatusItem>?, item: ProductStatusItem?, position: Int): Boolean {
        startActivity(
                Intent(this, ShowAdsActivity::class.java)
                        .putExtra(Constant.ADV_ID, item!!.product.advId)
                        .putExtra(Constant.SELLER_ID, userId)
        )
        return true
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun getId(): Int {
        return userId
    }

    override fun getSearchStr(): String {
        return searchEdt.text.toString().trim()
    }

    override fun onRemoveProductSuccess(check: Boolean) {
        if (check) {
            /*endlessListener.disable()
            endlessListener.resetPageCount()*/
            if (selectedAdvToBeDeleted > -1) {
                adapter.remove(selectedAdvToBeDeleted)
            }
        }
    }

    override fun publishProducts(list: List<StoreProductStatus>, pageNumber: Int) {
        if (pageNumber == 1) {
            adapter.clear()
        }
        for (product in list) {
            adapter.add(ProductStatusItem(product).withIdentifier(product.rowNum!!.toLong()))
        }
        if (pageNumber == 1) {
            endlessListener.enable()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            refreshData()
        }
    }
}