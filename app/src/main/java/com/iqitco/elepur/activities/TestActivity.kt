package com.iqitco.elepur.activities

import android.os.Bundle
import android.view.View
import com.iqitco.elepur.R
import kotlinx.android.synthetic.main.test_activity.*

/**
 * date 7/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class TestActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.test_activity)
        btn.setOnClickListener {
            slideDown(btn, 1F, 1F)
        }
    }

    fun slideDown(view: View, fromYValue: Float, toFloatValue: Float) {
        view.animate().translationY(-300F).setDuration(100).start()
        /*val slide = TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, fromYValue,
                Animation.RELATIVE_TO_SELF, toFloatValue)

        slide.duration = 150
        slide.fillAfter = true
        slide.isFillEnabled = true
        slide.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                view.visibility = View.INVISIBLE
            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })
        view.startAnimation(slide)*/

    }

}