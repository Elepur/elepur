package com.iqitco.elepur.activities.home.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.home.BaseHomeFragment
import com.iqitco.elepur.items.CategoryBrandItem
import com.iqitco.elepur_api.Constant.CATEGORY_ID
import com.iqitco.elepur_api.contracts.home.CategoryBrandsContract
import com.iqitco.elepur_api.contracts.home.CategoryBrandsPresneter
import com.iqitco.elepur_api.modules.Brand
import com.iqitco.elepur_api.modules.MainPageSection
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import kotlinx.android.synthetic.main.category_brands_fragment.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class CategoryBrandsFragment : BaseHomeFragment(), CategoryBrandsContract.View {

    private var categoryId: Int = 0
    private lateinit var presenter: CategoryBrandsContract.Presenter
    private val adapter = FastItemAdapter<CategoryBrandItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            categoryId = arguments?.getInt(CATEGORY_ID)!!
        } catch (ex: Exception) {
            throw Exception("categoryId cannot be null")
        }
        presenter = CategoryBrandsPresneter(this)
        adapter.withOnClickListener(this::onBrandList)
        return inflater.inflate(R.layout.category_brands_fragment, container, false)
    }

    private fun onBrandList(v: View?, adapter: IAdapter<CategoryBrandItem>?, item: CategoryBrandItem?, position: Int?): Boolean {
        val brandList = ArrayList<Int>()
        brandList.add(item!!.identifier.toInt())
        val parentFragment = parentFragment as MainPageContainerFragment
        parentFragment.changeFragment(CategoryAllAdsFragment.newInstance(categoryId, brandList))
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.adapter = adapter
        list.layoutManager = LinearLayoutManager(context)
        presenter.onCreate()
        val imageResId = when (categoryId) {
            1 -> R.drawable.ic_category_phone
            2 -> R.drawable.ic_category_tablets
            3 -> R.drawable.ic_category_back_cover
            4 -> R.drawable.ic_category_maintenance
            5 -> R.drawable.ic_category_smart_watch
            6 -> R.drawable.ic_category_headphone
            7 -> R.drawable.ic_category_3d_classes
            8 -> R.drawable.ic_category_sims
            else -> {
                throw Exception("categoryId should be between 1 and 8")
            }
        }

        categoryImage.setImageResource(imageResId)
        allBrandsBtn.setOnClickListener {
            val brandList = ArrayList<Int>()
            for (item in adapter.adapterItems) {
                brandList.add(item.identifier.toInt())
            }
            val parentFragment = parentFragment as MainPageContainerFragment
            parentFragment.changeFragment(CategoryAllAdsFragment.newInstance(categoryId, brandList))
        }

    }


    override fun onDestroyView() {
        presenter.onDestroy()
        super.onDestroyView()
    }

    override fun getCategoryId(): Int {
        return categoryId
    }

    override fun publishCategory(category: MainPageSection) {
        toolbar.titleTv.text = if (Locale.getDefault().toString().equals("ar", true)) category.categoryNameAr else category.categoryName
    }

    override fun publishBrands(list: List<Brand>) {
        adapter.clear()
        for (brand in list) {
            adapter.add(CategoryBrandItem(brand).withIdentifier(brand.id.toLong()))
        }
    }
}