package com.iqitco.elepur.activities.edit_seller_profile

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.dialogs.SelectTimeDialog
import com.iqitco.elepur.util.DateUtility
import com.iqitco.elepur.widget.CustomEditTextDialogView
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.modules.MainPageSection
import com.iqitco.elepur_api.modules.SellerProfile
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea
import com.iqitco.elepur_api.modules.request.RequestUpdateSellerProfile
import com.iqitco.elepur_api.viewmodel.EditSellerProfileViewModel
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.edit_seller_profile_activity.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * date 8/17/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class EditSellerProfileActivity : BaseActivity() {


    private lateinit var sellerProfile: SellerProfile
    private lateinit var viewModel: EditSellerProfileViewModel
    private lateinit var selectTimeDialog: SelectTimeDialog

    private var userId = -1
    private var firstTimeInit = true
    private var openTimeCalendar = Calendar.getInstance()
    private var closeTimeCalendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_seller_profile_activity)
        userId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
        initViewModel()


        selectTimeDialog = SelectTimeDialog(context)
        selectTimeDialog.setOnTimesSelect { openTime, closeTime, text ->
            openTimeCalendar = openTime
            closeTimeCalendar = closeTime
            workTimeEdt.setText(text)
            selectTimeDialog.dismiss()
        }

        workTimeEdt.inputType = InputType.TYPE_NULL
        workTimeEdt.setOnKeyListener(null)
        workTimeEdt.setOnTouchListener { _, event ->
            if (MotionEvent.ACTION_UP == event.action) {
                selectTimeDialog.show()
            }
            false
        }
        workTimeEdt.setOnFocusChangeListener { _, hasFocus -> if (hasFocus) selectTimeDialog.show() }

        registrationBtn.setOnClickListener(this::onEditClick)
    }

    private fun onEditClick(view: View) {
        val body = RequestUpdateSellerProfile()

        body.iDSeller = userId
        body.shopName = sellerProfile.shopName
        body.shopNameAr = sellerProfile.shopNameAr
        body.elego = if (serviceCheck.checkBox.isChecked) 1 else 0
        body.kind = descEnEdt.text!!.toString()
        body.kindAr = descArEdt.text!!.toString()

        body.location = addressEdt.text!!.toString()
        if (body.location.isNullOrEmpty()) {
            return
        }

        body.ownerName = ownerNameEdt.text.toString()
        if (body.ownerName.isNullOrEmpty()) {
            return
        }

        body.phone = telephoneEdt.text.toString()
        if (body.phone.isNullOrEmpty()) {
            return
        }

        if (stateEdt.selectionIds.size == 0) {
            return
        } else {
            body.stateId = stateEdt.selectionIds[0]
        }

        if (areaEdt.selectionIds.size == 0) {
            return
        } else {
            body.areaId = areaEdt.selectionIds[0]
        }
        body.sellerCategory = jobEdt.selectionIds
        body.picLogo = logoImage.createBase64()
        body.coverPhoto = frontImage.createBase64()
        body.picLogoFormat = "png"
        body.coverPhotoFormat = "png"

        body.timeOpen = DateUtility.getTime(openTimeCalendar)
        body.timeClose = DateUtility.getTime(closeTimeCalendar)
        showLoading()
        viewModel.updateSellerProfile(body)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        frontImage.onActivityResult(requestCode, resultCode, data)
        logoImage.onActivityResult(requestCode, resultCode, data)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(EditSellerProfileViewModel::class.java)

        viewModel.sellerProfileLd.observe(this, Observer {
            if (it!!.isStatus) {
                sellerProfile = it.data
                fillDate()
            } else {
                nestedScrollView.visibility = View.INVISIBLE
                closeLayout.visibility = View.VISIBLE
            }
        })

        viewModel.onUpdateSellerProfile.observe(this, Observer {
            if (it!!) {
                Toast.makeText(this@EditSellerProfileActivity, R.string.message_update_profile_done, Toast.LENGTH_LONG).show()
                hideLoading()
                finish()
            }
        })

        viewModel.messages.observe(this, Observer { showUserMessage(it!!) })
        viewModel.statesList.observe(this, Observer { publishStates(it!!) })
        viewModel.areasList.observe(this, Observer { publishAreas(it!!) })
        viewModel.categoriesList.observe(this, Observer { publishCategories(it!!) })
        viewModel.getSellerProfile(userId)
    }

    private fun publishCategories(list: List<MainPageSection>) {
        val items = ArrayList<CustomEditTextDialogView.Item>()
        for (category in list) {
            val title = if (Locale.getDefault().toString().equals("ar", true)) category.categoryNameAr else category.categoryName
            items.add(CustomEditTextDialogView.Item(category.id.toLong(), title))
        }
        jobEdt.initDialog(items, true)
        jobEdt.setSelectedIds(sellerProfile.sellerCategory)
    }

    private fun publishStates(list: List<State>) {
        val items = ArrayList<CustomEditTextDialogView.Item>()
        for (state in list) {
            val title = if (Locale.getDefault().toString().equals("ar", true)) state.nameStateAr else state.nameStateEn
            items.add(CustomEditTextDialogView.Item(state.id.toLong(), title))
        }
        stateEdt.initDialog(items, false)
        stateEdt.setOnMainButtonClick { viewModel.getAreas(stateEdt.selectionIds[0]) }
        stateEdt.setOnNonSelect { areaEdt.initDialog(listOf(), false) }
        if (firstTimeInit) {
            stateEdt.setSelectedIds(listOf(sellerProfile.stateId))
        }
    }

    private fun publishAreas(list: List<StateArea>) {
        val items = ArrayList<CustomEditTextDialogView.Item>()
        for (area in list) {
            val title = if (Locale.getDefault().toString().equals("ar", true)) area.nameAreaAr else area.nameAreaEn
            items.add(CustomEditTextDialogView.Item(area.id.toLong(), title))
        }
        areaEdt.initDialog(items, false)
        if (firstTimeInit) {
            areaEdt.setSelectedIds(listOf(sellerProfile.areaId))
            firstTimeInit = false
        }
    }

    @SuppressLint("SetTextI18n")
    private fun fillDate() {
        logoImage.loadImage(sellerProfile.picLogo)
        frontImage.loadImage(sellerProfile.picCover)
        shopNameEdt.isEnabled = false
        emailEdt.isEnabled = false
        shopNameEdt.setTextColor(resources.getColor(R.color.textColorSec))
        emailEdt.setTextColor(resources.getColor(R.color.textColorSec))
        if (Locale.getDefault().toString().equals("ar", true)) {
            shopNameEdt.setText(sellerProfile.shopNameAr)
        } else {
            shopNameEdt.setText(sellerProfile.shopName)
        }
        ownerNameEdt.setText(sellerProfile.ownerName)
        descArEdt.setText(sellerProfile.kindAr)
        descEnEdt.setText(sellerProfile.kind)
        emailEdt.visibility = View.GONE
        telephoneEdt.setText(sellerProfile.phone)
        addressEdt.setText(sellerProfile.location)
        serviceCheck.checkBox.isChecked = sellerProfile.elego!! == 1
        selectTimeDialog.setTime(sellerProfile.timeOpen, sellerProfile.timeClose)
        viewModel.getStates()
        viewModel.getCategories()
    }

}