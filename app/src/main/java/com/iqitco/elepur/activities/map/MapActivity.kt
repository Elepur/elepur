package com.iqitco.elepur.activities.map

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import kotlinx.android.synthetic.main.map_activity.*
import java.util.*

/**
 * date 7/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class MapActivity : BaseActivity(),
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveStartedListener {

    companion object {
        val LAT = "lat"
        val LONG = "long"
        val ADDRESS = "address"
        val REQUEST_CODE = 13121
    }

    private val GPS_RESULT_CODE = 1020
    private var mMap: GoogleMap? = null
    private var googleApiClient: GoogleApiClient? = null
    private var latitude: String? = null
    private var longitude: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.map_activity)
        try {
            latitude = intent.getStringExtra(LAT)
            longitude = intent.getStringExtra(LONG)
        } catch (ex: Exception) {
            /*NOTHING*/
        }
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        val mapView = mapFragment.view
        mapView!!.scaleX = resources.getInteger(R.integer.scale_x).toFloat()
        toolbar.visibility = View.GONE

        mapFragment.getMapAsync {
            mMap = it
            if (mMap == null) {
                return@getMapAsync
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return@getMapAsync
            }
            googleApiClient = GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build()


            googleApiClient!!.connect()
            it!!.isMyLocationEnabled = true
            it.setOnMapLongClickListener(this)
            it.setOnCameraMoveListener(this)
            it.setOnCameraMoveStartedListener(this)


        }

        btnConfirmAddress.setOnClickListener {
            var result = Activity.RESULT_OK
            val intent = Intent()
            try {
                val latLng = mMap!!.cameraPosition.target
                intent.putExtra(LAT, latLng.latitude.toString())
                intent.putExtra(LONG, latLng.longitude.toString())
                val geoCoder = Geocoder(this, Locale.getDefault())
                val address = geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1)[0]
                intent.putExtra(ADDRESS, address.getAddressLine(0))
            } catch (ex: Exception) {
                result = -1
            }
            setResult(result, intent)
            finish()
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        finish()
    }

    override fun onConnected(p0: Bundle?) {
        if (latitude != null && !latitude!!.isEmpty() && longitude != null && !longitude!!.isEmpty()) {
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude!!.toDouble(), longitude!!.toDouble()), 13f))
            mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(13f), 4000, null)
            return
        }
        if (checkGps()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }
            val location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)
            if (location != null) {
                try {
                    val geocoder = Geocoder(this, Locale.getDefault())
                    val address = geocoder.getFromLocation(location.latitude, location.longitude, 1)[0]
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 13f))
                    mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
                    mMap!!.animateCamera(CameraUpdateFactory.zoomTo(13f), 4000, null)
                } catch (e: Exception) {
                    Log.d("Tag", "onConnected: " + e.localizedMessage)
                }

            }
        } else {
            openSettings()
        }
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onMapLongClick(p0: LatLng?) {

    }

    override fun onCameraMove() {

    }

    override fun onCameraMoveStarted(p0: Int) {

    }

    private fun checkGps(): Boolean {
        return (getSystemService(Context.LOCATION_SERVICE) as LocationManager)
                .isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun openSettings() {
        MaterialDialog.Builder(this)
                .content(R.string.title_open_gps)
                .positiveText(android.R.string.yes)
                .negativeText(android.R.string.no)
                .onPositive { dialog, which -> startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_RESULT_CODE) }
                .onNegative { dialog, which -> dialog.dismiss() }
                .show()
    }
}