package com.iqitco.elepur.activities.elego_address

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.items.AddressItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.elego_address.ElegoAddressListContract
import com.iqitco.elepur_api.contracts.elego_address.ElegoAddressListPresenter
import com.iqitco.elepur_api.modules.CustomerAddress
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.elego_address_list_activity.*

/**
 * date 7/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ElegoAddressListActivity : BaseActivity(), ElegoAddressListContract.View {

    private val adapter = FastItemAdapter<AddressItem>()
    private lateinit var presenter: ElegoAddressListContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.elego_address_list_activity)
        adapter.withOnClickListener(this::onItemClick)
        presenter = ElegoAddressListPresenter(this)
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter
        mainBtn.setOnClickListener {
            startActivityForResult(Intent(this, ElegoAddressActivity::class.java), 1)
        }
        presenter.onCreate()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            presenter.onCreate()
        }
    }

    private fun onItemClick(view: View?, adapter: IAdapter<AddressItem>?, item: AddressItem, position: Int?): Boolean {
        startActivityForResult(
                Intent(this, ElegoAddressActivity::class.java)
                        .putExtra(Constant.ADDRESS, item.address)
                , 1
        )
        return true
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun publishAddresses(list: List<CustomerAddress>) {
        adapter.clear()
        for (address in list) {
            adapter.add(AddressItem(address).withIdentifier(address.locationId!!.toLong()))
        }
    }

    override fun getId(): Int {
        return SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
    }
}