package com.iqitco.elepur.activities.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import com.akexorcist.localizationactivity.LanguageSetting;
import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.iqitco.elepur.util.DrawableUtility;
import com.iqitco.elepur.widget.AdsInputView;
import com.iqitco.elepur.widget.AdsSeekView;
import com.iqitco.elepur.widget.AdsSwitchView;
import com.iqitco.elepur.widget.CustomToolbarView;
import com.iqitco.elepur_api.contracts.home.AllAdsFilterContract;
import com.iqitco.elepur_api.contracts.home.AllAdsFilterPresenter;
import com.iqitco.elepur_api.modules.Brand;
import com.iqitco.elepur_api.modules.Color;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.iqitco.elepur_api.modules.ProductKind;
import com.iqitco.elepur_api.modules.SubBrand;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AllAdsFilterActivity extends BaseActivity implements AllAdsFilterContract.View {

    public static final String RESULTS = "results";
    public static final int REQUEST_CODE = 123;

    private AllAdsFilterContract.Presenter presenter;

    private CustomToolbarView toolbar;
    private AdsInputView brandIn;
    private AdsInputView categoryIn;
    private AdsInputView classIn;
    private AdsInputView statusIn;
    private AdsInputView colorIn;
    private AdsInputView kindIn;
    private AdsSeekView priceIn;
    private AdsSwitchView elegoSwitchIn;

    private BaseBottomSheetDialog brandsDialog;
    private BaseBottomSheetDialog categoriesDialog;
    private BaseBottomSheetDialog subBrandDialog;
    private BaseBottomSheetDialog colorsDialog;
    private BaseBottomSheetDialog kindDialog;
    private BaseBottomSheetDialog statusDialog;

    private String newOldFlag = null;
    private Integer selectedCategory = null;
    private Result result;
    private int minValue = 0;
    private int maxValue = 2000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_ads_filter_activity);

        if (getIntent().getExtras() != null) {
            result = getIntent().getExtras().getParcelable(RESULTS);
        }

        toolbar = findViewById(R.id.toolbar);
        toolbar.getSecondBtn().setImageDrawable(DrawableUtility.getTintDrawable(this, R.drawable.ic_clear_oval, android.graphics.Color.WHITE));
        toolbar.getSecondBtn().setOnClickListener(v -> backWithResults(new Result()));
        brandIn = findViewById(R.id.brandIn);
        categoryIn = findViewById(R.id.categoryIn);
        classIn = findViewById(R.id.classIn);
        classIn.setEnabled(false);
        kindIn = findViewById(R.id.kindIn);
        kindIn.setVisibility(View.GONE);
        priceIn = findViewById(R.id.priceIn);
        elegoSwitchIn = findViewById(R.id.elegoSwitchIn);

        statusIn = findViewById(R.id.statusIn);
        initStatusDialog();
        statusIn.setOnClickListener(view -> {
            if (statusDialog != null && !statusDialog.isShowing()) {
                statusDialog.show();
            }
        });

        colorIn = findViewById(R.id.colorIn);

        presenter = new AllAdsFilterPresenter(this);
        presenter.onCreate();

        classIn.setOnClickListener(view -> {
            if (subBrandDialog != null && !subBrandDialog.isShowing()) {
                subBrandDialog.show();
            }
        });

        kindIn.setOnClickListener(view -> {
            if (kindDialog != null && !kindDialog.isShowing()) {
                kindDialog.show();
            }
        });

        brandIn.setOnClickListener(view -> {
            if (brandsDialog != null && !brandsDialog.isShowing()) {
                brandsDialog.show();
            }
        });

        categoryIn.setOnClickListener(view -> {
            if (categoriesDialog != null && !categoriesDialog.isShowing()) {
                categoriesDialog.show();
            }
        });

        colorIn.setOnClickListener(view -> {
            if (colorsDialog != null && !colorsDialog.isShowing()) {
                colorsDialog.show();
            }
        });

        priceIn.getRangeSeekBar().setOnRangeSeekbarFinalValueListener((minValue, maxValue) -> {
            this.minValue = minValue.intValue();
            this.maxValue = maxValue.intValue();
        });

        priceIn.getRangeSeekBar().setMinValue(result.getPriceStart());
        priceIn.getRangeSeekBar().setMaxValue(result.getPriceEnd());

        elegoSwitchIn.getSwitchOnOff().setChecked(result.isElegoFlag());

        findViewById(R.id.mainBtn).setOnClickListener(this::onMainClick);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void onMainClick(View view) {
        Result result = new Result();

        if (brandsDialog != null) {
            result.getBrandsIds().addAll(brandsDialog.getSelectionIds());
        }

        if (subBrandDialog != null) {
            result.getSubBrandsIds().addAll(subBrandDialog.getSelectionIds());
        }

        if (colorsDialog != null) {
            result.getColorsIds().addAll(colorsDialog.getSelectionIds());
        }

        if (kindDialog != null) {
            result.getKindsIds().addAll(kindDialog.getSelectionIds());
        }

        if (categoriesDialog != null && categoriesDialog.getSelectionIds().size() > 0) {
            result.setCategoryId(categoriesDialog.getSelectionIds().get(0));
        }

        result.setNewOldFlag(newOldFlag);

        result.setPriceStart(minValue);
        result.setPriceEnd(maxValue);

        result.setElegoFlag(elegoSwitchIn.getSwitchOnOff().isChecked());

        backWithResults(result);
    }

    private void backWithResults(Result result) {
        Intent data = new Intent();
        data.putExtra(RESULTS, result);
        setResult(RESULT_OK, data);
        finish();
    }

    private void initStatusDialog() {
        statusDialog = new BaseBottomSheetDialog(this) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                adapter.add(new BottomDialogItem1(getString(R.string.title_new)).withIdentifier(1));
                adapter.add(new BottomDialogItem1(getString(R.string.title_old)).withIdentifier(2));
            }
        };

        statusDialog.setOnNonSelect(() -> {
            newOldFlag = null;
            statusIn.getTextTv().setText(R.string.title_filter_status);
            if (statusDialog.isShowing()) {
                statusDialog.dismiss();
            }
        });

        statusDialog.setOnMainButtonClick(list -> {
            BottomDialogItem1 item = list.get(0);
            statusIn.getTextTv().setText(item.getTitle());
            if (item.getIdentifier() == 1) {
                newOldFlag = "n";
            } else {
                newOldFlag = "o";
            }
            if (statusDialog.isShowing()) {
                statusDialog.dismiss();
            }
        });

        if (result.getNewOldFlag() != null) {
            List<Integer> selected = new ArrayList<>();
            if (result.getNewOldFlag().equalsIgnoreCase("n")) {
                selected.add(1);
            } else {
                selected.add(2);
            }
            statusDialog.setSelectedIds(selected);
        }

    }

    @Override
    public void publishColors(List<Color> list) {
        colorsDialog = new BaseBottomSheetDialog(this, true) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (Color color : list) {
                    String title;
                    if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
                        title = color.getNameAr();
                    } else {
                        title = color.getNameEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(color.getId()));
                }
            }
        };

        colorsDialog.setOnNonSelect(() -> {
            colorIn.getTextTv().setText(R.string.title_filter_color);
            if (colorsDialog.isShowing()) {
                colorsDialog.dismiss();
            }
        });

        colorsDialog.setOnMainButtonClick(colorsList -> {
            StringBuilder builder = new StringBuilder();
            builder.append(colorsList.get(0).getTitle());
            for (int i = 1; i < colorsList.size(); i++) {
                builder.append(", ").append(colorsList.get(i).getTitle());
            }
            colorIn.getTextTv().setText(builder.toString());
            if (colorsDialog.isShowing()) {
                colorsDialog.dismiss();
            }
        });

        colorsDialog.setSelectedIds(result.getColorsIds());
    }

    @Override
    public void publishBrands(List<Brand> list) {
        brandsDialog = new BaseBottomSheetDialog(this, true) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                Set<Integer> ids = new HashSet<>();
                for (Brand brand : list) {
                    if (ids.add(brand.getId())) {
                        String title;
                        if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
                            title = brand.getBrandsNameAr();
                        } else {
                            title = brand.getBrandsNameEn();
                        }
                        adapter.add(new BottomDialogItem1(title).withIdentifier(brand.getId()));
                    }
                }
            }
        };
        brandsDialog.setOnNonSelect(() -> {
            brandIn.getTextTv().setText(R.string.title_filter_brand);
            if (brandsDialog.isShowing()) {
                brandsDialog.dismiss();
            }
        });
        brandsDialog.setOnMainButtonClick(brandsList -> {
            StringBuilder builder = new StringBuilder();
            builder.append(brandsList.get(0).getTitle());
            for (int i = 1; i < brandsList.size(); i++) {
                builder.append(", ").append(brandsList.get(i).getTitle());
            }
            brandIn.getTextTv().setText(builder.toString());
            if (brandsDialog.isShowing()) {
                brandsDialog.dismiss();
            }

            if (selectedCategory != null) {
                presenter.getSubBrand();
            }
        });

        brandsDialog.setSelectedIds(result.getBrandsIds());
    }

    @Override
    public void publishCategories(List<MainPageSection> list) {
        categoriesDialog = new BaseBottomSheetDialog(this) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (MainPageSection section : list) {
                    String title;
                    if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
                        title = section.getCategoryNameAr();
                    } else {
                        title = section.getCategoryName();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(section.getId()));
                }
            }
        };
        categoriesDialog.setOnNonSelect(() -> {
            selectedCategory = null;
            categoryIn.getTextTv().setText(R.string.title_filter_category);
            if (categoriesDialog.isShowing()) {
                categoriesDialog.dismiss();
            }
            kindIn.setEnabled(false);
        });
        categoriesDialog.setOnMainButtonClick(sectionList -> {
            BottomDialogItem1 item = sectionList.get(0);
            selectedCategory = (int) item.getIdentifier();
            categoryIn.getTextTv().setText(item.getTitle());
            if (categoriesDialog.isShowing()) {
                categoriesDialog.dismiss();
            }
            kindIn.setEnabled(true);
            if (brandsDialog.getSelectionIds().size() > 0) {
                presenter.getSubBrand();
            }
            presenter.getProductKind();
        });

        List<Integer> ids = new ArrayList<>();
        ids.add(result.getCategoryId());
        categoriesDialog.setSelectedIds(ids);
    }

    @Override
    public void publishProductKind(List<ProductKind> list) {
        if (list.size() == 0) {
            kindIn.setVisibility(View.GONE);
            return;
        }
        kindIn.setVisibility(View.VISIBLE);
        kindIn.setEnabled(true);
        kindDialog = new BaseBottomSheetDialog(this, true) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (ProductKind kind : list) {
                    String title;
                    if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
                        title = kind.getKindNameAr();
                    } else {
                        title = kind.getKindNameEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(kind.getId()));
                }
            }
        };
        kindDialog.setOnNonSelect(() -> {
            kindIn.getTextTv().setText(R.string.title_filter_kind);
            if (kindDialog.isShowing()) {
                kindDialog.dismiss();
            }
        });

        kindDialog.setOnMainButtonClick(kindsList -> {
            StringBuilder builder = new StringBuilder();
            builder.append(kindsList.get(0).getTitle());
            for (int i = 1; i < kindsList.size(); i++) {
                builder.append(", ").append(kindsList.get(i).getTitle());
            }
            kindIn.getTextTv().setText(builder.toString());
            if (kindDialog.isShowing()) {
                kindDialog.dismiss();
            }
        });

        kindDialog.setSelectedIds(result.getKindsIds());
    }

    @Override
    public void publishSubBrand(List<SubBrand> list) {
        if (list.size() == 0) {
            classIn.setEnabled(false);
            return;
        }
        classIn.setEnabled(true);
        subBrandDialog = new BaseBottomSheetDialog(this, true) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (SubBrand subBrand : list) {
                    String title;
                    if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
                        title = subBrand.getNameAr();
                    } else {
                        title = subBrand.getNameEn();
                    }
                    adapter.add(new BottomDialogItem1(title).withIdentifier(subBrand.getId()));
                }
            }
        };

        subBrandDialog.setOnNonSelect(() -> {
            classIn.getTextTv().setText(R.string.title_filter_class);
            if (subBrandDialog.isShowing()) {
                subBrandDialog.dismiss();
            }
        });

        subBrandDialog.setOnMainButtonClick(items -> {
            StringBuilder builder = new StringBuilder();
            builder.append(items.get(0).getTitle());
            for (int i = 1; i < items.size(); i++) {
                builder.append(", ").append(items.get(i).getTitle());
            }
            classIn.getTextTv().setText(builder.toString());
            if (subBrandDialog.isShowing()) {
                subBrandDialog.dismiss();
            }
        });

        subBrandDialog.setSelectedIds(result.getSubBrandsIds());
    }

    @Override
    public List<Integer> getBrands() {
        return brandsDialog.getSelectionIds();
    }

    @Override
    public Integer getCategories() {
        return selectedCategory;
    }

    /**
     * used to return result for all ads fragment
     */
    public static class Result implements Parcelable {
        private List<Integer> brandsIds;
        private List<Integer> subBrandsIds;
        private List<Integer> colorsIds;
        private List<Integer> kindsIds;
        private Integer categoryId;
        private String newOldFlag;
        private Integer priceStart;
        private Integer priceEnd;
        private boolean elegoFlag;

        public List<Integer> getBrandsIds() {
            return brandsIds;
        }

        public void setBrandsIds(List<Integer> brandsIds) {
            this.brandsIds = brandsIds;
        }

        public List<Integer> getSubBrandsIds() {
            return subBrandsIds;
        }

        public void setSubBrandsIds(List<Integer> subBrandsIds) {
            this.subBrandsIds = subBrandsIds;
        }

        public List<Integer> getColorsIds() {
            return colorsIds;
        }

        public void setColorsIds(List<Integer> colorsIds) {
            this.colorsIds = colorsIds;
        }

        public List<Integer> getKindsIds() {
            return kindsIds;
        }

        public void setKindsIds(List<Integer> kindsIds) {
            this.kindsIds = kindsIds;
        }

        public String getNewOldFlag() {
            return newOldFlag;
        }

        public void setNewOldFlag(String newOldFlag) {
            this.newOldFlag = newOldFlag;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public Integer getPriceStart() {
            return priceStart;
        }

        public void setPriceStart(Integer priceStart) {
            this.priceStart = priceStart;
        }

        public Integer getPriceEnd() {
            return priceEnd;
        }

        public void setPriceEnd(Integer priceEnd) {
            this.priceEnd = priceEnd;
        }

        public boolean isElegoFlag() {
            return elegoFlag;
        }

        public void setElegoFlag(boolean elegoFlag) {
            this.elegoFlag = elegoFlag;
        }

        public Result() {
            brandsIds = new ArrayList<>();
            subBrandsIds = new ArrayList<>();
            colorsIds = new ArrayList<>();
            kindsIds = new ArrayList<>();
            newOldFlag = null;
            categoryId = null;
            priceStart = 0;
            priceEnd = 2000;
            elegoFlag = false;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(this.brandsIds);
            dest.writeList(this.subBrandsIds);
            dest.writeList(this.colorsIds);
            dest.writeList(this.kindsIds);
            dest.writeValue(this.categoryId);
            dest.writeString(this.newOldFlag);
            dest.writeValue(this.priceStart);
            dest.writeValue(this.priceEnd);
            dest.writeByte(this.elegoFlag ? (byte) 1 : (byte) 0);
        }

        protected Result(Parcel in) {
            this.brandsIds = new ArrayList<Integer>();
            in.readList(this.brandsIds, Integer.class.getClassLoader());
            this.subBrandsIds = new ArrayList<Integer>();
            in.readList(this.subBrandsIds, Integer.class.getClassLoader());
            this.colorsIds = new ArrayList<Integer>();
            in.readList(this.colorsIds, Integer.class.getClassLoader());
            this.kindsIds = new ArrayList<Integer>();
            in.readList(this.kindsIds, Integer.class.getClassLoader());
            this.categoryId = (Integer) in.readValue(Integer.class.getClassLoader());
            this.newOldFlag = in.readString();
            this.priceStart = (Integer) in.readValue(Integer.class.getClassLoader());
            this.priceEnd = (Integer) in.readValue(Integer.class.getClassLoader());
            this.elegoFlag = in.readByte() != 0;
        }

        public static final Creator<Result> CREATOR = new Creator<Result>() {
            @Override
            public Result createFromParcel(Parcel source) {
                return new Result(source);
            }

            @Override
            public Result[] newArray(int size) {
                return new Result[size];
            }
        };
    }

}
