package com.iqitco.elepur.activities.notification;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.items.NotificationItem;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.notification.NotificationContract;
import com.iqitco.elepur_api.contracts.notification.NotificationPresenter;
import com.iqitco.elepur_api.modules.UserNotification;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

import java.util.List;

/**
 * date: 2018-07-02
 *
 * @author Mohammad Al-Najjar
 */
public class NotificationActivity extends BaseActivity implements NotificationContract.View {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private FastItemAdapter<NotificationItem> adapter = new FastItemAdapter<>();
    private EndlessRecyclerOnScrollListener endlessListener;
    private NotificationContract.Presenter presenter;
    private LinearLayoutManager layoutManager;
    private Integer loginId;
    private String role;
    private int selectedPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        role = SecurePrefManager.with(this).get(Constant.ROLL).defaultValue("").go();
        loginId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go();
        adapter.withOnClickListener(this::onItemClick);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        recyclerView = findViewById(R.id.list);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        presenter = new NotificationPresenter(this);
        presenter.onCreate();
        endlessListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                presenter.getNotification(currentPage + 1);
            }
        };
        recyclerView.addOnScrollListener(endlessListener);
        endlessListener.enable();

        swipeRefreshLayout.setOnRefreshListener(() -> {
            endlessListener.disable();
            endlessListener.resetPageCount();
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.accent);


    }

    private boolean onItemClick(View view, IAdapter<NotificationItem> adapter, NotificationItem item, int position) {
        selectedPosition = position;
        startActivityForResult(
                new Intent(this, ShowNotificationActivity.class)
                        .putExtra(Constant.NOTIFICATION_ID, item.getNotification()),
                10
        );
        if (item.getNotification().getSeen().equals(0)) {
            item.getNotification().setSeen(1);
            this.adapter.notifyAdapterDataSetChanged();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == RESULT_OK) {
            this.adapter.remove(selectedPosition);
            this.adapter.notifyAdapterDataSetChanged();
        }
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public Integer getId() {
        return loginId;
    }

    @Override
    public void publishNotification(List<UserNotification> list, int pageNumber) {
        if (pageNumber == 1) {
            adapter.clear();
        }
        for (UserNotification customerNotification : list) {
            adapter.add(
                    new NotificationItem(customerNotification)
                            .withIdentifier(customerNotification.getRowNum())
            );
        }
        if (pageNumber == 1) {
            endlessListener.enable();
        }
    }

    @Override
    public String getRole() {
        return role;
    }
}
