package com.iqitco.elepur.activities.products

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.dialogs.DropDownDialog
import com.iqitco.elepur.util.getNumber
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.editProduct.EditProductContract
import com.iqitco.elepur_api.contracts.editProduct.EditProductPresenter
import com.iqitco.elepur_api.modules.*
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import io.reactivex.Observable
import kotlinx.android.synthetic.main.edit_product_activity.*

/**
 * date 9/15/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class EditProductActivity : BaseActivity(), EditProductContract.View {

    /**
     * product data from api
     */
    private lateinit var product: EditProduct

    /**
     * list of other images
     */
    private val imagesArray by lazy { listOf(otherImage1, otherImage2, otherImage3, otherImage4, otherImage5) }

    /**
     * main presenter
     */
    private val presenter by lazy {
        EditProductPresenter(this)
    }

    /**
     * product id used by presenter
     */
    private val productId by lazy {
        intent.getIntExtra(Constant.ADV_ID, -1)
    }

    /**
     * seller id
     */
    private val userId by lazy {
        SecurePrefManager.with(context).get(Constant.ID_ID).defaultValue(-1).go()
    }

    private var category: Int = 0
    private var guaranteeTypeId: Int = 0
    private var brandId: Int = 0
    private var guaranteeDialog: DropDownDialog? = null
    private lateinit var requestBody: RequestUpdateProduct

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_product_activity)
        if (productId == -1) {
            finish()
            return
        }
        presenter.onCreate()
        statsDDL.addItem(DropDownDialog.Item(1, getString(R.string.title_new)))
        statsDDL.addItem(DropDownDialog.Item(2, getString(R.string.title_old)))

        categoryDDL.onSelect { _, list ->
            category = list[0].id.toInt()
            presenter.getBrands()
            presenter.getKind()
            enableMainBtn()
        }

        guaranteeDDL.onSelect { _, list ->
            guaranteeTypeId = list[0].id.toInt()
            presenter.getGuaranteeList()
        }

        brandDDL.onSelect { _, list ->
            brandId = list[0].id.toInt()
            presenter.getSubBrands()
            enableMainBtn()
        }
        mainBtn.setText(R.string.btn_edit)
        mainBtn.isEnabled = true

        mainImage.setOnSelectImage(this::onImageSelect)
        for (image in imagesArray) {
            image.setOnSelectImage(this::onImageSelect)
        }

        mainBtn.setOnClickListener {
            checkValues()
            presenter.updateProduct()
        }



        subBrandDDL.isEnabled = false
        kindDDL.isEnabled = false
    }

    private fun enableMainBtn() {
        mainBtn.isEnabled = checkValues()
    }

    private fun checkValues(): Boolean {
        requestBody = RequestUpdateProduct()

        requestBody.price = priceEdt.getNumber()
        if (requestBody.price == null) {
            return false
        }
        if (statsDDL.getSelectedId() == null) {
            return false
        } else {
            requestBody.newOldFlag = if (statsDDL.getSelectedId() == 1) "n" else "o"
        }

        if (guaranteeDialog != null) {
            requestBody.guaranteeId = guaranteeDialog!!.getSelectedId()
            if (requestBody.guaranteeId == null) {
                return false
            }
        } else {
            requestBody.guaranteeId = product.guaranteeId
        }

        requestBody.capacity = spaceEdt.getNumber()
        if (requestBody.capacity == null) {
            return false
        }
        requestBody.amount = quantityEdt.getNumber()
        if (requestBody.amount == null) {
            return false
        }
        requestBody.colors = colorDDL.getSelectedIds()
        if (requestBody.colors!!.isEmpty()) {
            return false
        }
        requestBody.freeGifts = freeGiftsDDL.getSelectedIds()

        requestBody.paymentId = paymentDDL.getSelectedId()
        if (requestBody.paymentId == null) {
            return false
        }

        requestBody.description = descEdt.text!!.toString().trim()
        requestBody.sellerId = userId
        requestBody.advId = productId
        return true
    }

    override fun getAdvId(): Int {
        return productId
    }

    override fun publishProduct(product: EditProduct) {
        this.product = product
        /*Load main image*/
        mainImage.loadImage(product.pic1)

        /*load other images by order in the there holder*/
        if (product.picList.isNotEmpty()) {
            product.picList.sortedWith(compareBy { it.picId })
            for (i in 0 until product.picList.size) {
                imagesArray[i].loadImage(product.picList[i].url)
            }
        }

        priceEdt.setText(product.price.toString())
        descEdt.setText(product.decs)
        spaceEdt.setText(product.capacity.toString())
        quantityEdt.setText(product.amount.toString())
        guaranteeDDL.setText(product.guarantee)

        Observable.fromArray(product.colorsList)
                .flatMapIterable { list -> list }
                .map { it.colorId }
                .toList()
                .subscribe({ colorDDL.setSelectedIds(it) }, {})

        Observable.fromArray(product.freeGiftList)
                .flatMapIterable { list -> list }
                .map { it.kindId }
                .toList()
                .subscribe({ freeGiftsDDL.setSelectedIds(it) }, {})

        categoryDDL.setSelectedIds(listOf(product.catId))

        paymentDDL.setSelectedIds(listOf(product.paymentId))
        val id = if (product.newOldFlag.equals("n", true)) 1 else 2
        statsDDL.setSelectedIds(listOf(id))

        checkValues()
    }

    override fun publishColors(list: List<Color>?) {
        colorDDL.clearItems()
        for (color in list!!) {
            val title = if (isLocalAr) color.nameAr else color.nameAr
            colorDDL.addItem(DropDownDialog.Item(color.id.toLong(), title, color.tag))
        }
    }

    override fun publishFreeGifts(list: List<FreeGifts>?) {
        freeGiftsDDL.clearItems()
        for (gift in list!!) {
            val title = if (isLocalAr) gift.kindNameAr else gift.kindNameEn
            freeGiftsDDL.addItem(DropDownDialog.Item(gift.id!!.toLong(), title!!))
        }
    }

    override fun publishGuaranteeType(list: List<GuaranteeType>?) {
        guaranteeDDL.clearItems()
        for (guaranteeType in list!!) {
            val title = if (isLocalAr) guaranteeType.nameAr else guaranteeType.nameEn
            guaranteeDDL.addItem(DropDownDialog.Item(guaranteeType.id!!.toLong(), title!!))
        }
    }

    override fun publishPaymentTypes(list: List<PaymentsType>?) {
        paymentDDL.clearItems()
        for (payment in list!!) {
            val title = if (isLocalAr) payment.nameAr!! else payment.nameEn!!
            paymentDDL.addItem(DropDownDialog.Item(payment.id!!.toLong(), title))
        }
    }

    override fun publishCategories(list: List<MainPageSection>) {
        categoryDDL.clearItems()
        for (category in list) {
            val title = if (isLocalAr) category.categoryNameAr else category.categoryName
            categoryDDL.addItem(DropDownDialog.Item(category.id.toLong(), title))
        }
        /*cannot be edited*/
        categoryDDL.isEnabled = false
    }

    override fun getCategoryId(): Int {
        return category
    }

    override fun getGuaranteeTypeId(): Int {
        return guaranteeTypeId
    }

    override fun publishGuarantee(list: List<Guarantee>) {
        if (guaranteeDialog != null && guaranteeDialog!!.isShowing) {
            guaranteeDialog!!.dismiss()
        }
        guaranteeDialog = DropDownDialog.Builder(this).withImages(true).build()
        guaranteeDialog!!.onSelect { s, _ -> guaranteeDDL.setText(s) }
        for (guarantee in list) {
            guaranteeDialog!!.addItem(DropDownDialog.Item(guarantee.id!!.toLong(), guarantee.name!!, guarantee.link!!))
        }
        guaranteeDialog!!.setSelectedIds(listOf(product.guaranteeId))
        guaranteeDialog!!.show()
    }

    override fun publishKind(list: List<ProductKind>) {
        kindDDL.clearItems()
        for (kind in list) {
            val title = if (isLocalAr) kind.kindNameAr else kind.kindNameEn
            kindDDL.addItem(DropDownDialog.Item(kind.id.toLong(), title))
        }
        kindDDL.visibility = if (list.isNotEmpty()) View.VISIBLE else View.GONE
        kindDDL.setSelectedIds(listOf(product.kindId))
        /*cannot be edited*/
        kindDDL.isEnabled = false
    }

    override fun getBrandId(): Int {
        return brandId
    }

    override fun publishSubBrands(list: List<SubBrand>) {
        subBrandDDL.clearItems()
        for (sub in list) {
            val title = if (isLocalAr) sub.nameAr else sub.nameEn
            subBrandDDL.addItem(DropDownDialog.Item(sub.id.toLong(), title))
        }
        subBrandDDL.setSelectedIds(listOf(product.subBrandId))
        /*cannot be edited*/
        subBrandDDL.isEnabled = false
    }

    override fun publishBrands(list: List<Brand>) {
        brandDDL.clearItems()
        for (brand in list) {
            val title = if (isLocalAr) brand.brandsNameAr else brand.brandsNameEn
            brandDDL.addItem(DropDownDialog.Item(brand.id.toLong(), title))
        }
        brandDDL.setSelectedIds(listOf(product.brandId))
        /*cannot be edited*/
        brandDDL.isEnabled = false
    }

    override fun getSellerId(): Int {
        return userId
    }

    override fun getAdvName(): String {
        return product.advName
    }

    override fun getMainPic(): ByteArray {
        return Base64.decode(mainImage.base64, Base64.DEFAULT)
    }

    override fun getPic(picId: Int): ByteArray {
        return Base64.decode(imagesArray[picId - 1].base64, Base64.DEFAULT)
    }

    override fun onUpdateMainPicSuccess() {
        Log.i(this::javaClass.name, "update main pic done")
        setResult(Activity.RESULT_OK)
    }

    override fun onUpdatePicSuccess(picId: Int) {
        Log.i(this::javaClass.name, "update pic $picId done")
        setResult(Activity.RESULT_OK)
    }

    override fun onProductUpdate() {
        setResult(Activity.RESULT_OK)
        Toast.makeText(this, R.string.message_update_product_done, Toast.LENGTH_LONG).show()
        finish()
    }

    override fun getRequestBodyProduct(): RequestUpdateProduct {
        return requestBody
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mainImage.onActivityResult(requestCode, resultCode, data)
        for (image in imagesArray) {
            image.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onImageSelect(view: View) {
        when (view.id) {
            mainImage.id -> presenter.updateMainPic()
            otherImage1.id -> presenter.updatePic(getPicId(0), 1)
            otherImage2.id -> presenter.updatePic(getPicId(1), 2)
            otherImage3.id -> presenter.updatePic(getPicId(2), 3)
            otherImage4.id -> presenter.updatePic(getPicId(3), 4)
            otherImage5.id -> presenter.updatePic(getPicId(4), 5)
        }
    }

    private fun getPicId(position: Int): Int? {
        return if (position < product.picList.size) product.picList[position].picId else null
    }
}