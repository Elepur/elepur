package com.iqitco.elepur.activities.main_search

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.activities.home.AllAdsFilterActivity
import com.iqitco.elepur.activities.show_ads.ShowAdsActivity
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog
import com.iqitco.elepur.items.BottomDialogItem1
import com.iqitco.elepur.items.KeywordItem
import com.iqitco.elepur.items.MainSearchItem
import com.iqitco.elepur.items.ProductItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.main_search.MainSearchContract
import com.iqitco.elepur_api.contracts.main_search.MainSearchPresenter
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.State
import com.iqitco.elepur_api.modules.StateArea
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import kotlinx.android.synthetic.main.main_search_activity.*
import java.util.*

/**
 * date 8/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class MainSearchActivity : BaseActivity(), MainSearchContract.View {

    private val userChoiceAdapter = FastItemAdapter<KeywordItem>()
    private val adsAdapter = FastItemAdapter<ProductItem>()
    private val searchWordsAdapter = FastItemAdapter<MainSearchItem>()
    private lateinit var presenter: MainSearchContract.Presenter
    private lateinit var endlessListener: EndlessRecyclerOnScrollListener
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var userRole: String
    private var deleteHistoryDialog: MaterialDialog? = null
    private var stateDialog: BaseBottomSheetDialog? = null
    private var areaDialog: BaseBottomSheetDialog? = null
    private var id: Int = -1
    private var stateId: Int = 0
    private var areaId: Int = 0
    private var role: String? = null
    private var customerId: Int = 0
    private var filterResult = AllAdsFilterActivity.Result()

    private val findSearchWordsWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            clearBtn.visibility = if (s!!.toString().isEmpty()) View.GONE else View.VISIBLE
            presenter.getSearchChoice()
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    private val enableSearchFindSearchWordsWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            searchEdt.removeTextChangedListener(this)
            clearBtn.visibility = if (s!!.toString().isEmpty()) View.GONE else View.VISIBLE
            presenter.getSearchChoice()
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_search_activity)
        userRole = SecurePrefManager.with(context).get(Constant.ROLL).defaultValue("").go()
        presenter = MainSearchPresenter(this)

        swipeRefreshLayout.setColorSchemeResources(R.color.accent)
        swipeRefreshLayout.setOnRefreshListener { initAdsList() }
        swipeRefreshLayout.isEnabled = false

        adsAdapter.withOnClickListener(this::onAdsClick)
        searchWordsAdapter.withOnClickListener(this::onSearchItemClick)

        id = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
        role = SecurePrefManager.with(this).get(Constant.ROLL).defaultValue("").go()
        if (role!!.equals(Constant.USER_ROLE_CUSTOMER, true)) {
            customerId = id
        }
        list.adapter = userChoiceAdapter
        layoutManager = GridLayoutManager(this, 1)
        list.layoutManager = layoutManager
        /*list.addItemDecoration(ItemDecorationAlbumColumns(resources.getDimensionPixelSize(R.dimen.mainItemSpacing), 1))*/
        endlessListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                presenter.getUserFilter(currentPage + 1)
            }
        }
        list.addOnScrollListener(endlessListener)
        endlessListener.disable()

        clearBtn.visibility = View.GONE
        clearBtn.setOnClickListener {
            searchEdt.setText("")
        }
        selectStateBtn.setOnClickListener { stateDialog!!.show() }
        selectAreaBtn.setOnClickListener { areaDialog!!.show() }
        deleteBtn.setOnClickListener(this::onDeleteClick)
        resultFilterBtn.setOnClickListener {
            startActivityForResult(
                    Intent(context!!, AllAdsFilterActivity::class.java)
                            .putExtra(AllAdsFilterActivity.RESULTS, filterResult),
                    AllAdsFilterActivity.REQUEST_CODE
            )
        }
        selectAreaBtn.isEnabled = false
        emptyLayout.visibility = View.INVISIBLE
        searchEdt.setOnEditorActionListener(this::onSearchKeyboardClick)
        searchEdt.addTextChangedListener(findSearchWordsWatcher)
        presenter.onCreate()

        if (userRole.equals(Constant.USER_ROLE_SELLER, true)) {
            lastLbl.visibility = View.GONE
            deleteBtn.visibility = View.GONE
        }

    }

    private fun onAdsClick(view: View?, adapter: IAdapter<ProductItem>?, item: ProductItem?, position: Int): Boolean {
        startActivity(
                Intent(this, ShowAdsActivity::class.java)
                        .putExtra(Constant.ADV_ID, item!!.o.idAdv)
                        .putExtra(Constant.SELLER_ID, item.o.shopId)

        )
        return true
    }

    private fun onSearchItemClick(view: View?, adapter: IAdapter<MainSearchItem>?, item: MainSearchItem?, position: Int): Boolean {
        searchEdt.removeTextChangedListener(findSearchWordsWatcher)
        searchEdt.setText(item!!.title)
        searchEdt.addTextChangedListener(enableSearchFindSearchWordsWatcher)
        initAdsList()
        return true
    }

    private fun onDeleteClick(view: View) {
        if (deleteHistoryDialog == null) {
            deleteHistoryDialog = MaterialDialog.Builder(this)
                    .contentColorRes(android.R.color.white)
                    .positiveColorRes(android.R.color.white)
                    .negativeColorRes(android.R.color.white)
                    .backgroundColorRes(R.color.border)
                    .content(R.string.message_clear_search_history)
                    .positiveText(R.string.btn_delete)
                    .negativeText(R.string.btn_cancel)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .onPositive { _, _ -> presenter.deleteUserSearchChoice() }
                    .build()
        }

        if (!deleteHistoryDialog!!.isShowing) {
            deleteHistoryDialog!!.show()
        }
    }

    private fun onSearchKeyboardClick(v: TextView?, actionId: Int?, event: KeyEvent?): Boolean {
        if (actionId != EditorInfo.IME_ACTION_SEARCH) return false
        initAdsList()
        return true
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun getChoice(): String {
        return searchEdt.text.toString().trim()
    }

    override fun getId(): Int {
        return id
    }

    override fun publishChoice(words: List<String>) {
        searchWordsAdapter.clear()
        for (word in words) {
            searchWordsAdapter.add(MainSearchItem(word).withIdentifier(word.hashCode().toLong()))
        }
        endlessListener.disable()
        if (list.adapter != searchWordsAdapter) {
            list.recycledViewPool.clear()
            list.swapAdapter(searchWordsAdapter, false)
            searchWordsAdapter.notifyAdapterDataSetChanged()
            searchWordsAdapter.notifyDataSetChanged()
        }
    }

    override fun publishUserChoice(list: List<String>) {
        userChoiceAdapter.clear()
        if (list.isNotEmpty()) {
            list.forEachIndexed { index, text -> userChoiceAdapter.add(KeywordItem(text).withIdentifier(index.toLong())) }
        } else {
            deleteBtn.isEnabled = false
        }
        endlessListener.enable()
    }

    override fun onDeleteChoiceSuccess(flag: Boolean) {
        if (flag) {
            presenter.getUserSearchChoice()
            Toast.makeText(this, R.string.message_delete_search_complete, Toast.LENGTH_LONG).show()
        }
    }

    override fun publishStates(list: List<State>) {
        stateDialog = object : BaseBottomSheetDialog(context!!) {
            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>) {
                for (state in list) {
                    val title = if (Locale.getDefault().toString().equals("ar", true)) state.nameStateAr else state.nameStateEn
                    adapter.add(BottomDialogItem1(title).withIdentifier(state.id.toLong()))
                }
            }
        }
        stateDialog!!.setOnMainButtonClick(this::onStateSelect)
        stateDialog!!.setOnNonSelect(this::onStateNonSelect)
    }

    private fun onStateNonSelect() {
        stateId = 0
        areaId = 0
        selectAreaBtn.isEnabled = false
        stateDialog!!.dismiss()
        initAdsList()
    }

    private fun onStateSelect(it: List<BottomDialogItem1>) {
        val item = it[0]
        stateId = item.identifier.toInt()
        presenter.getAreas()
        stateDialog!!.dismiss()
        selectAreaBtn.isEnabled = true
        initAdsList()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == AllAdsFilterActivity.REQUEST_CODE) {
            if (data!!.extras != null) {
                filterResult = data.extras!!.getParcelable(AllAdsFilterActivity.RESULTS)
                initAdsList()
            }
        }
    }

    override fun publishArea(list: List<StateArea>) {
        areaDialog = object : BaseBottomSheetDialog(context!!) {
            override fun initItems(context: Context?, adapter: FastItemAdapter<BottomDialogItem1>) {
                for (area in list) {
                    adapter.add(BottomDialogItem1(
                            if (Locale.getDefault().toString().equals("ar", true)) area.nameAreaAr else area.nameAreaEn
                    ).withIdentifier(area.id.toLong()))
                }
            }
        }
        areaDialog!!.setOnMainButtonClick(this::onAreaSelect)
        areaDialog!!.setOnNonSelect(this::onAreaNonSelect)
    }

    private fun onAreaNonSelect() {
        areaId = 0
        areaDialog!!.dismiss()
        initAdsList()
    }

    private fun onAreaSelect(it: List<BottomDialogItem1>) {
        val item = it[0]
        areaId = item.identifier.toInt()
        areaDialog!!.dismiss()
        initAdsList()
    }

    private fun initAdsList() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window.currentFocus.windowToken, 0)
        if (!swipeRefreshLayout.isEnabled) {
            swipeRefreshLayout.isEnabled = true
        }
        if (list.adapter != adsAdapter) {
            list.recycledViewPool.clear()
            list.swapAdapter(adsAdapter, false)
            adsAdapter.notifyAdapterDataSetChanged()
            adsAdapter.notifyDataSetChanged()
        }
        endlessListener.disable()
        endlessListener.resetPageCount()
    }

    override fun showFilterLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideFilterLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun publishAds(list: List<Ads>, pageNumber: Int) {
        deleteBtn.visibility = View.GONE
        lastLbl.visibility = View.GONE
        if (pageNumber == 1) {
            adsAdapter.clear()
        }
        for (ads in list) {
            adsAdapter.add(ProductItem(ads, 1).withIdentifier(ads.rowNum!!.toLong()))
        }
        if (pageNumber == 1) {
            endlessListener.enable()
        }
        emptyLayout.visibility = if (adsAdapter.itemCount == 0) View.VISIBLE else View.GONE
    }

    override fun getStateId(): Int {
        return stateId
    }

    override fun getAreaId(): Int {
        return areaId
    }

    override fun getSortColumn(): Int {
        return 0
    }

    override fun getSortDirection(): String {
        return "desc"
    }

    override fun getBrandsIds(): List<Int> {
        return filterResult.brandsIds
    }

    override fun getSubBrands(): List<Int> {
        return filterResult.subBrandsIds
    }

    override fun getColors(): List<Int> {
        return filterResult.colorsIds
    }

    override fun getKind(): List<Int> {
        return filterResult.kindsIds
    }

    override fun getNewOldFlag(): String? {
        return filterResult.newOldFlag
    }

    override fun getElegoFlag(): Boolean {
        return filterResult.isElegoFlag
    }

    override fun getLowestPrice(): Int {
        return filterResult.priceStart
    }

    override fun getHighestPrice(): Int {
        return filterResult.priceEnd
    }

    override fun getCustomerId(): Int {
        return customerId
    }
}