package com.iqitco.elepur.activities.show_ads

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareButton
import com.iqitco.elepur.R
import com.iqitco.elepur.activities.BaseActivity
import com.iqitco.elepur.activities.reports_adv.ReportsAdvActivity
import com.iqitco.elepur.activities.show_photos.ShowPhotoActivity
import com.iqitco.elepur.activities.show_seller.ShowSellerActivity
import com.iqitco.elepur.adapters.ViewPagerViewAdapter
import com.iqitco.elepur.items.ColorOvalItem
import com.iqitco.elepur.items.ProductItem
import com.iqitco.elepur_api.Constant
import com.iqitco.elepur_api.contracts.show_ads.ShowAdsContract
import com.iqitco.elepur_api.contracts.show_ads.ShowAdsPresenter
import com.iqitco.elepur_api.modules.Ads
import com.iqitco.elepur_api.modules.FullProduct
import com.iqitco.elepur_api.modules.RateResult
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import com.prashantsolanki.secureprefmanager.SecurePrefManager
import com.tooltip.Tooltip
import kotlinx.android.synthetic.main.show_ads_activity.*

/**
 * date 7/15/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ShowAdsActivity : BaseActivity(), ShowAdsContract.View {

    private val sellerProductAdapter = FastItemAdapter<ProductItem>()
    private val colorAdapter = FastItemAdapter<ColorOvalItem>()
    private lateinit var presenter: ShowAdsContract.Presenter
    private lateinit var photoPagerAdapter: ViewPagerViewAdapter
    private lateinit var product: FullProduct
    private var colorTooltip: Tooltip? = null
    private var advId: Int? = null
    private var sellerId: Int? = null
    private var customerId: Int? = null
    private var confirmCallDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            advId = intent.getIntExtra(Constant.ADV_ID, -1)
            if (advId == -1) {
                throw Exception("advId cannot be null")
            }
        } catch (ex: Exception) {
            throw Exception("advId cannot be null")
        }
        try {
            sellerId = intent.getIntExtra(Constant.SELLER_ID, -1)
            if (sellerId == -1) {
                throw Exception("sellerId cannot be null")
            }
        } catch (ex: Exception) {
            throw Exception("sellerId cannot be null")
        }

        customerId = SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go()
        setContentView(R.layout.show_ads_activity)
        photoPagerAdapter = ViewPagerViewAdapter()
        photoViewPager.adapter = photoPagerAdapter
        photoViewPager.isCycle = true
        photoViewPager.interval = 3000
        photoViewPager.startAutoScroll()

        sellerProductList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        sellerProductList.adapter = sellerProductAdapter

        sadEmoji.setOnClickListener {
            showVisitorDialog {
                if (it == 2) {
                    presenter.rateAdvSad()
                }
            }
        }

        happyEmoji.setOnClickListener {
            showVisitorDialog {
                if (it == 2) {
                    presenter.rateAdvHappy()
                }
            }
        }

        loveEmoji.setOnClickListener {
            showVisitorDialog {
                if (it == 2) {
                    presenter.rateAdvFav()
                }
            }
        }

        reportBtn.setOnClickListener {
            showVisitorDialog {
                startActivity(
                        Intent(this, ReportsAdvActivity::class.java)
                                .putExtra(Constant.ADV_ID, advId!!)
                )
            }
        }

        shareBtn.setOnClickListener {
            val uri = Uri.parse("http://www.elepur.com/ad/$advId/$sellerId")
            val shareButton = ShareButton(this@ShowAdsActivity)
            shareButton.shareContent = ShareLinkContent.Builder()
                    .setContentUrl(uri)
                    .build()
            shareButton.callOnClick()

           /* val actionButton = ShareMessengerURLActionButton.Builder()
                    .setTitle("Visit Elepur")
                    .setUrl(uri)
                    .build()
            val genericTemplateElement = ShareMessengerGenericTemplateElement.Builder()
                    .setTitle("Visit Facebook")
                    .setSubtitle("Visit Messenger")
                    .setImageUrl(Uri.parse(product.pic1))
                    .setButton(actionButton)
                    .build()
            val genericTemplateContent = ShareMessengerGenericTemplateContent.Builder()
                    .setPageId("Your Page Id") // Your page ID, required
                    .setGenericTemplateElement(genericTemplateElement)
                    .build()

            MessageDialog.show(this, genericTemplateContent)*/

        }

        sellerProductAdapter.withOnClickListener(this::onItemClick)

        colorAdapter.withOnClickListener(this::onColorClick)
        colorList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        colorList.adapter = colorAdapter

        sellerLogo.setOnClickListener(this::onSellerClick)
        sellerName.setOnClickListener(this::onSellerClick)
        callStoreBtn.setOnClickListener(this::showCallConfirmClick)

        presenter = ShowAdsPresenter(this)
        presenter.onCreate()
    }

    private fun showCallConfirmClick(view: View?) {
        showVisitorDialog {
            if (confirmCallDialog == null) {
                confirmCallDialog = MaterialDialog.Builder(this)
                        .content(getString(R.string.message_call_seller_f, product.phone))
                        .positiveText(R.string.btn_call)
                        .negativeText(R.string.btn_cancel)
                        .onPositive(this::onCallClick)
                        .autoDismiss(true)
                        .build()
            }

            if (!confirmCallDialog!!.isShowing) {
                confirmCallDialog!!.show()
            }
        }
    }

    private fun onCallClick(dialog: MaterialDialog, which: DialogAction) {
        presenter.insertCallCustomer()
    }

    private fun onSellerClick(view: View?) {
        startActivity(
                Intent(this, ShowSellerActivity::class.java)
                        .putExtra(Constant.SELLER_ID, sellerId)
        )
    }

    private fun onColorClick(view: View?, adapter: IAdapter<ColorOvalItem>?, item: ColorOvalItem?, position: Int): Boolean {
        if (colorTooltip != null) {
            colorTooltip!!.dismiss()
        }
        colorTooltip = Tooltip.Builder(view!!)
                .setBackgroundColor(Color.parseColor("#59595B"))
                .setTextColor(Color.WHITE)
                .setCancelable(true)
                .setGravity(Gravity.TOP)
                .setText(item!!.color.name!!)
                .build()
        colorTooltip!!.show()
        return true
    }

    private fun onItemClick(view: View?, adapter: IAdapter<ProductItem>?, item: ProductItem?, position: Int): Boolean {
        startActivity(
                Intent(this, ShowAdsActivity::class.java)
                        .putExtra(Constant.ADV_ID, advId)
                        .putExtra(Constant.SELLER_ID, sellerId)
        )
        return true
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun publishProduct(product: FullProduct) {
        this.product = product
        toolbar.titleTv.text = product.advName
        val pic1 = FullProduct.Pics()
        pic1.idPic = -1
        pic1.pics = product.pic1!!
        product.pics!!.add(0, pic1)
        for (i in 0 until product.pics!!.size) {
            val photo = product.pics!![i]
            val imageView = ImageView(this)
            imageView.scaleX = resources.getInteger(R.integer.scale_x).toFloat()
            imageView.setOnClickListener {
                val intent = Intent(this@ShowAdsActivity, ShowPhotoActivity::class.java)
                intent.putExtra(Constant.POSITION, i)
                intent.putParcelableArrayListExtra(Constant.PHOTOS, product.pics)
                startActivity(intent)
            }
            Glide.with(this)
                    .load(photo.pics)
                    .apply(RequestOptions()
                            .centerCrop()
                            .fallback(R.drawable.img_place_holder)
                            .placeholder(R.drawable.img_place_holder)
                            .error(R.drawable.img_place_holder))
                    .into(imageView)
            photoPagerAdapter.add(imageView)
        }

        Glide.with(this)
                .load(product.sellerLogo)
                .into(sellerLogo)
        sellerName.text = product.sellerName
        sellerBlue.visibility = if (product.sellerBlue == 1) View.VISIBLE else View.INVISIBLE
        productNameTv.text = product.advName
        productDescTv.text = product.decs
        priceTv.text = getString(R.string.title_jod_price_f, product.price)

        if (product.freeGift!!.isNotEmpty()) {
            val stringBuilder = StringBuilder()
            stringBuilder.append(product.freeGift!![0].kindName)
            for (i in 1 until product.freeGift!!.size) {
                stringBuilder.append(" + ").append(product.freeGift!![i].kindName)
            }
            giftsTv.text = stringBuilder.toString()
        } else {
            giftsTv.text = getString(R.string.title_no_gifts)
        }

        if (product.capacity != null && product.capacity!! > 0) {
            capacityTv.text = getString(R.string.title_capacity_f, product.capacity!!)
        } else {
            capacityTv.visibility = View.GONE
        }

        stateTv.text = if (product.no!!.equals("n", true)) getString(R.string.title_state_new) else getString(R.string.title_state_old)

        if (product.guarantee != null && product.guarantee!!.isNotEmpty()) {
            Glide.with(this)
                    .load(product.guarantee!!)

                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            guaranteeLayout.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            return false
                        }
                    })
                    .into(guaranteeLogo)
        } else {
            guaranteeLayout.visibility = View.GONE
        }

        if (product.amount != null && product.amount!! > 0) {
            availabilityTv.text = getString(R.string.title_availability_f, product.amount!!)
        } else {
            soldLayout.visibility = View.VISIBLE
            soldLayout.setOnClickListener {
                finish()
            }
        }

        if (product.color != null && product.color!!.isNotEmpty()) {
            for (color in product.color!!) {
                colorAdapter.add(ColorOvalItem(color).withIdentifier(color.idColor!!.toLong()))
            }
        } else {
            colorLayout.visibility = View.GONE
        }

        happyEmoji.counterTv.text = product.happy.toString()
        sadEmoji.counterTv.text = product.sad.toString()
        loveEmoji.counterTv.text = product.fav.toString()
        when {
            product.customerSad == 1 -> sadEmoji.isSelected = true
            product.customerHappy == 1 -> happyEmoji.isSelected = true
        }
        if (product.customerFav == 1) {
            loveEmoji.isSelected = true
        }

    }

    override fun publishSellerProduct(list: List<Ads>) {
        sellerProductAdapter.clear()
        for (product in list) {
            sellerProductAdapter.add(ProductItem(product, 3).withIdentifier(product.idAdv!!.toLong()))
        }
    }

    override fun publishRate(result: RateResult) {
        sadEmoji.counterTv.text = result.sad.toString()
        happyEmoji.counterTv.text = result.happy.toString()
        loveEmoji.counterTv.text = result.fav.toString()

        sadEmoji.isSelected = false
        happyEmoji.isSelected = false
        loveEmoji.isSelected = false

        when {
            result.checkSad == 1 -> sadEmoji.isSelected = true
            result.checkHappy == 1 -> happyEmoji.isSelected = true
        }
        if (result.checkFav == 1) {
            loveEmoji.isSelected = true
        }

    }

    override fun onReportSuccess() {
        Toast.makeText(this, "Reported", Toast.LENGTH_LONG).show()
    }

    override fun getAdvId(): Int {
        return advId!!
    }

    override fun getCustomerId(): Int {
        return customerId!!
    }

    override fun getSellerId(): Int {
        return sellerId!!
    }

    override fun onInsertCustomerSuccess(result: Boolean) {
        val dialIntent = Intent(Intent.ACTION_DIAL)
        dialIntent.data = Uri.parse("tel:" + product.phone)
        startActivity(dialIntent)
    }

    override fun getReportMessage(): String {
        return ""
    }

    override fun getReportHeader(): String {
        return ""
    }
}