package com.iqitco.elepur.activities.all_ads_by_brand;

import android.os.Bundle;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.activities.home.fragment.AllAdsFragment;

/**
 * date 6/27/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AllAdsByBrandActivity extends BaseActivity {

    public static final String BRAND_ID = "brandId";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_ads_by_brand_activity);

        Integer brandId = getIntent().getIntExtra(BRAND_ID, -1);
        if (brandId == -1) {
            finish();
            return;
        }

        AllAdsFragment fragment = new AllAdsFragment();
        fragment.getBrandsIds().add(brandId);
        fragment.setShowFilters(false);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, fragment)
                .commit();

    }
}
