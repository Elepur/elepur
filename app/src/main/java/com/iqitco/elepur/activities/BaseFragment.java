package com.iqitco.elepur.activities;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BaseFragment extends Fragment {

    protected BaseActivity getBaseActivity(){
        return (BaseActivity) getActivity();
    }

    public void showLoading() {
        if (!getBaseActivity().getProgressDialog().isShowing()) {
            getBaseActivity().getProgressDialog().show();
        }
    }

    public void hideLoading() {
        getBaseActivity().getProgressDialog().dismiss();
    }

    public void showUserMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    public void showErrorMessage(String message) {
        if (message != null) {
            Log.e("error", message);
        }
    }

}
