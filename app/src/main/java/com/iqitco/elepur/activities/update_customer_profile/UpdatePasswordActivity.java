package com.iqitco.elepur.activities.update_customer_profile;

import android.os.Bundle;
import android.widget.Toast;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.BaseActivity;
import com.iqitco.elepur.widget.CustomEditTextView;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.contracts.update_customer_profile.UpdatePasswordContract;
import com.iqitco.elepur_api.contracts.update_customer_profile.UpdatePasswordPresenter;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;

/**
 * date 7/3/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class UpdatePasswordActivity extends BaseActivity implements UpdatePasswordContract.View {

    private CustomEditTextView oldPasswordEdt;
    private CustomEditTextView newPasswordEdt;
    private UpdatePasswordContract.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_password_activity);
        presenter = new UpdatePasswordPresenter(this);
        oldPasswordEdt = findViewById(R.id.oldPasswordEdt);
        newPasswordEdt = findViewById(R.id.newPasswordEdt);
        findViewById(R.id.mainBtn).setOnClickListener(v -> presenter.updateCustomerPassword());
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public String getOldPassword() {
        return oldPasswordEdt.getText().toString();
    }

    @Override
    public String getNewPassword() {
        return newPasswordEdt.getText().toString();
    }

    @Override
    public Integer getId() {
        return SecurePrefManager.with(this).get(Constant.ID_ID).defaultValue(-1).go();
    }

    @Override
    public void errorOldPassword(boolean flag) {
        oldPasswordEdt.showError(flag);
        if (flag) {
            Toast.makeText(this, R.string.message_password_six_char, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void errorNewPassword(boolean flag) {
        newPasswordEdt.showError(flag);
        if (flag) {
            Toast.makeText(this, R.string.message_password_six_char, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onUpdateSuccess() {
        setResult(RESULT_OK);
        finish();
    }
}
