package com.iqitco.elepur.items

import android.view.View
import android.widget.TextView
import com.iqitco.elepur.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem

/**
 * date 9/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class MainSearchItem(val title: String) : AbstractItem<MainSearchItem, MainSearchItem.ViewHolder>() {

    override fun getType(): Int {
        return R.id.main_search_item
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getLayoutRes(): Int {
        return R.layout.main_search_item
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<MainSearchItem>(view) {

        private val titleTv = view.findViewById<TextView>(R.id.titleTv)

        override fun unbindView(item: MainSearchItem) {

        }

        override fun bindView(item: MainSearchItem, payloads: MutableList<Any>) {
            titleTv.text = item.title
        }
    }
}