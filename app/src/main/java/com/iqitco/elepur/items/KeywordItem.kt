package com.iqitco.elepur.items

import android.view.View
import android.widget.TextView
import com.iqitco.elepur.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem

/**
 * date 8/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class KeywordItem(val text: String) : AbstractItem<KeywordItem, KeywordItem.ViewHolder>() {

    override fun getType(): Int {
        return R.id.keyword_item
    }

    override fun getViewHolder(v: View?): ViewHolder {
        return ViewHolder(v!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.keyword_item
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<KeywordItem>(view) {

        private val textTv = view.findViewById<TextView>(R.id.textTv)

        override fun unbindView(item: KeywordItem?) {

        }

        override fun bindView(item: KeywordItem?, payloads: MutableList<Any>?) {
            textTv.text = item!!.text
        }
    }

}