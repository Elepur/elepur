package com.iqitco.elepur.items;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * date: 2018-07-01
 *
 * @author Mohammad Al-Najjar
 */
public class ProfileItem extends AbstractItem<ProfileItem, ProfileItem.ViewHolder> {

    @StringRes
    private int titleResId;

    @DrawableRes
    private int iconResId;

    public ProfileItem(@StringRes int titleResId, @DrawableRes int iconResId) {
        this.titleResId = titleResId;
        this.iconResId = iconResId;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.profile_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.profile_item;
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<ProfileItem> {

        private ImageView image;
        private TextView textTv;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            textTv = itemView.findViewById(R.id.textTv);
        }

        @Override
        public void bindView(ProfileItem item, List<Object> payloads) {
            image.setImageResource(item.iconResId);
            textTv.setText(item.titleResId);
        }

        @Override
        public void unbindView(ProfileItem item) {

        }
    }

}
