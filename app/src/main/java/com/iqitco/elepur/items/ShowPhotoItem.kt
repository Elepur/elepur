package com.iqitco.elepur.items

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.siyamed.shapeimageview.RoundedImageView
import com.iqitco.elepur.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem

/**
 * date 7/25/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ShowPhotoItem(private val imageUrl: String) : AbstractItem<ShowPhotoItem, ShowPhotoItem.ViewHolder>() {

    override fun getType(): Int {
        return R.id.show_photo_item
    }

    override fun getViewHolder(v: View?): ViewHolder {
        return ViewHolder(v!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.show_photo_item
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<ShowPhotoItem>(view) {
        private val image = view.findViewById<RoundedImageView>(R.id.image)

        override fun unbindView(item: ShowPhotoItem?) {

        }

        override fun bindView(item: ShowPhotoItem?, payloads: MutableList<Any>?) {
            Glide.with(image.context)
                    .load(item!!.imageUrl)
                    .apply(RequestOptions()
                            .centerCrop()
                            .fallback(R.drawable.img_place_holder)
                            .placeholder(R.drawable.img_place_holder)
                            .error(R.drawable.img_place_holder))
                    .into(image)
        }
    }

}