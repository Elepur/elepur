package com.iqitco.elepur.items;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iqitco.elepur.R;
import com.iqitco.elepur_api.modules.Ads;
import com.iqitco.elepur_api.modules.SellerProduct;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ProductItem extends AbstractItem<ProductItem, ProductItem.ViewHolder> {

    public static ProductItem getProductItem(SellerProduct sellerProduct, int showType, int sellerId) {
        Ads ads = new Ads();
        ads.setBrands(sellerProduct.getBrands());
        ads.setAdvName(sellerProduct.getAdvName());
        ads.setKind(sellerProduct.getKind());
        ads.setShopName(sellerProduct.getShopName());
        ads.setPrice(sellerProduct.getPrice());
        ads.setPic1(sellerProduct.getPic1());
        ads.setShopLogo(sellerProduct.getShopLogo());
        ads.setShopId(sellerId);
        ads.setIdAdv(sellerProduct.getIdAdv());
        return new ProductItem(ads, showType).withIdentifier(sellerProduct.getIdAdv());
    }

    private Ads ads;
    private int showType;

    public ProductItem(Ads ads, int showType) {
        this.ads = ads;
        this.showType = showType;
    }

    public Ads getO() {
        return ads;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        if (showType == 1) {
            return R.id.product_item;
        } else {
            return R.id.product_item_grid;
        }
    }

    @Override
    public int getLayoutRes() {
        if (showType == 1) {
            return R.layout.product_item;
        } else if (showType == 3) {
            return R.layout.product_item_grid_3;
        } else {
            return R.layout.product_item_grid;
        }
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<ProductItem> {

        private TextView text1Tv;
        private TextView text2Tv;
        private TextView text3Tv;
        private TextView text4Tv;
        private TextView text5Tv;
        private ImageView image;
        private ImageView storeLogo;
        private Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            text1Tv = itemView.findViewById(R.id.text1Tv);
            text2Tv = itemView.findViewById(R.id.text2Tv);
            text3Tv = itemView.findViewById(R.id.text3Tv);
            text4Tv = itemView.findViewById(R.id.text4Tv);
            text5Tv = itemView.findViewById(R.id.text5Tv);
            image = itemView.findViewById(R.id.image);
            storeLogo = itemView.findViewById(R.id.storeLogo);
        }

        @Override
        public void bindView(ProductItem item, List<Object> payloads) {
            text1Tv.setText(item.ads.getBrands());
            text2Tv.setText(item.ads.getAdvName());
            text3Tv.setText(item.ads.getKind());
            text4Tv.setText(item.ads.getShopName());
            text5Tv.setText(context.getString(R.string.title_jod_price_f, item.ads.getPrice()));

            Glide.with(context)
                    .asBitmap()
                    .load(item.ads.getPic1())
                    /*.apply(RequestOptions.bitmapTransform(new RoundedCornersTransformation((int) context.getResources().getDimension(R.dimen.productCorners), 0, RoundedCornersTransformation.CornerType.LEFT)))*/
                    .apply(
                            new RequestOptions()
                                    .fallback(R.drawable.img_place_holder)
                                    .placeholder(R.drawable.img_place_holder)
                                    .error(R.drawable.img_place_holder)
                    )
                    .into(image);

            Glide.with(context)
                    .asBitmap()
                    .load(item.ads.getShopLogo())
                    .into(storeLogo);

        }

        @Override
        public void unbindView(ProductItem item) {

        }
    }

}
