package com.iqitco.elepur.items

import android.view.View
import android.widget.TextView
import com.iqitco.elepur.R
import com.iqitco.elepur_api.modules.CustomerAddress
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem

/**
 * date 7/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class AddressItem(var address: CustomerAddress) : AbstractItem<AddressItem, AddressItem.ViewHolder>() {

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getType(): Int {
        return R.id.address_item
    }

    override fun getLayoutRes(): Int {
        return R.layout.address_item
    }

    inner class ViewHolder(view: View) : FastAdapter.ViewHolder<AddressItem>(view) {

        private val nameTv = view.findViewById<TextView>(R.id.nameTv)!!
        private val descTv = view.findViewById<TextView>(R.id.descTv)!!

        override fun unbindView(item: AddressItem?) {

        }

        override fun bindView(item: AddressItem, payloads: MutableList<Any>?) {
            nameTv.text = item.address.locationName
            descTv.text = item.address.location
        }
    }

}