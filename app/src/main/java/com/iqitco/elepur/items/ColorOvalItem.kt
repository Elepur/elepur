package com.iqitco.elepur.items

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import com.iqitco.elepur.R
import com.iqitco.elepur_api.modules.FullProduct
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import android.graphics.drawable.GradientDrawable
import android.util.Log


/**
 * date 7/23/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ColorOvalItem(val color: FullProduct.Color) : AbstractItem<ColorOvalItem, ColorOvalItem.ViewHolder>() {

    override fun getType(): Int {
        return R.id.color_oval_item
    }

    override fun getViewHolder(v: View?): ViewHolder {
        return ViewHolder(v!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.color_oval_item
    }

    inner class ViewHolder(view: View) : FastAdapter.ViewHolder<ColorOvalItem>(view) {

        private val image = view.findViewById<ImageView>(R.id.image)

        override fun unbindView(item: ColorOvalItem?) {

        }

        override fun bindView(item: ColorOvalItem?, payloads: MutableList<Any>?) {
            try {
                val shape = GradientDrawable()
                shape.shape = GradientDrawable.OVAL
                shape.setColor(Color.parseColor(item!!.color.tag))
                image.setImageDrawable(shape)
            } catch (ex: Exception) {
                Log.e("ColorOvalItem", "error", ex)
            }
        }
    }

}