package com.iqitco.elepur.items.on_click;

import android.view.View;

import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.listeners.OnClickListener;
import com.mikepenz.fastadapter.select.SelectExtension;

/**
 * date 6/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
@SuppressWarnings("unchecked")
public class MultiCheckBoxOnClickListener implements OnClickListener {

    private SelectExtension extension;

    public MultiCheckBoxOnClickListener(SelectExtension extension) {
        this.extension = extension;
    }

    @Override
    public boolean onClick(View v, IAdapter adapter, IItem item, int position) {
        extension.toggleSelection(position);
        return true;
    }
}