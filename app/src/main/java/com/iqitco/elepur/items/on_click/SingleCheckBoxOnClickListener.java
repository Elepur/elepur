package com.iqitco.elepur.items.on_click;

import android.view.View;

import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.listeners.OnClickListener;
import com.mikepenz.fastadapter.select.SelectExtension;

import java.util.Set;

/**
 * date 6/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

@SuppressWarnings("unchecked")
public class SingleCheckBoxOnClickListener implements OnClickListener {

    private SelectExtension extension;

    public SingleCheckBoxOnClickListener(SelectExtension extension) {
        this.extension = extension;
    }

    @Override
    public boolean onClick(View v, IAdapter adapter, IItem item, int position) {
        if (!item.isSelected()) {
            Set<Integer> selections = extension.getSelections();
            if (!selections.isEmpty()) {
                extension.deselect();
            }
            extension.select(position);
        } else {
            Set<Integer> selections = extension.getSelections();
            if (selections.size() == 1) {
                int id = selections.iterator().next();
                if (id == position) {
                    extension.deselect();
                }
            }
        }
        return true;
    }
}
