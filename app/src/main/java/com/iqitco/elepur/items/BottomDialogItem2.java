package com.iqitco.elepur.items;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iqitco.elepur.R;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BottomDialogItem2 extends AbstractItem<BottomDialogItem2, BottomDialogItem2.ViewHolder> {

    private static final String TAG = BottomDialogItem2.class.getSimpleName();

    private String title;
    private String imageUrl;

    public BottomDialogItem2(String title, String imageUrl) {
        this.title = title;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.bottom_dialog_item2;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bottom_dialog_item_2;
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<BottomDialogItem2> {

        private TextView textTv;
        private CheckBox checkBox;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            textTv = itemView.findViewById(R.id.textTv);
            checkBox = itemView.findViewById(R.id.checkbox);
            image = itemView.findViewById(R.id.image);
        }

        @Override
        public void bindView(BottomDialogItem2 item, List<Object> payloads) {
            checkBox.setChecked(item.isSelected());
            textTv.setText(item.getTitle());
            Glide.with(image.getContext()).load(item.getImageUrl()).into(image);
        }

        @Override
        public void unbindView(BottomDialogItem2 item) {

        }
    }

}
