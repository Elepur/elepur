package com.iqitco.elepur.items

import android.support.constraint.ConstraintLayout
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.iqitco.elepur.R
import com.iqitco.elepur.util.DateUtility
import com.iqitco.elepur.widget.EmojiBarView
import com.iqitco.elepur_api.modules.StoreProductStatus
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

/**
 * date 8/6/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ProductStatusItem(val product: StoreProductStatus) : AbstractItem<ProductStatusItem, ProductStatusItem.ViewHolder>() {

    override fun getType(): Int {
        return R.id.product_status_item
    }

    override fun getViewHolder(v: View?): ViewHolder {
        return ViewHolder(v!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.product_status_item
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<ProductStatusItem>(view) {

        private val image = view.findViewById<ImageView>(R.id.image)!!
        private val productNameTv = view.findViewById<TextView>(R.id.productNameTv)!!
        private val remTimeTv = view.findViewById<TextView>(R.id.remTimeTv)!!
        private val statusTv = view.findViewById<TextView>(R.id.statusTv)!!
        private val dateTv = view.findViewById<TextView>(R.id.dateTv)!!
        private val emojisBar = view.findViewById<EmojiBarView>(R.id.emojisBar)!!
        private val callsTv = view.findViewById<TextView>(R.id.callsTv)!!
        private val viewsTv = view.findViewById<TextView>(R.id.viewsTv)!!
        val editBtn = view.findViewById<ConstraintLayout>(R.id.editBtn)!!
        val removeBtn = view.findViewById<ConstraintLayout>(R.id.removeBtn)!!
        private val offerImage = view.findViewById<ImageView>(R.id.offerImage)!!
        private val context = view.context!!
        private val mainLayout = view.findViewById<ConstraintLayout>(R.id.mainLayout)


        override fun unbindView(item: ProductStatusItem?) {

        }

        override fun bindView(item: ProductStatusItem, payloads: MutableList<Any>?) {
            productNameTv.text = item.product.advName
            Glide.with(context).load(item.product.pic1)
                    .apply(bitmapTransform(RoundedCornersTransformation(context.resources.getDimension(R.dimen.productCorners).toInt(), 0, RoundedCornersTransformation.CornerType.ALL)))
                    .apply(
                            RequestOptions()
                                    .centerCrop()
                                    .fallback(R.drawable.img_place_holder)
                                    .placeholder(R.drawable.img_place_holder)
                                    .error(R.drawable.img_place_holder)
                    )
                    .into(image)
            emojisBar.happyEmoji!!.isSelected = true
            emojisBar.sadEmoji!!.isSelected = true
            emojisBar.loveEmoji!!.isSelected = true

            emojisBar.happyEmoji!!.counterTv.text = item.product.happy.toString()
            emojisBar.sadEmoji!!.counterTv.text = item.product.sad.toString()
            emojisBar.loveEmoji!!.counterTv.text = item.product.fav.toString()

            viewsTv.text = context.getString(R.string.title_number_of_views_f, item.product.views)
            callsTv.text = context.getString(R.string.title_quantity_f, item.product.amount!!)

            statusTv.text = if (item.product.newOldFlag!!.equals("n", true)) context.getString(R.string.title_state_new) else context.getString(R.string.title_state_old)
            dateTv.text = context.getString(R.string.title_since_f, DateUtility.formatNotificationDate(item.product.date))

            remTimeTv.text = context.getString(R.string.title_rem_time_f, item.product.expireDate)

            when (item.product.authorize) {
                0 -> {
                    offerImage.visibility = View.VISIBLE
                    mainLayout.setBackgroundResource(R.drawable.store_item_bg_red)
                }
                else -> {
                    offerImage.visibility = View.GONE
                    mainLayout.setBackgroundResource(R.drawable.store_item_bg)
                }
            }


        }
    }

}