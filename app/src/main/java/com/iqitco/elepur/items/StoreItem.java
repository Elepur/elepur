package com.iqitco.elepur.items;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.akexorcist.localizationactivity.LanguageSetting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iqitco.elepur.R;
import com.iqitco.elepur.util.DateUtility;
import com.iqitco.elepur.util.DrawableUtility;
import com.iqitco.elepur.widget.EmojiBarView;
import com.iqitco.elepur.widget.EmojiView;
import com.iqitco.elepur_api.modules.Seller;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.Locale;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * date 6/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class StoreItem extends AbstractItem<StoreItem, StoreItem.ViewHolder> {

    private Seller seller;

    public StoreItem(Seller seller) {
        this.seller = seller;
    }

    public Seller getSeller() {
        return seller;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.store_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.store_item;
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<StoreItem> {

        private ImageView image;
        private TextView storeNameTv;
        private TextView storeDescTv;
        private EmojiBarView emojiBar;
        private ImageView storeBlueImage;
        private ImageView offerImage;
        private ConstraintLayout rootView;
        private TextView timeTv;
        private TextView addressTv;
        private ImageView clock1;


        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            storeNameTv = itemView.findViewById(R.id.storeNameTv);
            storeDescTv = itemView.findViewById(R.id.storeDescTv);
            emojiBar = itemView.findViewById(R.id.emojiBar);
            storeBlueImage = itemView.findViewById(R.id.storeBlueImage);
            rootView = itemView.findViewById(R.id.rootView);
            offerImage = itemView.findViewById(R.id.offerImage);
            timeTv = itemView.findViewById(R.id.timeTv);
            addressTv = itemView.findViewById(R.id.addressTv);
            clock1 = itemView.findViewById(R.id.clock1);
        }

        @Override
        public void bindView(StoreItem item, List<Object> payloads) {
            Seller seller = item.getSeller();
            storeNameTv.setText(seller.getShopName());
            storeDescTv.setText(seller.getKind());
            Locale locale = Locale.getDefault();

            emojiBar.getLoveEmoji().getCounterTv().setText(String.format(locale, "%d", seller.getFav()));
            emojiBar.getHappyEmoji().getCounterTv().setText(String.format(locale, "%d", seller.getHappy()));
            emojiBar.getSadEmoji().getCounterTv().setText(String.format(locale, "%d", seller.getSad()));

            emojiBar.getLoveEmoji().setSelected(true);
            emojiBar.getHappyEmoji().setSelected(true);
            emojiBar.getSadEmoji().setSelected(true);

            Context context = image.getContext();

            Glide.with(context).load(seller.getPicLogo())
                    .apply(bitmapTransform(new RoundedCornersTransformation((int) context.getResources().getDimension(R.dimen.productCorners), 0, RoundedCornersTransformation.CornerType.ALL)))
                    .apply(
                            new RequestOptions()
                                    .centerCrop()
                                    .fallback(R.drawable.img_place_holder)
                                    .placeholder(R.drawable.img_place_holder)
                                    .error(R.drawable.img_place_holder)
                    )
                    .into(image);

            storeBlueImage.setVisibility(item.getSeller().getBlue() == 1 ? View.VISIBLE : View.INVISIBLE);
            if (item.getSeller().getDiscount() == 1) {
                rootView.setBackgroundResource(R.drawable.store_item_bg_red);
                offerImage.setVisibility(View.VISIBLE);
            } else {
                rootView.setBackgroundResource(R.drawable.store_item_bg);
                offerImage.setVisibility(View.INVISIBLE);
            }

            if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")){
                timeTv.setText(String.format(Locale.getDefault(), "%s - %s", DateUtility.convertTime(seller.getTimeClose()), DateUtility.convertTime(seller.getTimeOpen())));
            } else {
                timeTv.setText(String.format(Locale.getDefault(), "%s - %s", DateUtility.convertTime(seller.getTimeOpen()), DateUtility.convertTime(seller.getTimeClose())));
            }
            addressTv.setText(String.format(Locale.getDefault(), "%s - %s", seller.getState(), seller.getArea()));

            int color;
            if (seller.getTime() == 0) {
                color = context.getResources().getColor(R.color.border);
            } else {
                color = context.getResources().getColor(R.color.textColorPrimary);
            }

            timeTv.setTextColor(color);
            clock1.setImageDrawable(DrawableUtility.getTintDrawable(context, R.drawable.ic_clock1, color));



        }

        @Override
        public void unbindView(StoreItem item) {

        }
    }

}
