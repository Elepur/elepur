package com.iqitco.elepur.items;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.iqitco.elepur.R;
import com.iqitco.elepur_api.modules.BrandPic;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * date 6/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class MainPageBrandItem extends AbstractItem<MainPageBrandItem, MainPageBrandItem.ViewHolder> {

    private BrandPic brandPic;

    public MainPageBrandItem(BrandPic brandPic) {
        this.brandPic = brandPic;
        withIdentifier(brandPic.getId());
    }

    public BrandPic getBrandPic() {
        return brandPic;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.main_page_brand_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.main_page_brand_item;
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<MainPageBrandItem> {

        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.image);
        }

        @Override
        public void bindView(MainPageBrandItem item, List<Object> payloads) {
            Glide.with(image.getContext()).load(item.getBrandPic().getBrandPic()).into(image);
        }

        @Override
        public void unbindView(MainPageBrandItem item) {

        }
    }

}
