package com.iqitco.elepur.items

import android.view.View
import android.widget.TextView
import com.iqitco.elepur.R
import com.iqitco.elepur_api.modules.Brand
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import java.util.*

/**
 * date 7/13/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class CategoryBrandItem(val brand: Brand) : AbstractItem<CategoryBrandItem, CategoryBrandItem.ViewHolder>() {

    override fun getType(): Int {
        return R.id.category_brand_item
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getLayoutRes(): Int {
        return R.layout.category_brand_item
    }

    inner class ViewHolder(view: View) : FastAdapter.ViewHolder<CategoryBrandItem>(view) {

        private val textTv = view.findViewById<TextView>(R.id.textTv)

        override fun unbindView(item: CategoryBrandItem) {

        }

        override fun bindView(item: CategoryBrandItem, payloads: MutableList<Any>?) {
            textTv.text = if (Locale.getDefault().toString().equals("ar", true)) item.brand.brandsNameAr else item.brand.brandsNameEn
        }
    }

}