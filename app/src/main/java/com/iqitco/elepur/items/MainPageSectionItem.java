package com.iqitco.elepur.items;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.akexorcist.localizationactivity.LanguageSetting;
import com.bumptech.glide.Glide;
import com.iqitco.elepur.R;
import com.iqitco.elepur_api.modules.MainPageSection;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * date 6/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class MainPageSectionItem extends AbstractItem<MainPageSectionItem, MainPageSectionItem.ViewHolder> {

    private MainPageSection mainPageSection;

    public MainPageSectionItem(MainPageSection mainPageSection) {
        this.mainPageSection = mainPageSection;
    }

    public MainPageSection getMainPageSection() {
        return mainPageSection;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.main_page_section_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.main_page_section_item;
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<MainPageSectionItem> {

        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.image);
        }

        @Override
        public void bindView(MainPageSectionItem item, List<Object> payloads) {
            String imageUrl;
            if (LanguageSetting.getLanguage().equalsIgnoreCase("ar")) {
                imageUrl = item.getMainPageSection().getPicAr();
            } else {
                imageUrl = item.getMainPageSection().getPicEn();
            }
            Glide.with(image.getContext()).load(imageUrl).into(image);
        }

        @Override
        public void unbindView(MainPageSectionItem item) {

        }
    }

}
