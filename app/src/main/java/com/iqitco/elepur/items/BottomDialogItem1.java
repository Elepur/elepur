package com.iqitco.elepur.items;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BottomDialogItem1 extends AbstractItem<BottomDialogItem1, BottomDialogItem1.ViewHolder> {

    private static final String TAG = BottomDialogItem1.class.getSimpleName();

    private String title;

    public BottomDialogItem1(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.bottom_dialog_item1;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bottom_dialog_item_1;
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<BottomDialogItem1> {

        private TextView textTv;
        private CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            textTv = itemView.findViewById(R.id.textTv);
            checkBox = itemView.findViewById(R.id.checkbox);
        }

        @Override
        public void bindView(BottomDialogItem1 item, List<Object> payloads) {
            checkBox.setChecked(item.isSelected());
            textTv.setText(item.getTitle());
        }

        @Override
        public void unbindView(BottomDialogItem1 item) {

        }
    }

}
