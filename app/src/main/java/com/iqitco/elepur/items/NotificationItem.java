package com.iqitco.elepur.items;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.iqitco.elepur.util.DateUtility;
import com.iqitco.elepur_api.modules.UserNotification;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * date: 2018-07-02
 *
 * @author Mohammad Al-Najjar
 */
public class NotificationItem extends AbstractItem<NotificationItem, NotificationItem.ViewHolder> {

    private UserNotification notification;

    public NotificationItem(UserNotification notification) {
        this.notification = notification;
    }

    public UserNotification getNotification() {
        return notification;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.notification_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.notification_item;
    }

    public static class ViewHolder extends FastItemAdapter.ViewHolder<NotificationItem> {

        private ConstraintLayout rootView;
        private ImageView image;
        private TextView titleTv;
        private TextView textTv;
        private TextView dateTv;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView.findViewById(R.id.rootView);
            image = itemView.findViewById(R.id.image);
            titleTv = itemView.findViewById(R.id.titleTv);
            textTv = itemView.findViewById(R.id.textTv);
            dateTv = itemView.findViewById(R.id.dateTv);
        }

        @Override
        public void bindView(NotificationItem item, List<Object> payloads) {
            try{
                int resId = 0;
                switch (item.notification.getType()) {
                    case 1:
                        resId = R.drawable.ic_notification_elego;
                        break;
                    case 2:
                        resId = R.drawable.ic_profile_elego;
                        break;
                    case 3:
                        resId = R.drawable.ic_profile_notification;
                        break;
                }
                image.setImageResource(resId);
            } catch (Exception e){
                /*NOTHING*/
            }
            titleTv.setText(item.getNotification().getHeader());
            textTv.setText(item.getNotification().getMessage());
            dateTv.setText(DateUtility.formatNotificationDate(item.getNotification().getDate()));
            rootView.setBackgroundResource(item.getNotification().getSeen() == 1 ? R.drawable.store_item_bg : R.drawable.notification_not_seen);
        }

        @Override
        public void unbindView(NotificationItem item) {

        }
    }

}
