package com.iqitco.elepur.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;

import com.iqitco.elepur.R;
import com.iqitco.elepur.dialogs.BaseBottomSheetDialog;
import com.iqitco.elepur.items.BottomDialogItem1;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.List;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CustomEditTextDialogView extends AppCompatEditText {

    private BaseBottomSheetDialog dialog;
    private Context context;
    private OnMainButtonClick onMainButtonClick;
    private OnNonSelect onNonSelect;


    public CustomEditTextDialogView(Context context) {
        this(context, null);
    }

    public CustomEditTextDialogView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @SuppressLint("ClickableViewAccessibility")
    public CustomEditTextDialogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setInputType(InputType.TYPE_NULL);
        setOnKeyListener(null);
        setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus && dialog != null && !dialog.isShowing() && dialog.getItemCount() > 0) {
                dialog.show();
            }
        });
        setOnTouchListener((view, event) -> {
            if (MotionEvent.ACTION_UP == event.getAction() && dialog != null && !dialog.isShowing() && dialog.getItemCount() > 0) {
                dialog.show();
            }
            return true;
        });
    }

    public void initDialog(List<Item> list, boolean multiSelect) {
        dialog = new BaseBottomSheetDialog(context, multiSelect) {
            @Override
            protected void initItems(Context context, FastItemAdapter<BottomDialogItem1> adapter) {
                for (Item item : list) {
                    adapter.add(new BottomDialogItem1(item.title).withIdentifier(item.id));
                }
            }
        };
        dialog.setOnMainButtonClick(items -> {
            StringBuilder builder = new StringBuilder();
            builder.append(items.get(0).getTitle());
            for (int i = 1; i < items.size(); i++) {
                builder.append(", ").append(items.get(i).getTitle());
            }
            setText(builder.toString());
            if (onMainButtonClick != null) {
                onMainButtonClick.onMainButtonClick();
            }
        });

        dialog.setOnNonSelect(() -> {
            setText("");
            if (onNonSelect != null) {
                onNonSelect.onNonSelect();
            }
        });

        if (list.size() == 0) {
            setText("");
        }

    }

    public List<Integer> getSelectedList() {
        return dialog.getSelectedList();
    }

    public List<Integer> getSelectionIds() {
        return dialog.getSelectionIds();
    }

    public void setSelectedIds(List<Integer> ids) {
        dialog.setSelectedIds(ids);
    }

    public Button getMainBtn() {
        return dialog.getMainBtn();
    }

    public void showError(boolean flag) {
        setBackgroundResource(flag ? R.drawable.edittext_rounded_white_error : R.drawable.edittext_rounded_white);
    }

    public void setOnMainButtonClick(OnMainButtonClick onMainButtonClick) {
        this.onMainButtonClick = onMainButtonClick;
    }

    public void setOnNonSelect(OnNonSelect onNonSelect) {
        this.onNonSelect = onNonSelect;
    }

    @FunctionalInterface
    public interface OnMainButtonClick {
        void onMainButtonClick();
    }

    @FunctionalInterface
    public interface OnNonSelect {
        void onNonSelect();
    }

    public static class Item {
        private long id;
        private String title;

        public Item(long id, String title) {
            this.id = id;
            this.title = title;
        }
    }
}
