package com.iqitco.elepur.widget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.iqitco.elepur.R
import kotlinx.android.synthetic.main.emoji_bar_view.view.*

/**
 * date 7/27/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class EmojiBarView : ConstraintLayout {

    var sadEmoji: EmojiView? = null
    var happyEmoji: EmojiView? = null
    var loveEmoji: EmojiView? = null

    constructor(context: Context?) : super(context) {
        init(context!!, null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context!!, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context!!, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.emoji_bar_view, this, true)
        sadEmoji = findViewById(R.id.sadEmoji)
        happyEmoji = findViewById(R.id.happyEmoji)
        loveEmoji = findViewById(R.id.loveEmoji)
    }

}