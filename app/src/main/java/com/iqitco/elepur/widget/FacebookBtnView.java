package com.iqitco.elepur.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.facebook.login.widget.LoginButton;
import com.iqitco.elepur.R;

/**
 * date 6/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class FacebookBtnView extends ConstraintLayout {

    private LoginButton facebookBtn;


    public FacebookBtnView(Context context) {
        this(context, null);
    }

    public FacebookBtnView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FacebookBtnView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.custom_facebook_btn, this, true);
        facebookBtn = findViewById(R.id.fBtn);
        setOnClickListener(this::onClick);
    }

    private void onClick(View view) {
        facebookBtn.callOnClick();
    }

    public LoginButton getFacebookBtn() {
        return facebookBtn;
    }
}
