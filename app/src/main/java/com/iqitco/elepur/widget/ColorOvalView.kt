package com.iqitco.elepur.widget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet

/**
 * date 7/23/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class ColorOvalView : ConstraintLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
}