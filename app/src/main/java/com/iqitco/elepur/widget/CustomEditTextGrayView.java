package com.iqitco.elepur.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.iqitco.elepur.R;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CustomEditTextGrayView extends AppCompatEditText {

    public CustomEditTextGrayView(Context context) {
        super(context);
    }

    public CustomEditTextGrayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditTextGrayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public void showError(boolean flag){
        setBackgroundResource(flag ? R.drawable.edittext_rounded_gray_error : R.drawable.edittext_rounded_gray);
    }
}
