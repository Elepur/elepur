package com.iqitco.elepur.widget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import com.iqitco.elepur.R
import kotlinx.android.synthetic.main.rounded_spinner_view.view.*

/**
 * date 7/8/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */

class RoundedSpinnerView : ConstraintLayout {

    private val layout: ConstraintLayout
    var flag: Boolean = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.rounded_spinner_view, this, true)
        layout = findViewById(R.id.layout)
        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.RoundedSpinnerView)
            val arraySize = array.indexCount

            for (i in 0..arraySize) {
                val attr = array.getIndex(i)
                if (attr == R.styleable.RoundedSpinnerView_android_text) {
                    textTv.text = array.getString(attr)
                }
            }
            array.recycle()
        }


    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if (enabled) {
            textTv.setTextColor(resources.getColor(R.color.textColorPrimary))
        } else {
            textTv.setTextColor(resources.getColor(R.color.textColorSec))
        }
    }

    fun setError(flag: Boolean) {
        this.flag = flag
        val resId = if (flag) R.drawable.edit_text_small_rounded_white else R.drawable.small_rounded_white
        layout.setBackgroundResource(resId)
    }

    fun getTextTv(): TextView {
        return textTv
    }
}