package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class NavDrawerButtonView extends ConstraintLayout {

    private ImageView image;
    private TextView textTv;


    public NavDrawerButtonView(Context context) {
        this(context, null);
    }

    public NavDrawerButtonView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavDrawerButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.nav_drawer_button_view, this, true);
        image = findViewById(R.id.image);
        textTv = findViewById(R.id.textTv);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.NavDrawerButtonView);
        int arraySize = array.getIndexCount();
        for (int i = 0; i < arraySize; i++) {
            int attr = array.getIndex(i);
            if (attr == R.styleable.NavDrawerButtonView_android_text) {
                textTv.setText(array.getString(attr));
            } else if (attr == R.styleable.NavDrawerButtonView_android_src) {
                int resId = array.getResourceId(attr, 0);
                if (resId != 0) {
                    image.setImageResource(resId);
                }
            }
        }
        array.recycle();
    }

    public ImageView getImage() {
        return image;
    }

    public TextView getTextTv() {
        return textTv;
    }
}
