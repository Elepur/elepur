package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 6/19/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class DownMenuView extends ConstraintLayout {

    private ImageView image;
    private TextView textTv;

    private Drawable drawable;
    private String title;

    public DownMenuView(Context context) {
        this(context, null);
    }

    public DownMenuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DownMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.down_menu_view, this, true);
        image = findViewById(R.id.image);
        textTv = findViewById(R.id.textTv);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.DownMenuView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.DownMenuView_android_text) {
                    textTv.setText(array.getString(attr));
                } else if (attr == R.styleable.DownMenuView_android_src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        image.setImageResource(resId);
                    }
                }
            }
            array.recycle();
        }

        if (drawable != null) {
            image.setImageDrawable(drawable);
        }

        if (title != null) {
            textTv.setText(title);
        }

    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
        if (image != null) {
            image.setImageDrawable(drawable);
        }
    }

    public void setTitle(String title) {
        this.title = title;
        if (textTv != null) {
            textTv.setText(title);
        }
    }

    public ImageView getImage() {
        return image;
    }

    public TextView getTextTv() {
        return textTv;
    }
}
