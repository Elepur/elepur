package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageButton;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AboutUsExpandableView extends ConstraintLayout {

    private ImageButton image;
    private TextView titleTv;
    private TextView textTv;
    private boolean isExpand = false;


    public AboutUsExpandableView(Context context) {
        this(context, null);
    }

    public AboutUsExpandableView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AboutUsExpandableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.about_us_expandable_view, this, true);
        image = findViewById(R.id.image);
        titleTv = findViewById(R.id.titleTv);
        textTv = findViewById(R.id.textTv);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AboutUsExpandableView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.AboutUsExpandableView_android_text) {
                    textTv.setText(array.getString(attr));
                } else if (attr == R.styleable.AboutUsExpandableView_title) {
                    titleTv.setText(array.getString(attr));
                } else if (attr == R.styleable.AboutUsExpandableView_android_src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        image.setImageResource(resId);
                    }
                }
            }
            array.recycle();
        }

        image.setOnClickListener(this::onClick);
        titleTv.setOnClickListener(this::onClick);
    }

    private void onClick(View view) {
        if (!isExpand) {
            expand(textTv);
            image.animate().rotation(180).start();
        } else {
            collapse(textTv);
            image.animate().rotation(0).start();
        }
        isExpand = !isExpand;
    }

    private void expand(final View view) {
        view.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int targtetHeight = view.getMeasuredHeight();

        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? LayoutParams.WRAP_CONTENT
                        : (int) (targtetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration((int) (targtetHeight / view.getContext().getResources().getDisplayMetrics().density));
        view.startAnimation(animation);
    }

    private void collapse(final View view) {
        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration((int) (initialHeight / view.getContext().getResources().getDisplayMetrics().density));
        view.startAnimation(animation);
    }
}
