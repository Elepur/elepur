package com.iqitco.elepur.widget;

import android.animation.Animator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.iqitco.elepur.R;

/**
 * date 6/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BottomNavView extends ConstraintLayout {

    private ImageView mainBtnImage;
    private View mainBtn;
    private BottomNavButtonView[] buttons = new BottomNavButtonView[4];
    private OnNavClick onNavClick;
    private ImageButton blackOverlayBtn;
    private Drawable mainBtnIcon;

    private Context context;
    private boolean isOpen = false;
    private FrameLayout itemsLayout;

    public BottomNavView(Context context) {
        this(context, null);
    }

    public BottomNavView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BottomNavView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.bottom_nav, this, true);
        itemsLayout = findViewById(R.id.itemsLayout);
        mainBtn = findViewById(R.id.mainBtn);
        blackOverlayBtn = findViewById(R.id.blackOverlayBtn);
        mainBtnImage = findViewById(R.id.mainBtnImage);
        mainBtnImage.bringToFront();
        mainBtnImage.setOnClickListener(this::onMainButton);
        mainBtn.setOnClickListener(this::onMainButton);
        buttons[0] = findViewById(R.id.btn1);
        buttons[1] = findViewById(R.id.btn2);
        buttons[2] = findViewById(R.id.btn3);
        buttons[3] = findViewById(R.id.btn4);

        buttons[0].setOnClickListener(this::onButtonClick);
        buttons[1].setOnClickListener(this::onButtonClick);
        buttons[2].setOnClickListener(this::onButtonClick);
        buttons[3].setOnClickListener(this::onButtonClick);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.BottomNavView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.BottomNavView_btn1Src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        buttons[0].setImageResId(resId);
                        buttons[0].setSelected(false);
                    }
                } else if (attr == R.styleable.BottomNavView_btn2Src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        buttons[1].setImageResId(resId);
                        buttons[1].setSelected(false);
                    }
                } else if (attr == R.styleable.BottomNavView_btn3Src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        buttons[2].setImageResId(resId);
                        buttons[2].setSelected(false);
                    }
                } else if (attr == R.styleable.BottomNavView_btn4Src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        buttons[3].setImageResId(resId);
                        buttons[3].setSelected(false);
                    }
                } else if (attr == R.styleable.BottomNavView_btn1Text) {
                    buttons[0].getTextTv().setText(array.getString(attr));
                } else if (attr == R.styleable.BottomNavView_btn2Text) {
                    buttons[1].getTextTv().setText(array.getString(attr));
                } else if (attr == R.styleable.BottomNavView_btn3Text) {
                    buttons[2].getTextTv().setText(array.getString(attr));
                } else if (attr == R.styleable.BottomNavView_btn4Text) {
                    buttons[3].getTextTv().setText(array.getString(attr));
                } else if (attr == R.styleable.BottomNavView_mainBtnSrc) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        mainBtnImage.setImageResource(resId);
                    }
                }
            }
            array.recycle();
        }

        if (mainBtnIcon != null) {
            mainBtnImage.setImageDrawable(mainBtnIcon);
        }

    }

    public void changeButtonIcon(int position, @DrawableRes int resId) {
        if (position < 0 || position >= buttons.length) {
            return;
        }

        buttons[position].setImageResId(resId);
    }

    public void changeButtonText(int position, @StringRes int resId) {
        if (position < 0 || position >= buttons.length) {
            return;
        }

        buttons[position].setStringResId(resId);
    }

    private void onButtonClick(View view) {
        int index = -1;
        switch (view.getId()) {
            case R.id.btn1:
                index = 0;
                break;
            case R.id.btn2:
                index = 1;
                break;
            case R.id.btn3:
                index = 2;
                break;
            case R.id.btn4:
                index = 3;
                break;
        }
        setSelected(index);
    }

    public void setMainBtnIcon(Drawable mainBtnIcon) {
        this.mainBtnIcon = mainBtnIcon;
        if (mainBtnImage != null) {
            mainBtnImage.setImageDrawable(mainBtnIcon);
        }
    }

    private void onMainButton(View view) {
        if (itemsLayout.getChildCount() == 0) {
            return;
        }

        if (isOpen) {
            for (int i = 0; i < itemsLayout.getChildCount(); i++) {
                slideDown(itemsLayout.getChildAt(i), i + 1);
            }
            blackOverlayBtn.setVisibility(GONE);
            blackOverlayBtn.setOnClickListener(null);
            enableNavButtons(true);
        } else {
            for (int i = 0; i < itemsLayout.getChildCount(); i++) {
                slideUp(itemsLayout.getChildAt(i), i + 1);
            }
            blackOverlayBtn.setVisibility(VISIBLE);
            blackOverlayBtn.setOnClickListener(this::onRootViewTouch);
            enableNavButtons(false);
        }
        isOpen = !isOpen;
    }

    private void enableNavButtons(boolean flag) {
        for (BottomNavButtonView btn : buttons) {
            btn.setEnabled(flag);
        }
    }

    public void addDownMenuItem(@StringRes int strResId, @DrawableRes int DrawableResId, View.OnClickListener onClickListener) {
        addDownMenuItem(
                context.getString(strResId),
                context.getResources().getDrawable(DrawableResId),
                onClickListener
        );
    }

    public void addDownMenuItem(String title, Drawable icon, View.OnClickListener onClickListener) {
        DownMenuView item = new DownMenuView(context);
        item.setDrawable(icon);
        item.setTitle(title);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        item.setLayoutParams(params);
        item.setVisibility(INVISIBLE);
        item.setOnClickListener(onClickListener);
        itemsLayout.addView(item);
    }

    public void slideDown(View view, int position) {
        view.animate()
                .translationY(0)
                .setDuration(150)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
        /*TranslateAnimation slide = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, fromYValue,
                Animation.RELATIVE_TO_SELF, toFloatValue);

        slide.setDuration(150);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        slide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(slide);*/
    }

    private boolean onRootViewTouch(View v) {
        onMainButton(v);
        return true;
    }

    public void close() {
        if (isOpen) {
            onMainButton(null);
        }
    }

    public void slideUp(View view, int position) {
        view.animate().setListener(null);
        view.setVisibility(VISIBLE);
        view.animate()
                .translationY(position * view.getHeight() * -1)
                .setDuration(150)
                .start();
        /*view.setVisibility(VISIBLE);
        view.invalidate();
        TranslateAnimation slide = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, yValue);

        slide.setDuration(150);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        view.startAnimation(slide);*/
    }

    public void setSelected(int position) {
        setSelected(position, true);
    }

    public void setSelected(int position, boolean invokeOnClick) {
        for (int i = 0; i < 4; i++) {
            buttons[i].setSelected(position == i);
        }
        if (onNavClick != null && invokeOnClick) {
            onNavClick.onNavClick(position);
        }
    }

    public ImageView getMainBtnImage() {
        return mainBtnImage;
    }

    public BottomNavButtonView getBtn1() {
        return buttons[0];
    }

    public BottomNavButtonView getBtn2() {
        return buttons[1];
    }

    public BottomNavButtonView getBtn3() {
        return buttons[2];
    }

    public BottomNavButtonView getBtn4() {
        return buttons[3];
    }

    public void setOnNavClick(OnNavClick onNavClick) {
        this.onNavClick = onNavClick;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public interface OnNavClick {
        void onNavClick(int btnIndex);
    }
}
