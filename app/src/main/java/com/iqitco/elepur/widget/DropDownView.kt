package com.iqitco.elepur.widget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.iqitco.elepur.R
import com.iqitco.elepur.dialogs.DropDownDialog
import kotlinx.android.synthetic.main.drop_down_view.view.*

/**
 * date 8/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class DropDownView : ConstraintLayout {

    private lateinit var dialog: DropDownDialog
    private var title: String? = null
    private var onSelectCallBack: ((String, List<DropDownDialog.Item>) -> Unit)? = null

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.drop_down_view, this, true)
        setDialog(DropDownDialog.Builder(context).build())
        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.DropDownView)
            val arraySize = array.indexCount

            for (i in 0..arraySize) {
                val attr = array.getIndex(i)
                when (attr) {
                    R.styleable.DropDownView_android_text -> {
                        title = array.getString(attr)
                        titleTv.text = title
                    }
                    R.styleable.DropDownView_android_background -> {
                        layout.setBackgroundResource(attr)
                    }
                }
            }
            array.recycle()
        }

        setOnClickListener(this::onClick)

    }

    private fun onClick(view: View){
        dialog.show()
    }

    fun clearItems() {
        dialog.clearItems()
    }

    fun setText(text: String) {
        titleTv.text = text
    }

    fun removeText() {
        titleTv.text = title
    }

    fun addItem(item: DropDownDialog.Item) {
        dialog.addItem(item)
        isEnabled = this.dialog.count() > 0
    }

    fun setDialog(dialog: DropDownDialog) {
        setDialog(dialog, true)
    }

    fun setDialog(dialog: DropDownDialog, withOnSelect: Boolean) {
        this.dialog = dialog
        if (withOnSelect) {
            dialog.onSelect { str, list ->
                titleTv.text = str
                onSelectCallBack?.invoke(str, list)
            }
            dialog.onSelectNon { titleTv.text = title }
        }
        isEnabled = this.dialog.count() > 0
    }

    fun onSelect(onSelectCallBack: (String, List<DropDownDialog.Item>) -> Unit) {
        this.onSelectCallBack = onSelectCallBack
    }

    fun getSelectedIds(): MutableList<Int> {
        return dialog.getSelectedIds()
    }

    fun getSelectedId(): Int? {
        return dialog.getSelectedId()
    }

    fun setSelectedIds(ids: List<Int>) {
        dialog.setSelectedIds(ids)
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if (enabled) {
            titleTv.setTextColor(context.resources.getColor(R.color.textColorPrimary))
            setOnClickListener(this::onClick)
        } else {
            titleTv.setTextColor(context.resources.getColor(R.color.textColorSec))
            setOnClickListener(null)
        }

    }


}
