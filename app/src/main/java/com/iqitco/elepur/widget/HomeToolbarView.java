package com.iqitco.elepur.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.iqitco.elepur.R;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * date 6/10/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class HomeToolbarView extends ConstraintLayout {

    private AutoScrollViewPager pager;
    private ImageButton menuBtn;
    private ImageButton searchBtn;
    private ConstraintLayout notificationBtn;
    private ConstraintLayout ticketBtn;
    private ImageView notificationGreenLight;
    private ImageView ticketGreenLight;


    public HomeToolbarView(Context context) {
        this(context, null);
    }

    public HomeToolbarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeToolbarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.home_toolbar_view, this, true);
        pager = findViewById(R.id.pager);
        menuBtn = findViewById(R.id.menuBtn);
        searchBtn = findViewById(R.id.searchBtn);
        notificationBtn = findViewById(R.id.notificationBtn);
        ticketBtn = findViewById(R.id.ticketBtn);
        notificationGreenLight = findViewById(R.id.notificationGreenLight);
        ticketGreenLight = findViewById(R.id.ticketGreenLight);
    }

    public AutoScrollViewPager getPager() {
        return pager;
    }

    public ImageButton getMenuBtn() {
        return menuBtn;
    }

    public ImageButton getSearchBtn() {
        return searchBtn;
    }

    public ConstraintLayout getNotificationBtn() {
        return notificationBtn;
    }

    public ConstraintLayout getTicketBtn() {
        return ticketBtn;
    }

    public ImageView getNotificationGreenLight() {
        return notificationGreenLight;
    }

    public ImageView getTicketGreenLight() {
        return ticketGreenLight;
    }
}
