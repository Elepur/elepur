package com.iqitco.elepur.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.iqitco.elepur.activities.delivery_info.DeliveryInfoActivity;

/**
 * date 6/4/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CheckboxInputView extends ConstraintLayout {

    private TextView textTv;
    private CheckBox checkBox;

    public CheckboxInputView(Context context) {
        this(context, null);
    }

    public CheckboxInputView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CheckboxInputView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.checkbox_input_view, this, true);
        textTv = findViewById(R.id.textTv);
        checkBox = findViewById(R.id.checkbox);

        textTv.setOnClickListener(v -> {
            Activity activity = (Activity) context;
            activity.startActivity(new Intent(activity, DeliveryInfoActivity.class));
        });

        if (attrs == null) return;

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CheckboxInputView);
        int arraySize = array.getIndexCount();
        for (int i = 0; i < arraySize; i++) {
            int attr = array.getIndex(i);
            if (attr == R.styleable.CheckboxInputView_android_text) {
                textTv.setText(array.getString(attr));
            }
        }
        array.recycle();

    }

    public TextView getTextTv() {
        return textTv;
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }
}
