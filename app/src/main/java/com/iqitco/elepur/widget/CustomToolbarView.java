package com.iqitco.elepur.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CustomToolbarView extends ConstraintLayout {

    private ImageButton backBtn;
    private ImageButton secondBtn;
    private TextView titleTv;
    private Activity activity;

    public CustomToolbarView(Context context) {
        this(context, null);
    }

    public CustomToolbarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.custom_toolbar_view, this, true);
        backBtn = findViewById(R.id.backBtn);
        secondBtn = findViewById(R.id.secondBtn);
        titleTv = findViewById(R.id.titleTv);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CustomToolbarView);
        int arraySize = array.getIndexCount();
        for (int i = 0; i < arraySize; i++) {
            int attr = array.getIndex(i);
            if (attr == R.styleable.CustomToolbarView_android_text) {
                titleTv.setText(array.getString(attr));
            } else if (attr == R.styleable.CustomToolbarView_android_src) {
                int resId = array.getResourceId(attr, 0);
                if (resId != 0) {
                    secondBtn.setImageResource(resId);
                }
            } else if (attr == R.styleable.CustomToolbarView_android_background) {
                int color = array.getColor(attr, context.getResources().getColor(R.color.primary));
                findViewById(R.id.rootView).setBackgroundColor(color);
            } else if (attr == R.styleable.CustomToolbarView_android_textColor) {
                int color = array.getColor(attr, Color.WHITE);
                titleTv.setTextColor(color);
            }
        }
        array.recycle();

        if (context instanceof Activity) {
            activity = (Activity) context;
            backBtn.setOnClickListener(v -> activity.onBackPressed());
        }

    }

    public ImageButton getBackBtn() {
        return backBtn;
    }

    public ImageButton getSecondBtn() {
        return secondBtn;
    }

    public TextView getTitleTv() {
        return titleTv;
    }
}
