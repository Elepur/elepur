package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.iqitco.elepur.R;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AdsSeekView extends ConstraintLayout {

    private TextView titleTv;
    private CrystalRangeSeekbar rangeSeekBar;


    public AdsSeekView(Context context) {
        this(context, null);
    }

    public AdsSeekView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdsSeekView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.ads_seek_view, this, true);
        titleTv = findViewById(R.id.titleTv);
        rangeSeekBar = findViewById(R.id.rangeSeekBar);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AdsSeekView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.AdsSeekView_android_text) {
                    titleTv.setText(array.getString(attr));
                }
            }
            array.recycle();
        }
    }

    public TextView getTitleTv() {
        return titleTv;
    }

    public CrystalRangeSeekbar getRangeSeekBar() {
        return rangeSeekBar;
    }
}
