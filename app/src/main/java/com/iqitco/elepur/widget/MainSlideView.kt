package com.iqitco.elepur.widget

import android.content.Context
import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.akexorcist.localizationactivity.LanguageSetting
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.iqitco.elepur.R
import com.iqitco.elepur_api.modules.Slides

/**
 * date 7/14/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class MainSlideView : ConstraintLayout {

    private var imageView: ImageView? = null
    private var slide: Slides? = null
    var onSlideClick: OnSlideClick? = null

    constructor(context: Context?, slide: Slides) : super(context) {
        init(context!!, slide)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context!!, null)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context!!, null)
    }

    private fun init(context: Context, slide: Slides?) {
        LayoutInflater.from(context).inflate(R.layout.main_slide_image, this, true)
        imageView = findViewById(R.id.image)
        this.slide = slide
        imageView!!.scaleType = ImageView.ScaleType.CENTER_CROP
        imageView!!.setBackgroundColor(Color.TRANSPARENT)
        setOnClickListener(this::onClick)
    }

    private fun onClick(view: View) {
        onSlideClick!!.onSlideClick(slide!!)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val imageUrl: String = if (LanguageSetting.getDefaultLanguage().equals("ar", ignoreCase = true)) {
            slide!!.picAr
        } else {
            slide!!.picEn
        }
        Glide.with(this)
                .load(imageUrl)
                .apply(
                        RequestOptions()
                                .centerCrop()
                                .fallback(R.drawable.img_place_holder)
                                .placeholder(R.drawable.img_place_holder)
                                .error(R.drawable.img_place_holder)
                )
                .into(imageView!!)
    }

    interface OnSlideClick {
        fun onSlideClick(slide: Slides)
    }
}