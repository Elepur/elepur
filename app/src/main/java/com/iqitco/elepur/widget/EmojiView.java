package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 6/22/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class EmojiView extends ConstraintLayout {

    private TextView counterTv;
    private ImageView image;

    @DrawableRes
    private int selectedDrawable;

    @DrawableRes
    private int defaultDrawable;

    private boolean isSelected = false;


    public EmojiView(Context context) {
        this(context, null);
    }

    public EmojiView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmojiView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.emoji_view, this, true);
        counterTv = findViewById(R.id.counterTv);
        image = findViewById(R.id.image);

        if (attrs != null){
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.EmojiView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.EmojiView_android_text) {
                    counterTv.setText(array.getString(attr));
                } else if (attr == R.styleable.EmojiView_defaultSrc) {
                    defaultDrawable = array.getResourceId(attr, 0);
                }  else if (attr == R.styleable.EmojiView_selectedSrc) {
                    selectedDrawable = array.getResourceId(attr, 0);
                }  else if (attr == R.styleable.EmojiView_selected) {
                    isSelected = array.getBoolean(attr, isSelected);
                }

            }
            array.recycle();
            image.setImageResource(isSelected ? selectedDrawable : defaultDrawable);
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        isSelected = selected;
        image.setImageResource(isSelected ? selectedDrawable : defaultDrawable);
    }

    public TextView getCounterTv() {
        return counterTv;
    }

    public ImageView getImage() {
        return image;
    }
}
