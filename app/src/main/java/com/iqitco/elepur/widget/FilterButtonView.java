package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.iqitco.elepur.util.DrawableUtility;

/**
 * date 6/16/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class FilterButtonView extends ConstraintLayout {

    private ImageView image;
    private TextView textTv;
    private Context context;

    @DrawableRes
    private int drawableResId;

    public FilterButtonView(Context context) {
        this(context, null);
    }

    public FilterButtonView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FilterButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.filter_button_view, this, true);
        image = findViewById(R.id.image);
        textTv = findViewById(R.id.textTv);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.FilterButtonView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.FilterButtonView_android_text) {
                    textTv.setText(array.getString(attr));
                } else if (attr == R.styleable.FilterButtonView_android_src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        drawableResId = resId;
                        image.setImageResource(resId);
                    }
                }
            }
            array.recycle();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        int color;
        if (enabled) {
            color = context.getResources().getColor(R.color.textColorPrimary);
        } else {
            color = context.getResources().getColor(R.color.textColorSec);
        }
        textTv.setTextColor(color);
        image.setImageDrawable(DrawableUtility.getTintDrawable(context, drawableResId, color));
    }
}
