package com.iqitco.elepur.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;

/**
 * date 5/31/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class GifView extends View {

    private Movie movie;
    private long movieStart;
    private long movieEnd;
    private OnFinish onFinish;
    private boolean inFinish = false;

    public GifView(Context context) throws IOException {
        super(context);
    }

    public GifView(Context context, AttributeSet attrs) throws IOException {
        super(context, attrs);
    }

    public GifView(Context context, AttributeSet attrs, int defStyle) throws IOException {
        super(context, attrs, defStyle);
    }

    public void loadGIFResource(Context context, int id) {
        //turn off hardware acceleration
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        InputStream is = context.getResources().openRawResource(id);
        movie = Movie.decodeStream(is);
    }

    public void loadGIFAsset(Context context, String filename) {
        InputStream is;
        try {
            is = context.getResources().getAssets().open(filename);
            movie = Movie.decodeStream(is);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (movie == null || inFinish) {
            return;
        }

        long now = android.os.SystemClock.uptimeMillis();

        if (movieStart == 0) {
            movieStart = now;
            movieEnd = movieStart + movie.duration();
        }

        int relTime;

        if (now > movieEnd) {
            movie.setTime((int) (movieEnd - 50) % movie.duration());
            movie.draw(canvas, 0, 0);
            if (onFinish != null) {
                onFinish.onFinish();
                inFinish = true;
            }
            return;
        }

        relTime = (int) ((now - movieStart) % movie.duration());
        movie.setTime(relTime);
        movie.draw(canvas, 0, 0);
        this.invalidate();
    }

    public void setOnFinish(OnFinish onFinish) {
        this.onFinish = onFinish;
    }

    public interface OnFinish {
        void onFinish();
    }
}

