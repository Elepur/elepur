package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AdsSwitchView extends ConstraintLayout {

    private TextView titleTv;
    private TextView textTv;
    private Switch switchOnOff;

    public AdsSwitchView(Context context) {
        this(context, null);
    }

    public AdsSwitchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdsSwitchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.ads_switch_view, this, true);
        titleTv = findViewById(R.id.titleTv);
        textTv = findViewById(R.id.textTv);
        switchOnOff = findViewById(R.id.switchOnOff);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AdsSwitchView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.AdsSwitchView_android_text) {
                    textTv.setText(array.getString(attr));
                } else if (attr == R.styleable.AdsSwitchView_title) {
                    titleTv.setText(array.getString(attr));
                }
            }
            array.recycle();
        }
    }

    public TextView getTitleTv() {
        return titleTv;
    }

    public TextView getTextTv() {
        return textTv;
    }

    public Switch getSwitchOnOff() {
        return switchOnOff;
    }
}
