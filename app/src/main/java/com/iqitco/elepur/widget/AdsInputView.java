package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 6/23/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class AdsInputView extends ConstraintLayout {

    private TextView titleTv;
    private ImageView image;
    private TextView textTv;


    public AdsInputView(Context context) {
        this(context, null);
    }

    public AdsInputView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdsInputView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.ads_input_view, this, true);
        titleTv = findViewById(R.id.titleTv);
        image = findViewById(R.id.image);
        textTv = findViewById(R.id.textTv);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AdsInputView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.AdsInputView_android_text) {
                    textTv.setText(array.getString(attr));
                    titleTv.setText(array.getString(attr));
                } else if (attr == R.styleable.AdsInputView_android_src) {
                    int resId = array.getResourceId(attr, 0);
                    if (resId != 0) {
                        image.setImageResource(resId);
                    }
                }
            }
            array.recycle();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        int color;
        if (enabled) {
            color = getResources().getColor(R.color.textColorPrimary);
        } else {
            color = getResources().getColor(R.color.textColorSec);
        }
        textTv.setTextColor(color);
    }

    public TextView getTitleTv() {
        return titleTv;
    }

    public ImageView getImage() {
        return image;
    }

    public TextView getTextTv() {
        return textTv;
    }
}
