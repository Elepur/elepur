package com.iqitco.elepur.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.media.ExifInterface;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.iqitco.elepur.R;
import com.iqitco.elepur.dialogs.SelectImageDialog;
import com.iqitco.elepur.items.BottomDialogItem1;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class SelectImageView extends ConstraintLayout {

    private static final String TAG = SelectImageDialog.class.getSimpleName();

    private ImageView selectedImage;
    private ImageView imageView;
    private TextView textTv;
    private Context context;
    private SelectImageDialog dialog;
    private String base64;

    private static final int REQUEST_CODE_CAMERA = 7485;
    private static final int REQUEST_CODE_GALLERY = 7486;

    private int requestCode;
    private int internalRequestCode;

    public SelectImageView(Context context) {
        this(context, null);
    }

    public SelectImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        dialog = new SelectImageDialog(context);
        LayoutInflater.from(context).inflate(R.layout.select_image_view, this, true);
        imageView = findViewById(R.id.image);
        textTv = findViewById(R.id.textTv);
        selectedImage = findViewById(R.id.selectedImage);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.SelectImageView);
        int arraySize = array.getIndexCount();
        for (int i = 0; i < arraySize; i++) {
            int attr = array.getIndex(i);
            if (attr == R.styleable.SelectImageView_android_text) {
                textTv.setText(array.getString(attr));
            } else if (attr == R.styleable.SelectImageView_android_src) {
                int resId = array.getResourceId(attr, 0);
                if (resId != 0) {
                    imageView.setImageResource(resId);
                }
            }
        }
        array.recycle();
        setOnClickListener(this::onClick);
    }

    private void onClick(View v) {
        dialog.setOnMainButtonClick(this::onMainButtonClick);
        dialog.show();
    }

    private void onMainButtonClick(List<BottomDialogItem1> list) {
        BottomDialogItem1 item = list.get(0);
        long id = item.getIdentifier();
        Activity activity = (Activity) context;
        requestCode = new Random().nextInt(9999);
        if (id == SelectImageDialog.FROM_CAMERA) {

            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            internalRequestCode = REQUEST_CODE_CAMERA;
            /*File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Elepur");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH);
            File imageFile = new File(file.getAbsolutePath() + File.separator + dateFormat.format(new Date()) + ".jpg");
            filePath = imageFile.getAbsolutePath();
            imageUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", imageFile);
            takePicture.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);*/
            activity.startActivityForResult(takePicture, requestCode);

        } else if (id == SelectImageDialog.FROM_GALLERY) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            internalRequestCode = REQUEST_CODE_GALLERY;
            activity.startActivityForResult(pickPhoto, requestCode);
        }
        dialog.dismiss();
    }

    public void showError(boolean flag) {
        imageView.setBackgroundResource(flag ? R.drawable.white_oval_error : R.drawable.white_oval);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK || requestCode != this.requestCode) return;
        Uri selectedImage = data.getData();
        Log.d(this.getClass().getSimpleName(), String.valueOf(selectedImage));
        Bitmap bitmap = null;
        switch (internalRequestCode) {
            case REQUEST_CODE_CAMERA:
                bitmap = (Bitmap) data.getExtras().get("data");
                Glide.with(context).load(bitmap).apply(RequestOptions.circleCropTransform()).into(this.selectedImage);

                break;
            case REQUEST_CODE_GALLERY:
                Uri contentURI = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), contentURI);
                    Glide.with(context).load(bitmap).apply(RequestOptions.circleCropTransform()).into(this.selectedImage);
                    Log.d(TAG, "Bitmap is null: " + String.valueOf(bitmap == null));

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Failed!");
                }
                break;
        }
        Log.d(TAG, "Bitmap is null: " + String.valueOf(bitmap == null));
        if (bitmap == null) return;
        createBase64(bitmap);
    }

    public void loadImage(String url) {
        Glide.with(context)
                .asBitmap()
                .load(url)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        createBase64(resource);
                        return false;
                    }
                })
                .apply(RequestOptions.circleCropTransform())
                .into(this.selectedImage);
    }

    private void createBase64(Bitmap bitmap) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bmp2 = bitmap.copy(bitmap.getConfig(), true);
            bmp2.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            base64 = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
            bmp2.recycle();
        } catch (Exception e) {
            Log.e(TAG, "error", e);
        }
    }

    public String createBase64() {
        try {
            Bitmap bitmap = ((BitmapDrawable) selectedImage.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bmp2 = bitmap.copy(bitmap.getConfig(), true);
            bmp2.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            String base64 = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
            bmp2.recycle();
            return base64;
        } catch (Exception e) {
            return null;
        }
    }

    /*public static String getBase64Image(Context context, String path, int newWidth, String waterMark) {
        try {
            Bitmap bm = scaleImage(path, newWidth);
            if (bm == null) {
                return null;
            }
            if (waterMark != null) {
                bm = addWaterMark(context, bm, waterMark);
            }

            File tadweerDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Tadweer");
            tadweerDir.mkdirs();
            String fileName = String.valueOf(Calendar.getInstance().getTimeInMillis()) + ".jpg";
            File photoFile = new File(tadweerDir.getAbsolutePath() + File.separator + fileName);
            File photoFileCom = new File(tadweerDir.getAbsolutePath() + File.separator + "compress_" + fileName + ".jpg");
            saveImage(photoFile, bm);
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(photoFile)));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);

            String base64 = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
            bm.recycle();
            return new Pictures(fileName, base64, photoFile.getAbsolutePath());
        } catch (Exception e) {
            Log.e(TAG, "error", e);
            return null;
        }
    }*/

    private static Bitmap scaleImage(Bitmap bitmap, int newWidth) throws FileNotFoundException {
        /*BitmapFactory.Options options = new BitmapFactory.Options();
        BufferedInputStream stream = new BufferedInputStream(new FileInputStream(new File(path)));
        Bitmap bitmap = BitmapFactory.decodeStream(stream, null, options);*/
        /*Log.e(TAG, "bitmap == null: " + String.valueOf(bitmap == null));
        if (bitmap == null) {
            return null;
        }*/
        int originalHeight = bitmap.getHeight();
        int originalWidth = bitmap.getWidth();
        if (originalWidth <= newWidth) {
            return bitmap;
        }
        int newHeight = (int) (((double) originalHeight / (double) originalWidth) * newWidth);
        Bitmap out = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);


        try {
            ExifInterface exif = new ExifInterface("");
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle;
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    angle = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    angle = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    angle = 270;
                    break;
                default:
                    angle = 0;
                    break;
            }
            Matrix mat = new Matrix();
            if (angle == 0 && bitmap.getWidth() > bitmap.getHeight())
                mat.postRotate(90);
            else
                mat.postRotate(angle);
            return Bitmap.createBitmap(out, 0, 0, out.getWidth(), out.getHeight(), mat, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getBase64() {
        return base64;
    }

    public String getExt() {
        return base64 == null ? null : "png";
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTextTv() {
        return textTv;
    }


}
