package com.iqitco.elepur.widget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.iqitco.elepur.R
import kotlinx.android.synthetic.main.store_result_emoji_view.view.*

/**
 * date 8/4/18
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
class StoreResultEmojisView : ConstraintLayout {

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context!!).inflate(R.layout.store_result_emoji_view, this, true)
        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.StoreResultEmojisView)
            val arraySize = array.indexCount

            for (i in 0..arraySize) {
                val attr = array.getIndex(i)
                if (attr == R.styleable.StoreResultEmojisView_android_text) {
                    cardTitleTv.text = array.getString(attr)
                }
            }
            array.recycle()
        }
    }


}