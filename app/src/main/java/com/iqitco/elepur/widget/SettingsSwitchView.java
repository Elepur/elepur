package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.iqitco.elepur.R;

/**
 * date 6/19/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class SettingsSwitchView extends ConstraintLayout {

    private Switch switchFlag;
    private TextView textTv;
    private OnSwitchChange onSwitchChange;

    public SettingsSwitchView(Context context) {
        this(context, null);
    }

    public SettingsSwitchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SettingsSwitchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.settings_switch_view, this, true);
        switchFlag = findViewById(R.id.switchFlag);
        textTv = findViewById(R.id.textTv);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.SettingsSwitchView);
            int arraySize = array.getIndexCount();
            for (int i = 0; i < arraySize; i++) {
                int attr = array.getIndex(i);
                if (attr == R.styleable.SettingsSwitchView_android_text) {
                    textTv.setText(array.getString(attr));
                } else if (attr == R.styleable.SettingsSwitchView_android_textColor) {
                    textTv.setTextColor(array.getColor(attr, context.getResources().getColor(R.color.textColorPrimary)));
                }
            }
            array.recycle();
        }

        switchFlag.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (onSwitchChange != null) {
                onSwitchChange.onSwitchChange(buttonView, isChecked);
            }
        });
    }

    public void setOnSwitchChange(OnSwitchChange onSwitchChange) {
        this.onSwitchChange = onSwitchChange;
    }

    public Switch getSwitchFlag() {
        return switchFlag;
    }

    public TextView getTextTv() {
        return textTv;
    }

    public interface OnSwitchChange {
        void onSwitchChange(CompoundButton compoundButton, boolean isChecked);
    }
}
