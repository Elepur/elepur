package com.iqitco.elepur.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqitco.elepur.R;
import com.iqitco.elepur.util.DrawableUtility;

/**
 * date 6/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class BottomNavButtonView extends ConstraintLayout {

    private TextView textTv;
    private ImageView image;
    private int imageResId;
    private int stringResId;

    public BottomNavButtonView(Context context) {
        this(context, null);
    }

    public BottomNavButtonView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BottomNavButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.bottom_nav_btn, this, true);
        textTv = findViewById(R.id.textTv);
        image = findViewById(R.id.image);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.BottomNavButtonView);
        int arraySize = array.getIndexCount();
        for (int i = 0; i < arraySize; i++) {
            int attr = array.getIndex(i);
            if (attr == R.styleable.BottomNavButtonView_android_text) {
                textTv.setText(array.getString(attr));
            } else if (attr == R.styleable.BottomNavButtonView_android_src) {
                int resId = array.getResourceId(attr, 0);
                if (resId != 0) {
                    imageResId = resId;
                    image.setImageResource(resId);
                }
            }
        }
        array.recycle();
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
        if (image != null) {
            image.setImageResource(this.imageResId);
        }
    }

    public void setStringResId(int stringResId) {
        this.stringResId = stringResId;
        if (textTv != null) {
            textTv.setText(this.stringResId);
        }
    }

    public TextView getTextTv() {
        return textTv;
    }

    public ImageView getImage() {
        return image;
    }

    public void setSelected(boolean flag) {
        int color;
        if (flag) {
            color = Color.parseColor("#785efd");
        } else {
            color = Color.parseColor("#919396");
        }
        try {
            image.setImageDrawable(DrawableUtility.getTintDrawable(image.getContext(), imageResId, color));
        } catch (Exception e) {
            /*NOTING*/
        }
        textTv.setTextColor(color);
    }
}
