package com.iqitco.elepur.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.iqitco.elepur.R;

/**
 * date 5/28/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class CustomEditTextSmallRoundedView extends AppCompatEditText {

    private boolean flag = false;

    public CustomEditTextSmallRoundedView(Context context) {
        super(context);
    }

    public CustomEditTextSmallRoundedView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditTextSmallRoundedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public void showError(boolean flag) {
        this.flag = flag;
        setBackgroundResource(flag ? R.drawable.edit_text_small_rounded_white : R.drawable.small_rounded_white);
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
