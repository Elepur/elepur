package com.iqitco.elepur.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * date 6/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private final List<String> mFragmentNameList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title, String fragmentName) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        mFragmentNameList.add(fragmentName);
        notifyDataSetChanged();
    }

    //this is called when notifyDataSetChanged() is called
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

    public void removeFragmentAtPosition(int position) {
        mFragmentList.remove(position);
        mFragmentTitleList.remove(position);
        mFragmentNameList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        // give an ID different from position when position has been changed
        return position;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public String getFragmentName(int position) {
        return mFragmentNameList.get(position);
    }
}