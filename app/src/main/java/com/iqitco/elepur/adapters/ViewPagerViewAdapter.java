package com.iqitco.elepur.adapters;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * date 6/9/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ViewPagerViewAdapter extends PagerAdapter {
    private ArrayList<View> rootList = new ArrayList<View>();

    @Override
    public int getCount() {
        return rootList.size();
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        container.addView(rootList.get(position));
        return rootList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void add(View view) {
        rootList.add(view);
        notifyDataSetChanged();
    }

    public void removeAll() {
        rootList.clear();
        notifyDataSetChanged();
    }
}