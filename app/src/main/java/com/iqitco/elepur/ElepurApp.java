package com.iqitco.elepur;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.JobIntentService;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.iqitco.elepur.activities.blocked_account.BlockedAccountActivity;
import com.iqitco.elepur.activities.home.HomeSellerActivity;
import com.iqitco.elepur.activities.landing_activity.LandingActivity;
import com.iqitco.elepur.util.DateUtility;
import com.iqitco.elepur_api.Constant;
import com.iqitco.elepur_api.modules.UserData;
import com.iqitco.elepur_api.services.CacheService;
import com.iqitco.elepur_api.services.CheckBlockService;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;
import com.prashantsolanki.secureprefmanager.SecurePrefManagerInit;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

/**
 * date 5/26/2018
 *
 * @author Mohammad Al-Najjar (MxNINJA)
 */
public class ElepurApp extends MultiDexApplication {

    public static final String TAG = ElepurApp.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        SecurePrefManagerInit.Initializer initializer = new SecurePrefManagerInit.Initializer(this);
        initializer.initialize();

        /*services*/
        JobIntentService.enqueueWork(this, CacheService.class, 1, new Intent(this, CacheService.class));
        JobIntentService.enqueueWork(this, CheckBlockService.class, 2, new Intent(this, CheckBlockService.class));

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this::onInstanceIdSuccess);
        initFabric();
    }

    private void onInstanceIdSuccess(InstanceIdResult instanceIdResult) {
        SecurePrefManager.with(this).set(Constant.NOTIFICATION_ID).value(instanceIdResult.getToken()).go();
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        Log.d(TAG, "getCreationTime: " + DateUtility.formatDate(FirebaseInstanceId.getInstance().getCreationTime(), DateUtility.API_IN_DATE_FORMAT, Locale.ENGLISH));
    }

    public static Object pxToDp(int px) {
        return (px / Resources.getSystem().getDisplayMetrics().density);
    }

    private void initFabric() {
        if (!BuildConfig.DEBUG_MODE) {
            Fabric.with(this, new Crashlytics());
            Crashlytics.setInt("AndroidVersion", Build.VERSION.SDK_INT);
            Crashlytics.setBool("IsDebugMode", BuildConfig.DEBUG_MODE);
            Crashlytics.setString("Manufacturer", Build.MANUFACTURER);
            Crashlytics.setString("DeviceModel", Build.MODEL);
            Crashlytics.setString("DeviceProduct", Build.PRODUCT);
            Crashlytics.setString("ScreenHeight", String.valueOf(Resources.getSystem().getDisplayMetrics().heightPixels) + "px");
            Crashlytics.setString("ScreenWidth", String.valueOf(Resources.getSystem().getDisplayMetrics().widthPixels) + "px");
            Crashlytics.setString("UserEmail", SecurePrefManager.with(this).get(Constant.EMAIL).defaultValue("").go());
            Crashlytics.setString("PxToDp", String.valueOf(pxToDp(1)));
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            Crashlytics.setInt("Density", (int) (metrics.density * 160f));
        }
    }

    /**
     * @param activity some devices required activity to start another activity like new Samsung devices
     * @param body     user data from api
     */
    public void loginUser(Activity activity, UserData body) {
        SecurePrefManager securePrefManager = SecurePrefManager.with(this);

        securePrefManager.set(Constant.ID_LOGIN).value(body.getIdLogin()).go();
        securePrefManager.set(Constant.ID_ID).value(body.getIdId()).go();
        securePrefManager.set(Constant.BLOCK).value(body.getBlock()).go();

        if (!TextUtils.isEmpty(body.getRoll())) {
            securePrefManager.set(Constant.ROLL).value(body.getRoll()).go();
        }

        if (!TextUtils.isEmpty(body.getEmail())) {
            securePrefManager.set(Constant.EMAIL).value(body.getEmail()).go();
        }
        if (!TextUtils.isEmpty(body.getFacebookId())) {
            securePrefManager.set(Constant.FACEBOOK_ID).value(body.getFacebookId()).go();
        }

        if (!TextUtils.isEmpty(body.getPassword())) {
            securePrefManager.set(Constant.PASSWORD).value(body.getPassword()).go();
        }

        if (!TextUtils.isEmpty(body.getName())) {
            securePrefManager.set(Constant.NAME).value(body.getName()).go();
        }

        activity.finish();

        boolean isBlocked = body.getBlock() == 1;
        Class nextActivity;
        if (isBlocked) {
            nextActivity = BlockedAccountActivity.class;
        } else {
            nextActivity = HomeSellerActivity.class;
        }
        activity.startActivity(new Intent(activity, nextActivity).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void logoutUser(Activity activity) {
        SecurePrefManager securePrefManager = SecurePrefManager.with(this);

        securePrefManager.remove(Constant.ID_LOGIN).confirm();
        securePrefManager.remove(Constant.ID_ID).confirm();
        securePrefManager.remove(Constant.BLOCK).confirm();
        securePrefManager.remove(Constant.ROLL).confirm();
        securePrefManager.remove(Constant.EMAIL).confirm();
        securePrefManager.remove(Constant.PASSWORD).confirm();
        securePrefManager.remove(Constant.NAME).confirm();
        securePrefManager.remove(Constant.FACEBOOK_ID).confirm();
        securePrefManager.remove(Constant.IS_FACEBOOK_USER).confirm();

        activity.finish();
        activity.startActivity(new Intent(activity, LandingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

}
